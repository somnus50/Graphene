module AuxiliaryFunctions
implicit none
contains


	subroutine indexArrayReal(n,Array,Index)
			implicit none
			integer(4), intent(in)  :: n
			real(8)   , intent(in)  :: Array(n)
			integer(4), intent(out) :: Index(n)
			integer(4), parameter   :: nn=15, nstack=50
			integer(4)              :: k,i,j,indext,jstack,l,r
			integer(4)              :: istack(nstack)
			real(8)                 :: a
			do j = 1,n
					Index(j) = j
			end do
			jstack=0
			l=1
			r=n
			do
					if (r-l < nn) then
							do j=l+1,r
									indext=Index(j)
									a=Array(indext)
									do i=j-1,l,-1
											if (Array(Index(i)) <= a) exit
											Index(i+1)=Index(i)
									end do
									Index(i+1)=indext
							end do
							if (jstack == 0) return
							r=istack(jstack)
							l=istack(jstack-1)
							jstack=jstack-2
					else
							k=(l+r)/2
							call swap(Index(k),Index(l+1))
							call exchangeIndex(Index(l),Index(r))
							call exchangeIndex(Index(l+1),Index(r))
							call exchangeIndex(Index(l),Index(l+1))
							i=l+1
							j=r
							indext=Index(l+1)
							a=Array(indext)
							do
									do
											i=i+1
											if (Array(Index(i)) >= a) exit
									end do
									do
											j=j-1
											if (Array(Index(j)) <= a) exit
									end do
									if (j < i) exit
									call swap(Index(i),Index(j))
							end do
							Index(l+1)=Index(j)
							Index(j)=indext
							jstack=jstack+2
							if (jstack > nstack) then
									write(*,*) 'NSTACK too small in indexArrayReal()'   ! xxx
									error stop
							end if
							if (r-i+1 >= j-l) then
									istack(jstack)=r
									istack(jstack-1)=i
									r=j-1
							else
									istack(jstack)=j-1
									istack(jstack-1)=l
									l=i
							end if
					end if
			end do
	contains
			subroutine exchangeIndex(i,j)
					integer(4), intent(inout) :: i,j
					integer(4)                :: swp
					if (Array(j) < Array(i)) then
							swp=i
							i=j
							j=swp
					end if
			end subroutine exchangeIndex
			pure elemental subroutine swap(a,b)
					implicit none
					integer(4), intent(inout) :: a,b
					integer(4) :: dum
					dum=a
					a=b
					b=dum
			end subroutine swap
	end subroutine indexArrayReal

	subroutine SortRows(A,m,n,B)
		implicit none
		integer(4), intent(in)::m,n
		real(8), intent(in)::A(m,n)
		real(8), intent(out)::B(m,n)
		real(8)::buf(n)
		integer(4)::irow,krow

		B=A

		do irow = 1, m
			krow = minloc( B( irow:m, 1 ), dim=1 ) + irow - 1

			buf( : )     = B( irow, : )
			B( irow, : ) = B( krow, : )
			B( krow, : ) = buf( : )
		enddo

	end subroutine SortRows

	real(8) function x2logx(x)
		real(8)::x,y0,y1,b,b2,x2,dy
		integer(4)::i
		integer(4),parameter::imax=20
		real(8),parameter::tol=1D-10
		! This function uses on order 3 (Householder's method)[https://en.wikipedia.org/wiki/Householder%27s_method]
		! to evaluate x^2*ln(x). This is to avoid the singularity of ln near 0.

		if (x.eq.0D0) then
			write(*,*) "x=0"
			x2logx=0D0
			return
		endif
		i=0
		x2=x**2
		dy=10D0*tol
		y0=0D0 ! Initial guess
		do while ((dy.gt.tol).and.(i.lt.imax))
			b=dexp(-y0/x2)
			b2=b**2
			y1=y0+3D0*x2*(x2*b2-1D0)/(x2*b2+4D0*x*b+1D0)
			dy=dabs(y1-y0)
			write(*,*) i,y1,y0,dy
			y0=y1
			i=i+1
		enddo
		write(*,*) "x2logx converged in",i,"steps."
		x2logx=y1

	end function x2logx

	real(8) function sdsqrt(x)
		real(8)::x

		sdsqrt=sign(dsqrt(dabs(x)),x)
	end function sdsqrt

	real(8) function NumDelta(x,sigma)
		use Constants
		real(8)::x,sigma
		real(8), parameter::temp=1D0/dsqrt(pi)

		NumDelta=temp*dexp(-(x/sigma)**2)/sigma
	end function NumDelta

	subroutine UniformGrid(a,b,num,grid)
		implicit none
		integer(4), intent(in)::num
		real(8), intent(in)::a,b
		real(8), intent(out)::grid(num)
		real(8)::dx
		integer(4)::i

		dx=(b-a)/real(num, kind=8)
		grid(1)=a
		do i=2,num
			grid(i)=grid(i-1)+dx
		enddo
	end subroutine UniformGrid

	subroutine BinSearch(grid,size,value,index)
		implicit none
		integer (kind=4),intent(in)::size
		real (kind=8),intent(in)::grid(size),value
		integer (kind=4),intent(out)::index
		integer (kind=4)::a,b,m

		a=1
		b=size
		m=size/2

		if ((value.gt.grid(size)).or.(value.lt.grid(1))) then
			write(*,*) "BinSearch out of range of grid"
			write(*,*) value,grid(1),grid(size)
			call exit(-1)
		endif

		do while ((b-a).gt.1)
			if (value.lt.grid(m)) then
				b=m-1
			else
				a=m+1
			endif
			m=(a+b)/2
		enddo

		index=a

	end subroutine BinSearch

	subroutine LogGrid(dr,steps,xgrid)
		implicit none
		integer (kind=4),intent(in)::steps
		real    (kind=8),intent(in)::dr
		real    (kind=8),intent(out)::xgrid(steps)
		integer (kind=4)::i


		do i=1,steps
			xgrid(i)=dexp(real(i-steps, kind=8)*dr)
		enddo

	end subroutine LogGrid

	real (kind=8) function LGdrSuggest(mean,steps)
		implicit none
		real (kind=8)::mean,s,x
		integer (kind=4)::steps

		s=real(steps, kind=8)

		LGdrSuggest=RootBrentsMethod2(15D0,1D-10,1D-4,anc,x)

	contains
		real (kind=8) function anc(x,dr)
			implicit none
			real (kind=8)::dr,x ! x is here because RootBrentsMethod is dumb
			! Equation that relates M to dr

			anc=LGMean(dr,s)-mean
		end function anc

		real (kind=8) function LGMean(dr,s)
			implicit none
			real (kind=8)::dr,s


			LGMean=(1D0-dexp(-s*dr))/(1D0-dexp(-dr))/s
		end function LGMean
	end function LGdrSuggest

	subroutine NC10ListIntegrate(num,list,dx,res)
		integer(4),intent(in)::num
		real(8),intent(in)::list(num),dx
		real(8),intent(out)::res
		integer(4)::i,a,b

		if ( modulo(num,10).ne.0 ) then
			write(*,*) "Module.AuxiliaryFunctions:NC10ListIntegrate"
			write(*,*) "num must be multiple of 10"
			call exit(-1)
		end if

		res=NC10(list(1:10))
		do i=2,num/10
			a=10*(i-1)
			b=10*i
			res=res+NC10(list(a:b))
		enddo
		res=res*dx
		contains
			real(8) function NC10(list)
				real(8)::list(10),dx
				integer(4)::i
				real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				do i=1,10
					NC10=NC10+list(i)*Newt10(i)
			enddo
			end function NC10
	end subroutine NC10ListIntegrate

	subroutine StupidIntegrateNC(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8), external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8) !NC10 takes 10 steps

		! x=a+dx/2D0 ! Why am I doing this?...
		x=a!+dx/2D0
		res=0D0
		do i=1,steps
			res=res+NC10(x,x+dx,f)
			x=x+dx
		enddo
		!res=res*dx

	contains
		real (kind=8) function NC10(a,b,fn)
			integer (kind=4)::i
			real (kind=8) ::a,b,x,dx,khat(2)
			real (kind=8), external::fn
			real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
													1080D0,19344D0,&
													5778D0,	5778D0,&
													19344D0,1080D0,&
													15741D0,2857D0&
													/)*9D0/89600D0

			NC10=0D0
			x=a
			dx=(b-a)/9D0
			do i=1,10
				NC10=NC10+Newt10(i)*fn(x)
				x=x+dx
			enddo
			NC10=NC10*dx
		end function NC10
	end subroutine StupidIntegrateNC


	subroutine StupidIntegrateNC2(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8), external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8) !NC10 takes 10 steps

		x=a+dx/2D0
		res=0D0
		do i=1,steps
			res=res+NC10(x,x+dx,f)
			x=x+dx
		enddo
		!res=res*dx

	contains
		real (kind=8) function NC10(a,b,fn)
			integer (kind=4)::i
			real (kind=8) ::a,b,x,dx,khat(2)
			real (kind=8), external::fn
			real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
													1080D0,19344D0,&
													5778D0,	5778D0,&
													19344D0,1080D0,&
													15741D0,2857D0&
													/)*9D0/89600D0

			NC10=0D0
			x=a
			dx=(b-a)/9D0
			do i=1,10
				NC10=NC10+Newt10(i)*fn(x)
				x=x+dx
			enddo
			NC10=NC10*dx
		end function NC10
	end subroutine StupidIntegrateNC2

		subroutine spline(x,y,n,yp1,ypn,y2)
			implicit none
			integer (kind=4), intent(in)::n
			integer (kind=4):: i,k
			integer (kind=4), parameter:: NMAX=1000000
			real (kind=8), intent(in) :: yp1,ypn,x(n),y(n)
			real (kind=8), intent(out):: y2(n)
			real (kind=8):: p,qn,sig,un,u(NMAX)
			! Given arrays x(1:n) and y(1:n) containing a tabulated function,
			! with the xi's in ascending order, and given yp1 and ypn for the first derivative
			! of the interpolating function at points 1 and n, respectively, this routine
			! returns  an array y2(1:n) of length n which contains the second derivatives of the
			! interpolating function at the tabulated points x(i). If yp1 and/or ypn are equal to
			! 1D30 or larger, the subroutine is then signaled to set the corresponding boundary
			! condition for a natural spline, with zero second derivative on that boundary.
			! Parameter NMAX is the largest expected value of n.

			if (yp1.gt..99d30) then
				y2(1)=0.d0
				u(1)=0.d0
			else
				y2(1)=-0.5d0
				u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
			endif

			do i=2,n-1
				sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
				p=sig*y2(i-1)+2.
				y2(i)=(sig-1.)/p
				u(i)=(6.d0*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))&
				        /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
			enddo

			if (ypn.gt..99d30) then
				qn=0.d0
				un=0.d0
			else
				qn=0.5d0
				un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
			endif

			y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)

			do k=n-1,1,-1
				y2(k)=y2(k)*y2(k+1)+u(k)
			enddo
		end subroutine spline

		subroutine splint(xa,ya,y2a,n,x,y) ! Spline Interpolation
			implicit none
			integer (kind=4), intent(in):: n
			real (kind=8), intent(in)   :: x,xa(n),ya(n),y2a(n)
			real (kind=8), intent(out)  ::y
			integer (kind=4):: k,khi,klo
			real (kind=8)   :: a,b,h
			! Given the arrays xa(1:n) and ya(1:n) of length n, which tabulate
			! a function (with the xa's in order), and given the array y2a(1:n),
			! which is the output from spline(), and given a value of x, this
			! routine returns a cubic-spline interpolated value y.

			klo=1
			khi=n
			! Gotos are dumb...
1			     if (khi-klo.gt.1) then
			         k=(khi+klo)/2
			         if (xa(k).gt.x) then
			            khi=k
			         else
			            klo=k
			         endif
			      goto 1
			      endif


			!do while ((khi-klo).gt.(1))   !This block is searching for the index range of xa that best approximates x
			!	k=(khi+klo)/2
			!	if (xa(k).gt.x) then
			!		khi=k
			!	else
			!		klo=k
			!	endif
			!enddo

			h=xa(khi)-xa(klo)
			if (h.eq.0.d0) then
				write(*,*)'bad xa input in splint'
				call exit(-2)
			endif
			a=(xa(khi)-x)/h
			b=(x-xa(klo))/h
			y=a*ya(klo)+b*ya(khi)+&
				((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
		return
		end subroutine splint

		real (kind=8) function Heaviside(x)
			real (kind=8):: x
			Heaviside = 0D0
			if (x .ge. 0D0) Heaviside = 1D0
		end function Heaviside

		logical function HeavisideQ(x)
			real (kind=8):: x
			HeavisideQ = .false.
			if (x .ge. 0D0) HeavisideQ = .true.
		end function HeavisideQ


		real (kind=8) function NormC(v)
			implicit none
			real (kind=8) ::v(2)
			! v must be a 2D Cartesian vector

			NormC=(v(1))**2+(v(2))**2
			NormC=dsqrt(NormC)
		end function

		real (kind=8) function HexBound(amp,theta)
			implicit none
			real (kind=8)::amp,theta
			real (kind=8),parameter::Hslope=1D0/dsqrt(3D0),pithird=dacos(-1D0)/3D0

				HexBound=modulo(theta-pithird/2D0,pithird)
				HexBound=Hslope*dsin(HexBound)+dcos(HexBound)
				HexBound=amp/HexBound
		end function HexBound

		real (kind=8) function DiamondBound(amp,theta)
			use Constants
			implicit none
			real (kind=8)::amp,theta
			real (kind=8),parameter::a1=dsin(dacos(-1D0)/6D0),pithird=dacos(-1D0)/3D0&
					,twopithird=2D0*dacos(-1D0)/3D0,threepihalf=3D0*dacos(-1D0)/2D0

				DiamondBound=dcos(theta2(pithird,threepihalf,twopithird,theta))
				DiamondBound=amp*a1/DiamondBound
			contains
				real (kind=8) function theta2(am,d,w,theta)
					use Constants
					implicit none
					real (kind=8)::theta,am,d,w


					theta2=(theta-d)/w
					theta2=real(nint(theta2), kind=8)-theta2
					theta2=1D0-2D0*dabs(theta2)
					theta2=theta2*am
				end function theta2
		end function DiamondBound

		subroutine Cart2Pol(cv,pv)
			use Constants
			implicit none
			real (kind=8),intent(in)::cv(2)
			real (kind=8),intent(out)::pv(2)
			real (kind=8),parameter::twopi=2D0*dacos(-1D0)

				pv(1)=dsqrt(cv(1)**2+cv(2)**2)
				!pv(2)=modulo(datan2(cv(2),cv(1)),twopi)
				pv(2)=datan2(cv(2),cv(1))

		end subroutine Cart2Pol

		subroutine Pol2Cart(pv,cv)
			implicit none
			real (kind=8),intent(in)::pv(2)
			real (kind=8),intent(out)::cv(2)

				cv(1)=pv(1)*dcos(pv(2))
				cv(2)=pv(1)*dsin(pv(2))
		end subroutine Pol2Cart

		function PolAdd(pv1,pv2) result (rv)
			implicit none
			real (kind=8),intent(in)::pv1(2),pv2(2)
			real (kind=8)::cv1(2),cv2(2),rv(2)

			call Pol2Cart(pv1,cv1)
			call Pol2Cart(pv2,cv2)
			call Cart2Pol((/cv1(1)+cv2(1),cv1(2)+cv2(2)/),rv)
		end function PolAdd

		function PolAdd2(pv1,pv2) result (rv)
			implicit none
			real (kind=8),intent(in)::pv1(2),pv2(2)
			real (kind=8)::rv(2)

			rv(1)=dsqrt(pv1(1)**2+pv2(1)**2+2D0*pv1(1)*pv2(1)*dcos(pv2(2)-pv1(2)))
			rv(2)=pv1(2)+datan2(pv2(1)*dsin(pv2(2)-pv1(2)),pv1(1)+pv2(1)*dcos(pv2(2)-pv1(2)))
		end function PolAdd2


		real (kind=8) function NumDeriv1(f,x,h)
			real (kind=8)::x,h
			real (kind=8), external::f

				NumDeriv1=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
				NumDeriv1=NumDeriv1/12D0/h
		end function NumDeriv1

		real (kind=8) function NumDeriv2(f,x,h)
			real (kind=8)::x,h
			real (kind=8), external::f

			NumDeriv2=f(x-h)-2D0*f(x)+f(x+h)
			NumDeriv2=NumDeriv2/h**2

		end function NumDeriv2


	subroutine ErrExit()
	!	if (evinfo.lt.0) then
	!		write(*,*) -evinfo, "th argument had an illegal value"
	!	elseif (evinfo.gt.0)
	!		write(*,*) evinfo
	!		write(*,*) "the QR algorithm failed to compute all the"&
	!				,"eigenvalues, and no eigenvectors have been computed;"&
    !            	,"elements i+1:N of WR and WI contain eigenvalues which"&
    !            	,"have converged."&
	!	endif

		call EXIT(-1)
	end subroutine ErrExit





	subroutine FindBracket2(func,x,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4), intent(in) :: steps
		real    (kind=8), intent(in) :: ao,bo,x
		integer (kind=4), intent(out):: stat
		real    (kind=8), intent(out):: a,b
		real    (kind=8), external   :: func
		integer (kind=4)::i
		real    (kind=8)::dv


	!Finds an interval that brackets a zero of a given function.
	!Starts from b and moves toward a.

		i=1

		dv=(ao-bo)/(real(steps, kind=8))
		b=bo+dv
		a=bo

		do while (((func(x,a)*func(x,b)).gt.(0D0)).and.(i.lt.steps))
			!write(10,*) i, func(x,a)*func(x,b)
			a=a+dv
			b=b+dv
			i=i+1

		enddo
		stat=-1
		if ((func(x,a)*func(x,b)).lt.0D0) then
			stat=1
		endif
		return
	end subroutine FindBracket2

	subroutine FindBracketList(func,x,ao,bo,array,steps)
		implicit none
		integer (kind=4) :: steps,stat,i,j
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8) :: array(4,3)
		real    (kind=8), external:: func

	!Finds an interval that brackets a zero of a given function.
	!Starts from b and moves toward a.

		i=1
		j=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
		array=-1D0!reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))

		do while (i.lt.steps)
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			if ((func(x,a)*func(x,b)).lt.(0D0)) then
				array(j,:)=(/a,b,1D0/)
				j=j+1

			endif
			i=i+1

		enddo
		return
	end subroutine FindBracketList

	subroutine FindBracketList2(EpsList,n,q,wa,dw,array)
		implicit none
		integer (kind=4) :: steps,i,j,n
		real    (kind=8) :: q,dw,wa
		real    (kind=8) :: array(20,3),EpsList(n)
		!real    (kind=8), external:: func

	!Finds an interval that brackets a zero of a given function.
	!Starts from b and moves toward a.
		array=-1D0
		steps=n-1
		j=1
		do i=1,steps
			if ((EpsList(i)*EpsList(i+1)).lt.(0D0)) then
				!write(*,*) (/q,dw*real(i),dw*real(i+1)/)
				!array(j,1)=q
				array(j,:)=(/q,wa+dw*real(i, kind=8),wa+dw*real(i+1, kind=8)/)
				j=j+1
			endif
		enddo



		return
	end subroutine FindBracketList2


	subroutine FindBracket(func,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4), intent(in) :: steps
		real    (kind=8), intent(in) :: ao,bo
		integer (kind=4), intent(out):: stat
		real    (kind=8), intent(out):: a,b
		real    (kind=8), external   :: func
		integer (kind=4)::i
		real    (kind=8)::dv,fa,fb


	!Finds an interval that brackets a zero of a given function.
	!Starts from b and moves toward a.

		i=1

		dv=-(ao-bo)/(real(steps, kind=8))
		b=ao+dv
		a=ao

		fa=func(a)
		fb=func(b)
		do while (((fa*fb).gt.(0D0)).and.(i.lt.steps))
			! fa=func(a)
			! write(*,*) i, b, fa,fb
			! a=b
			b=b+dv
			i=i+1
			fb=func(b)
		enddo
		stat=-1
		if ((fa*fb).lt.0D0) then
			a=b-2D0*dv
			! fa=func(a)
			stat=1
		endif
		! fa=func(a)
		! fb=func(b)
		! write(*,*) "a,b,f(a),f(b):",a,b,fa,fb

		return
	end subroutine FindBracket

	subroutine FindBracket1(func,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4), intent(in) :: steps
		real    (kind=8), intent(in) :: ao,bo
		integer (kind=4), intent(out):: stat
		real    (kind=8), intent(out):: a,b
		real    (kind=8), external   :: func
		integer (kind=4)::i
		real    (kind=8)::dv,fa,fb


	!Finds an interval that brackets a zero of a given function.
	!Starts from b and moves toward a.

		i=1

		dv=-(ao-bo)/(real(steps, kind=8))
		b=ao+dv
		a=ao

		fa=func(a)
		fb=func(b)
		do while (((fa*fb).gt.(0D0)).and.(i.lt.steps))
			! fa=func(a)
			! write(*,*) i, b, fa,fb
			! a=b
			b=b+dv
			i=i+1
			fb=func(b)
		enddo
		stat=-1
		if ((fa*fb).lt.0D0) then
			a=b-2D0*dv
			! fa=func(a)
			stat=1
		endif
		! fa=func(a)
		! fb=func(b)
		! write(*,*) "a,b,f(a),f(b):",a,b,fa,fb

		return
	end subroutine FindBracket1

	subroutine RootFinder(fn,a,b,eps,resa,resfn)
		use Globals
		implicit none
		real (kind=8), intent(in)  ::a,b,eps
		real (kind=8), intent(out) ::resa,resfn
		real (kind =8), external   ::fn
		integer (kind=4), parameter::BracketSteps=500
		real (kind=8)   ::a1,b1
		integer (kind=4)::stat

		a1=a
		b1=b


		! write(*,*) "RootFinder"
		! write(*,*) "a,b,f(a),f(b)",a,b,fn(a),fn(b)
		if (fn(a)*fn(b).gt.(0D0)) then
			write(*,*) "Looking for a bracket"
			call FindBracket(fn,a,b,a1,b1,BracketSteps,stat)
			if (stat.eq.(-1)) then
				write(*,*) "Can't find a bracket."
				write(*,*) "a,b,steps"
				write(*,*) a1,b1,BracketSteps
				resa=-0D0
				resfn=fn(resa)
				Globstat=stat
				return
				! call exit(stat)
			endif
		endif


		resa=RootBrentsMethod3(a1,b1,eps,fn)
		! resfn=fn(resa)



	end subroutine RootFinder


	subroutine RootFinder1(fn,a,b,eps,resa,resfn)
		use Globals
		implicit none
		real (kind=8), intent(in)  ::a,b,eps
		real (kind=8), intent(out) ::resa,resfn
		real (kind =8), external   ::fn
		integer (kind=4), parameter::BracketSteps=300
		real (kind=8)   ::a1,b1
		integer (kind=4)::stat

		a1=a
		b1=b


		! write(*,*) "RootFinder"
		! write(*,*) "a,b,f(a),f(b)",a,b,fn(a),fn(b)
		if (fn(a)*fn(b).gt.(0D0)) then
			write(*,*) "Looking for a bracket"
			call FindBracket1(fn,a,b,a1,b1,BracketSteps,stat)
			if (stat.eq.(-1)) then
				write(*,*) "Can't find a bracket."
				write(*,*) "a,b,steps"
				write(*,*) a1,b1,BracketSteps
				resa=-0D0
				resfn=fn(resa)
				Globstat=stat
				return
				! call exit(stat)
			endif
		endif

		write(*,*) "Root finding."
		resa=RootBrentsMethod31(a1,b1,eps,fn)
		! resfn=fn(resa)



	end subroutine RootFinder1

		real (kind=8) function RootBrentsMethod(aa,ba,delta,f)
			implicit none
			real (kind=8) ::a,b,aa,ba,c,d,s,swap1,swap2,delta
			real (kind=8), external:: f
			integer (kind=4) ::mflag,ind

		! This function finds the root of a function, f in [a,b] with the precision of delta
		! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
		! a and b must be REAL.
		! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method

			a=aa
			b=ba

			if (f(a)*f(b).gt.(0D0)) then
				write(*,*) "rootfinder problems"
				write(*,*) "a,b,f(a),f(b)"
				write(*,*) a,b,f(a),f(b)
				RootBrentsMethod=-0D0
				return
				!call EXIT(1)
			endif

			if (dabs(f(a)).gt.dabs(f(b))) then
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif

			c=a
			mflag=1
			s=a
			ind=1

			do while ((dabs(b-a).gt.delta/100D0).and.(dabs(f(b)).gt.delta).and.(dabs(f(s)).gt.delta).and.(ind.lt.1000000))
				ind=ind+1
				if ((f(a).ne.f(c)).and.(f(b).ne.f(c))) then
					s=(a*f(b)*f(c))/((f(a)-f(b))*(f(a)-f(c)))+(b*f(a)&
						*f(c))/((f(b)-f(a))*(f(b)-f(c)))+(c*f(a)*f(b))&
						/((f(c)-f(a))*(f(c)-f(b)))
					!Inverse Quadratic Interpolation
				else
					s=b-f(b)*(b-a)/(f(b)-f(a))
					!Secant Method
				endif

				if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((dabs(s-b))&
					 .ge.(dabs(b-c)/2D0))).or.((mflag.eq.(0)).and.((dabs(s-b)).ge.&
					 (dabs(c-d)/2D0))).or.((mflag.eq.(1)).and.(dabs(b-c).lt.delta)).or.&
					 ((mflag.eq.(0)).and.(dabs(c-d).lt.delta))) then
					s=(a+b)/2
					!Bisection Method
					mflag=1
				else
					mflag=0
				endif

				d=c
				c=b

				if ((f(a)*f(s)).lt.(0D0)) then
					b=s
				else
					a=s
				endif

				if (dabs(f(a)).gt.dabs(f(b))) then
					swap1=a
					swap2=b
					a=swap2
					b=swap1
				endif

			enddo

			if ((f(b)).lt.(f(s))) then
				RootBrentsMethod=b
			else
				RootBrentsMethod=s
			endif
			!write(*,*) "Root steps", ind, f(RootBrentsMethod)
			return
		end function RootBrentsMethod



	real (kind=8) function RootBrentsMethod3(aa,ba,delta,f)
		implicit none
		real (kind=8) ::a,b,aa,ba,c,d,s,swap1,swap2,delta,fa,fb,fc,fs
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind

	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL.
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method

		a=aa
		b=ba

		fa=f(a)
		fb=f(b)

		if (fa*fb.gt.(0D0)) then
			write(*,*) "rootfinder problems"
			write(*,*) "a,b,f(a),f(b)"
			write(*,*) a,b,fa,fb
			RootBrentsMethod3=-0D0
			return
			!call EXIT(1)
		endif

		if (dabs(f(a)).gt.dabs(f(b))) then
			swap1=fa
			swap2=fb
			fa=swap2
			fb=swap1
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif

		c=a
		fc=fa!f(c)
		mflag=1
		s=a
		fs=fa!f(s)
		ind=1


		do while ((dabs(b-a).gt.delta/100D0).and.(dabs(fb).gt.delta).and.(dabs(fs).gt.delta).and.(ind.lt.1000000))
			ind=ind+1
			if ((fa.ne.fc).and.(fb.ne.fc)) then
				s=(a*fb*fc)/((fa-fb)*(fa-fc))+(b*fa&
					*fc)/((fb-fa)*(fb-fc))+(c*fa*fb)&
					/((fc-fa)*(fc-fb))
				!Inverse Quadratic Interpolation
			else
				s=b-fb*(b-a)/(fb-fa)
				!Secant Method
			endif

			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((dabs(s-b))&
				 .ge.(dabs(b-c)/2D0))).or.((mflag.eq.(0)).and.((dabs(s-b)).ge.&
				 (dabs(c-d)/2D0))).or.((mflag.eq.(1)).and.(dabs(b-c).lt.delta)).or.&
				 ((mflag.eq.(0)).and.(dabs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif
			fs=f(s)


			d=c
			c=b
			fc=fb


			if ((fa*fs).lt.(0D0)) then
				b=s
				fb=fs
			else
				a=s
				fa=fs
			endif

			if (dabs(fa).gt.dabs(fb)) then
				swap1=fa
				swap2=fb
				fa=swap2
				fb=swap1
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif


			! write(*,*) "Brent:",fa,fb,fc,fs
		enddo

		if ((fb).lt.(fs)) then
			RootBrentsMethod3=b
			fc=fb
		else
			RootBrentsMethod3=s
			fc=fs
		endif
		write(*,*) "Root steps", ind, fc
		return
	end function RootBrentsMethod3


	real (kind=8) function RootBrentsMethod31(aa,ba,delta,f)
		implicit none
		real (kind=8) ::a,b,aa,ba,c,d,s,swap1,swap2,delta,fa,fb,fc,fs
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind

	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL.
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method

		a=aa
		b=ba

		fa=f(a)
		fb=f(b)

		if (fa*fb.gt.(0D0)) then
			write(*,*) "rootfinder problems"
			write(*,*) "a,b,f(a),f(b)"
			write(*,*) a,b,fa,fb
			RootBrentsMethod31=-0D0
			return
			!call EXIT(1)
		endif

		if (dabs(f(a)).gt.dabs(f(b))) then
			swap1=fa
			swap2=fb
			fa=swap2
			fb=swap1
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif

		c=a
		fc=fa!f(c)
		mflag=1
		s=a
		fs=fa!f(s)
		ind=1


		do while ((dabs(b-a).gt.delta/100D0).and.(dabs(fb).gt.delta).and.(dabs(fs).gt.delta).and.(ind.lt.100))
			ind=ind+1
			if ((fa.ne.fc).and.(fb.ne.fc)) then
				s=(a*fb*fc)/((fa-fb)*(fa-fc))+(b*fa&
					*fc)/((fb-fa)*(fb-fc))+(c*fa*fb)&
					/((fc-fa)*(fc-fb))
				!Inverse Quadratic Interpolation
			else
				s=b-fb*(b-a)/(fb-fa)
				!Secant Method
			endif

			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((dabs(s-b))&
				 .ge.(dabs(b-c)/2D0))).or.((mflag.eq.(0)).and.((dabs(s-b)).ge.&
				 (dabs(c-d)/2D0))).or.((mflag.eq.(1)).and.(dabs(b-c).lt.delta)).or.&
				 ((mflag.eq.(0)).and.(dabs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif
			fs=f(s)


			d=c
			c=b
			fc=fb


			if ((fa*fs).lt.(0D0)) then
				b=s
				fb=fs
			else
				a=s
				fa=fs
			endif

			if (dabs(fa).gt.dabs(fb)) then
				swap1=fa
				swap2=fb
				fa=swap2
				fb=swap1
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif


			! write(*,*) "Brent:",fa,fb,fc,fs
		enddo

		if ((fb).lt.(fs)) then
			RootBrentsMethod31=b
			fc=fb
		else
			RootBrentsMethod31=s
			fc=fs
		endif
		write(*,*) "Root steps", ind, fc
		return
	end function RootBrentsMethod31



	subroutine RootFinder2(fn,a,b,x,eps,resa,resfn)
		implicit none
		real (kind=8), intent(in)  ::a,b,x,eps
		real (kind=8), intent(out) ::resa,resfn
		real (kind =8), external   ::fn
		integer (kind=4), parameter::BracketSteps=50000
		real (kind=8)   ::a1,b1
		integer (kind=4)::stat

		a1=a
		b1=b

		if (fn(x,a)*fn(x,b).gt.(0D0)) then
		!	write(*,*) "Looking for a bracket"
			call FindBracket2(fn,x,a,b,a1,b1,BracketSteps,stat)
			if (stat.eq.(-1)) then
		!		write(*,*) "Can't find a bracket."
		!		write(*,*) "x,a,b,steps"
		!		write(*,*) x,a1,b1,BracketSteps
				resa=-0D0
				resfn=fn(x,resa)
				return
				!call exit(stat)
			endif
		endif


		resa=RootBrentsMethod2(a1,b1,eps,fn,x)
		resfn=fn(x,resa)



	end subroutine RootFinder2



	real (kind=8) function RootBrentsMethod2(aa,ba,delta,f,x)
		implicit none
		real (kind=8) ::a,b,aa,ba,c,d,s,swap1,swap2,delta,x
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind

	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL. f must be a function that takes two inputs (x gets passed along to f).
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method

		a=aa
		b=ba

		if (f(x,a)*f(x,b).gt.(0D0)) then
			write(*,*) "rootfinder problems"
			write(*,*) "a,b,f(x,a),f(x,b)"
			write(*,*) a,b,f(x,a),f(x,b)
			RootBrentsMethod2=-0D0
			return
			!call EXIT(1)
		endif

		if (dabs(f(x,a)).gt.dabs(f(x,b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif

		c=a
		mflag=1
		s=a
		ind=1

		do while ((dabs(b-a).gt.delta/100D0).and.(dabs(f(x,b)).gt.delta).and.(dabs(f(x,s)).gt.delta).and.(ind.lt.1000000))
			ind=ind+1
			if ((f(x,a).ne.f(x,c)).and.(f(x,b).ne.f(x,c))) then
				s=(a*f(x,b)*f(x,c))/((f(x,a)-f(x,b))*(f(x,a)-f(x,c)))+(b*f(x,a)&
				  *f(x,c))/((f(x,b)-f(x,a))*(f(x,b)-f(x,c)))+(c*f(x,a)*f(x,b))&
				  /((f(x,c)-f(x,a))*(f(x,c)-f(x,b)))
				!Inverse Quadratic Interpolation
			else
				s=b-f(x,b)*(b-a)/(f(x,b)-f(x,a))
				!Secant Method
			endif

			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((dabs(s-b))&
			   .ge.(dabs(b-c)/2D0))).or.((mflag.eq.(0)).and.((dabs(s-b)).ge.&
			   (dabs(c-d)/2D0))).or.((mflag.eq.(1)).and.(dabs(b-c).lt.delta)).or.&
			   ((mflag.eq.(0)).and.(dabs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif

			d=c
			c=b

			if ((f(x,a)*f(x,s)).lt.(0D0)) then
				b=s
			else
				a=s
			endif

			if (dabs(f(x,a)).gt.dabs(f(x,b))) then
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif

		enddo

		if ((f(x,b)).lt.(f(x,s))) then
			RootBrentsMethod2=b
		else
			RootBrentsMethod2=s
		endif
		!write(*,*) "Root steps", ind, f(x,RootBrentsMethod)
		return
	end function RootBrentsMethod2


	recursive subroutine GaussAdaptQuad(fn,a,b,eps,res)
		implicit none
		real (kind=8), intent(in) ::a,b,eps
		real (kind=8), intent(out) ::res
		real (kind=8), external::fn
		!Wrapper subroutine to properly initialize the the
		!auxiliary subroutine.
		!!!fn  :: real; function
		!!!a,b :: real; integration limits, a<b
		!!!eps :: real; local error tolerance
		!!!res :: real; integration result


		call GaussAdaptQuadAux(fn,a,b,eps,0,0D0,res)

	end subroutine GaussAdaptQuad

	recursive subroutine GaussAdaptQuad2(fn,a,b,eps,res)
		implicit none
		real (kind=8), intent(in) ::a,b,eps
		real (kind=8), intent(out) ::res
		real (kind=8), external::fn
		!Wrapper subroutine to properly initialize the the
		!auxiliary subroutine.
		!!!fn  :: real; function
		!!!a,b :: real; integration limits, a<b
		!!!eps :: real; local error tolerance
		!!!res :: real; integration result


		call GaussAdaptQuadAux2(fn,a,b,eps,0,0D0,res)

	end subroutine GaussAdaptQuad2


	recursive subroutine GaussAdaptQuadAux(fn,a,b,eps,ct,totin,totout)
		implicit none
		integer (kind=4), intent(in)::ct
		real (kind=8), intent(in) ::a,b,eps,totin
		real (kind=8), intent(out) ::totout
		real (kind=8) ::lint,rint,m,tot1,tot2,totnew,totold
		real (kind=8), external::fn
		!Adaptive integration subroutine
		!!!fn    :: real; function
		!!!a,b   :: real; integration limits
		!!!eps   :: real; local error tolerance
		!!!totin :: real; previous integration result
		!!!totout:: real; current integration result
		!!!ct    ::  int; subdivision counter



			!Don't go deeper than 1000 subdivisions
			if (ct.gt.1000) then
				write(*,*) "Integral didn't converge within 1000 subdivisions"
				write(*,*) "a,b,eps,|err|"
				write(*,*) a,b,eps,dabs(totnew-totold)

				call EXIT(5)
			endif


			!Halve the interval and approximate each side with
			!8-point Gaussian-Legendre Quadrature
			m=(a+b)/2D0
			lint=G8(fn,a,m)
			rint=G8(fn,m,b)


			!If this is the first call then approximate the whole interval.
			!Otherwise use the previous approximation
			if (ct.eq.0) then
				totold=G8(fn,a,b)
			else
				totold=totin
			endif

			!New approximation
			totnew=lint+rint



			!Stopping conditions
			! if (((dabs(totnew-totold)).lt.eps).or.((dabs(a-b)).lt.eps)) then
			if ((dabs(totnew-totold)).lt.eps) then
				totout=totnew
			else
				!Subdivide further
				call GaussAdaptQuadAux(fn,a,m,eps/2D0,ct+1,lint,tot1)
				call GaussAdaptQuadAux(fn,m,b,eps/2D0,ct+1,rint,tot2)
				totout=tot1+tot2
			endif


	contains

		real (kind=8) recursive function G8(fn,l,r)
			implicit none
			real (kind=8)::l,r,x,h
			real (kind=8),external::fn
			!Abscissae and weights for 8-point Gaussian-Legendre Quadrature over [-1,1]
			!https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
			real (kind=8), parameter::G8x1=1.83434642495649805D-01&
									 ,G8x2=5.25532409916328986D-01&
									 ,G8x3=7.96666477413626740D-01&
									 ,G8x4=9.60289856497536232D-01&
									 ,G8w1=3.62683783378361983D-01&
									 ,G8w2=3.13706645877887287D-01&
									 ,G8w3=2.22381034453374471D-01&
									 ,G8w4=1.01228536290376259D-01
			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			!8-point Gaussian-Legendre Approximation
			!!!fn    :: real; function
			!!!l,r   :: real; interval limits

				!x and h map [a,b] to [-1,1]
				x=(l+r)/2D0
				h=(r-l)/2D0
				G8=h*(G8w1*(fn(x+h*G8x1)+fn(x-h*G8x1))&
					 +G8w2*(fn(x+h*G8x2)+fn(x-h*G8x2))&
					 +G8w3*(fn(x+h*G8x3)+fn(x-h*G8x3))&
					 +G8w4*(fn(x+h*G8x4)+fn(x-h*G8x4)))
		end function G8

	end subroutine GaussAdaptQuadAux

	recursive subroutine GaussAdaptQuadAux2(fn,a,b,eps,ct,totin,totout)
		implicit none
		integer (kind=4), intent(in)::ct
		real (kind=8), intent(in) ::a,b,eps,totin
		real (kind=8), intent(out) ::totout
		real (kind=8) ::lint,rint,m,tot1,tot2,totnew,totold
		real (kind=8), external::fn
		!Adaptive integration subroutine
		!!!fn    :: real; function
		!!!a,b   :: real; integration limits
		!!!eps   :: real; local error tolerance
		!!!totin :: real; previous integration result
		!!!totout:: real; current integration result
		!!!ct    ::  int; subdivision counter



			!Don't go deeper than 1000 subdivisions
			if (ct.gt.1000) then
				write(*,*) "Integral didn't converge within 1000 subdivisions"
				write(*,*) "a,b,eps,|err|"
				write(*,*) a,b,eps,dabs(totnew-totold)

				call EXIT(5)
			endif


			!Halve the interval and approximate each side with
			!8-point Gaussian-Legendre Quadrature
			m=(a+b)/2D0
			lint=G8(fn,a,m)
			rint=G8(fn,m,b)


			!If this is the first call then approximate the whole interval.
			!Otherwise use the previous approximation
			if (ct.eq.0) then
				totold=G8(fn,a,b)
			else
				totold=totin
			endif

			!New approximation
			totnew=lint+rint



			!Stopping conditions
			if (((dabs(totnew-totold)).lt.eps).or.((dabs(a-b)).lt.eps)) then
				totout=totnew
			else
				!Subdivide further
				call GaussAdaptQuadAux2(fn,a,m,eps/2D0,ct+1,lint,tot1)
				call GaussAdaptQuadAux2(fn,m,b,eps/2D0,ct+1,rint,tot2)
				totout=tot1+tot2
			endif


	contains

		real (kind=8) recursive function G8(fn,l,r)
			implicit none
			real (kind=8)::l,r,x,h
			real (kind=8),external::fn
			!Abscissae and weights for 8-point Gaussian-Legendre Quadrature over [-1,1]
			!https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
			real (kind=8), parameter::G8x1=1.83434642495649805D-01&
									 ,G8x2=5.25532409916328986D-01&
									 ,G8x3=7.96666477413626740D-01&
									 ,G8x4=9.60289856497536232D-01&
									 ,G8w1=3.62683783378361983D-01&
									 ,G8w2=3.13706645877887287D-01&
									 ,G8w3=2.22381034453374471D-01&
									 ,G8w4=1.01228536290376259D-01
			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			!8-point Gaussian-Legendre Approximation
			!!!fn    :: real; function
			!!!l,r   :: real; interval limits

				!x and h map [a,b] to [-1,1]
				x=(l+r)/2D0
				h=(r-l)/2D0
				G8=h*(G8w1*(fn(x+h*G8x1)+fn(x-h*G8x1))&
					 +G8w2*(fn(x+h*G8x2)+fn(x-h*G8x2))&
					 +G8w3*(fn(x+h*G8x3)+fn(x-h*G8x3))&
					 +G8w4*(fn(x+h*G8x4)+fn(x-h*G8x4)))
		end function G8

	end subroutine GaussAdaptQuadAux2

	subroutine EigVals(Mat,Num,EV)
		!use lapack
		integer (kind=4)::Num,info
		real    (kind=8)::Mat(Num,Num),WR(Num),WI(Num),VL(Num),VR(Num),&
						  Work(4*Num),Matrix(Num,Num),EV(2,Num)

		Matrix=Mat
		call dgeev('N','N',Num,Matrix,Num,WR,WI,VL,1,VR,1,Work,4*Num,info)
		EV(1,:)=WR
		EV(2,:)=WI
	end subroutine EigVals

	real (kind=8) function Prbld(q,w,k)
		implicit none
		real (kind=8)::q(2),w,k(2)

		Prbld=0D0
		if (pf(k).ge.5D-1) then
			Prbld=k(1)**2*dcos(k(2))**2
		endif
	end function Prbld

	real (kind=8) function pf(k)
		implicit none
		real (kind=8)::k(2)

		pf=0D0
		if (k(1).le.1D0) then
			pf=1d0
		endif
	end function pf


	subroutine SimpleBisect(f,ai,bi,del,res)
		real(8), intent(in)::ai,bi,del
		real(8), external  ::f
		real(8), intent(out)::res
		real(8)::a,b,c,absdel,fa,fb,fc
		integer(4)::i
		integer(4),parameter::imax=100

		absdel=del*dabs(bi-ai)
		a=ai
		b=bi
		c=(bi-ai)/2D0
		fa=f(a)
		fb=f(b)
		fc=f(c)

		i=1
		do while (( dabs(b-a).gt.absdel ).and.(i.lt.imax))
			if ( fa*fc.gt.0D0 ) then
				a=c
				fa=fc
			else
				b=c
				fb=fc
			end if
			c=(b-a)/2D0
			fc=f(c)
			i=i+1
		end do

		write(*,*) "Bisection Steps:",i
		res=c
	end subroutine SimpleBisect

	subroutine StupidIntegrateG8(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8), external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8) !NC10 takes 10 steps

		! x=a+dx/2D0 ! Why am I doing this?...
		x=a!+dx/2D0
		res=0D0
		do i=1,steps
			res=res+G8(x,x+dx,f)
			x=x+dx
		enddo
		!res=res*dx

	contains
		real (kind=8) function G8(l,r,fn)
			implicit none
			real (kind=8)::l,r,x,h
			real (kind=8),external::fn
			!Abscissae and weights for 8-point Gaussian-Legendre Quadrature over [-1,1]
			!https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
			real (kind=8), parameter::G8x1=1.83434642495649805D-01&
									 ,G8x2=5.25532409916328986D-01&
									 ,G8x3=7.96666477413626740D-01&
									 ,G8x4=9.60289856497536232D-01&
									 ,G8w1=3.62683783378361983D-01&
									 ,G8w2=3.13706645877887287D-01&
									 ,G8w3=2.22381034453374471D-01&
									 ,G8w4=1.01228536290376259D-01
			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			!8-point Gaussian-Legendre Approximation
			!!!fn    :: real; function
			!!!l,r   :: real; interval limits

				!x and h map [a,b] to [-1,1]
				x=(l+r)/2D0
				h=(r-l)/2D0
				G8=h*(G8w1*(fn(x+h*G8x1)+fn(x-h*G8x1))&
					 +G8w2*(fn(x+h*G8x2)+fn(x-h*G8x2))&
					 +G8w3*(fn(x+h*G8x3)+fn(x-h*G8x3))&
					 +G8w4*(fn(x+h*G8x4)+fn(x-h*G8x4)))
		end function G8
	end subroutine StupidIntegrateG8


	real (kind=8) function G8(fn,l,r)
		implicit none
		real (kind=8)::l,r,x,h
		real (kind=8),external::fn
		!Abscissae and weights for 8-point Gaussian-Legendre Quadrature over [-1,1]
		!https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
		real (kind=8), parameter::G8x1=1.83434642495649805D-01&
								 ,G8x2=5.25532409916328986D-01&
								 ,G8x3=7.96666477413626740D-01&
								 ,G8x4=9.60289856497536232D-01&
								 ,G8w1=3.62683783378361983D-01&
								 ,G8w2=3.13706645877887287D-01&
								 ,G8w3=2.22381034453374471D-01&
								 ,G8w4=1.01228536290376259D-01
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!8-point Gaussian-Legendre Approximation
		!!!fn    :: real; function
		!!!l,r   :: real; interval limits

			!x and h map [a,b] to [-1,1]
			x=(l+r)/2D0
			h=(r-l)/2D0
			G8=h*(G8w1*(fn(x+h*G8x1)+fn(x-h*G8x1))&
				 +G8w2*(fn(x+h*G8x2)+fn(x-h*G8x2))&
				 +G8w3*(fn(x+h*G8x3)+fn(x-h*G8x3))&
				 +G8w4*(fn(x+h*G8x4)+fn(x-h*G8x4)))
	end function G8
end module AuxiliaryFunctions
