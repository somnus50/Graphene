#!/bin/bash

findees="out/*/bands_xo_DS2_EIG"

for file in $findees
do
echo $file
grep -A 1 "0.3333  0.3333" $file > $(dirname $file)/splits.txt
done
