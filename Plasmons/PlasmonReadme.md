# Plasmons

This program finds the plasmon dispersion relation numerically.

Compile with: `gfortran -o plasmons Modules.f95 Plasmons.f95`
