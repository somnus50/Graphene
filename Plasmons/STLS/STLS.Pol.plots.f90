program TwoDEGSTLSGsc
use Constants
use Globals
use PhysicsPol2DEG
use STLS
use AuxiliaryFunctions
implicit none
real (kind=8)::rs,qs(2),q,dq,w
integer (kind=4)::i,j,k,steps
complex (kind=8)::temp1,temp2,temp3,temp4


	rs=1D0
	steps=1000



	!!!! Initialize some situational constants
	call ConstInit(rs,1D-1,GlobEf,Globeta) 
    write(*,*) GlobEf,GlobkfU,GlobkfD
	qs=(/0D0,5D0*GlobkfU/)
    dq=(qs(2)-qs(1))/real(steps, kind=8)
!	call exit()

	open(unit=9, file='Chi0.dat')


    w=0.5D0*GlobEf
    q=0D0
	!!! Initialize some files for data storage
    do i=1,steps
        temp1=Chi0ssp(1,1,q,w)
        temp2=Chi0ssp(-1,-1,q,w)
        temp3=Chi0ssp(1,-1,q,w)
        temp4=Chi0ssp(-1,1,q,w)
        write(9,*) q/GlobkfU, w/GlobEf, dreal(temp1), dimag(temp1), dreal(temp2), dimag(temp2), &
                    dreal(temp3), dimag(temp3), dreal(temp4), dimag(temp4)
        q=q+dq

    enddo
    

	!!! Close files
	close(unit=9)

end program TwoDEGSTLSGsc
