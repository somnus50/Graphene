program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene3
use AuxiliaryFunctions
use fxc2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,w,dq,a,b1,bb,k1,k2,ww,w2,zs(11),ns(7),ss,tt
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,valid,l,p
character (len=128)::fmt1,fmt2,cl,cp,fname



	ss=-1D0
	tt=1D0

	zs=-(/1D-8,1D-4,1D-2,5D-2,1D-1,2D-1,3D-1,4D-1,5D-1,8D-1,95D-2/)
	! zs=-zs
	ns=(/1D12,2D13,3.5D13,5D13,7D13,10D13,1D0/(dsqrt(3D0)*aGraphene**2)*2D0*cmSq2a0sq/)!,1D10,1D7

	do l=1,7
   ! write(cl,"(A)") l
		do p=1,11
			! write(cp,"(A)") p
			Globstat=0
			write(*,*) '######################################################'
		 	write(*,*) '######################################################'
		 	write(*,*) '######################################################'
		 	write(*,*) 'n:',l,ns(l),'z:',p,zs(p)

		 	write(fname,'(a,i2.2,a,i2.2,a)') 'Queue.wmagnon.LDA-PGG.n',l,'.z',p,'.dat'

	!!!! Initialize some situational constants
	Globgs=2D0
	Globgv=2D0
	call ConstInit(ns(l)/cmSq2a0sq,zs(p))
	Globeta=min(1D-1*GlobZeem,1D-4*GlobEf)
	GlobEtaSq=GlobEta**2
	! write(*,*) "Z*:",GlobZeem,GlobEf,GLobZeem/GlobEf
	! Globn=1D0*Globn0
	! Globkf=dsqrt(pi*Globn)
	! globkf0=globkf
	! Globntot=Globn+Globn0
	! Globfxc=200D0*(cfx(Globrs)+cfc(Globrs))

	!Globeta=1D-3*GlobEf
	fmt1='(3ES24.15)'
	!write(9,*) "# q/kf,wp/ef,low q limit"


	write(*,*) "kf",Globkf
	write(*,*) "Ef",GlobEf
	write(*,*) "n1/n0",Globn/Globn0


	qsteps=100

	q=0D0!3D0*Globkf
	write(*,*) "Looking for q intercept"
	dq=SpinQIntercept((1D0-1D-10)*GlobZeem/gam,1D-10*GLobZeem/gam,ss,tt)/real(qsteps, kind=8)!*GLobZeem/gam
	if (Globstat.lt.0) then
		write(*,*) "SpinQIntercept couldn't find the intercept. Cycling."
		cycle
	endif
	!!! Initialize some files for data storage
	open(unit=9, file=fname)

	!dw=5D0/real(wsteps, kind=8)!*GlobEf
	q=0D0!dq!0.7D0*Globkf
	write(9,*) "# kf,Ef,n1,n0,z,zp"
	write(9,*) "# ",Globkf,GlobEf,Globn,Globn0,Globz,Globzp
	write(9,*) "# q/kf, wp/Ef (LDA), wp/Ef (PGG), Spin-flip Ctm, PGGud, LDA, LDAx, LDAc"
	! write(9,*) 0D0,0D0,0D0,0D0
	do i=1,qsteps+10
		write(*,*) "qstep:",i
		write(*,*) "q:",q/Globkf
		ww=wmagnonLDA(q,0D0,GlobZeem-(q)*gam,ss,tt)!+1D-2*GlobEf
		w2=wmagnonPGG(q,0D0,GlobZeem-(q)*gam,ss,tt)!+1D-2*GlobEf
		if (Globstat.lt.0) EXIT
		! w2=wplasmonPGG(q,q*gam+1D-3*GlobEf,2.5D0*GlobEf)!+1D-2*GlobEf
		! write(9,*) q/Globkf, ww/GlobZeem, 1D0-gam*q/GlobZeem!,dsqrt(4D0*GlobEf/2D0*q)/GlobEf!, ReChi0(q,ww), ReChi0Ex(q,ww)-Globkc/(2D0*pi*gam)!, Vq(q),PGG(q)
		write(9,*) q, ww, w2, GlobZeem-gam*q, PGGcst(q)&
							, Globfxc,cfxssp(n2rs(Globntot),Globz),cfcssp(n2rs(Globntot),globz)
		! write(9,*) q/Globkf, Globfxc, PGGcst(q), PGG(q)
		! write(9,*) q/Globkc, PGG(q),StructExt(q)!,-Gp0(q)*2D0*pi/q!, ImChi0(q,10000000D0*GlobEf)
		write(*,*) "wp:", ww/GlobZeem,w2/GlobZeem,1D0-gam*q/GlobZeem!,dsqrt(4D0*GlobEf/2D0*q)/GlobEf
		q=q+dq
	enddo
	! dq=3D0*Globkc/real(qsteps, kind=8)
	! q=q+dq
	! do i=1,qsteps
	! 	write(*,*) "qstep:",i
	! 	write(*,*) "q:",q/Globkf
	! 	!ww=wplasmonRPA(q,q*gam+1D-10*GlobEf,3D0*GlobEf)
	! 	!write(9,*) q/Globkf, ww/GlobEf,dsqrt(4D0*GlobEf/2D0*q)/GlobEf, ReChi0(q,ww), ReChi0Ex(q,ww)-Globkc/(2D0*pi*gam)!, Vq(q),PGG(q)
	! 	write(9,*) q/Globkc, PGG(q), -Gp0(q)*2D0*pi/q,StructExt(q)!, ImChi0(q,10000000D0*GlobEf)
	! 	q=q+dq
	! enddo

	close(unit=9)
end do

end do
! write(*,*)

end program GraphenePlasmons
