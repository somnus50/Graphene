program ZeemApprox
use Constants
use Response
use PolynomialRoots
implicit none
real (kind=8), parameter::n0=2D0/(dsqrt(3D0)*aGraphene**2)
real (kind=8)           :: KernVec(4),Ef,Zeem,dnu,dnd,rs,z,dvu,dvd,n,x,y,xo,xf,dx&
						, aq(5),temp,Ebar,nbar,crap(8),tempr,tempi,Ec,Efone,ns(10),fz
integer (kind=4)        ::i,j,k,l
complex (kind=8)        ::rts(4)
character (len=32)      ::nomo
 

ns=(/1D12,5D12,1D13,5D13,1D14,5D14,1D15,5D15,n0*cmSq2a0sq,6D0*n0*cmSq2a0sq/)/cmSq2a0sq


do k=1,10

n=ns(k)

write(nomo,'(a,I2.2,a)') "ZeemApprox.",k,".dat"

write(*,*) nomo



!call exit(0)

open(unit=9, file=nomo)

!n=1D12/cmSq2a0sq
!n=5D12/cmSq2a0sq
!n=1D13/cmSq2a0sq
!n=5D13/cmSq2a0sq
!n=1D14/cmSq2a0sq
!n=5D14/cmSq2a0sq
!n=1D15/cmSq2a0sq
!n=5D15/cmSq2a0sq
!n=n0
!n=10D0*n0

Ef=gam*dsqrt(pi*n)
nbar=n+n0
rs=dsqrt(1D0/pi/(nbar))

Ebar=gam*dsqrt(pi*nbar)
Ec=gam*dsqrt(pi*n0)


write(9,*) "### n",n
write(9,*) "### -zeta,    ZeemXC/Z*,   Zeem,    Bext "

xo=0D0
y=Ebar/Ef
xf=dsqrt(2D0)*dsqrt(y**2+dsqrt(y**4-1D0))

dx=(xf-xo)/500D0

x=xo
do i=1,500
Zeem=x*Ef

z=fz(y,x)

dnu=nbar*z/2D0
dnd=-dnu


call KernelConstr(rs,z,KernVec)
!call ConstInit()

dvu=KernVec(1)*dnu+KernVec(3)*(dnd)
dvd=KernVec(2)*dnd+KernVec(3)*(dnu)





!write(*,*) "i,   n,   Ef,   Zeem,    z"
!write(*,*) i, n,Ef,Zeem,z
!write(*,*) dvd/Zeem+dvu/Zeem
!write(*,*) "***************************"
write(9,*) -z,(dvd+dvu)/(Zeem+dvd+dvu),Zeem,Zeem/(2D0*bmHapT)!,crap

x=x+dx

enddo

 close(unit=9)

enddo













end program ZeemApprox



real (kind=8) function fz(y,x)
implicit none
real (kind=8)::x,y
real (kind=8), parameter::sqrttwo=dsqrt(2D0)
integer (kind=4)::i

if (x.lt.sqrttwo) then
fz=x*dsqrt(1D0-x**2/4D0)
else
fz=(x**(-2)+x**2/4D0)
endif

fz=-fz/y**2

!return fz
end function fz








