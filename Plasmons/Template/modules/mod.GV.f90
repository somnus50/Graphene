module GV
use Globals
use constants
implicit none


contains


real (kind=8) function ReChi0s(q,w,s)
	implicit none
	real (kind=8)::q,w,s,a,b,a2,b2

		ReChi0s=0D0
		a=v(q,w,s,1D0)
		b=v(q,w,s,-1D0)
		a2=a**2-1D0
		b2=b**2-1D0
		if (b2.gt.0D0) then
			ReChi0s=ReChi0s+sign(1D0,b)*dsqrt(b2)
		endif
		if (a2.gt.0D0) then
			ReChi0s=ReChi0s-sign(1D0,a)*dsqrt(a2)
		endif

		ReChi0s=ReChi0s/x(q,s)+1D0
		ReChi0s=ReChi0s/(-2D0*pi)

end function ReChi0s

real (kind=8) function ImChi0s(q,w,s)
	implicit none
	real (kind=8)::q,w,s,a,b,a2,b2

		ImChi0s=0D0
		a=v(q,w,s,1D0)
		b=v(q,w,s,-1D0)
		a2=1D0-a**2
		b2=1D0-b**2
		if (b2.gt.0D0) then
			ImChi0s=ImChi0s+dsqrt(b2)
		endif
		if (a2.gt.0D0) then
			ImChi0s=ImChi0s-dsqrt(a2)
		endif

		ImChi0s=ImChi0s/x(q,s)
		ImChi0s=ImChi0s*(-2D0*pi)

end function ImChi0s


real (kind=8) function x(q,s)
	implicit none
	real (kind=8)::q,s
	
	if (s.eq.1D0) then
		x=q/GlobkfU
	elseif (s.eq.(-1D0)) then
		x=q/GlobkfD
	else
		write(*,*) "s=",s
		call exit(-1)
	endif

end function x

real (kind=8) function v(q,w,s,b)
	implicit none
	real (kind=8)::w,s,q,b
	
	if (s.eq.1D0) then
		v=w/GlobvfU/q+b*q/2D0/GlobkfU
	elseif (s.eq.-1D0) then
		v=w/GlobvfD/q+b*q/2D0/GlobkfD
	else
		write(*,*) "s=",s
		call exit(-1)
	endif

end function v

	real (kind=8) function ChiCalc(q,w)
		implicit none
		real    (kind=8)::q,w,eta,ReChi,ImChi,k,dk,phi,dphi,Nm,Np
		integer (kind=4)::i,j
		integer (kind=4), parameter::Nphi=1000,Nk=1000

		!eta=0.01D0*kf**2/2D0
		dphi=2D0*pi/real(Nphi, kind=8)
		dk=GlobkfU/real(Nk, kind=8)
		phi=0D0
		k=0D0
		ReChi=0D0

		do i=1,Nphi
			!phi=real(i, kind=8)*dphi-dphi/2D0
			k=0D0
			do j=1,Nk
				!k=real(j, kind=8)*dk
				Nm=w-k*q*dcos(phi)-q**2/2D0
				Np=w+k*q*dcos(phi)+q**2/2D0
				ReChi=ReChi+Nm*k/(Nm**2+Globeta**2)&
					-Np*k/(Np**2+Globeta**2)
				k=k+dk
			enddo
			phi=phi+dphi
		enddo

		ReChi=ReChi*dk*dphi/(2D0*pi)**2


		ChiCalc=ReChi*2D0

		return
	end function ChiCalc


end module GV
