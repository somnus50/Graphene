program TwoDEGSTLSGsc
use Constants
use Globals
use Physics2DEG
use STLS
use AuxiliaryFunctions
use SelfConsistent
implicit none
real (kind=8)::rs,qs(2),mix,wp,wa,wb,w,dw,q,qp,ws(2),resa,resfn,resa2,resfn2
real (kind=8), allocatable::wline(:),res(:),plas(:,:)
integer (kind=4)::i,j,k,SCsteps,qstps,MPImaster,wsteps,Nproc,IntSteps,params(4),stat,mixsteps
character (len=32)::tempchar


	rs=2D0
	mix=0.5D0
	SCsteps=30
	qstps=301


	Nproc=1
	MPImaster=0
	IntSteps=1000
	!qsteps=200
	wsteps=200



	!!!! Initialize some situational constants
	! call ConstInit(rs,0D0,GlobEf,Globeta,Globfx,Globfc)
	call ConstInit(rs,0D0)
	write(*,*) "kf0",Globkf,Globkf0

	! write(*,*) Globfx, Globfc, Globfx+Globfc
	! qs=(/0D0,5D0*Globkf/)
	qs = (/0D0,1D0/)
	ws=(/0D0,5D0*GlobEf/)
	call STLSSCInit(SCsteps,qstps,qs)

	params=(/Nproc,IntSteps,qsteps,wsteps/)

	!!! Initialize some files for data storage




	call SCInit(qstps,qs,GRPA)

	! qgrid = xgrid


	! do i = 1, qstps
	! 	write(*,*) qgrid(i),(1D0-qgrid(i))/qgrid(i),Structbar(qgrid(i))
	! end do
	!
	! write(*,*) Struct(0D0)
	! write(*,*) Chibar(0.5D0,1D0)
	! call exit()

	allocate(res(SCsteps),plas(xsteps,3))

	call StructbarsplineUpdate()

	write(*,*) "#############################################"
	write(*,*) "##################  GpA   ###################"
	write(*,*) "#############################################"

	! call SCInit(qstps,qs,GRPA)
	do k = 1, qstps
		GSCold(k)=GpAbar(qgrid(k))
	end do

	call spline(qgrid,GSCold,qsteps,1D31,1D31,GSCspline)

	open(unit=29,file='Plas.GpA.dat')
	write(29,*) "#Plasmons GpA"
	write(29,*) "#q/kf,wp/Ef,GpA,PH+,PH-,Eps(q,wp)"

	! (1D0-qgrid(k))/qgrid(k)

	do k=1,qsteps
		! call RootFinder(epst,ws(1),ws(2),1D-15,resa,resfn)
		call wplast((1D0-qgrid(k))/qgrid(k),ws,1D-15,resa,resfn)
		write(29,*) (1D0-qgrid(k))/qgrid(k)/Globkf,resa/GlobEf,AnalyticPlasmon((1D0-qgrid(k))/qgrid(k))/GlobEf,&
		PH(1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,PH(-1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,resfn
	enddo

	close(unit=29)


	write(*,*) "#############################################"
	write(*,*) "##################  LDA   ###################"
	write(*,*) "#############################################"


	! call SCInit(qstps,qs,GLSDAbar)
	do k = 1, qstps
		GSCold(k)=GLSDAbar(qgrid(k))
	end do

	call spline(qgrid,GSCold,qsteps,1D31,1D31,GSCspline)


	open(unit=29,file='Plas.LDA.dat')
	write(29,*) "#Plasmons LDA"
	write(29,*) "#q/kf,wp/Ef,Analytic,PH+,PH-,Eps(q,wp)"

	! (1D0-qgrid(k))/qgrid(k)

	do k=1,qsteps
		! call RootFinder(epst,ws(1),ws(2),1D-15,resa,resfn)
		call wplast((1D0-qgrid(k))/qgrid(k),ws,1D-15,resa,resfn)
		write(29,*) (1D0-qgrid(k))/qgrid(k)/Globkf,resa/GlobEf,AnalyticPlasmon((1D0-qgrid(k))/qgrid(k))/GlobEf,&
		PH(1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,PH(-1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,resfn
	enddo

	close(unit=29)

	write(*,*) "#############################################"
	write(*,*) "#################  STLS0   ##################"
	write(*,*) "#############################################"

	! call SCInit(qstps,qs,GRPA)
	do k = 1, qstps
		GSCold(k)=0D0!GRPA(qgrid(k))
	end do

	call spline(qgrid,GSCold,qsteps,1D31,1D31,GSCspline)

	do k = 1, qstps

		write(*,*) "STLS0 qstep",k,qstps
		GSC(k)=Gpbar(qgrid(k))
		! write(*,*) "epst(q,w)",epst((1D0-qgrid(k))/qgrid(k),0.5D0*GlobEf)
	end do

	GSCold=GSC

	call spline(qgrid,GSCold,qsteps,1D31,1D31,GSCspline)


	open(unit=29,file='Plas.STLS0.dat')
	write(29,*) "#Plasmons STLS0"
	write(29,*) "#q/kf,wp/Ef,Analytic,PH+,PH-,Eps(q,wp)"

	! (1D0-qgrid(k))/qgrid(k)

	do k=1,qsteps
		! call RootFinder(epst,ws(1),ws(2),1D-15,resa,resfn)
		call wplast((1D0-qgrid(k))/qgrid(k),ws,1D-15,resa,resfn)
		write(29,*) (1D0-qgrid(k))/qgrid(k)/Globkf,resa/GlobEf,AnalyticPlasmon((1D0-qgrid(k))/qgrid(k))/GlobEf,&
		PH(1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,PH(-1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,resfn
	enddo

	close(unit=29)

	write(*,*) "#############################################"
	write(*,*) "##################  STLS  ###################"
	write(*,*) "#############################################"
	call SCInit(qstps,qs,GRPA)

	GSCold=1D0
	call spline(qgrid,GSCold,qsteps,1D31,1D31,GSCspline)

	Sbargrid=0D0
	call spline(qgrid,Sbargrid,qsteps,1D31,1D31,Sbarspline)


	! call StructbarsplineUpdate()

	call SCSolve(Gpbar,SCsteps,mix,1D-8,res,StructbarsplineUpdate,GSCold,GSCspline,stat)
	write(*,*) "SC stat:",stat


	open(unit=19, file='STLSbar.dat')
	write(19,*) "#q/kf,STLS0,STLS,PGG,Analytic,LSDA"

	do k=1,qsteps
		write(19,*) (1D0-qgrid(k))/qgrid(k)/Globkf, GSC(k), GSCold(k),&
		 GPGG((1D0-qgrid(k))/qgrid(k)),GpA((1D0-qgrid(k))/qgrid(k)),GLSDA((1D0-qgrid(k))/qgrid(k))
	enddo

	close(unit=19)

	open(unit=29,file='Plas.STLS.dat')
	write(29,*) "#Plasmons STLS"
	write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"

	! (1D0-qgrid(k))/qgrid(k)

	do k=1,qsteps
		! call RootFinder(epst,ws(1),ws(2),1D-15,resa,resfn)
		call wplast((1D0-qgrid(k))/qgrid(k),ws,1D-15,resa,resfn)
		write(29,*) (1D0-qgrid(k))/qgrid(k)/Globkf,resa/GlobEf,AnalyticPlasmon((1D0-qgrid(k))/qgrid(k))/GlobEf,&
		PH(1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,PH(-1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,resfn
	enddo

	close(unit=29)

	write(*,*) "#############################################"
	write(*,*) "##################  STLSA  ##################"
	write(*,*) "#############################################"

	! call SCInit(qstps,qs,GpAbar)
	do k = 1, qstps
		GSCold(k)=GpAbar(qgrid(k))
	end do

	call spline(qgrid,GSCold,qsteps,1D31,1D31,GSCspline)

	GSC=GSCold

	call StructbarsplineUpdate()

	call SCSolve(Gpbar,SCsteps,0.5D0,1D-8,res,StructbarsplineUpdate,GSCold,GSCspline,stat)
	write(*,*) "SC stat:",stat

	open(unit=19, file='STLSAbar.dat')
	write(19,*) "#q/kf,GpA,STLSA,GPGG,GpA,GLSDA"

	do k=1,qsteps
		write(19,*) (1D0-qgrid(k))/qgrid(k)/Globkf, GSC(k), GSCold(k),&
		 GPGG((1D0-qgrid(k))/qgrid(k)),GpA((1D0-qgrid(k))/qgrid(k)),GLSDA((1D0-qgrid(k))/qgrid(k))
	enddo

	close(unit=19)

	open(unit=29,file='Plas.STLSA.dat')
	write(29,*) "#Plasmons STLS"
	write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"

	! (1D0-qgrid(k))/qgrid(k)



	do k=1,qsteps
		! call RootFinder(epst,ws(1),ws(2),1D-15,resa,resfn)
		call wplast((1D0-qgrid(k))/qgrid(k),ws,1D-15,resa,resfn)
		write(29,*) (1D0-qgrid(k))/qgrid(k)/Globkf,resa/GlobEf,AnalyticPlasmon((1D0-qgrid(k))/qgrid(k))/GlobEf,&
		PH(1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,PH(-1D0,(1D0-qgrid(k))/qgrid(k))/GlobEf,resfn
	enddo

	!open(unit=29, file='Plasmons.2.dat')
	!write(29,*) "#Plasmons STLS10"
	!write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


	!do i=1,qsteps
	!	write(29,*) qgrid(i)/4D0/Globkf,plas(i,:)/GlobEf,AnalyticPlasmon(qgrid(i)/4D0)/GlobEf
	!enddo

	!close(unit=29)



end program TwoDEGSTLSGsc
