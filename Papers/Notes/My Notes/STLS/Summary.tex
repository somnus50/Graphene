\documentclass [12pt] {article}
\usepackage{graphicx}
\topmargin -2cm
\oddsidemargin 0.5cm
\textheight 23.5cm
\textwidth 16.05cm
\begin{document}
\baselineskip 18pt
\parskip0pt
\parindent8mm
\noindent

\newcommand{\dy}{\displaystyle}

\newcommand{\kf}{{k_F}}
\newcommand{\bfr}{{\bf r}}
\newcommand{\bfq}{{\bf q}}
\newcommand{\bfk}{{\bf k}}
\newcommand{\bfx}{{\bf x}}

\newcommand{\ua}{\uparrow}
\newcommand{\da}{\downarrow}

\newcommand{\nuu}{n_{\uparrow\uparrow}}
\newcommand{\nud}{n_{\uparrow\downarrow}}
\newcommand{\ndu}{n_{\downarrow\uparrow}}
\newcommand{\ndd}{n_{\downarrow\downarrow}}
\newcommand{\uu}{\uparrow\uparrow}
\newcommand{\ud}{\uparrow\downarrow}
\newcommand{\du}{\downarrow\uparrow}
\newcommand{\dd}{\downarrow\downarrow}

\begin{center}
\bf \LARGE General spin-dependent STLS
\end{center}



\section{Response function for a homogeneous spin-polarized system}
Consider a homogeneous system which is spin polarized along $z$.
In matrix notation, the response equation is
\begin{eqnarray*}
\underline{n}^{(1)}
&=&
\underline{\chi} \underline{v}^{(1)} \\
&=&
\underline{\chi}^{(0)}[ \underline{v}^{(1)} + \underline{f}^{\rm Hxc} \underline{n}^{(1)}]
\end{eqnarray*}
so
\begin{displaymath}
\underline{n}^{(1)} - \underline{\chi}^{(0)}\underline{f}^{\rm Hxc} \underline{n}^{(1)}
= \underline{\chi}^{(0)} \underline{v}^{(1)}
\end{displaymath}
which means that the full response function is
\begin{equation} \label{full}
\underline{\chi} = [\underline{1}  - \underline{\chi}^{(0)}\underline{f}^{\rm Hxc}]^{-1}  \underline{\chi}^{(0)}
\end{equation}
In this case, the noninteracting response function is diagonal:
\begin{displaymath}
\underline{\chi}^{(0)} = \left( \begin{array}{cccc}
\chi^{(0)}_{\ua\ua,\ua\ua} & & & \\
& \chi^{(0)}_{\ua\da,\ua\da} & & \\
& & \chi^{(0)}_{\da\ua,\da\ua} & \\
& & & \chi^{(0)}_{\da\da,\da\da} \end{array}\right)
\end{displaymath}
and the Hxc kernel is
\begin{displaymath}
\underline{f}^{\rm Hxc} = \left( \begin{array}{cccc}
w + f^{\rm xc}_{\ua\ua,\ua\ua} & & & w + f^{\rm xc}_{\ua\ua,\da\da} \\
& f^{\rm xc}_{\ua\da,\ua\da} & & \\
& & f^{\rm xc}_{\da\ua,\da\ua} & \\
w + f^{\rm xc}_{\da\da,\ua\ua}& & & w + f^{\rm xc}_{\da\da,\da\da} \end{array}\right)
\end{displaymath}
The full response function (\ref{full}) follows as
\begin{equation}
\underline\chi =
\left( \begin{array}{cccc}
\frac{1-\chi^{(0)}_{\da\da,\da\da}f^{\rm Hxc}_{\da\da,\da\da}}{M} \chi^{(0)}_{\ua\ua,\ua\ua} & & & \frac{\chi^{(0)}_{\ua\ua,\ua\ua}f^{\rm Hxc}_{\ua\ua,\da\da}}{M}\chi^{(0)}_{\da\da,\da\da} \\
& \frac{\chi^{(0)}_{\ua\da,\ua\da}}{1-\chi^{(0)}_{\ua\da,\ua\da}f^{\rm xc}_{\ua\da,\ua\da}} & & \\
& & \frac{\chi^{(0)}_{\da\ua,\da\ua}}{1-\chi^{(0)}_{\da\ua,\da\ua}f^{\rm xc}_{\da\ua,\da\ua}} & \\
\frac{\chi^{(0)}_{\da\da,\da\da}f^{\rm Hxc}_{\da\da,\ua\ua}}{M}\chi^{(0)}_{\ua\ua,\ua\ua} & & & \frac{1-\chi^{(0)}_{\ua\ua,\ua\ua}f^{\rm Hxc}_{\ua\ua,\ua\ua}}{M}\chi^{(0)}_{\da\da,\da\da} \end{array}\right)
\end{equation}
where
\begin{displaymath}
M = (1-\chi^{(0)}_{\ua\ua,\ua\ua}f^{\rm Hxc}_{\ua\ua,\ua\ua})(1-\chi^{(0)}_{\da\da,\da\da}f^{\rm Hxc}_{\da\da,\da\da})
 - \chi^{(0)}_{\ua\ua,\ua\ua}f^{\rm Hxc}_{\ua\ua,\da\da}\chi^{(0)}_{\da\da,\da\da}f^{\rm Hxc}_{\da\da,\ua\ua}
\end{displaymath}
Clearly, the response function is block diagonal in the longitudinal and transverse channels.

\subsection{Lindhard function}
The noninteracting spin-density-matrix response function for a magnetic field along $z$ is
\begin{displaymath}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(r,r',\omega) =
\sum_{j,k=1}^\infty (f_{k\sigma'} - f_{j\sigma})
\frac{\varphi_{j\sigma}(r) \varphi_{k\sigma'}^*(r) \varphi_{j\sigma}^*(r')\varphi_{k\sigma'}(r')}
{\omega - \varepsilon_{j\sigma} + \varepsilon_{k\sigma'} + i\eta}\:.
\end{displaymath}
Now let's consider a homogeneous system where $\varepsilon_{j\sigma} = \varepsilon_\sigma + k^2/2$, so
\begin{displaymath}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(r,r',\omega) =
\sum_{k,k'} \Big[\theta(k_{F\sigma'}-k) - \theta(k_{F\sigma}-k') \Big]
\frac{e^{-i(k-k')(r-r')}}
{\omega + \varepsilon_{\sigma'} - \varepsilon_\sigma - k'^2/2 + k^2/2 + i\eta}
\end{displaymath}
or, defining $\omega_{\sigma\sigma'} = \omega + \varepsilon_{\sigma'} - \varepsilon_\sigma$,
\begin{displaymath}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(r,r',\omega) =
\sum_{k,k+q} \Big[\theta(k_{F\sigma'}-k) - \theta(k_{F\sigma}-|k+q|) \Big]
\frac{e^{iq(r-r')}}{\omega_{\sigma\sigma'} + k^2/2 - |k+q|^2/2 + i\eta}\:,
\end{displaymath}
so the spin-resolved Lindhard function is
\begin{displaymath}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega) =
\sum_{k} \Big[\theta(k_{F\sigma'}-k) - \theta(k_{F\sigma}-|k+q|) \Big]
\frac{1}{\omega_{\sigma\sigma'} + k^2/2 - |k+q|^2/2 + i\eta}\:.
\end{displaymath}
We now go through the derivation, following GV chapter 4.4. First, we make a change of variables in the second term, so
\begin{eqnarray*}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
\sum_{k} \frac{\theta(k_{F\sigma'}-k)}{\omega_{\sigma\sigma'} + k^2/2 - |k+q|^2/2 + i\eta}
+
\sum_{k} \frac{\theta(k_{F\sigma}-k)}{-\omega_{\sigma\sigma'} + k^2/2 - |k+q|^2/2 - i\eta}\\
&=&
\sum_{k} \frac{\theta(k_{F\sigma'}-k)}{\omega_{\sigma\sigma'} -kq\cos\theta - q^2/2 + i\eta}
+
\sum_{k} \frac{\theta(k_{F\sigma}-k)}{-\omega_{\sigma\sigma'} -kq\cos\theta - q^2/2 - i\eta}\\
&=&
\frac{1}{k_{F\sigma'}q}\sum_{k} \frac{\theta(k_{F\sigma'}-k)}{\dy \frac{\omega_{\sigma\sigma'}}{q k_{F\sigma'}} - \frac{q}{2k_{F\sigma'}}
-\frac{k}{k_{F\sigma'}}\cos\theta  + i\eta}\\
&+&
\frac{1}{k_{F\sigma}q}\sum_{k} \frac{\theta(k_{F\sigma}-k)}{\dy -\frac{\omega_{\sigma\sigma'}}{q k_{F\sigma}} - \frac{q}{2k_{F\sigma}}
-\frac{k}{k_{F\sigma}}\cos\theta - i\eta}
\end{eqnarray*}
The sum is now converted into an integral:
\begin{eqnarray*}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
\frac{ k_{F\sigma'}^{d-1}}{(2\pi)^d q}\int_0^1 dx\: x^{d-1} \int  \frac{d\Omega_d}{\dy \frac{\omega_{\sigma\sigma'}}{q k_{F\sigma'}} - \frac{q}{2k_{F\sigma'}}
- x\cos\theta  + i\eta}\\
&+&
\frac{k_{F\sigma}^{d-1}}{(2\pi)^d q}\int_0^1 dx\: x^{d-1} \int  \frac{d\Omega_d}{\dy -\frac{\omega_{\sigma\sigma'}}{q k_{F\sigma}} - \frac{q}{2k_{F\sigma}}
-x\cos\theta - i\eta}
\end{eqnarray*}
Defining
\begin{displaymath}
\Psi_d(z)=\int_0^1 dx\: x^{d-1} \int  \frac{d\Omega_d}{\Omega_d} \frac{1}{z- x\cos\theta}
\end{displaymath}
we obtain
\begin{eqnarray*}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
\frac{ k_{F\sigma'}^{d-1}\Omega_d}{(2\pi)^d q} \Psi_d \left(\frac{\omega_{\sigma\sigma'}}{q k_{F\sigma'}} - \frac{q}{2k_{F\sigma'}} + i\eta \right)
+
\frac{k_{F\sigma}^{d-1}\Omega_d}{(2\pi)^d q}  \Psi_d  \left(-\frac{\omega_{\sigma\sigma'}}{q k_{F\sigma}} - \frac{q}{2k_{F\sigma}} - i\eta\right)\\[3mm]
&=&
\frac{ k_{F\sigma'}^{d-1}\Omega_d}{(2\pi)^d q} \Psi_d \left(\frac{\omega_{\sigma\sigma'}+i\eta}{q k_{F\sigma'}} - \frac{q}{2k_{F\sigma'}} \right)
-
\frac{k_{F\sigma}^{d-1}\Omega_d}{(2\pi)^d q}  \Psi_d  \left(\frac{\omega_{\sigma\sigma'}+i\eta}{q k_{F\sigma}} + \frac{q}{2k_{F\sigma}} \right)
\end{eqnarray*}
where we used the antisymmetry of $\Psi_d(z)$. Using $\Omega_3=4\pi$, $\Omega_2=2\pi$ and $\Omega_1=2$, we get
\begin{equation} \label{23}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega) =
N_{\sigma'}(0)\frac{k_{F\sigma'}}{q} \Psi_d \left(\frac{\omega_{\sigma\sigma'}+i\eta}{q k_{F\sigma'}} - \frac{q}{2k_{F\sigma'}} \right)
-
N_{\sigma}(0)\frac{k_{F\sigma}}{q}  \Psi_d  \left(\frac{\omega_{\sigma\sigma'}+i\eta}{q k_{F\sigma}} + \frac{q}{2k_{F\sigma}} \right)
\end{equation}
where
\begin{displaymath}
N_\sigma^{\rm 3D}(0)=\frac{k_{F\sigma}}{2\pi^2} \qquad \qquad
N_\sigma^{\rm 2D}(0)=\frac{1}{2\pi} \qquad \qquad
N_\sigma^{\rm 1D}(0)=\frac{1}{\pi k_{F\sigma}}
\end{displaymath}
We can now use Table 4.1, page 162 of GV. Introducing
\begin{displaymath}
\bar q_\sigma = \frac{q}{k_{F\sigma}} \qquad \qquad
\nu_{\pm\sigma} = \frac{\omega_{\sigma\sigma'}}{q k_{F\sigma}} \pm \frac{q}{2 k_{F\sigma}}
\end{displaymath}
we can write Eq. (\ref{23}) as
\begin{equation} \label{23}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega) =
\frac{N_{\sigma'}(0)}{\bar q_{\sigma'}} \Psi_d \left(\nu_{-\sigma'} +i\eta \right)
-
\frac{N_{\sigma}(0)}{\bar q_\sigma}  \Psi_d  \left(\nu_{+\sigma} + i\eta \right)
\end{equation}
We now need to consider the 1D, 2D and 3D cases separately. All branch cuts are along the negative real axis.

\subsubsection{1D case}
We have
\begin{displaymath}
\Psi_1(z) = \frac{1}{2} \ln \frac{z+1}{z-1} \:.
\end{displaymath}
The natural logarithm is defined as
\begin{displaymath}
\ln \tilde z = \ln |\tilde z| + i \: \varphi_0
\end{displaymath}
We have
\begin{eqnarray}
\tilde z
&=&
\frac{\nu + 1 + i\eta}{\nu - 1 + i\eta}
=  \frac{(\nu + 1 + i\eta)(\nu - 1 - i\eta)}{(\nu - 1 + i\eta)(\nu - 1 - i\eta)} \nonumber \\
&=&
\frac{\nu^2-1 - 2i\eta + \eta^2}{(\nu - 1)^2 + \eta^2} \nonumber\\
&=&
\frac{\nu^2-1 - 2i\eta}{(\nu - 1)^2}  \label{25}
\end{eqnarray}
Letting $\eta\to 0^+$ (and $z$ approaching the real axis from above) we have
\begin{displaymath}
|\tilde z| = \left|\frac{\nu+1 }{\nu - 1}\right| \qquad \qquad
\tan \varphi_0 = \frac{\Im \tilde z}{\Re \tilde z} = \frac{2\eta}{1-\nu^2} \to 0
\end{displaymath}
If $\nu^2-1>0$ we can set $\phi_0=0$. But if $\nu^2-1<0$ we set $\phi_0=-\pi$ since then $\tilde z$ lies just
below the negative real axis, as can be seen from (25).

This gives the following result for the 1D response function:
\begin{eqnarray}
\Re \chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
\frac{N_{\sigma'}(0)}{2\bar q_{\sigma'}} \ln \left| \frac{\nu_{-\sigma'} + 1}{\nu_{-\sigma'}-1}\right|
-
\frac{N_{\sigma}(0)}{2\bar q_\sigma} \ln \left| \frac{\nu_{+\sigma} + 1}{\nu_{+\sigma}-1}\right|
\\[3mm]
\Im \chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
-\frac{N_{\sigma'}(0)}{2\bar q_{\sigma'}} \pi \theta(1-\nu_{-\sigma'}^2)
+
\frac{N_{\sigma}(0)}{2\bar q_\sigma} \pi\theta(1-\nu_{+\sigma}^2)
\end{eqnarray}

\subsubsection{3D case}
We have
\begin{displaymath}
\Psi_3(z) = \frac{z}{2} + \frac{1-z^2}{4} \ln \frac{z+1}{z-1}
\end{displaymath}
so we can use the same arguments as in the 1D case. This gives
\begin{eqnarray}
\Re \chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
\frac{N_{\sigma'}(0)}{\bar q_{\sigma'}}
\left\{\frac{\nu_{-\sigma'}}{2} +  \frac{1-\nu_{-\sigma'}^2}{4}\ln \left| \frac{\nu_{-\sigma'} + 1}{\nu_{-\sigma'}-1}\right|\right\}
\nonumber\\
&-&
\frac{N_{\sigma}(0)}{\bar q_\sigma}
\left\{ \frac{\nu_{+\sigma}}{2} +  \frac{1-\nu_{+\sigma}^2}{4}\ln \left| \frac{\nu_{+\sigma} + 1}{\nu_{+\sigma}-1}\right|\right\}
\\[3mm]
\Im \chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
-\frac{\pi N_{\sigma'}(0)}{4\bar q_{\sigma'}} (1-\nu_{-\sigma'}^2) \theta(1-\nu_{-\sigma'}^2)
+
\frac{\pi N_{\sigma}(0)}{4 \bar q_\sigma} (1-\nu_{+\sigma}^2) \theta(1-\nu_{+\sigma}^2)
\end{eqnarray}

\subsubsection{2D case}
We have
\begin{displaymath}
\Psi_2(z) = z - \mbox{sign}(\Re z) \sqrt{z^2-1}
\end{displaymath}
The complex square root is defined as
\begin{displaymath}
\tilde z = |\tilde z| e^{i\varphi_0} \qquad \qquad \sqrt{\tilde z} = \sqrt{|\tilde z|} e^{i \varphi_0/2}
\end{displaymath}
where the branch cut is along the negative real axis. Here we have
\begin{displaymath}
\tilde z = (\nu + i\eta)^2-1 = \nu^2 + 2i\nu \eta - 1
\end{displaymath}
so the imaginary part of of the argument of the square root is just above the real axis if $\nu>0$, and $\varphi_0 = \pi$,
and just below the real axis if $\nu<0$, and $\varphi_0=-\pi$.  This cancels the sign function in the imaginary part.
We obtain
\begin{eqnarray}
\Re \chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
\frac{N_{\sigma'}(0)}{\bar q_{\sigma'}}
\left\{ \nu_{-\sigma'} - \mbox{sign}(\nu_{-\sigma'})\theta(\nu_{-\sigma'}^2-1) \sqrt{\nu_{-\sigma'}^2-1}  \right\}
\nonumber\\
&-&
\frac{N_{\sigma}(0)}{\bar q_\sigma}
\left\{\nu_{+\sigma} - \mbox{sign}(\nu_{+\sigma})\theta(\nu_{+\sigma}^2-1) \sqrt{\nu_{+\sigma}^2-1} \right\}
\\[3mm]
\Im \chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(q,\omega)
&=&
-\frac{N_{\sigma'}(0)}{\bar q_{\sigma'}}  \theta(1-\nu_{-\sigma'}^2)\sqrt{1-\nu_{-\sigma'}^2}
+
\frac{N_{\sigma}(0)}{\bar q_\sigma}  \theta(1-\nu_{+\sigma}^2)\sqrt{1-\nu_{+\sigma}^2}
\end{eqnarray}

\newpage
\subsubsection{Zero wavevector case}
We have
\begin{displaymath}
\chi^{(0)}_{\sigma\sigma',\sigma\sigma'}(0,\omega)
=
\frac{1}{\omega_{\sigma\sigma'}}\sum_{k} \theta(k_{F\sigma'}-k)
-
\frac{1}{\omega_{\sigma\sigma'}}\sum_{k} \theta(k_{F\sigma}-k)
\end{displaymath}
We obtain immediately
\begin{displaymath}
\sum_{k} \theta(k_{F\sigma}-k) = \frac{\Omega_d}{(2\pi)^d}\int_0^{k_{F\sigma}} k^{d-1}dk
= \frac{\Omega_d}{(2\pi)^d} \frac{k_{F\sigma}^d}{d}
= \frac{\Omega_d}{(2\pi)^d}\frac{k_{F}^d}{d}(1\pm \zeta)
\end{displaymath}
With this, we get
\begin{eqnarray*}
\chi^{(0)}_{\ua\da,\ua\da}(0,\omega)  &=&
-\frac{2\zeta}{(\omega+\varepsilon_\da - \varepsilon_\ua)}\frac{\Omega_d}{(2\pi)^d}\frac{k_{F}^d}{d}\\
\chi^{(0)}_{\da\ua,\da\ua}(0,\omega)  &=&
\frac{2\zeta}{(\omega + \varepsilon_\ua - \varepsilon_\da)}\frac{\Omega_d}{(2\pi)^d}\frac{k_{F}^d}{d}
\end{eqnarray*}
We have in all dimensions
\begin{displaymath}
\frac{\Omega_d}{(2\pi)^d}\frac{k_{F}^d}{d} = \frac{n}{2}
\end{displaymath}
so
\begin{eqnarray*}
\chi^{(0)}_{\ua\da,\ua\da}(0,\omega)  &=&
-\frac{n\zeta}{\omega+\varepsilon_\da - \varepsilon_\ua}\\
\chi^{(0)}_{\da\ua,\da\ua}(0,\omega)  &=&
\frac{n\zeta}{\omega + \varepsilon_\ua - \varepsilon_\da}
\end{eqnarray*}


\section{PGG kernels}
The PGG exchange kernels are given by
\begin{equation}
f^{\rm x}_{\uu\uu}(\bfr,\bfr') = -\frac{\gamma_{\uu}(\bfr,\bfr')\gamma_{\uu}(\bfr',\bfr)}{\nuu(\bfr)\nuu(\bfr')|\bfr - \bfr'|}
\qquad
f^{\rm x}_{\dd\dd}(\bfr,\bfr') = -\frac{\gamma_{\dd}(\bfr,\bfr')\gamma_{\dd}(\bfr',\bfr)}{\ndd(\bfr)\ndd(\bfr')|\bfr - \bfr'|}
\end{equation}
\begin{equation}
f^{\rm x}_{\ud\ud}(\bfr,\bfr') = -4\frac{\gamma_{\uu}(\bfr,\bfr')\gamma_{\dd}(\bfr',\bfr)}{n(\bfr)n(\bfr')|\bfr - \bfr'|}
\qquad
f^{\rm x}_{\du\du}(\bfr,\bfr') = -4\frac{\gamma_{\dd}(\bfr,\bfr')\gamma_{\uu}(\bfr',\bfr)}{n(\bfr)n(\bfr')|\bfr - \bfr'|}
\end{equation}
where $\gamma_{\sigma\sigma'}(\bfr,\bfr')$ is the spin-resolved Kohn-Sham density matrix, defined as
\begin{displaymath}
\gamma_{\sigma\sigma}(\bfr,\bfr')=\sum_{i=1}^{N_\sigma} \varphi_{i\sigma}(\bfr)\varphi_{i\sigma}^*(\bfr')
\end{displaymath}
and the total density is
\begin{displaymath}
n(\bfr) = n_{\uu}(\bfr) +n_{\dd}(\bfr)
\end{displaymath} 

\newpage

\section{STLS}
The spin-resolved static structure factor is given by
\begin{equation}\label{S}
S_{\sigma\sigma',\tau\tau'}(\bfr,\bfr') = -\frac{1}{\pi}\int_0^\infty \Im \chi_{\sigma\sigma',\tau \tau'}(\bfr,\bfr',\omega) d\omega
\end{equation}

In the collinear case which we consider here, the xc kernels are given by
\begin{displaymath}
\left( \begin{array}{c}
f_{\uu\uu}  \\ f_{\ud\ud} \\ f_{\du\du}  \\ f_{\dd\dd}
\end{array}\right)
=
\frac{1}{4}
\left( \begin{array}{c}
\frac{1}{\nuu(\bfr) \nuu(\bfr')}\: d_{\uu\uu}
\\  \frac{4}{n(\bfr)n(\bfr')}\: d_{\ud\ud} \\  \frac{4}{n(\bfr)n(\bfr')} \:  d_{\du\du}
\\ \frac{1}{\ndd(\bfr) \ndd(\bfr')}\: d_{\dd\dd}
\end{array}\right)
\end{displaymath}
where
\begin{equation}
d_{\sigma\sigma',\alpha\alpha'}(\bfr,\bfr')
=
\frac{4}{|\bfr - \bfr'|} \left[
S_{\sigma\sigma',\alpha\alpha'}(\bfr,\bfr') - \delta_{\sigma\alpha}\delta(\bfr-\bfr') n_{\alpha'\sigma'}(\bfr)\right]
\end{equation}



\end{document} 