program main
    use AuxiliaryFunctions
    implicit none
    integer(4), parameter::m=3,n=2
    real(8) :: A(m,n), B(m,n)
    integer(4) :: i,ind(m)

    A( 1, : ) = [ 3D0, -1D0]
    A( 2, : ) = [ 2D0, 1D0]
    A( 3, : ) = [ 0D0, -1D0]

    !call SortRows(A,m,n,B)

    !do i=1,m
    !write(*,*) A(i,:)
    !enddo
    !write(*,*) "-------"
    !do i=1,m
    !write(*,*) B(i,:)
    !enddo

    call indexArrayReal(m,A,ind)

    write(*,*) "Index: ",ind
    write(*,*) "A(Index,:): ",A(ind,:)


end
