program TwoDEGSTLSGsc
use Constants
use Globals
use PhysicsPol2DEG
use STLS
use AuxiliaryFunctions
use SelfConsistent
implicit none
real (kind=8)::rs,qs(2),mix,wp,wa,wb,w,dw,q,qp,ws(2),resa,resfn,resa2,resfn2,temp1,temp2,&
				res1(2),res2(2),z,s0,s1,s2,dgdq(3),h,qq
real (kind=8), allocatable::wline(:),res(:),plas(:,:)
integer (kind=4)::i,j,k,SCsteps,qstps,MPImaster,wsteps,Nproc,IntSteps,params(4),stat,mixsteps
character (len=32)::tempchar
logical::exist

	rs=dble(myrs)
	mix=0.95D0
	SCsteps=30
	qstps=350
	qsteps=qstps
	z=dble(myz)

	Nproc=1
	MPImaster=0
	IntSteps=1000
	!qsteps=200
	wsteps=200


	!!!! Initialize some situational constants
	call ConstInit(rs,z,GlobEf,Globeta)
	qs=(/0D0,1D0/)
	ws=(/0D0,10D0*GlobEf/)
	call SCInit(qstps,qs,fn0) !GPGGDU
	allocate(res(SCsteps),plas(xsteps,3),wline(qsteps),Ssbarspline(qsteps),Ssbargrid(qsteps),fxcgrid(qsteps),fxcspline(qsteps))

!	allocate(SDUbargrid(qstps),SDUbarspline(qstps))
	call STLSSCInit2(SCsteps,qstps,qs,SUDbarsplineUpdate)

	write(*,*) "qgrid",qgrid

	params=(/Nproc,IntSteps,qsteps,wsteps/)

	!!! Initialize some files for data storage


	call fDUbarUpdate()




	xgrid=qgrid

	GSC=GSCold


	! GSCold=0D0
	! Looking at S
	! do i = 2, qsteps
	! 	write(*,*) "qstep",i,qsteps
	! 	qp=(1D0-qgrid(i))/qgrid(i)
	! 	write(*,*) qp
	! 	write(*,*) "s0"
	! 	s0=S0ssp(-1,1,qp)
	! 	write(*,*) s0
	! 	write(*,*) "s1"
	! 	s1=Structssp2bar(-1,1,qgrid(i))
	! 	write(*,*) s1
	! 	write(*,*) "s2"
	! 	s2=Structssp(-1,1,qp)
	! 	write(*,*) s2
	! 	write(89,*) qgrid(i), qp, s0, s1, s2, GSCold(i)
	! end do
	! call exit()
	! Looking at S



	!call SDUsplineUpdate()
	write(*,*) "SC 0"
	call SCSolve(GUDforcebar,1,0D0,1D-20,res,SUDbarsplineUpdate,GSCold,GSCspline,stat)
	write(*,*) "SC stat:",stat

	GSC=GSCold


	write(*,*) "SC 1"

	call SCSolve(GUDforcebar,SCsteps,mix,1D-6,res,SUDbarsplineUpdate,GSCold,GSCspline,stat)
	write(*,*) "SC stat:",stat

	call fDUbarUpdate()

!	do i=1,qsteps
!		call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
!		call RootFinder(eps2,ws(1),ws(2),qgrid(i),1D-6,resa2,resfn2)
!		plas(i,1)=resa
!		plas(i,2)=resa2
!	enddo




	!call SCInit(qstps,qs,fn0)
	!call SCSolve(fxcUD,SCsteps,mix,1D-4,res,GSCold,GSCspline,stat)
	!write(*,*) "SC stat:",stat

	!do i=1,qsteps
	!	call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
	!	plas(i,3)=resa
	!enddo

!	open(unit=29,file='Plas0.F.dat')
!	write(29,*) "#Plasmons STLS0"
!	write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


!	do i=1,qsteps
!		call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
!		write(29,*) qgrid(i)/Globkf,resa/GlobEf,LowQPlasmon(qgrid(i))/GlobEf,resfn
!	enddo

	!write(*,*) "n,nu,nd"
	!write(*,*) Globn,Globnu,Globnd

!	close(unit=29)
	open(unit=19, file='myfile')
	write(19,*) "# 2DEG fSTLSUD rs,z:",rs,z
	write(19,*) "# q/kf, PGGUD, STLS0, STLS-30, S, S0, GLSDA, STLSfxc"

	do k=1,qsteps
		!write(*,*) k
		! write(19,*) qgrid(k)/Globkf0, -PGGDU(qgrid(k))*qgrid(k)/2d0/pi, GSC(k), GSCold(k)&
		! 						,SDUsplint(qgrid(k)), S0ssp(-1,1,qgrid(k))-Globnu,GLSDA(qgrid(k))
		write(19,*) (1D0-qgrid(k))/qgrid(k)/Globkf0, -PGGDU((1D0-qgrid(k))/qgrid(k))*(1D0-qgrid(k))/qgrid(k)/2d0/pi,&
		 GSC(k), GSCold(k),SUDbarsplint(qgrid(k)), S0ssp(1,-1,(1D0-qgrid(k))/qgrid(k))-Globnd,GLSDA((1D0-qgrid(k))/qgrid(k)),&
		 fxcgrid(k)
	enddo
	close(unit=19)



	inquire(file="fxc.fSTLS.rs.z.txt", exist=exist)
  if (exist) then
    open(12, file="fxc.fSTLS.rs.z.txt", status="old", position="append", action="write")
  else
    open(12, file="fxc.fSTLS.rs.z.txt", status="new", action="write")
		write(12,*) "# 2DEG fSTLSUD[rs,z](q=0)"
		write(12,*) "# rs,z,fSTLS(0),dfdq(0),d2fdq2(0),d3fdq3(0),S1(rs,z),S2(rs,z)"
  end if
	call fDUqDerivs15(0D0,qgrid(3),dgdq)
  write(12, *) rs,z,fxcgrid(qsteps),dgdq,-(1D0/Globz+2D0*pi/fxcgrid(qsteps)/Globz+Globn*Globz*dgdq(2)/2D0),&
							1D0/Globz+2D0*pi/(Globz*fxcgrid(qsteps))+&
							pi**2*Globn*Globz*((fxcgrid(qsteps)+pi)*dgdq(2)-2D0*dgdq(1)**2)/(2D0*(fxcgrid(qsteps)+pi)**3)
  close(12)





end program TwoDEGSTLSGsc
