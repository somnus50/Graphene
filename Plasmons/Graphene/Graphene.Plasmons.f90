program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene
use AuxiliaryFunctions
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,PlasmonArray(20,3),wstart,qhat(2),w0,n
real (kind=8), allocatable::LindLine(:),EpsLine(:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	n=5D14/cmSq2a0sq
	call ConstInit(n,0D0,GlobEf,Globeta,Globfx,Globfc)
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	open(unit=19, file='Lind.Diam.q3.3.dat', status='old', action='read')
	open(unit=9, file='Graphene.Plasmons.q3.3.dat')
	write(9,*) "# q/kf,wp/ef,low q limit"
	read(19,*)
	read(19,*)
	read(19,*)
	read(19,'(4I10)') NProc, IntSteps, qsteps, wsteps
	write(*,*) NProc, IntSteps, qsteps, wsteps
	read(19,*)
	read(19,*)

	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	write(*,*) fmt2
	allocate(LindLine(wsteps),EpsLine(wsteps))
	w0=4D0*GlobEf/2D0/kappa*e**2
	w0=dsqrt(w0)
	qhat=(/0D0,-1D0/)

	write(*,*) "kf",Globkf
	write(*,*) "Ef",GlobEf

	do i=1,qsteps
		write(*,*) "qstep:",i
		q=0D0
		read(19,fmt1) q, wstart, dw
		q=q*Globkf
		write(*,*) "q",q
		read(19,fmt2) LindLine
		call ReNEpsLine(q*qhat,wsteps,-LindLine/pi/pi,EpsLine)
		call FindBracketList2(EpsLine,wsteps,q,wstart,dw,PlasmonArray)
		do j=1,20
			if ((PlasmonArray(j,2)).gt.(0D0)) then
				write(*,*) PlasmonArray(j,1)/Globkf,&
				(PlasmonArray(j,2)+PlasmonArray(j,3))/(2D0*GlobEf),&       
				w0*dsqrt(q)/GlobEf

				write(9,fmt1) PlasmonArray(j,1)/Globkf,&
						(PlasmonArray(j,2)+PlasmonArray(j,3))/(2D0*GlobEf),&
						w0*dsqrt(q)/GlobEf
			endif
		enddo
	enddo

	!!! Close files
	close(unit=19)
	close(unit=9)

end program GraphenePlasmons
