program fxcTEST
use Constants
use Globals
use fxc2DEG
use Response
implicit none
real (kind=8)::r,dr,z,dz
integer (kind=4)::rsteps,zsteps,i,j



	rsteps=1
	zsteps=100
	r=1.25D0
	dr=2D0/real(rsteps, kind=8)
	z=-1D0
	dz=2D0/real(zsteps, kind=8)

	do i=1,rsteps
		do j=1,zsteps
			call KernelConstr(r,z,GlobKernVec)
			write(*,*) r,z,GlobKernVec
			z=z+dz

		enddo
		r=r+dr
	enddo



















end program fxcTEST
