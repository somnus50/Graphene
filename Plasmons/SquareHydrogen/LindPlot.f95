program LindPlot
	use constants	
	use NSquareHLindFunctions
	use HelperFunctions
	implicit none
	real    (kind=8):: q(2),G(2),w
	integer (kind=4):: i,j,steps,stat

	q = 1D0*(/1D0,0D0/)!pi/(2D0*alat)/)
	G = (/0D0,0D0/)
	steps=150
	open(unit=9, file='LindPlot.NC.11.dat')
	write(9,*) "# w/Ef"!                      ReNLind                     ImNLind"

	w=0D0
	do i=1,steps
		write(*,*) i
		w = 3D0*real(i-1)*Ef/real(steps, kind=8)
		
		write(9,*) w/Ef,ReNLind3(q,w)!,ReNLindIntPol(q,0D0,w*(/dcos(0D0),dsin(0D0)/))!

	enddo

	close(unit=9)

end program LindPlot
