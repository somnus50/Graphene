



	


	real (kind=8) function PolIntegrate(steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine
		real (kind=8), external:: fn
		
		! NCorder must match what was chosen in ReNLind2
		NCorder=11

		bk=0D0
		PolIntegrate=0D0
		dphi=2D0*pi/real(NCorder*steps, kind=8)
		phi=0D0
		k=0D0

		do phistep=1,NCorder*steps+1
			philine=0D0
			bk=FermiVec((/dcos(phi),dsin(phi)/),1D-6)
			dk=bk/real(NCorder*steps, kind=8)
			k=dk


	!!!AdaptQuad (this one sucks)
	!		dk=(bk-1D-4)/5D0
	!		k=dk
	!		do kstep=1,5
	!			philine=philine+AdaptQuad(fn,k-dk,k,phi,q,w,5D-6,1)
	!			k=k+dk
	!		enddo
	!		PolIntegrate=PolIntegrate+philine
	!!!AdaptQuad

	!!!NewtonCotes
			do kstep=1,NCorder*steps+1
				!philine=philine+dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  									!NC1
				!philine=philine+IntCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC4
				!philine=philine+BooleCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC5
				!philine=philine+IntCoeff10(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC10
				philine=philine+IntCoeff11(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC11
				k=k+dk
			enddo
  
			!PolIntegrate=PolIntegrate+philine  									!1
			!PolIntegrate=PolIntegrate+IntCoeff(phistep,NCorder*steps+1)*philine  	!4
			!PolIntegrate=PolIntegrate+BooleCoeff(phistep,NCorder*steps+1)*philine  !5
			!PolIntegrate=PolIntegrate+IntCoeff10(phistep,NCorder*steps+1)*philine  !10
			PolIntegrate=PolIntegrate+IntCoeff11(phistep,NCorder*steps+1)*philine  	!11
	!!!NewtonCotes


			phi=phi+dphi
		enddo



		PolIntegrate=PolIntegrate*dphi

	end function PolIntegrate
