program TWODEGMPILindPlot
use Constants
use Globals
use Physics2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,LindSum
real (kind=8), allocatable::MPILind(:),Lind(:),MPIa(:),MPIb(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps,i,j,wsteps,tempi
!complex (kind=8)::





	!!!! Initialize some situational constants
	call ConstInit(5D-1,0D0,GlobEf,Globeta,Globfx,Globfc) 
	MPImaster=0
	IntSteps=1000
	q=(/0.5D0,0D0/)*Globkf
	a=0D0
	b2=FermiVec(q,1D-6)
	write(*,*) b2
	w=0D0
	wsteps=1
	dw=(3D0*GlobEf)/real(wsteps, kind=8)
	!!! Initialize some files for data storage
	open(unit=9, file='LindPlot.Serial.dat')


	NProc=4
	allocate(MPIa(NProc),MPIb(NProc))
	MPISteps=IntSteps/NProc
	write(9,*) "# w/Ef, ReNLind   ##q=kf*(1,0), "




	do i=0,NProc-1
		temp=real(i, kind=8)/real(NProc, kind=8)
		MPIa(i+1)=a*(1D0-temp)+temp*b2
		!MPIa=a !DEBUG
		temp=real((i+1), kind=8)/real(NProc, kind=8)
		MPIb(i+1)=a*(1D0-temp)+temp*b2
		!MPIb=b !DEBUG
	enddo
	do i=1,NProc
		write(*,*) "Region",i,MPIa(i),MPIb(i)
	enddo
	!!!!!!!!!!!!!!!!!!!
	!!!Do some Stuff!!!
	!!!!!!!!!!!!!!!!!!!
	allocate(MPILind(NProc+2))
	allocate(Lind(NProc))
	MPILind=0D0
	Lind=0D0
	w=0D0
	tempi=NProc*MPISteps
	do i=1,wsteps	
		MPILind=0D0
		MPILind(1)=PolIntegrate3(MPIa(1),MPIb(NProc),tempi,tempi,ReNLindInt,q,w)
		!MPILind(1)=PolIntegrate3(MPIa(1),MPIb(NProc),tempi,tempi,MPITest2,q,w)
		!MPILind(1)=f((/MPIa(1),0D0/)) !DEBUG
		!MPILind(1)=CartIntegrate((/-b,b,-b,b/),MPIsteps*NProc,ReNLindInt,q,w)!Cart
		!MPILind(3)=CartIntegrate((/-b,b,-b,-b/2D0/),MPIsteps*NProc,ReNLindInt,q,w)!Cart
		!MPILind(4)=CartIntegrate((/-b,b,-b/2D0,0D0/),MPIsteps*NProc,ReNLindInt,q,w)!Cart
		!MPILind(5)=CartIntegrate((/-b,b,0D0,b/2D0/),MPIsteps*NProc,ReNLindInt,q,w)!Cart
		!MPILind(6)=CartIntegrate((/-b,b,b/2D0,b/),MPIsteps*NProc,ReNLindInt,q,w)!Cart
		do j=3,NProc+2
			MPILind(j)=PolIntegrate3(MPIa(j-2),MPIb(j-2),MPIsteps,tempi,ReNLindInt,q,w)
			!MPILind(j)=PolIntegrate3(MPIa(j-2),MPIb(j-2),MPIsteps,tempi,MPITest2,q,w)
			MPILind(2)=MPILind(2)+MPILind(j)
			!MPILind(j-1)=f((/MPIb(j-2),0D0/))
		enddo

		write(9,*) w/GlobEf,MPILind
		w=w+dw
	enddo



	!!! Close files
	close(unit=9)

end program TWODEGMPILindPlot
