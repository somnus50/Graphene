program NSqHPlasmons
	use constants	
	use NSquareHLindFunctions
	use HelperFunctions
	implicit none
	real (kind=8)::q,dw,phi,dphi,Gx,Gy,DATalat,DATt,DATFillingFrac,DATetafrac
	real (kind=8), allocatable:: LindLine(:), EpsLine(:,:),PlasmonArray(:,:)
	integer (kind=4):: steps,gstep,qstep,phistep,i,gsteps,qsteps,phisteps,wsteps
	integer (kind=4):: Gindex,wstep,j
	character (len=20):: trash

	
	open(unit=20,file='2DEGPlasmonfx2.dat')
	!open(unit=21,file='SqHPlasmonG10.dat')
	!open(unit=22,file='SqHPlasmonG01.dat')
	!open(unit=23,file='SqHPlasmonGm0.dat')
	!open(unit=24,file='SqHPlasmonG0m.dat')
	open(unit=10,file='2DEGLindMatrix.dat',status='old')
	read(10,*) !trash
	read(10,*) DATalat, DATt, DATFillingFrac, DATetafrac
	call ConstInit(DATFillingFrac,0D0,Ef,eta,Globfx,Globfc)
	read(10,*) !trash
	read(10,*) qsteps, phisteps, wsteps
	read(10,*) !trash

	allocate(LindLine(wsteps))
	allocate(EpsLine(5,wsteps))
	allocate(PlasmonArray(10,3))

	i=0
	Gindex=19
	do qstep=1,qsteps
		read(10,*) !trash
		read(10,*) q, dw, dphi
		read(10,*) !trash
		do phistep=1,phisteps
			phi=dphi*real(phistep)
			read(10,*) LindLine(:)
			call ReNEpsLine(q,phi,dw,wsteps,LindLine,EpsLine)
			do Gstep=1,1
				write(*,*) i, qstep, phistep, Gstep, wsteps
				call FindBracketList2(EpsLine(Gstep,:),wsteps,q,dw,PlasmonArray)
				i=i+1
				do j=1,10
					if ((PlasmonArray(j,2)).gt.(0D0)) then
						write(Gindex+Gstep,*) phi, PlasmonArray(j,1)/kf,&
						(PlasmonArray(j,2)+PlasmonArray(j,3))/(2D0*Ef),&
						kf*dsqrt(q)/Ef
					endif
				enddo
			enddo
		enddo
	enddo


	close(unit=10,status='keep')
	close(unit=20,status='keep')
	close(unit=21,status='keep')
	close(unit=22,status='keep')
	close(unit=23,status='keep')
	close(unit=24,status='keep')





end program NSqHPlasmons
