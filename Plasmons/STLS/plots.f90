program TwoDEGSTLSGsc
use Constants
use Globals
use AuxiliaryFunctions
use Physics2DEG
use fxc2DEG
use STLS
implicit none
real (kind=8)::rs,qs(2),mix,wp,wa,wb,w,dw,q,qp,ws(2),resa,resfn,resa2,resfn2,dq
real (kind=8), allocatable::wline(:),Ggrid(:,:),res(:),plas(:,:)
integer (kind=4)::i,j,k,SCsteps,qstps,MPImaster,wsteps,Nproc,IntSteps,params(4),stat,mixsteps
character (len=32)::tempchar


	rs=2D0
	mix=0D0
	SCsteps=1
	qstps=200


	Nproc=1
	MPImaster=0
	IntSteps=1000
	!qsteps=200
	wsteps=200



	!!!! Initialize some situational constants
	call ConstInit(rs,-1D-1,GlobEf,Globeta,Globfx,Globfc)
	write(*,*) "kf0",Globkf,Globkf0

	write(*,*) Globfx, Globfc, Globfx+Globfc
	qs=(/0D0,500D0*Globkf/)
	ws=(/0D0,5D0*GlobEf/)

	allocate(wline(qstps))



	q=0D0
	dq=10D0*Globkf0/real(qstps, kind=8)
	do k=1,qstps
		write(10,*) q/Globkf0, GpA(q), GLSDA(q), -q*(cfxssp(rs,-1D-1)+cfcssp(rs,-1D-1))
		q=q+dq
	enddo


end program TwoDEGSTLSGsc
