program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene3
use AuxiliaryFunctions
use fxc2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,w,dq,a,b1,bb,k1,k2,ww
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,valid
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	Globgs=2D0
	Globgv=2D0
	call ConstInit(1D12/cmSq2a0sq,-0.1D0)
	GlobEta=min(1D-4*GLobZeem,GlobEta)
	GlobEtaSq=GlobEta**2

	! Globkc=10D0*Globkc
	!Globg=4D0*(cfx(Globrs)+cfc(Globrs))!/kappa

	!call ConstInit(0D0,0D0)
	! Globeta=1D-1*GlobEf
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	open(unit=9, file='ChiSplit.test.dat')
	! open(unit=10, file='ReChi0kf.dat')

	!write(9,*) "# q/kf,wp/ef,low q limit"


	write(*,*) "kf",Globkf
	write(*,*) "Ef",GlobEf

	qsteps=1
	wsteps=300

	! q=0D0
	! dq=0D0!1.666666666666666D0*Globkf!5D0/real(qsteps, kind=8)*Globkf
	! q=dq
	q=0.0D0*GLobZeem/gam
	! dw=((3D0*Globkf)*gam)/real(wsteps, kind=8)
	dw=(GlobZeem-gam*q)/real(wsteps, kind=8)

	!q=2.9999999999999999D-2*Globkf

	write(9,*) "# kf,Ef,kc,Ec"
	write(9,*) "# ",Globkf,GlobEf,Globkc,Globkc*gam
	!write(9,*) "# q/kf, w/kf, ReNLind(q,w), ReNLindDumb(q,w), ReN2Lind(q,w), ReChi0Ex(q,w)"
	write(9,*) "# q/kf, w/kf, ImNLind(q,w), ImChi0Ex(q,w), ImChi0(q,w)"
	! write(10,*) "# kf,Ef,kc,Ec"
	! write(10,*) "# ",Globkf,GlobEf,Globkc,Globkc*gam
	! !write(9,*) "# q/kf, w/kf, ReNLind(q,w), ReNLindDumb(q,w), ReN2Lind(q,w), ReChi0Ex(q,w)"
	! write(10,*) "# q/kf, w/kf, ReNLind(q,w), ReChi0Ex(q,w), ReChi0(q,w)"
	do i=1,qsteps
		write(*,*) "qstep:",i,"/",qsteps
		write(*,*) "q",q!/Globkf
		w=dw!0D0!*GlobEf
		do j=1,wsteps
			write(*,*) "wstep:",j,"/",wsteps!, w, gam, globeta
			! write(9,*) q/Globkf,w/GlobEf,ImNChiDelta(q,w,1D0,1D0,1D0)+ImNChiDelta(q,w,1D0,-1D0,1D0)&
			! 														+ImNChiDelta(q,w,-1D0,1D0,1D0)+ImNChiDelta(q,w,-1D0,-1D0,1D0),&
			! 					 ImNLindPart(q,w,1D0,1D0,1D0)+ImNLindPart(q,w,1D0,-1D0,1D0)+ImNLindPart(q,w,-1D0,1D0,1D0)+ImNLindPart(q,w,-1D0,-1D0,1D0)
			! write(9,*) q/Globkf,w/GlobEf, ReNLind(q,w), ReNLindDumb(q,w), ReN2Lind(q,w), ReChi0Ex(q,w)
			! write(9,*) q/Globkf,w/Globkc/gam, ImNLind(q,w)*4D0*pi**2, ImChi0Ex(q,w), ImChi0(q,w)!, ImNLind(q,w)
			! write(9,*) q/Globkf,w/Globkc/gam, ImChi0Ex(q,w), ImChiPeak(q,w)!, ImChiKK(q,w)!, ImN2Lind(q,w)!, ImNLind(q,w), ImNLindHybrid(q,w)
			!ww=ReChitest(q,w)
			! write(9,*) q/Globkf,w/GlobZeem, ReChi0Ex(q,w),ReChiPeakst(q,w,-1D0,1D0),epsLDAst(q,w,-1D0,1D0)!, ReChi0(q,w),ReChiPeak(q,w),epsRPA(q,w)!,ReChiPeakPart(q,w,1D0,1D0,-1D0)&
			write(9,*) q/Globkf,w/GlobZeem, ReChiAnDU(q,w),ReChiPeakst(q,w,-1D0,1D0),&
								ReChiPeakPartst(q,w,1D0,-1D0,-1D0,-1D0,1D0)+ReChiPeakPartst(q,w,-1D0,-1D0,-1D0,-1D0,1D0)
			w=w+dw
		enddo
		q=q+dq
	enddo

	! write(*,*) "q,wp:",q/Globkf,wmagnonLDA(q,dw,GlobZeem,-1D0,1D0)/GlobZeem!-gam*(q+1D-10*Globkf)

!write(*,*) '##########################'
!call krange(Globkf,globEf,1D0,1D0,1D0,k1,k2,valid)
!write(*,*) '##########################'
!write(*,*) k1,k2,valid
	!write(*,*) PolIntegratephi11(test,0D0,1D0,0D0,0D0,phirange0,2000,q,w), pi/4D0

	!!! Close files
	close(unit=9)
	close(unit=10)



end program GraphenePlasmons
