
module constants
	implicit none
	
	real (kind=8), parameter::rs=1.79D0,pi=acos(-1D0),&
				N=1D0/(pi*rs),kf=DSQRT(1D0*pi*N),&
				kappa=1D0,e=1D0!,kf=sqrt(4D0*pi*n/(gs*gv))

end module constants

module LindFns
	use constants
	implicit none

	contains
	
	complex (kind=8) function Chi(q,w)
		implicit none
		real (kind=8)::q,w,Vp,Vm,SIp,SIm,Dp,Dm,ReChi,ImChi

		Vp=w/(q*kf)+q/(2D0*kf)
		Vm=w/(q*kf)-q/(2D0*kf)

		if (Vp.ge.(0D0)) then
			SIp=1D0
		else
			SIp=-1D0
		endif

		if (Vm.ge.(0D0)) then
			SIm=1D0
		else
			SIm=-1D0
		endif

		Dp=Vp**2-1D0
		Dm=Vm**2-1D0

		ReChi=0D0

		if (Dm.ge.(0D0)) then
			ReChi=ReChi+SIm*dsqrt(Dm)
		endif

		if (Dp.ge.(0D0)) then
			ReChi=ReChi-SIp*dsqrt(Dp)
		endif

		ReChi=ReChi*kf/q+1D0
		ReChi=-ReChi/pi

		ImChi=0D0

		if (Dm.le.(0D0)) then
			ImChi=ImChi+dsqrt(-Dm)
		endif

		if (Dp.le.(0d0)) then
			ImChi=ImChi-dsqrt(-dp)
		endif

		ImChi=-ImChi*kf/q
		ImChi=ImChi/pi

		Chi=ReChi*(1D0,0D0)+ImChi*(0D0,1D0)

		return
	end function Chi

	complex (kind=8) function ChiCalc(q,w)
		implicit none
		real    (kind=8)::q,w,eta,ReChi,ImChi,k,dk,phi,dphi,Nm,Np
		integer (kind=4)::i,j
		integer (kind=4), parameter::Nphi=1000,Nk=1000

		eta=0.01D0*kf**2/2D0
		dphi=2D0*pi/Nphi
		dk=kf/nk

		ReChi=0D0
		ImChi=0D0

		do i=1,Nphi
			phi=i*dphi-dphi/2D0
			do j=1,Nk
				k=j*dk
				Nm=w-k*q*dcos(phi)-q**2/2D0
				Np=w+k*q*dcos(phi)+q**2/2D0
				ReChi=ReChi+Nm*k/(Nm**2+eta**2)&
					-Np*k/(Np**2+eta**2)
				ImChi=ImChi-eta*k/(Nm**2+eta**2)&
					+eta*k/(Np**2+eta**2)
			enddo
		enddo

		ReChi=ReChi*dk*dphi/(2D0*pi)**2
		ImChi=ImChi*dk*dphi/(2D0*pi)**2

		ChiCalc=ReChi*(2D0,0D0)+ImChi*(0D0,2D0)

		return
	end function ChiCalc

	real (kind=8) function Vc(q)
		use constants
		implicit none
		real (kind=8) ::q
	
		Vc=2D0*pi*e**2/(kappa*q)
	end function Vc

	complex (kind=8) function EpsLind(q,w)
		implicit none
		real    (kind=8)::q,w,Coul

		Coul=Vc(q)
		EpsLind=(1D0,0D0)+Coul*Chi(q,w)
		return
	end function EpsLind

	real (kind=8) function ReEpsLind(q,w)
		implicit none
		real    (kind=8)::q,w,Coul

		Coul=Vc(q)
		ReEpsLind=1D0-Coul*dreal(Chi(q,w))
		return
	end function ReEpsLind

	real (kind=8) function ImEpsLind(q,w)
		implicit none
		real    (kind=8)::q,w,Coul

		Coul=Vc(q)
		ImEpsLind=1D0+Coul*dimag(Chi(q,w))
		return
	end function ImEpsLind

	complex (kind=8) function EpsLindCalc(q,w)
		implicit none
		real    (kind=8)::q,w,Coul

		Coul=Vc(q)
		EpsLindCalc=(1D0,0D0)+Coul*ChiCalc(q,w)
		return
	end function EpsLindCalc

subroutine FindBracket(func,x,ao,bo,a,b,steps,stat)
	implicit none
	integer (kind=4) :: steps,stat,i
	real    (kind=8) :: ao,bo,a,b,x,v
	real    (kind=8), external:: func

	i=1
	b=bo
	a=bo+real(1)*(ao-bo)/(real(steps))

	do while (((func(x,a)*func(x,b)).gt.(0D0)).and.(i.lt.steps))
		!write(10,*) i, func(x,a)*func(x,b)
		a=bo+real(i+1)*(ao-bo)/(real(steps))
		b=bo+real(i)*(ao-bo)/(real(steps))
		i=i+1
		!write(*,*) a,b,i
		!write(*,*) func(a,v
	enddo
	stat=-1
	if (int(sign(1D0,(func(x,a)*func(x,b)))).eq.(-1)) then
		stat=1
	endif
	return
end subroutine FindBracket

real (kind=8) function RootBrentsMethod(a,b,delta,f,x)
!	use functions
	implicit none
	real (kind=8) ::a,b,c,d,s,swap1,swap2,delta,x
	real (kind=8), external:: f
	integer (kind=4) ::mflag,ind
!	abstract interface
!		function fn (y,z)
!			real (kind=8) :: fn
!			real (kind=8), intent (in) :: y,z
!		end function fn
!	end interface
!	abstract interface
!		function func (y,z)
!			real (kind=8) :: func
!			real (kind=8), intent (in) :: y,z
!		end function func
!	end interface
!	procedure (func), real(kind=8), external::fn
!	procedure (func), pointer :: f => fn

!	procedure(func), pointer:: f => fn




! This function finds the root of a function, f in [a,b] with the precision of delta
! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
! a and b must be REAL. f must be a function that takes one input.
! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method


	
	if (f(x,a)*f(x,b).gt.(0D0)) then
		call EXIT(1)
	endif

	if (abs(f(x,a)).gt.abs(f(x,b))) then
		swap1=a
		swap2=b
		a=swap2
		b=swap1
	endif

	c=a
	mflag=1
	s=a
	ind=1

	do while ((abs(b-a).gt.delta).or.(abs(f(x,b)).gt.delta).or.(abs(f(x,s)).gt.delta).or.(ind.lt.1000))
		ind=ind+1
		if ((f(x,a).ne.f(x,c)).and.(f(x,b).ne.f(x,c))) then
			s=(a*f(x,b)*f(x,c))/((f(x,a)-f(x,b))*(f(x,a)-f(x,c)))+(b*f(x,a)&
			  *f(x,c))/((f(x,b)-f(x,a))*(f(x,b)-f(x,c)))+(c*f(x,a)*f(x,b))&
			  /((f(x,c)-f(x,a))*(f(x,c)-f(x,b)))
			!Inverse Quadratic Interpolation
		else
			s=b-f(x,b)*(b-a)/(f(x,b)-f(x,a))
			!Secant Method
		endif
		
		if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b))&
		   .ge.(abs(b-c)/2D0))).or.((mflag.eq.(0)).and.((abs(s-b)).ge.&
		   (abs(c-d)/2D0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta)).or.&
		   ((mflag.eq.(0)).and.(abs(c-d).lt.delta))) then
			s=(a+b)/2
			!Bisection Method
			mflag=1
		else
			mflag=0
		endif
		
		d=c
		c=b
		
		if ((f(x,a)*f(x,s)).lt.(0D0)) then
			b=s
		else
			a=s
		endif

		if (abs(f(x,a)).gt.abs(f(x,b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
		
	enddo

	if ((f(x,b)).lt.(f(x,s))) then
		RootBrentsMethod=b
	else
		RootBrentsMethod=s
	endif

	return
end function RootBrentsMethod
	
end module LindFns
