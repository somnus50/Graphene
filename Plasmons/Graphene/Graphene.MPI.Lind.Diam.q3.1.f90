program GrapheneMPILindq3
use mpi
use Constants
use Globals
use PhysicsGraphene
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq(2),Lind,MPILind,qarray(6),dphi,qtemp(2),qhat(2),qtheta,n
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	n=5D13/cmSq2a0sq
	call ConstInit(n,0D0,GlobEf,Globeta,Globfx,Globfc)
	MPImaster=0
	IntSteps=1000
	qsteps=100
	wsteps=300
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps))
	w=0D0
	dw=(3D0*GlobEf)/real(wsteps, kind=8)
	qtheta=90D0*pi/180D0
	qhat=(/dcos(qtheta),dsin(qtheta)/)
	!!!!
	!!!! Initialize MPI
	call MPI_Init(MPIerr)
	call MPI_Comm_size(MPI_COMM_WORLD,NProc,MPIerr)
	call MPI_Comm_rank(MPI_COMM_WORLD,MPIid,MPIerr)

	if (NProc.ne.3) then
		write(*,*) "Number of threads:",NProc
		write(*,*) "Need to run on 3 threads. Might make this flexible later"
		call EXIT(-Nproc)
	endif
	!!!!
	!!!! Initialize Data files
	if (MPIid.eq.MPImaster) then
		open(unit=9, file='Lind.Diam.q3.1.dat')
		MPISteps=IntSteps/NProc
		wtime=MPI_Wtime()
		write(9,*) "### ReNLind(q,w)"
		write(9,*) "### qtheta", qtheta, "n", n*cmSq2a0sq
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."
	endif

	call MPI_Bcast(MPISteps,1,MPI_INTEGER,MPIMaster,MPI_COMM_WORLD,MPIerr)
	!!!!
	!!!! Begin integration loop
	phia=2D0*pi/3D0+pi/2D0
	phib=phia+2D0*pi/3D0
	dq=3D0*Globkf*qhat/real(qsteps, kind=8)
	q=dq
	do i=1,qsteps
		MPILind=0D0
		Lind=0D0
		w=0D0
		wline=0D0
		if (MPIid.eq.MPImaster) then
			!!! Rotate q for each diamond of the hexagon
			call qrots(q,qarray) 
			write(9,fmt1) NormC(q)/Globkf, w, dw
			write(*,*) "q",i,"of",qsteps
		endif
		!!! Broadcast rotated q's
		call MPI_Bcast(qarray,6,MPI_DOUBLE_PRECISION,MPIMaster,MPI_COMM_WORLD,MPIerr)
		do j=1,wsteps
			!!! Broadcast w. Mostly used for synchronization of w loop
			call MPI_Bcast(w,1,MPI_DOUBLE_PRECISION,MPIMaster,MPI_COMM_WORLD,MPIerr)		
			Lind=0D0
			MPILind=0D0
			!do qindex=0,0!1
				!!! Each process calculates two of the diamonds				
				qtemp=(/qarray(2*MPIid+1),qarray(2*MPIid+2)/)
				MPILind=MPILind+RadFnPolInt2(DBwrapper,phia,phib,IntSteps,ReNLindInt,qtemp,w)
			!enddo
			call MPI_Reduce(MPILind,Lind,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPImaster,MPI_COMM_WORLD,MPIerr)
			if (MPIid.eq.MPImaster) then
				wline(j)=Lind
				w=w+dw
			endif
		enddo
		if (MPIid.eq.MPImaster) then
			!!! Write data
			write(9,fmt2) wline
		endif
		q=q+dq
	enddo
	!!!!
	if (MPIid.eq.MPImaster) then
		wtime=MPI_Wtime()-wtime
		write(*,*) "Time:",wtime
	endif
	call MPI_Finalize(MPIerr)
	close(unit=9)
end program GrapheneMPILindq3
