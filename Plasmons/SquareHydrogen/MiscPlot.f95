program MiscPlot
	use constants	
	use NSquareHLindFunctions
	use HelperFunctions
	implicit none
	real    (kind=8):: q(2),w,dk,kx,ky,temp
!	real (kind=8), allocatable :: vect(:)
	integer (kind=4):: i,j,steps

	q = 1D0*(/1D0,0D0/)
	w = 2.75D0*Ef

	!read(*,*) steps
	steps=200
	dk=2D0/real(steps, kind=8)
	kx=-1D0
	ky=-1D0

	do i=1,steps
		do j=1,steps
			temp=ReNLindInt(q,w,(/kx,ky/)*pi/alat)
			!temp=f((/kx*pi/alat,ky*pi/alat/))
			if (temp.ne.0D0) then
				write(*,*) kx,ky,temp
			endif
			ky=ky+dk
		enddo
		ky=-1D0
		kx=kx+dk
	enddo


end program MiscPlot
