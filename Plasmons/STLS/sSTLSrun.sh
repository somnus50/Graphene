#!/usr/bin/env bash



input="rszgen.dat"

template="SCTest.Pol.Parallel.f90"

delimiter=' '

#rm -rf "bin/tmp/fxc.rs.z.txt"

while IFS= read -r line
do

echo "'"$line"'"

IFS=' ' read -ra array <<< "$line"

tmpfile="tmp"${array[2]}
echo "$tmpfile"

sed -e "s/myrs/${array[0]}/;s/myz/${array[1]}/;s/myfile/fSTLS-par-${array[2]}.dat/" "$template" > "$tmpfile.f90"

./compilemain.sh -lc gfortran "$tmpfile.f90"
# rm -f "$tmpfile.f90"
done < "$input"

mkdir -p bin/tmp
cd bin/tmp
find '../' -type f -name 'tmp*' | parallel -j 6 {}
