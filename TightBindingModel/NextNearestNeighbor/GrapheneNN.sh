#!/bin/bash

if [ ! -f ./grapheneNN ]; then
	echo "This program uses LAPACK. Make sure you have LAPACK and its development files installed."
	read -p "Press ENTER to continue or CTRL-C to exit"
	gfortran GrapheneNextNearest.f95 -llapack -fbounds-check -g -o grapheneNN
fi

./grapheneNN
echo ""
ls *NN*.gnu
echo ""
echo "Try plotting any of the above files with GNUPlot"
echo "e.g.     gnuplot ZZNNEnNr4.dat.lines.gnu"
echo "Or       gnuplot *.gnu    will cycle through all of them"
