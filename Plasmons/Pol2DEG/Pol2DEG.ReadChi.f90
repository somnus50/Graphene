program Pol2DEGPlasmons
use Constants
use Globals
use PhysicsPol2DEG
use SelfConsistent
use STLS

!use SomeModule !As many as you need
implicit none
character(len=64) :: Cname,Qname,Wname,Cname2
integer(4)::wsteps,stat,k,SCsteps,i!,qsteps
real(8), allocatable::Chi0out(:,:),qout(:),wout(:),ress(:)
real(8)::tol,mix,q,w

Cname='Pol2DEG.ReChi0UD.rs5.z2.log.dat'
Cname2='Pol2DEG.ImChi0UD.rs5.z2.log.dat'
Qname='Pol2DEG.ReChi0UD.rs.z.log.qgrid.dat'
Wname='Pol2DEG.ReChi0UD.rs.z.log.wgrid.dat'

	call ConstInit(30d0,0.255D0,GlobEf,Globeta)


call Chi0ImportAll(Cname2,ImChi0grid,Qname,Modqgrid,Wname,Modwgrid,Modqsteps,Modwsteps)
call Chi0ImportAll(Cname,ReChi0Grid,Qname,Modqgrid,Wname,Modwgrid,Modqsteps,Modwsteps)

Modqgrid=Modqgrid*Globkf0 ! I'm an idiot that divided by kf when I wrote it out
Modwgrid=Modwgrid*GlobEf
qsteps=Modqsteps
wsteps=Modwsteps

write(*,*) "q ",qsteps,", w ",wsteps




allocate(SUDgrid(qsteps),SUDspline(qsteps),ReChiGrid(qsteps,wsteps),ImChiGrid(qsteps,wsteps)&
				,GSCold2(qsteps),GSCspline2(qsteps),GSC(qsteps)&
				,fxcgrid(qsteps),fxcspline(qsteps),SDUgrid(qsteps),SDUspline(qsteps))


SCsteps=7
mix=5D-1
tol=1D-20

allocate(ress(SCsteps))
call SCInit(qsteps,Modqgrid,fn0)

call Gupdate(GLSDA,GSCold2,GSCspline2)

do i=1,Modqsteps
	write(57,*) Modqgrid(i), GUDspl(Modqgrid(i),0D0), GLSDA(Modqgrid(i))
enddo

!call spline()
GSCold2=0D0
GSCspline2=0D0
call ChiBuildGrid(ReChi0grid,ImChi0grid,Modqgrid,Modwgrid,Modqsteps,&
										Modwsteps,GUDspl,ReChigrid,ImChigrid)

!write(299,*) ImChi0grid
call SCUpdate()




!call SCSolve(GUD,1,0D0,tol,ress,SCUpdate,GSCold2,GSCspline2,stat)
GSC=GSCold2
!call SCSolve(GUD,SCsteps,mix,tol,ress,SCUpdate,GSCold2,GSCspline2,stat)

write(*,*) "SC stat:",stat

!GSC=GSCold2

!call SCSolve(GUD,SCsteps,mix,tol,ress,SCUpdate,GSCold2,GSCspline2,stat)
!write(*,*) "SC stat:",stat


open(unit=19, file='STLS.pol.dat')
write(19,*) "# q/kf, PGGDU, STLS0, STLS"

i=100
w=Modwgrid(i)

do k=1,qsteps
	!write(*,*) k
	!write(19,*) qgrid(k)/Globkf0, S0ssp(-1,1,qgrid(k)), SDUInt(qgrid(k)), PGGDU(qgrid(k)), PGGDUInt(qgrid(k))
	!call SDUIntCheck(qgrid(k),res)
	!write(19,*) qgrid(k)/Globkf0, S0ssp(-1,1,qgrid(k))-Globnu, SDUInt(qgrid(k))-Globnu, -DMatssp(-1,1,qgrid(k))!, (res(1)+res(2))

	!q=Modqgrid(k)
	!write(*,*) k,qsteps
	write(19,*) Modqgrid(k)/Globkf0, -PGGUD(Modqgrid(k),0D0)*Modqgrid(k)/2d0/pi, GSC(k), GSCold2(k),GLSDA(Modqgrid(k))
	!write(19,*) qgrid(k)/Globkf0, Structssp(-1,1,1D0*qgrid(k))-Globnu, S0ssp(-1,1,1D0*qgrid(k))-Globnu,SDUsplint(1D0*qgrid(k))
	!write(19,*) q/Globkf0, ImChiInterp(q,w),ImChiGrid(k,i),dimag(Chi0ssp(1,-1,q,w)),ImChi0grid(k,i)&
	!						,-2D0*pi*GUDspl(q,w)/q!,dimag(ChiUD((/q,0d0/),w,fxcDU2)),ImChi0sspNum((/q,0d0/),w,1D0,-1D0,1)
enddo
close(unit=19)

end program Pol2DEGPlasmons
