      PROGRAM XC   
      IMPLICIT NONE

      INTEGER I
      DOUBLE PRECISION PI,EXC,ZETA,RS,N
      EXTERNAL EXC
      PI = 3.141592653589793D0

      ZETA = -0.1D0

      DO 10 I=1,100
         RS = 0.1D0*I
         N = 1.D0/(PI*RS**2)
         WRITE(1,*)real(RS),real(EXC(RS,ZETA))
         WRITE(2,*)real(N),real(N*EXC(RS,ZETA))
10    CONTINUE

      END
C********************************************************************
      DOUBLE PRECISION FUNCTION EXC(RS,ZETA)
      IMPLICIT NONE

      DOUBLE PRECISION PI,RS,ZETA,EX,EX0,EX6
      DOUBLE PRECISION A0,B0,C0,D0,E0,F0,G0,H0,
     &                 A1,B1,C1,D1,E1,G1,H1,
     &                 A2,B2,C2,D2,E2,H2,
     &                 ALPHA0,ALPHA1,ALPHA2,BETA

      PI = 3.141592653589793D0

      A0 = -0.1925D0
      A1 = 0.117331D0
      A2 = 0.0234188D0

      B0 = 0.0863136D0
      B1 = -3.394D-2
      B2 = -0.037093D0

      C0 = 0.057234D0
      C1 = -7.66765D-3
      C2 = 0.0163618D0

      E0 = 1.0022D0
      E1 = 0.4133D0
      E2 = 1.424301D0

      F0 = -0.02069D0
   
      G0 = 0.34D0
      G1 = 6.68467D-2
  
      H0 = 1.747D-2
      H1 = 7.799D-4
      H2 = 1.163099D0

      D0 = -A0*H0
      D1 = -A1*H1
      D2 = -A2*H2

      ALPHA0 = A0 + (B0*RS + C0*RS**2 + D0*RS**3)*
     &         DLOG(1.D0 + 1.D0/(E0*RS + F0*RS**1.5D0 +
     &                                   G0*RS**2 + H0*RS**3))
      ALPHA1 = A1 + (B1*RS + C1*RS**2 + D1*RS**3)*
     &         DLOG(1.D0 + 1.D0/(E1*RS + G1*RS**2 + H1*RS**3))
      ALPHA2 = A2 + (B2*RS + C2*RS**2 + D2*RS**3)*
     &         DLOG(1.D0 + 1.D0/(E2*RS + H2*RS**3))

      BETA = 1.3386D0


      EX0 = -4.D0*DSQRT(2.D0)/(3.D0*PI*RS) 

      EX = EX0*( (1.D0+ZETA)**1.5D0 + (1.D0-ZETA)**1.5D0)/2.D0

      EX6 = EX - (1.D0 + 3.D0*ZETA**2/8D0 + 3.D0*ZETA**4/128.D0)*EX0

      EXC = (DEXP(-BETA*RS)-1.D0)*EX6
     &      + ALPHA0 + ALPHA1*ZETA**2 + ALPHA2*ZETA**4

      RETURN
      END
