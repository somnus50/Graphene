program TwoDEGSTLSGsc
use Constants
use Globals
use PhysicsPol2DEG
use STLS
use AuxiliaryFunctions
use SelfConsistent
implicit none
real (kind=8)::rs,qs(2),mix,wp,wa,wb,w,dw,q,qp,ws(2),resa,resfn,resa2,resfn2,temp1,temp2,&
				res1(2),res2(2),z,s0,s1,s2,dgdq(3),h,qq
real (kind=8), allocatable::wline(:),res(:),plas(:,:)
integer (kind=4)::i,j,k,SCsteps,qstps,MPImaster,wsteps,Nproc,IntSteps,params(4),stat,mixsteps
character (len=32)::tempchar


	rs=2D0
	mix=0.95D0
	SCsteps=30
	qstps=350
	qsteps=qstps
	z=-5D-2

	Nproc=1
	MPImaster=0
	IntSteps=1000
	!qsteps=200
	wsteps=200


	!!!! Initialize some situational constants
	call ConstInit(rs,z,GlobEf,Globeta)
	qs=(/0D0,1D0/)
	ws=(/0D0,10D0*GlobEf/)
	call SCInit(qstps,qs,fn0) !GPGGDU
	allocate(res(SCsteps),plas(xsteps,3),wline(qsteps),SDUbarspline(qsteps),SDUbargrid(qsteps),fxcgrid(qsteps),fxcspline(qsteps))

!	allocate(SDUbargrid(qstps),SDUbarspline(qstps))
	call STLSSCInit2(SCsteps,qstps,qs,SDUbarsplineUpdate)

	write(*,*) "qgrid",qgrid

	params=(/Nproc,IntSteps,qsteps,wsteps/)

	!!! Initialize some files for data storage


	call fDUbarUpdate()




	xgrid=qgrid

	GSC=GSCold


	! GSCold=0D0
	! Looking at S
	do i = 2, qsteps
		write(*,*) "qstep",i,qsteps
		qp=(1D0-qgrid(i))/qgrid(i)
		write(*,*) qp
		write(*,*) "s0"
		s0=S0ssp(-1,1,qp)
		write(*,*) s0
		write(*,*) "s1"
		s1=Structssp2bar(-1,1,qgrid(i))
		write(*,*) s1
		write(*,*) "s2"
		s2=Structssp(-1,1,qp)
		write(*,*) s2
		write(89,*) qgrid(i), qp, s0, s1, s2, GSCold(i)
	end do
	! call exit()
	! Looking at S



	!call SDUsplineUpdate()
	write(*,*) "SC 0"
	call SCSolve(GDUforcebar,1,0D0,1D-20,res,SDUbarsplineUpdate,GSCold,GSCspline,stat)
	write(*,*) "SC stat:",stat

	GSC=GSCold


	write(*,*) "SC 1"

	call SCSolve(GDUforcebar,SCsteps,mix,1D-6,res,SDUbarsplineUpdate,GSCold,GSCspline,stat)
	write(*,*) "SC stat:",stat

	call fDUbarUpdate()

!	do i=1,qsteps
!		call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
!		call RootFinder(eps2,ws(1),ws(2),qgrid(i),1D-6,resa2,resfn2)
!		plas(i,1)=resa
!		plas(i,2)=resa2
!	enddo




	!call SCInit(qstps,qs,fn0)
	!call SCSolve(fxcUD,SCsteps,mix,1D-4,res,GSCold,GSCspline,stat)
	!write(*,*) "SC stat:",stat

	!do i=1,qsteps
	!	call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
	!	plas(i,3)=resa
	!enddo

!	open(unit=29,file='Plas0.F.dat')
!	write(29,*) "#Plasmons STLS0"
!	write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


!	do i=1,qsteps
!		call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
!		write(29,*) qgrid(i)/Globkf,resa/GlobEf,LowQPlasmon(qgrid(i))/GlobEf,resfn
!	enddo

	!write(*,*) "n,nu,nd"
	!write(*,*) Globn,Globnu,Globnd

!	close(unit=29)
	open(unit=19, file='STLS.pol.new.force.30.dat')
	write(19,*) "# q/kf, PGGDU, STLS0, STLS-30, S, S0, GLSDA, STLSfxc"

	do k=1,qsteps
		!write(*,*) k
		! write(19,*) qgrid(k)/Globkf0, -PGGDU(qgrid(k))*qgrid(k)/2d0/pi, GSC(k), GSCold(k)&
		! 						,SDUsplint(qgrid(k)), S0ssp(-1,1,qgrid(k))-Globnu,GLSDA(qgrid(k))
		write(19,*) (1D0-qgrid(k))/qgrid(k)/Globkf0, -PGGDU((1D0-qgrid(k))/qgrid(k))*(1D0-qgrid(k))/qgrid(k)/2d0/pi,&
		 GSC(k), GSCold(k),SDUbarsplint(qgrid(k)), S0ssp(-1,1,(1D0-qgrid(k))/qgrid(k))-Globnu,GLSDA((1D0-qgrid(k))/qgrid(k)),&
		 fxcgrid(k)
	enddo
	close(unit=19)


	h=2D0/3D0/3D0
	qq=0D0
	call GDUqDerivs7(qq,h,dgdq)
	write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "7  dG/dq|q=0 :",dgdq
	call GDUqDerivs10(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "10 dG/dq|q=0 :",dgdq
	call GDUqDerivs12(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "12 dG/dq|q=0 :",dgdq
	call GDUqDerivs15(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "15 dG/dq|q=0 :",dgdq
	call GDUqDerivs21(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "21 dG/dq|q=0 :",dgdq
	call fDUqDerivs12(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "12 df/dq|q=0 :",dgdq
	call fDUqDerivs15(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "15 df/dq|q=0 :",dgdq
	h=qgrid(2)
	call GDUqDerivs7(qq,h,dgdq)
	write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "7  dG/dq|q=0 :",dgdq
	call GDUqDerivs10(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "10 dG/dq|q=0 :",dgdq
	call GDUqDerivs12(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "12 dG/dq|q=0 :",dgdq
	call GDUqDerivs15(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "15 dG/dq|q=0 :",dgdq
	call GDUqDerivs21(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "21 dG/dq|q=0 :",dgdq
	call fDUqDerivs12(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "12 df/dq|q=0 :",dgdq
	call fDUqDerivs15(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "15 df/dq|q=0 :",dgdq
	h=qgrid(2)/2D0
	call GDUqDerivs7(qq,h,dgdq)
	write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "7  dG/dq|q=0 :",dgdq
	call GDUqDerivs10(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "10 dG/dq|q=0 :",dgdq
	call GDUqDerivs12(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "12 dG/dq|q=0 :",dgdq
	call GDUqDerivs15(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "15 dG/dq|q=0 :",dgdq
	call GDUqDerivs21(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "21 dG/dq|q=0 :",dgdq
	call fDUqDerivs12(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "12 df/dq|q=0 :",dgdq
	call fDUqDerivs15(qq,h,dgdq)
	! write(*,*) "h =", h, (1D0-h)/h
	write(*,*) "15 df/dq|q=0 :",dgdq




	!open(unit=29, file='Plasmons.2.dat')
	!write(29,*) "#Plasmons STLS10"
	!write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


	!do i=1,qsteps
	!	write(29,*) qgrid(i)/Globkf,plas(i,:)/GlobEf,AnalyticPlasmon(qgrid(i))/GlobEf
	!enddo

	!close(unit=29)



end program TwoDEGSTLSGsc
