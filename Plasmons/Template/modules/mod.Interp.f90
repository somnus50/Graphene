module Interp
implicit none


contains



  subroutine x2(grid,fn,res)
    implicit none
    real (kind=8),external::fn
    real (kind=8), intent(in) ::grid(4)
    real (kind=8), intent(out)::res(16)
    real (kind=8)::fs(2,2),df(2)
    integer (kind=4)::i,j

    ! grid = (x1,x2,y1,y2)

    do i=1,2
      do j=1,2
        fs(i,j)=fn(grid(i),grid(j))
      enddo
    enddo

    res(1)=fs(1,1)
    res(2)=fs(2,1)
    res(3)=fs(1,2)
    res(4)=fs(2,2)



    do i=1,2
      df(i)=(fs(2,i)-fs(1,i))/(grid(2)-grid(1))
    enddo

    res(5)=df(1)
    res(6)=df(2)
    res(7)=df(1)
    res(8)=df(2)

    do i=1,2
      df(i)=(fs(i,2)-fs(i,1))/(grid(4)-grid(3))
    enddo

    res(9)=df(1)
    res(10)=df(2)
    res(11)=df(1)
    res(12)=df(2)

    do i=0,1
      df(i+1)=(res(7+i)-res(5+i))/(grid(4)-grid(3))
    enddo

    res(13)=df(1)
    res(14)=df(2)
    res(15)=df(1)
    res(16)=df(2)

  end subroutine x2






  subroutine BiLinInterp(grid,fn,pt,res)
    implicit none
    real (kind=8),intent(in)::grid(4),pt(2)
    real (kind=8),external  ::fn
    real (kind=8),intent(out)::res
    real (kind=8)::a,b,c,d

    a=(grid(1)-pt(1)) ! x1-x
    b=(grid(2)-pt(1)) ! x2-x
    c=(grid(3)-pt(2)) ! y1-y
    d=(grid(4)-pt(2)) ! y2-y

    res=b*d*fn(grid(1),grid(3))-a*d*fn(grid(2),grid(3))-b*c*fn(grid(1),grid(4))+a*c*fn(grid(2),grid(4))
    !write(*,*) a,b,c,d,res
    res=res/(grid(2)-grid(1))/(grid(4)-grid(3))


  end subroutine BiLinInterp


  subroutine BiLinInterpTab(grid,fngrid,pt,res)
    implicit none
    real (kind=8),intent(in) ::grid(4),pt(2)
    real (kind=8),intent(in) ::fngrid(4)
    real (kind=8),intent(out)::res
    real (kind=8)::a,b,c,d

    a=(grid(1)-pt(1)) ! x1-x
    b=(grid(2)-pt(1)) ! x2-x
    c=(grid(3)-pt(2)) ! y1-y
    d=(grid(4)-pt(2)) ! y2-y

    !fngrid = ( fn11, fn21, fn12, fn22 )

    res=b*d*fngrid(1)-a*d*fngrid(2)-b*c*fngrid(3)+a*c*fngrid(4)
    !write(*,*) a,b,c,d,res
    res=res/(grid(2)-grid(1))/(grid(4)-grid(3))


  end subroutine BiLinInterpTab

  subroutine BiLinterp(x,y,num,xgrid,ygrid,fngrid,res)
    use AuxiliaryFunctions
    implicit none
    integer (kind=4),intent(in)::num
    real (kind=8)   ,intent(in)::x,y,xgrid(num),ygrid(num),fngrid(num,num)
    real (kind=8)  ,intent(out)::res
    integer (kind=4)::xind,yind
    real (kind=8)::fng(4),grid(4),pt(2)

    call BinSearch(xgrid,num,x,xind)
    call BinSearch(ygrid,num,y,yind)


    if (xind.eq.num) then
      xind=num-1
    endif
    if (yind.eq.num) then
      yind=num-1
    endif

    pt=(/x,y/)
    grid=(/xgrid(xind),xgrid(xind+1),ygrid(yind),ygrid(yind+1)/)
    fng=(/fngrid(xind,yind),fngrid(xind+1,yind),fngrid(xind,yind+1),fngrid(xind+1,yind+1)/)

    call BiLinInterpTab(grid,fng,pt,res)
  end subroutine BiLinterp


  subroutine BiCubicConv(tx,ty,fgrid,res)
    implicit none
    real (kind=8),intent(in)::tx,ty,fgrid(16)
    real (kind=8),intent(out)::res
    real (kind=8)::bm1,b0,b1,b2



    bm1=p(tx,fgrid(1),fgrid(2),fgrid(3),fgrid(4))
    b0= p(tx,fgrid(5),fgrid(6),fgrid(7),fgrid(8))
    b1= p(tx,fgrid(9),fgrid(10),fgrid(11),fgrid(12))
    b2= p(tx,fgrid(13),fgrid(14),fgrid(15),fgrid(16))

    res=p(ty,bm1,b0,b1,b2)

    contains
      real (kind=8) function p(t,fm1,f0,f1,f2)
        implicit none
        real (kind=8)::t,fm1,f0,f1,f2,t2,t3


          t2=t**2
          t3=t**3

          p=fm1*(2D0*t2-t3-t)+f0*(2D0-5D0*t2+3D0*t3)&
           +f1*(t+4D0*t2-3D0*t3)+f2*(t3-t2)
          p=p*0.5D0
      end function p
  end subroutine BiCubicConv



  subroutine BiCubInterp(x,y,num,xgrid,ygrid,fngrid,res)
    use AuxiliaryFunctions
    implicit none
    integer (kind=4),intent(in)::num
    real (kind=8)   ,intent(in)::x,y,xgrid(num),ygrid(num),fngrid(num,num)
    real (kind=8)  ,intent(out)::res
    integer (kind=4)::xind,yind
    real (kind=8)::fng(16),grid(4),pt(2)

    call BinSearch(xgrid,num,x,xind)
    call BinSearch(ygrid,num,y,yind)

    if (xind.gt.(num-2)) then
      xind=num-2
    endif
    if (yind.gt.(num-2)) then
      yind=num-2
    endif
    if (xind.lt.(2)) then
      xind=2
    endif
    if (yind.lt.(2)) then
      yind=2
    endif



    fng=(/fngrid(xind-1,yind-1),fngrid(xind,yind-1),fngrid(xind+1,yind-1),fngrid(xind+2,yind-1),&
          fngrid(xind-1,yind),fngrid(xind,yind),fngrid(xind+1,yind),fngrid(xind+2,yind),&
          fngrid(xind-1,yind+1),fngrid(xind,yind+1),fngrid(xind+1,yind+1),fngrid(xind+2,yind+1),&
          fngrid(xind-1,yind+2),fngrid(xind,yind+2),fngrid(xind+1,yind+2),fngrid(xind+2,yind+2)/)

    call BiCubicConv(x-xgrid(xind),y-ygrid(yind),fng,res)
  end subroutine BiCubInterp



















end module Interp
