module SpinWave
implicit none
real (kind=8), parameter::  UUUU=(/1D0,1D0,1D0,1D0/),&
							DDDD=(/-1D0,-1D0,-1D0,-1D0/),&
							UDUD=(/1D0,-1D0,1D0,-1D0/),&
							DUDU=(/-1D0,1D0,-1D0,1D0/),&
							

real (kind=8):: Globevinfo,fxcVec


contains

	subroutine KernelConstr(frac,z,Ef,eta,KernVec)
		implicit none
		!integer (kind=4) :: 
		real    (kind=8), intent(in)::frac,z 
		real    (kind=8), intent(out)::Ef,eta, KernVec(4)
		real    (kind=8)::n 
			
			! Easy Stuff
			Ef=8D0*t*frac
			eta=5D-2*Ef
			n=2D0*frac

			! Hard Stuff
			! The kernel is actually a somewhat sparse matrix so I'll define it 
			! as a vector thusly:
			! KernVec=< fxc^^^^ , fxcvvvv , fxc^^vv=fxcvv^^ ,fxc^v^v=fxcv^v^ >
			! ^ : up ; v : down

			KernVec(1)=2D0*dnexct(n,z)+n*d2nexct(n,z)+2D0*(1D0-z)*d2nzexct(n,z)&
					   +(1D0-z)**2/n*d2zexct(n,z)
			KernVec(2)=2D0*dnexct(n,z)+n*d2nexct(n,z)-2D0*(1D0+z)*d2nzexct(n,z)&
					   +(1D0+z)**2/n*d2zexct(n,z)
			KernVec(3)=2D0*dnexct(n,z)+n*d2nexct(n,z)-2D0*z*d2nzexct(n,z)&
					   -(1D0-z**2)/n*d2zexct(n,z)
			KernVec(4)=2D0/n/z*dzexct(n,z)

		contains

			real (kind=8) function rs2n(rs)
				implicit none
				real (kind=8)::rs

					rs2n=pi*rs**2
					rs2n=1D0/rs2n
			end function rs2n

			real (kind=8) function n2rs(n)
				implicit none
				real (kind=8)::n

					n2rs=1D0/dsqrt(pi*n)
			end function n2rs

			real (kind=8) function dnz(n,z)
				implicit none
				real (kind=8)::n,z
		
					dnz=0D0
					!dnz=-z/n
			end function dnz

			real (kind=8) function dnrs(n,rs)
				implicit none
				real (kind=8)::n,rs

					dnrs=-1D0/(2D0*dsqrt(pi))*n**(-1.5D0)
			end function dnrs

			real (kind=8) function d2nrs(n,rs)
				implicit none
				real (kind=8)::n,rs

					d2nrs=1.5D0/(2D0*dsqrt(pi))*n**(-2.5D0)
			end function d2nrs

			real (kind=8) function fz(z)
				implicit none
				real (kind=8)::z

					fz=(1D0+z)**(4D0/3D0)+(1D0-z)**(4D0/3D0)-2D0
					fz=fz/(2D0*(2D0**(1D0/3D0)-1D0))
			end function fz

			real (kind=8) function dzfz(z)
				implicit none
				real (kind=8)::z

					dzfz=(1D0+z)**(1D0/3D0)-(1D0-z)**(1D0/3D0)
					dzfz=dzfz*2D0/3D0/(2D0**(1D0/3D0)-1D0)
			end function dzfz

			real (kind=8) function exh(n,z)
				implicit none
				real (kind=8)::n,z

					exh=1D0+(dsqrt(2D0)-1D0)*fz(z)
					exh=-exh*4D0*dsqrt(2D0/pi)/3D0*n**(1.5D0)
			end function exh

			real (kind=8) function dnexh(n,z)
				implicit none
				real (kind=8)::n,z

					dnexh=-2D0*dsqrt(2D0*n/pi)
					dnexh=dnexh*(1D0+(dsqrt(2D0)-1D0)*fz(z))
			end function dnexh

			real (kind=8) function d2nexh(n,z)
				implicit none
				real (kind=8)::n,z

					d2nexh=-dsqrt(2D0/(pi*n))
					d2nexh=d2nexh*(1D0+(dsqrt(2D0)-1D0)*fz(z))
			end function d2nexh

			real (kind=8) function alphay(indy,indalpha,rs)
				implicit none
				integer (kind=4)::indy,indalpha,ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
										  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
										  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
										  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
										  fi(3)=(/-2.069D-2,0D0,0D0/),&
										  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
										  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
										  di(3)=-ai(:)*hi(:)

					alphay=0D0
					ind=indalpha+1
					if (indy.eq.1) then
						alphay=bi(ind)*rs+ci(ind)*rs**2&
							  +di(ind)*rs**3
					elseif (indy.eq.2) then
						alphay=ei(ind)*rs+fi(ind)*rs**(1.5D0)&
							  +gi(ind)*rs**2+hi(ind)*rs**3
					endif
			end function alphay

			real (kind=8) function drsalphay(indy,indalpha,rs)
				implicit none
				integer (kind=4)::indy,indalpha,ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
										  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
										  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
										  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
										  fi(3)=(/-2.069D-2,0D0,0D0/),&
										  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
										  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
										  di(3)=-ai(:)*hi(:)

					drsalphay=0D0
					ind=indalpha+1
					if (indy.eq.1) then
						drsalphay=bi(ind)+2D0*ci(ind)*rs&
							  +3D0*di(ind)*rs**2
					elseif (indy.eq.2) then
						drsalphay=ei(ind)+1.5D0*fi(ind)*dsqrt(rs)&
							  +2D0*gi(ind)*rs+3D0*hi(ind)*rs**2
					endif
			end function drsalphay

			real (kind=8) function d2rsalphay(indy,indalpha,rs)
				implicit none
				integer (kind=4)::indy,indalpha,ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
										  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
										  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
										  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
										  fi(3)=(/-2.069D-2,0D0,0D0/),&
										  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
										  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
										  di(3)=-ai(:)*hi(:)

					d2rsalphay=0D0
					ind=indalpha+1
					if (indy.eq.1) then
						d2rsalphay=2D0*ci(ind)+6D0*di(ind)*rs
					elseif (indy.eq.2) then
						d2rsalphay=0.75D0*fi(ind)/dsqrt(rs)&
								  +2D0*gi(ind)+6D0*hi(ind)*rs
					endif
			end function d2rsalphay

			real (kind=8) function alpha(ind,rs)
				implicit none
				integer (kind=4)::ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/)

					alpha=alphay(2,ind,rs)
					alpha=1D0+1D0/alpha
					alpha=dlog(alpha)
					alpha=alpha*alphay(1,ind,rs)+ai(ind+1)
			end function alpha

			real (kind=8) function dnalpha(ind,rs)
				implicit none
				integer (kind=4)::ind
				real (kind=8)::rs,temp


					dnalpha=dlog(1D0+1D0/alphay(2,ind,rs))*drsalphay(1,ind,rs)
					dnalpha=dnalpha-alphay(1,ind,rs)*drsalphay(2,ind,rs)&
							/alphay(2,ind,rs)/(alphay(2,ind,rs)+1D0)
					dnalpha=dnalpha*dnrs(n,rs)


					!Debug
					!dnalpha=1D0+1D0/alphay(1,ind,rs)
			end function dnalpha

			real (kind=8) function d2nalpha(ind,rs)
				implicit none
				integer (kind=4)::ind
				real (kind=8)::rs,temp


					!dnf1aj
					d2nalpha=-1D0/(1D0+alphay(2,ind,rs))/(alphay(2,ind,rs)**2)&
							*drsalphay(1,ind,rs)*drsalphay(2,ind,rs)
					d2nalpha=d2nalpha+dlog(1D0/(1D0+alphay(2,ind,rs)))*d2rsalphay(1,ind,rs)
					!dnf2aj
					temp=-alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0)*(drsalphay(2,ind,rs)&
						 *drsalphay(1,ind,rs)+alphay(1,ind,rs)*d2rsalphay(2,ind,rs))&
						 +(2D0*alphay(2,ind,rs)+1D0)*alphay(1,ind,rs)*drsalphay(2,ind,rs)**2
					temp=temp/((alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0))**2)


					d2nalpha=d2nalpha+temp
					d2nalpha=d2nalpha*dnrs(n,rs)
					d2nalpha=d2nalpha+dnalpha(ind,rs)*d2nrs(n,rs)/dnrs(n,rs)						
			end function d2nalpha

			real (kind=8) function ex6(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)
					ex6=exh(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*exh(nrs,0D0)
					ex6=ex6/nrs
			end function ex6

			real (kind=8) function dnex6(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)
						
					dnex6=dnexht(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*dnexht(nrs,0D0)
			end function dnex6

			real (kind=8) function d2nex6(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)
						
					d2nex6=d2nexht(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*d2nexht(nrs,0D0)
			end function d2nex6

			real (kind=8) function dnexht(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)
						
					dnexht=dnexh(nrs,z)/n-exh(nrs,z)/n**2
			end function dnexht

			real (kind=8) function d2nexht(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)
						
					d2nexht=d2nexh(nrs,z)/n-2D0*dnexh(nrs,z)/n**2+2D0*exh(nrs,z)/n**3
			end function d2nexht

			real (kind=8) function echt(rs,z)
				implicit none
				real (kind=8)::rs,z
				real (kind=8), parameter::beta=1.3386D0

					echt=(dexp(-beta*rs)-1D0)*ex6(rs,z)
					echt=echt+alpha(0,rs)+alpha(1,rs)*z**2+alpha(2,rs)*z**4
			end function echt

			real (kind=8) function dnecht(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs
				real (kind=8), parameter::beta=1.3386D0

					nrs=rs2n(rs)

					dnecht=(dexp(-beta*rs)-1D0)*dnex6(rs,z)+dnalpha(0,rs)&
					+dnalpha(1,rs)*z**2+dnalpha(2,rs)*z**4&
					-beta*dexp(-beta*rs)*dnrs(nrs,rs)*ex6(rs,z)

					!Debug
					!dnecht=dnalpha(0,rs)
			end function dnecht

			real (kind=8) function d2necht(rs,z)
				implicit none
				real (kind=8)::nrs,z,rs
				real (kind=8), parameter::beta=1.3386D0

					nrs=rs2n(rs)

					d2necht=(dexp(-beta*rs)-1D0)*d2nex6(rs,z)&
					-2D0*beta*dexp(-beta*rs)*dnrs(nrs,rs)*dnex6(rs,z)&
					-beta*dexp(-beta*rs)*d2nrs(nrs,rs)*ex6(rs,z)&
					+ex6(rs,z)*dexp(-beta*rs)*beta*(beta*(dnrs(nrs,rs)**2)-d2nrs(nrs,rs))&
					+d2nalpha(0,rs)+d2nalpha(1,rs)*z**2+d2nalpha(2,rs)*z**4


						!Debug
			end function d2necht

			real (kind=8) function NumDeriv1(f,x,h)
				real (kind=8)::x,h
				real (kind=8), external::f

					NumDeriv1=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
					NumDeriv1=NumDeriv1/12D0/h
			end function NumDeriv1

			real (kind=8) function NumDeriv2(f,x,h)
				real (kind=8)::x,h
				real (kind=8), external::f

				NumDeriv2=f(x-h)-2D0*f(x)+f(x+h)
				NumDeriv2=NumDeriv2/h**2

			end function NumDeriv2

			real (kind=8) function Numfc(n,z)
				implicit none
				real (kind=8)::n,z

					Globz=z
					Numfc=NumDeriv2(help,n,1D-4)
			end function Numfc

			real (kind=8) function Numdnech(n,z)
				implicit none
				real (kind=8)::n,z

					Globz=z
					Numdnech=NumDeriv1(help,n,1D-4)
			end function Numdnech

			real (kind=8) function help(n)
				real (kind=8)::n,rsn
					rsn=n2rs(n)
					help=n*echt(rsn,Globz)
			end function help

			real (kind=8) function dnech(n,z)
				implicit none
				real (kind=8)::n,z,rsn

					rsn=n2rs(n)

					dnech=n*dnecht(rsn,z)+echt(rsn,z)
				
			end function dnech

			real (kind=8) function d2nzexht(n,z)
				implicit none
				real (kind=8)::n,z
				real (kind=8),parameter::param=2D0*(2D0-dsqrt(2D0))/3D0/dsqrt(pi)

				d2nzexht=param/dsqrt(n)*dzfz(z)
				
			end function d2nzexht

			real (kind=8) function d2nzex6(n,z)
				implicit none
				real (kind=8)::n,z
				
				d2nzex6=(z+z**3/8D0)/dsqrt(2D0*pi*n)+d2nzexht(n,z)
			end function d2nzex6

			real (kind=8) function d2nzecht(n,z)
				implicit none
				real (kind=8)::n,z,rsn
				real (kind=8), parameter::beta=1.3386D0

				rsn=n2rs(n)
				d2nzecht=-beta*dexp(-beta*rsn)*dnrs(n,rsn)*dzex6()&
						 +(dexp(-beta*rsn)-1D0)*d2nzex6(n,z)&
						 +2D0*z*dnrs(n,rsn)*drsalpha(1,rsn)&
						 +4D0*z**3*dnrs(n,rsn)*drsalpha(2,rsn)
			end function d2nzecht

			real (kind=8) function dzex6(n,z)
				implicit none
				real (kind=8)::n,z
				
					dzex6=dsqrt(2D0*n/pi)*(z+z**3/8D0)+dzexht(n,z)
			end function dzex6

			real (kind=8) function dzexht(n,z)
				implicit none
				real (kind=8)::n,z


					dzexht=-4D0*dsqrt(2D0*n/pi)/3D0*(dsqrt(2D0)-1D0)*dzfz(z)
			end function dzexht

			real (kind=8) function d2zexht(n,z)
				implicit none
				real (kind=8)::n,z


					d2zexht=-4D0*dsqrt(2D0*n/pi)/3D0*(dsqrt(2D0)-1D0)*d2zfz(z)
			end function d2zexht

			real (kind=8) function d2zfz(n,z)
				implicit none
				real (kind=8)::n,z
				real (kind=8),parameter::param=2D0/9D0/(2D0**(1D0/3D0)-1D0)

					d2zfz=param*((1D0+z)**(-2D0/3D0)+(1D0-z)**(-2D0/3D0))
			end function d2zfz

			real (kind=8) function d2zecht(n,z)
				implicit none
				real (kind=8)::n,z,rsn
				real (kind=8), parameter::beta=1.3386D0

					rsn=n2rs(n)
					d2zecht=(dexp(-beta*rsn)-1D0)*d2zex6(n,z)&
							+2D0*alpha(1,rsn)+12D0*z**2*alpha(2,rsn)
			end function d2zcht

			real (kind=8) function d2zex6(n,z)
				implicit none
				real (kind=8)::n,z


					d2zex6=d2zexht(n,z)+(1D0+3D0/8D0*z**2)*dsqrt(2D0*n/pi)
			end function d2zex6

			real (kind=8) function d2nzexc(n,z)
				implicit none
				real (kind=8)::n,z


					d2nzexc=dzecht(n,z)+dzexht(n,z)&
							+n*(d2nzecht(n,z)+d2nzexht(n,z))
			end function d2nzexc

			real (kind=8) function d2nzexct(n,z)
				implicit none
				real (kind=8)::n,z


					d2nzexct=(d2nzecht(n,z)+d2nzexht(n,z))
			end function d2nzexct

			real (kind=8) function d2nexct(n,z)
				implicit none
				real (kind=8)::n,z


					d2nexct=(d2necht(n,z)+d2nexht(n,z))
			end function d2nexct

			real (kind=8) function d2zexct(n,z)
				implicit none
				real (kind=8)::n,z


					d2zexct=(d2zecht(n,z)+d2zexht(n,z))
			end function d2zexct

			real (kind=8) function d2nexht(n,z)
				implicit none
				real (kind=8)::n,z


					d2nexht=
			end function d2nexht

			real (kind=8) function d2necht(n,z)
				implicit none
				real (kind=8)::n,z


					d2necht=
			end function d2necht


function d2nexht
function d2necht



	end subroutine KernelConstr


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



	
	real (kind=8) function ReChi0(q,w,Svec)
		implicit none
		real (kind=8)::Svec(4),q(2),w

		call Integrate(ReChi0Int,blah,ReChi0)
		

	end function ReChi0

	real (kind=8) function ReChi0Int(q,w,k,Svec)
		implicit none
		real (kind=8)::Svec(4),q(2),k(2),w

		if (Svec.eq.UUUU) then
			ReChi0Int=IntAux(-1D0,-1D0,-1D0,k+q)-IntAux(-1D0,-1D0,-1D0,k)
		elseif (Svec.eq.DUDU) then
			ReChi0Int=IntAux(-1D0,1D0,1D0,k+q)-IntAux(-1D0,1D0,-1D0,k)
		elseif (Svec.eq.UDUD) then
			ReChi0Int=IntAux(1D0,-1D0,-1D0,k+q)-IntAux(1D0,-1D0,1D0,k)
		elseif (Svec.eq.DDDD) then
			ReChi0Int=IntAux(1D0,1D0,1D0,k+q)-IntAux(1D0,1D0,1D0,k)
		endif
		
		contains
			real (kind=8) function IntAux(a,b,c,q,w)
					real (kind=8):: a,b,c,q(2),w

						IntAux=w-En(a,k)+En(b,k-q)
						IntAux=IntAux/(IntAux**2+eta**2)*f(c,k)
			end function IntAux

	end function ReChi0Int

	subroutine RespCnstr(q,w,Chi)
		real (kind=8)::  Chi0UUUU,Chi0DDDD,Chi0UUDD,Chi0DDUU,fHxcUUUU,fHxcDDDD&
						,fHxcUDUD,fHxcDUDU,fxcUDUD,fxcDUDU,M,q(2),w
		real (kind=8), intent(out)::Chi(4,4)
		!real (kind=8), intent(in)::KernVec(4)

			! Init
			Chi0UUUU=ReChi0(q,w,uu)
			Chi0DDDD=ReChi0(q,w,dd)
			Chi0UDUD=ReChi0(q,w,ud)
			Chi0DUDU=ReChi0(q,w,du)
			fHxcUUUU=Vc(q)+GlobKernVec(1)
			fHxcDDDD=Vc(q)+GlobKernVec(2)
			fHxcDDUU=Vc(q)+GlobKernvec(3)
			fHxcUUDD=Vc(q)+GlobKernvec(3)
			fxcUDUD=GlobKernvec(4)
			fxcDUDU=GlobKernvec(4)
			M=(1D0-Chi0UUUU*fHxcUUUU)*(1D0-Chi0DDDD*fHxcDDDD)&
			  -Chi0UUUU*Chi0DDDD*fHxcUDUD*fHxcDUDU
			Chi(:,:)=0D0

			Chi(1,1)=(1D0-Chi0DDDD*fHxcDDDD)*Chi0UUUU/M
			Chi(4,1)=Chi0UUUU*fHxcUDUD*Chi0DDDD/M
			Chi(1,4)=Chi0DDDD*fHxcDUDU*Chi0UUUU/M
			Chi(4,4)=(1D0-Chi0UUUU*fHxcUUUU)*Chi0DDDD/M
			Chi(2,2)=Chi0UDUD/(1D0-Chi0UDUD*fxcUDUD)
			Chi(3,3)=Chi0DUDU/(1D0-Chi0DUDU*fxcDUDU)

		



	end subroutine RespCnstr

	real (kind=8) function Det(Mat)
		real (kind=8):: Mat(4,4),sgn
		integer (kind=4)::i,ipiv(4),info,num

		ipiv=0
		num=4

		call dgetrf(num, num, Mat, num, ipiv, info)
		Det=1D0
		do i=1,num
			Det = Det*Mat(i,i)
		end do

		sgn=1D0
		do i=1,num
			if(ipiv(i).ne.i)then
				sgn=-sgn
			endif
		enddo
		Det=sgn*Det	
	end function Det


	subroutine EigVals(Mat,evs,Num)
		integer (kind=4), intent(in)::Num
		integer (kind=4)            ::evinfo,ldvl,ldvr,lwork
		real (kind=8), intent(in)   ::Mat(Num,Num)
		real (kind=8), intent(out)  ::evs(Num)
		real (kind=8)               ::temp(Num,Num),wi(Num),vr(Num),vl(Num)
		

		temp=Mat
		ldvl=1
		ldvr=1
		lwork=4*Num

		call dgeev('N','N',Num,Mat,Num,evs,vl,ldvl,vr,ldvr,work,lwork,evinfo)
		

		if (evinfo.ne.0) then
			Globevinfo=evinfo
			call ErrExit()
		endif		



	end subroutine EigVals


	subroutine ErrExit()
		if (evinfo.lt.0) then
			write(*,*) -evinfo, "th argument had an illegal value"
		elseif (evinfo.gt.0)
			write(*,*) evinfo
			write(*,*) "the QR algorithm failed to compute all the"&
					,"eigenvalues, and no eigenvectors have been computed;"&
                	,"elements i+1:N of WR and WI contain eigenvalues which"&
                	,"have converged."&
		endif

		call EXIT(-1)
	end subroutine ErrExit






end module SpinWave


























