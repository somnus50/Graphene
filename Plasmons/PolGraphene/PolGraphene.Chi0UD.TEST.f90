program PolGraphenePlasmons
use readlib
use Constants
use Globals
use PhysicsPolGraphene
use AuxiliaryFunctions
use Response
use GV
use fxc2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,PlasmonArray(4,20,3),wstart,qhat(2),w0,Chi0(4),Chi(4,4),n,z,w
real (kind=8), allocatable::LindLine(:),EpsLine(:),qwgrid(:,:)&
						   ,Chigrid(:,:),EV(:,:),EVwline(:,:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,labelnum,temp,k
integer (kind=4), allocatable::labels(:),info(:,:)
character (len=32)::fmt1,fmt2,star,fmt3
character (len=32),allocatable::empty(:)




	!!!! Initialize some situational constants
	Globgs=4D0
	Globgv=2D0
	n=1D12/cmSq2a0sq
	z=-4D-2
	call ConstInit(n,z)
	call KernelConstr(Globrs,z,GlobKernVec)
	!write(*,*) GlobKernVec
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	labelnum=4
	star='(A)'
	fmt3='(4I10)'
	allocate(labels(labelnum), empty(labelnum), qwgrid(labelnum,3), info(labelnum,4),EV(2,labelnum)) 

	!!! Create labels for files
	temp=19
	do i=1,labelnum
		labels(i)=temp
		temp=temp+10
	enddo

	!!! Open files
	open(unit=labels(1), file='PolGraphene.Chi0UU.small.FAKE.dat', status='old', action='read')
	open(unit=labels(2), file='PolGraphene.Chi0DD.small.FAKE.dat', status='old', action='read')
	open(unit=labels(3), file='PolGraphene.Chi0UD.z4.small.q1.dat', status='old', action='read')
	open(unit=labels(4), file='PolGraphene.Chi0DU.z4.small.q1.dat', status='old', action='read')
	open(unit=9, file='PolGraphene.Chi0UD.TEST.dat')
	write(9,*) "# q/kf,wp/ef,low q limit"

	!!! Throw out comment lines
	call multiread(labels,star,empty)
	call multiread(labels,star,empty)
	call multiread(labels,star,empty)

	!!! Read the metadata at the top of the file
	call multiread(labels,fmt3,info)
	!!! Sanity check to make sure all the data files are compatible
	do i=2,labelnum
		do j=1,4
			if ((info(1,j)).ne.(info(i,j))) then
				write(*,*) "Data file parameters don't match. Please investigate."
				!call exit(-1)
			endif
		enddo
	enddo
	NProc=info(1,1)
	IntSteps=info(1,2)
	qsteps=info(1,3)
	wsteps=info(1,4)

	!!! Throw out comment lines
	call multiread(labels,star,empty) 
	call multiread(labels,star,empty) 

	allocate(Chigrid(labelnum,wsteps),EVwline(labelnum,wsteps))
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	!write(*,*) fmt2
	allocate(LindLine(wsteps),EpsLine(wsteps))
	w0=4D0*GlobEf/2D0!/kappa*e**2
	w0=dsqrt(w0)
	qhat=(/0D0,-1D0/)

	!write(*,*) "kf",Globkf0
	!write(*,*) "Ef",GlobEf
	!write(*,*) GlobKernVec(4), GlobKernVec(4)*n*z/2D0

	do i=1,qsteps
		!write(*,*) "qstep:",i
		q=0D0
		call multiread(labels,fmt1,qwgrid)
		!Do something to readvals (q, wstart, dw)
		q=-qwgrid(3,1)*Globkf0
		!write(*,*) q
		w=qwgrid(3,2)
		dw=qwgrid(3,3)
		!write(*,*) "q",q
		write(9,*)
		write(9,fmt2) q,w,dw
		call multiread(labels,fmt2,Chigrid) !read(19,fmt2) LindLine
		do j=1,wsteps
			Chi0=Chigrid(:,j)/2D0
			Chi0(2)=ReChi0UDLowq(q,w)
			Chi0(1)=Chi0(2)-Chi0(3)
			EVwline(:,j)=Chi0
			if (i.eq.1) then
				write(*,*) w, Chi0(3), Chi0(2)
			endif
			w=w+dw
		enddo
		do j=1,3
			!write(9,fmt2) EVwline(j,:)
		enddo
	enddo

	!!! Close files
	do i=1,labelnum
		close(unit=labels(i))
	enddo
	close(unit=9)

end program PolGraphenePlasmons
















