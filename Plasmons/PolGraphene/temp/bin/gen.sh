#!/bin/bash


for i in {1..10}
do
cat << EOF > jobUD$i.pbs
#!/bin/bash
#PBS -q batch
#PBS -l nodes=1:ppn=1
#PBS -l walltime=172:00:00
#PBS -N GrapheneChi0UD.$i
#PBS -e pbs.$i.err
#PBS -o pbs.$i.out
#PBS -m abe
#PBS -M palicu92@gmail.com
cd \$PBS_O_WORKDIR

./PolGraphene.Chi0UD.$i.bin > PolGraphene.Chi0UD.$i.bin.log
EOF
done
