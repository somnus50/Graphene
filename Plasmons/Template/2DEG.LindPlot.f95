program LindPlot
	use Constants
	use Globals	
	use Physics2DEG
	use AuxiliaryFunctions
	implicit none
	real    (kind=8):: q(2),G(2),w
	integer (kind=4):: i,j,steps,stat

	call ConstInit(5D-1,0D0,GlobEf,Globeta,Globfx,Globfc)

	q = 1D0*(/1D0,0D0/)!pi/(2D0*alat)/)
	G = (/0D0,0D0/)
	steps=100
	open(unit=9, file='LindPlot.dat')
	write(9,*) "# w/Ef"!                      ReNLind                     ImNLind"

	w=0D0
	do i=1,steps
		write(*,*) i
		w = 3D0*real(i)*GlobEf/real(steps)
		
		write(9,*) w/GlobEf,ReNLind(q,w)!,ReNLind(q,w)!,ReNLind2(q,w)!,ReNEps(q,w,G)!,KsiG((/w,0D0/),(/0D0,0D0/))

	enddo

	close(unit=9)

end program LindPlot
