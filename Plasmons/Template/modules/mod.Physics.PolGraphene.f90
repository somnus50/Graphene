module PhysicsPolGraphene
use Constants
use Globals
implicit none
real (kind=8), parameter::  UUUU(4)=(/1D0,1D0,1D0,1D0/),&      !Svec=1
							DDDD(4)=(/-1D0,-1D0,-1D0,-1D0/),&  !Svec=2
							UDUD(4)=(/1D0,-1D0,1D0,-1D0/),&    !Svec=3
							DUDU(4)=(/-1D0,1D0,-1D0,1D0/)      !Svec=4



contains

	subroutine ConstInit(n,z)
		use Globals
		use fxc2DEG
		implicit none
		!integer (kind=4) ::
		real    (kind=8), intent(in) ::n,z
		!real    (kind=8), intent(out)::Ef,eta
		real    (kind=8)             ::rs
		!real    (kind=8)::


			! Easy Stuff
			!Ef=8D0*t*frac !Specific to how I calculated the Lindhard Matrix
			!n=Ef/pi
			rs=1D0/kappa/gam
			Globkf0=dsqrt(4D0*pi*n/Globgs/Globgv) !dsqrt(4D0*pi*n/(gs*gv))
			GlobEf=gam*Globkf0
			Globn=n
			Globeta=1D-10!*GlobEf
			GlobkfU=Globkf0*dsqrt((1D0+z))
			GlobkfD=Globkf0*dsqrt((1D0-z))
			GlobvfU=hbar*GlobkfU/m
			GlobvfD=hbar*GlobkfD/m
			GlobkfMAX=max(GlobkfU,GlobkfD)
			Globrs=rs!n2rs(n)!rs
			GLobZeem=-2D0*GlobEf*z
			HalfGlobZeem=GlobZeem/2D0
			Globz=z
			Globzt=dsqrt(1D0-z)+dsqrt(1D0+z)
			Globzt=Globzt/dsqrt(2D0)



	end subroutine ConstInit

	real (kind=8) function Norm(v)
		implicit none
		real (kind=8) ::v(2)

		!do i=1,len(v)
		Norm=(v(1))**2+(v(2))**2
		Norm=dsqrt(Norm)
	end function

	real (kind=8) function EnDC(s,q)
		implicit none
		real (kind=8) ::q(2),s


			EnDC=s*GlobZeem/2D0+gam*q(1)  ! Don't need to dabs(q(1)) since it is a polar vector.
	end function EnDC

	real (kind=8) function En(s,qp)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::s,qp(2),qc(2),temp
		real (kind=8), parameter::DCshift(2)=(/0D0,BZkMAX/)

			call Pol2Cart(qp,qc)
			temp=-anc(qc+DCshift) !temp=-b*anc(qc)    !!!! Only considering upper band right now.
			En=tTBEh*temp!+e2p !e2p usually zero. Trying to be efficient.
			En=En/(1D0+sTB*temp)+s*HalfGlobZeem

		contains
			real (kind=8) function anc(q)
				implicit none
				real (kind=8) :: q(2)
				real (kind=8),parameter::temp1=dsqrt(3D0)/2D0
				! Ancillary function

				anc=dcos(q(2)*HalfaGraphene)
				anc=dcos(q(1)*aGraphene*temp1)*anc+anc**2
				anc=1D0+4D0*anc
				anc=dsqrt(anc)
			end function anc
	end function En

	real (kind=8) function FermiVec(q,delta)
		implicit none
		real (kind=8)::q(2),delta

		!FermiVec=Globkf
	end function FermiVec

	real (kind=8) function f(s,q)
		implicit none
		real (kind=8) ::q(2),s
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector

			f=0D0
			!!!Zero Temperature
			if ((EnDC(s,q)).le.(GlobEf)) then
				f=1D0
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(s,q)-GlobEf)/kT))
	end function f

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector

		NVc=2D0*pi/Norm(q)!*e**2/kappa
	end function NVc

	real (kind=8) function IntAux(a,b,c,k,q,w)
		use AuxiliaryFunctions
		real (kind=8):: a,b,c,q(2),w,k(2)

			IntAux=0D0
			if (f(a,k).gt.(5D-1)) then
				IntAux=w+c*(En(a,k)-En(b,PolAdd(k,(/c*q(1),q(2)/)))) !c=1 -> k+q , c=-1 -> k-q
				IntAux=IntAux/(IntAux**2+Globeta**2)!*f(c,kp)
			endif
	end function IntAux

	real (kind=8) function IntAux1(a,b,k,q,w)  ! = IntAux(a,b,1,q,w)
		use AuxiliaryFunctions
		real (kind=8):: a,b,c,q(2),w,k(2),kp(2)

			IntAux1=0D0
			if (f(a,k).gt.(5D-1)) then
				kp=PolAdd2(k,q)
				IntAux1=w+En(a,k)-En(b,kp)
				IntAux1=IntAux1/(IntAux1**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))!*f(c,kp)
			endif
	end function IntAux1

	real (kind=8) function IntAux2(a,b,k,q,w)  ! = IntAux(a,b,-1,q,w)
		use AuxiliaryFunctions
		real (kind=8):: a,b,c,q(2),w,k(2),kp(2)

			IntAux2=0D0
			if (f(a,k).gt.(5D-1)) then
				kp=PolAdd2(k,(/-q(1),q(2)/))
				IntAux2=w-En(a,k)+En(b,kp)
				IntAux2=IntAux2/(IntAux2**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))!*f(c,kp)
			endif
	end function IntAux2

	real (kind=8) function ReChi0UUInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UUInt=IntAux(1D0,1D0,1D0,k,q,w)-IntAux(1D0,1D0,-1D0,k,q,w)
		ReChi0UUInt=IntAux1(1D0,1D0,k,q,w)-IntAux2(1D0,1D0,k,q,w)
	end function ReChi0UUInt

	real (kind=8) function ReChi0UDIntraU(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp

		ReChi0UDIntraU=0D0
		kp=PolAdd(q,k)
		temp=f(-1D0,k)-f(1D0,kp)
		if (temp**2.gt.5D-1) then
			temp=temp*(w+En(-1D0,k)-En(1D0,kp))
			ReChi0UDIntraU=temp/(temp**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))
		endif
		!ReChi0UDIntraU=IntAux1(-1D0,1D0,k,q,w)-IntAux2(1D0,-1D0,k,q,w)
	end function ReChi0UDIntraU

	real (kind=8) function ReChi0UDLowq(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,w,temp,w2

			w2=w-GlobZeem
			temp=dlog((1D0-Globz)/(1D0+Globz))/8D0/Globkf0**2
			temp=temp+gam**2*Globz/2D0/w2**2
			temp=temp-gam*Globzt/2D0/w2/Globkf0
			temp=temp*q**2+Globz
			ReChi0UDLowq=-temp*Globn/w2
	end function ReChi0UDLowq

	real (kind=8) function ReChi0DUIntraU(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp

		ReChi0DUIntraU=0D0
		kp=PolAdd(q,k)
		temp=f(1D0,k)-f(-1D0,kp)
		if (temp**2.gt.5D-1) then
			temp=temp*(w+En(1D0,k)-En(-1D0,kp))
			ReChi0DUIntraU=temp/(temp**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))
		endif
		!ReChi0DUIntraU=IntAux1(1D0,-1D0,k,q,w)-IntAux2(-1D0,1D0,k,q,w)
	end function ReChi0DUIntraU

	real (kind=8) function ReChi0UUIntraU(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp

		ReChi0UUIntraU=0D0
		kp=PolAdd(q,k)
		temp=f(1D0,k)-f(1D0,kp)
		if (temp**2.gt.5D-1) then
			temp=temp*(w+En(1D0,k)-En(1D0,kp))
			ReChi0UUIntraU=temp/(temp**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))
		endif
		!ReChi0DUIntraU=IntAux1(1D0,-1D0,k,q,w)-IntAux2(-1D0,1D0,k,q,w)
	end function ReChi0UUIntraU

	real (kind=8) function ReChi0UUIntraU2(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),km(2),w

		ReChi0UUIntraU2=0D0
		kp=PolAdd(q,k)
		km=PolAdd((/q(1),q(2)+pi/),k) ! the pi makes it a subtraction

		if (f(1D0,k).gt.5D-1) then
			ReChi0UUIntraU2=anc(k,kp)-anc(km,k)
		endif

	contains
		real (kind=8) function anc(k1,k2)
			implicit none
			real (kind=8)::k1(2),k2(2),temp

				temp=w+En(1D0,k1)-En(1D0,k2)
				anc=temp/(temp**2+Globeta**2)*(1D0+dcos(k2(2)-k1(2)))
		end function anc
	end function ReChi0UUIntraU2

	real (kind=8) function ImChi0UUIntraU2(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),km(2),w

		ImChi0UUIntraU2=0D0
		kp=PolAdd(q,k)
		km=PolAdd((/q(1),q(2)+pi/),k) ! the pi makes it a subtraction

		if (f(1D0,k).gt.5D-1) then
			ImChi0UUIntraU2=-anc(km,k)+anc(k,kp)!-anc(km,k)
		endif

	contains
		real (kind=8) function anc(k1,k2)
			implicit none
			real (kind=8)::k1(2),k2(2),temp

				temp=w+EnDC(1D0,k1)-EnDC(1D0,k2)
				anc=-(1D0+dcos(k2(2)-k1(2)))*Globeta/(temp**2+Globeta**2)
		end function anc
	end function ImChi0UUIntraU2

	real (kind=8) function ImChi0UUIntraU(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp

		ImChi0UUIntraU=0D0
		kp=PolAdd(q,k)
		temp=f(1D0,k)-f(1D0,kp)
		if (temp**2.gt.5D-1) then
			temp=temp*(w+EnDC(1D0,k)-EnDC(1D0,kp))
			ImChi0UUIntraU=Globeta/(temp**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))
		endif
		!ReChi0DUIntraU=IntAux1(1D0,-1D0,k,q,w)-IntAux2(-1D0,1D0,k,q,w)
	end function ImChi0UUIntraU

	real (kind=8) function ImChi0DUIntraU(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp

		ImChi0DUIntraU=0D0
		kp=PolAdd(q,k)
		temp=f(-1D0,k)-f(1D0,kp)
		if (temp**2.gt.0D0) then
			temp=temp*(w+EnDC(-1D0,k)-EnDC(1D0,kp))
			ImChi0DUIntraU=1D0!Globeta/(temp**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))
		endif
		!ReChi0DUIntraU=IntAux1(1D0,-1D0,k,q,w)-IntAux2(-1D0,1D0,k,q,w)
	end function ImChi0DUIntraU

	real (kind=8) function ReChi0DDIntraU(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp

		ReChi0DDIntraU=0D0
		kp=PolAdd(q,k)
		temp=f(-1D0,k)-f(-1D0,kp)
		if (temp**2.gt.5D-1) then
			temp=temp*(w+En(-1D0,k)-En(-1D0,kp))
			ReChi0DDIntraU=temp/(temp**2+Globeta**2)*(1D0+dcos(k(2)-kp(2)))
		endif
		!ReChi0DUIntraU=IntAux1(1D0,-1D0,k,q,w)-IntAux2(-1D0,1D0,k,q,w)
	end function ReChi0DDIntraU

	real (kind=8) function ReChi0UUInt1(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UUInt=IntAux(1D0,1D0,1D0,k,q,w)-IntAux(1D0,1D0,-1D0,k,q,w)
		ReChi0UUInt1=IntAux1(1D0,1D0,k,q,w)
	end function ReChi0UUInt1

	real (kind=8) function ReChi0UUInt2(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UUInt=IntAux(1D0,1D0,1D0,k,q,w)-IntAux(1D0,1D0,-1D0,k,q,w)
		ReChi0UUInt2=IntAux2(1D0,1D0,k,q,w)
	end function ReChi0UUInt2

	real (kind=8) function ReChi0DDInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0DDInt=IntAux(-1D0,-1D0,1D0,k,q,w)-IntAux(-1D0,-1D0,-1D0,k,q,w)
		ReChi0DDInt=IntAux1(-1D0,-1D0,k,q,w)-IntAux2(-1D0,-1D0,k,q,w)
	end function ReChi0DDInt

	real (kind=8) function ReChi0UDInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UDInt=IntAux(-1D0,1D0,1D0,k,q,w)-IntAux(1D0,-1D0,-1D0,k,q,w)
		ReChi0UDInt=IntAux1(-1D0,1D0,k,q,w)-IntAux2(1D0,-1D0,k,q,w)
	end function ReChi0UDInt

	real (kind=8) function ReChi0UDIntAux1(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UDInt=IntAux(-1D0,1D0,1D0,k,q,w)-IntAux(1D0,-1D0,-1D0,k,q,w)
		ReChi0UDIntAux1=IntAux1(-1D0,1D0,k,q,w)!-IntAux2(1D0,-1D0,k,q,w)
	end function ReChi0UDIntAux1

	real (kind=8) function ReChi0UDIntAux2(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UDInt=IntAux(-1D0,1D0,1D0,k,q,w)-IntAux(1D0,-1D0,-1D0,k,q,w)
		ReChi0UDIntAux2=-IntAux2(1D0,-1D0,k,q,w)
	end function ReChi0UDIntAux2

	real (kind=8) function ReChi0DUIntAux1(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UDInt=IntAux(-1D0,1D0,1D0,k,q,w)-IntAux(1D0,-1D0,-1D0,k,q,w)
		ReChi0DUIntAux1=IntAux1(1D0,-1D0,k,q,w)!-IntAux2(1D0,-1D0,k,q,w)
	end function ReChi0DUIntAux1

	real (kind=8) function ReChi0DUIntAux2(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0UDInt=IntAux(-1D0,1D0,1D0,k,q,w)-IntAux(1D0,-1D0,-1D0,k,q,w)
		ReChi0DUIntAux2=-IntAux2(-1D0,1D0,k,q,w)
	end function ReChi0DUIntAux2

	real (kind=8) function ReChi0DUInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ReChi0DUInt=IntAux(1D0,-1D0,1D0,k,q,w)-IntAux(-1D0,1D0,-1D0,k,q,w)
		ReChi0DUInt=IntAux1(1D0,-1D0,k,q,w)-IntAux2(-1D0,1D0,k,q,w)
	end function ReChi0DUInt

	real (kind=8) function PolIntegratek(fn,a,b,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: i,j,steps,ier
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),res
		real (kind=8), external:: fn
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps, kind=8)
			dphi=twopi/real(steps, kind=8)
			PolIntegratek=0D0
			k=a
			phi=0D0
			do i=1,steps+1
				k=a
				do j=1,steps
					!kv=(/k,phi/)
					!PolIntegratek=PolIntegratek+k*fn(q,w,(/k,phi/))
					call StupidIntegrateNC(anc,k,k+dk,1,res,ier)
					PolIntegratek=PolIntegratek+res
					!PolIntegratek=PolIntegratek+NC10Pol(k,k+dk,phi,fn,q,w)
					k=k+dk
				enddo
				phi=phi+dphi
			enddo
			!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point
			PolIntegratek=PolIntegratek*dphi!/fourpisq     !NC10


		contains

			real (kind=8) function anc(k)
				implicit none
				real (kind=8)::k


				anc=k*fn(q,w,(/k,phi/))
			end function anc

				real (kind=8) function NC10Pol(a,b,phi,fn,q,w)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,phi,q(2),w
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10Pol=0D0
					x=a
					dx=(b-a)/9D0
					do i=1,10
						NC10Pol=NC10Pol+Newt10(i)*x*fn(q,w,(/x,phi/))
						x=x+dx
					enddo
					NC10Pol=NC10Pol*dx
				end function NC10Pol


	end function PolIntegratek



	real (kind=8) function PolIntegratek2(fn,a,b,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: i,j,steps,ier,phisteps,t1,t2
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),res,temp
		real (kind=8),allocatable::temp1(:)
		real (kind=8), external:: fn
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			!phisteps=(steps/9)*9 +1
			phisteps=(100/9)*9 +1
			!dk=(b-a)/real(steps, kind=8)
			dphi=twopi/real(phisteps, kind=8)
			PolIntegratek2=0D0
			k=a
			phi=0D0
			allocate(temp1(phisteps))
			do i=1,phisteps
				!k=a
				!temp=0D0
				!do j=1,steps
					!kv=(/k,phi/)
					!PolIntegratek=PolIntegratek+k*fn(q,w,(/k,phi/))
					res=0D0
					call StupidIntegrateNC(anc,a,b,steps,res,ier)
					!temp=temp+res
					!PolIntegratek2=PolIntegratek2+res
					!PolIntegratek=PolIntegratek+NC10Pol(k,k+dk,phi,fn,q,w)
					!k=k+dk
				!enddo
				temp1(i)=res!temp
				phi=phi+dphi
			enddo
			!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


			t1=1
			t2=10
			do i=1,(phisteps/9)
				PolIntegratek2=PolIntegratek2+NC10PolList(dphi,temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratek2=PolIntegratek2*dphi    !/fourpisq     !NC10


		contains

			real (kind=8) function anc(k)
				implicit none
				real (kind=8)::k


				anc=k*fn(q,w,(/k,phi/))
			end function anc

				real (kind=8) function NC10Pol(a,b,phi,fn,q,w)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,phi,q(2),w
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10Pol=0D0
					x=a
					dx=(b-a)/9D0
					do i=1,10
						NC10Pol=NC10Pol+Newt10(i)*x*fn(q,w,(/x,phi/))
						x=x+dx
					enddo
					NC10Pol=NC10Pol*dx
				end function NC10Pol

				real (kind=8) function NC10PolList(dx,flist)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10PolList=0D0
					do i=1,10
						NC10PolList=NC10PolList+Newt10(i)*flist(i)
					enddo
					!NC10PolList=NC10PolList*dx/9D0
				end function NC10PolList
	end function PolIntegratek2



		real (kind=8) function PolIntegratek3(fn,a,b,steps,q,w)
			use AuxiliaryFunctions
			implicit none
			integer (kind=4):: i,j,steps,ier,phisteps,t1,t2
			real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),res,temp
			real (kind=8),allocatable::temp1(:)
			real (kind=8), external:: fn
			real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
			! 2D polar integrator over k and phi.
			! S fn(q,w)*dk*dphi
			! fn    :: real, external function to be integrated
			! steps :: integer, number of
			! q,w   :: real, arguments of fn


				!phisteps=(steps/9)*9 +1
				phisteps=(100/9)*9 +1
				dk=(b-a)/real(steps, kind=8)
				dphi=twopi/real(phisteps, kind=8)
				PolIntegratek3=0D0
				k=a
				phi=0D0
				allocate(temp1(phisteps))
				do i=1,phisteps
					k=a
					temp=0D0
					write(*,*) "phi:",i,"of",phisteps
					do j=1,steps
						res=0D0
						!write(*,*) "IntStep:",i,j,k/Globkf0, q/Globkf0, w
						call GaussAdaptQuad(anc,k,k+dk,1D-8,res)
						temp=temp+res
						k=k+dk
					enddo
					temp1(i)=temp
					phi=phi+dphi
				enddo
				!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


				t1=1
				t2=10
				do i=1,(phisteps/9)
					PolIntegratek3=PolIntegratek3+NC10PolList(dphi,temp1(t1:t2))!*dphi!/fourpisq     !NC10
					t1=t2
					t2=t2+9
				enddo

				PolIntegratek3=PolIntegratek3*dphi    !/fourpisq     !NC10


			contains

				real (kind=8) function anc(k)
					implicit none
					real (kind=8)::k

					anc=k*fn(q,w,(/k,phi/))
				end function anc

				real (kind=8) function NC10PolList(dx,flist)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10PolList=0D0
					do i=1,10
						NC10PolList=NC10PolList+Newt10(i)*flist(i)
					enddo
					!NC10PolList=NC10PolList*dx/9D0
				end function NC10PolList
		end function PolIntegratek3

		real (kind=8) function PolIntegratek4(fn,a,b,steps,q,w)
			use AuxiliaryFunctions
			implicit none
			integer (kind=4):: i,j,steps,ier,phisteps,t1,t2
			real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),res,temp,res1,res2,c,res3
			real (kind=8),allocatable::temp1(:)
			real (kind=8), external:: fn
			real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
			! 2D polar integrator over k and phi.
			! S fn(q,w)*dk*dphi
			! fn    :: real, external function to be integrated
			! steps :: integer, number of
			! q,w   :: real, arguments of fn


				!phisteps=(steps/9)*9 +1
				phisteps=(100/9)*9 +1
				dk=(b-a)/real(steps, kind=8)
				dphi=twopi/real(phisteps, kind=8)
				PolIntegratek4=0D0
				k=a
				phi=0D0
				!c=q(1)*dsec(phi)
				allocate(temp1(phisteps))
				do i=1,phisteps
					k=a
					temp=0D0
					!write(*,*) "phi:",i,"of",phisteps
					!do j=1,steps
						write(*,*) "IntStep:",i,k/Globkf0, phi, q/Globkf0, w
						res1=0D0
						res2=0D0
						res3=0D0
						c=dabs(q(1)/dcos(phi))
						if (c.gt.b) then
							call GaussAdaptQuad(anc,a,b,1D-8,res1)
							res2=0D0
							res3=0D0
						else
							call GaussAdaptQuad(anc,a,c-1D-2/dabs(b-a),1D-8,res1)
							call GaussAdaptQuad(anc,c+1D-2/dabs(b-a),b,1D-8,res2)
							call StupidIntegrateNC(anc,c-1D-2/dabs(b-a),c+1D-2/dabs(b-a),steps,res3,ier)
						endif


						temp=temp+res1+res2+res3
						k=k+dk
					!enddo
					temp1(i)=temp
					phi=phi+dphi
				enddo
				!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


				t1=1
				t2=10
				do i=1,(phisteps/9)
					PolIntegratek4=PolIntegratek4+NC10PolList(dphi,temp1(t1:t2))!*dphi!/fourpisq     !NC10
					t1=t2
					t2=t2+9
				enddo

				PolIntegratek4=PolIntegratek4*dphi    !/fourpisq     !NC10


			contains

				real (kind=8) function anc(k)
					implicit none
					real (kind=8)::k

					anc=k*fn(q,w,(/k,phi/))
				end function anc

				real (kind=8) function NC10PolList(dx,flist)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10PolList=0D0
					do i=1,10
						NC10PolList=NC10PolList+Newt10(i)*flist(i)
					enddo
					!NC10PolList=NC10PolList*dx/9D0
				end function NC10PolList
		end function PolIntegratek4


	real (kind=8) function ImIntAux1(a,b,k,q,w)  ! = IntAux(a,b,1,q,w)
		use AuxiliaryFunctions
		real (kind=8):: a,b,c,q(2),w,k(2)

			ImIntAux1=0D0
			if (f(a,k).gt.(5D-1)) then
				ImIntAux1=w+En(a,k)-En(b,PolAdd(k,(/q(1),q(2)/)))
				ImIntAux1=-Globeta/(ImIntAux1**2+Globeta**2)!*f(c,kp)
			endif
	end function ImIntAux1

	real (kind=8) function ImIntAux2(a,b,k,q,w)  ! = IntAux(a,b,-1,q,w)
		use AuxiliaryFunctions
		real (kind=8):: a,b,c,q(2),w,k(2)

			ImIntAux2=0D0
			if (f(a,k).gt.(5D-1)) then
				ImIntAux2=w-En(a,k)+En(b,PolAdd(k,(/-q(1),q(2)/)))
				ImIntAux2=-Globeta/(ImIntAux2**2+Globeta**2)!*f(c,kp)
			endif
	end function ImIntAux2

	real (kind=8) function ImChi0UUInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ImChi0UUInt=ImIntAux(1D0,1D0,1D0,k,q,w)-ImIntAux(1D0,1D0,-1D0,k,q,w)
		ImChi0UUInt=ImIntAux1(1D0,1D0,k,q,w)-ImIntAux2(1D0,1D0,k,q,w)
	end function ImChi0UUInt

	real (kind=8) function ImChi0DDInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ImChi0DDInt=ImIntAux(-1D0,-1D0,1D0,k,q,w)-ImIntAux(-1D0,-1D0,-1D0,k,q,w)
		ImChi0DDInt=ImIntAux1(-1D0,-1D0,k,q,w)-ImIntAux2(-1D0,-1D0,k,q,w)
	end function ImChi0DDInt

	real (kind=8) function ImChi0UDInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ImChi0UDInt=ImIntAux(-1D0,1D0,1D0,k,q,w)-ImIntAux(1D0,-1D0,-1D0,k,q,w)
		ImChi0UDInt=ImIntAux1(-1D0,1D0,k,q,w)-ImIntAux2(1D0,-1D0,k,q,w)
	end function ImChi0UDInt

	real (kind=8) function ImChi0DUInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),w

		!ImChi0DUInt=ImIntAux(1D0,-1D0,1D0,k,q,w)-ImIntAux(-1D0,1D0,-1D0,k,q,w)
		ImChi0DUInt=ImIntAux1(1D0,-1D0,k,q,w)-ImIntAux2(-1D0,1D0,k,q,w)
	end function ImChi0DUInt





	real (kind=8) function KsiG(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		!
		!

		KsiG=(1D0+(1D0/(b**2))*(Norm(q+G))**2)**3
		KsiG=1D0/KsiG
	end function KsiG

	real (kind=8) function KsiGb(bparam,q,G)
		implicit none
		real (kind=8) ::q(2),G(2),bparam
		!
		!

		KsiGb=(1D0+(1D0/(bparam**2))*(Norm(q+G))**2)**3
		KsiGb=1D0/KsiGb
	end function KsiGb

	real (kind=8) function KsiGauss(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		!
		!

		KsiGauss=dexp(-bgauss**2*(Norm(q+G))**2)
	end function KsiGauss

	subroutine ReNEpsLine(q,wstart,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,wstep
		real (kind=8) ::q,phi,dw,w,qv(2),wstart
		real (kind=8) ::LindLine(wsteps),EpsLine(wsteps)

		phi=0D0
		qv=q*(/dcos(phi),dsin(phi)/)
			w=wstart
		do wstep=1,wsteps
				EpsLine(wstep)=1D0+(2D0*(NVc(qv))*LindLine(wstep))
				w=w+dw
		enddo

	end subroutine ReNEpsLine

	subroutine ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2),bp
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs!(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G))*KsiGb(bp,qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLineb



	real (kind=8) function PolIntegrate(fn,a,b,steps,q,w,G)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,G
		real (kind=8), external:: fn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps)
			dphi=2D0*pi/real(steps)
			PolIntegrate=0D0
			k=a
			phi=0D0
			do i=0,steps
				do j=0,steps
				PolIntegrate=PolIntegrate+fn(k,phi,q,w)
				phi=phi+dphi
				enddo
				k=k+dk
				phi=0D0
			enddo
			PolIntegrate=PolIntegrate*dk*dphi/(4D0*pi**2)
	end function PolIntegrate


	real (kind=8) function PolIntegrate2(a,b,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine,a,b,khat(2)
		real (kind=8), external:: fn

		! NCorder must match what was chosen in ReNLind2
		NCorder=1

		!bk=0D0
		PolIntegrate2=0D0
		dphi=2D0*pi/real(NCorder*steps, kind=8)
		phi=0D0
		k=a
		bk=b
		do phistep=1,NCorder*steps!+1
			!bk=min(FermiVec((/dcos(phi),dsin(phi)/),1D-6),b)
			!bk=a+1D0 !DEBUG
			!if (bk.le.a) cycle !if the integration range is above the Fermi Vector then cycle back since it will be 0
			philine=0D0
			dk=(bk-a)/real(NCorder*steps, kind=8)
			khat=(/dcos(phi),dsin(phi)/)
			!k=dk


	!!!NewtonCotes
			do kstep=1,NCorder*steps!+1
				philine=philine+dk*k*fn(q,w,k*khat)  									!NC1
				!philine=philine+IntCoeff(s,kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC4
				!philine=philine+BooleCoeff(s,kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC5
				!philine=philine+IntCoeff10(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC10
				!philine=philine+IntCoeff11(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC11
				k=k+dk
			enddo

			PolIntegrate2=PolIntegrate2+philine  									!1
			!PolIntegrate=PolIntegrate+IntCoeff(s,phistep,NCorder*steps+1)*philine  	!4
			!PolIntegrate=PolIntegrate+BooleCoeff(s,phistep,NCorder*steps+1)*philine  !5
			!PolIntegrate=PolIntegrate+IntCoeff10(phistep,NCorder*steps+1)*philine  !10
			!PolIntegrate=PolIntegrate+IntCoeff11(phistep,NCorder*steps+1)*philine  	!11
	!!!NewtonCotes


			phi=phi+dphi
		enddo



		PolIntegrate2=PolIntegrate2*dphi

	end function PolIntegrate2

	real (kind=8) function PolIntegrate3(a,b,rsteps,phisteps,fn,q,w)
		implicit none
		integer (kind=4):: rsteps,phisteps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine,a,b,khat(2)
		real (kind=8), external:: fn



		PolIntegrate3=0D0
		dphi=2D0*pi/real(phisteps, kind=8)
		phi=0D0
		k=a
		do phistep=1,phisteps!+1
			philine=0D0
			dk=(b-a)/real(rsteps, kind=8)
			khat=(/dcos(phi),dsin(phi)/)
			k=a

	!!!NewtonCotes
			do kstep=1,rsteps!+1
				philine=philine+dk*k*fn(q,w,k*khat)  									!NC1
				k=k+dk
			enddo

			PolIntegrate3=PolIntegrate3+philine  									!1
	!!!NewtonCotes
			phi=phi+dphi
		enddo



		PolIntegrate3=PolIntegrate3*dphi

	end function PolIntegrate3



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!! Testing Analytic response
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real(8) function ImChi0phi(q,w,k,a,bb,b)
	implicit none
	real(8) :: q,w,k,a,bb,b



	ImChi0phi=((a*b*w/gam+2D0*k)**2-q**2)/(q**2-(w/gam)**2)
	ImChi0phi=-pi*dsqrt(ImChi0phi)*a*bb/gam

end function ImChi0phi


real(8) function overlap(k,q,phi,a,b)
	implicit none
	real(8)::k,q,phi,a,b

	overlap=dcos(phi)
	overlap=0.5D0*(1D0+b*(k+a*q*overlap)/(dsqrt(k**2+q**2+2D0*a*k*q*overlap)))
end function overlap

real(8) function ImChiInt2(q,w,k,phi,a,bb,b)
	implicit none
	real(8)::q,w,k,phi,a,bb,b


	ImChiInt2=-a*Globeta*overlap(k,q,phi,a,b)/((w+a*b*gam*(k-bb*dsqrt(k**2+q**2+2D0*a*k*q*dcos(phi))))+Globeta**2)
end function ImChiInt2


real(8) function ImChi02(q,w,k,a,bb,b)
	implicit none
	real(8)::q,w,k,a,bb,b



	call GaussAdaptQuad(anc,0D0,2D0*pi,1D-8,ImChi02)
contains
	real(8) function anc(phi)
		implicit none
		real(8)::phi


		anc=ImChiInt2(q,w,k,phi,a,bb,b)
	end function anc
end function ImChi02

real(8) function Gab(q,wt,a,bb,b,k1,k2)
	real(8)::q,wt,a,bb,b,k1,k2
	real(8)::q2,w1,w2,w12,w22,s1,s2
	real(8), parameter::c=1D0/(16D0*pi*gam)

	q2=q**2
	w1=a*bb*wt+2D0*k1
	w2=a*bb*wt+2D0*k2
	w12=w1**2
	w22=w2**2
	s1=dsqrt(w12-q2)
	s2=dsqrt(w22-q2)
	Gab=(w2)*s2-(w1)*s1+q2*dlog((w1+s1)/(w2+s2))
	Gab=-a*bb*c*Gab/dsqrt(q2-wt**2)
end function Gab

real(8) function ImChi0Ap(q,w,a,b)
		real(8)::q,w,a,b


end function ImChi0Ap

end module PhysicsPolGraphene
