program Lindhard
	use constants	
	use LindFns
	implicit none
	real    (kind=8):: q,w,a,b,ao,bo,wo,aon,bon
	integer (kind=4):: i,j,steps,stat
!	complex (kind=8):: Chi,ChiCalc

	q = 0.558D0*kf

	open(unit=8, file='EpsLindReal.dat')
	open(unit=9, file='EpsLindImag.dat')
	open(unit=10, file='Bracket.dat')

	do i=1,100
		!write(*,*) i
		w = 0.015D0*real(i)
		write(8,*) w/kf**2,ReEpsLind(q,w)!,dreal(EpsLindCalc(q,w))
		write(9,*) w/kf**2,ImEpsLind(q,w)!,dimag(EpsLindCalc(q,w))
	enddo
	
	call FindBracket(ReEpsLind,q,0.001D0,1D0,ao,bo,10000,stat)	
	
	write(*,*) "a = ",ao,"b = ",bo,stat	
	
	close(unit=8)
	close(unit=9)
	close(unit=10)

end program Lindhard
