program Lindhard
	use constants	
	use GLindFunctions
	use NGLindFunctions
	use HelperFunctions
	implicit none
	real    (kind=8):: q,w,a,b,ao,bo,wo,aon,bon,nl,el,k,kp,theta,phi,dk,dtheta
	integer (kind=4):: i,j,steps,stat
!	complex (kind=8):: Chi,ChiCalc

	q = 0.5D0*kf
	w = Ef

	!open(unit=8, file='EpsPlot.dat')
	open(unit=9, file='LindPlot.dat')
	!open(unit=10, file='Bracket.dat')
	write(9,*) "# w/Ef                      RePI                      NLind"
	!write(8,*) "# w/Ef                      EpsNlind                    EpsRePi"

	w=0D0
	do i=1,100
		write(*,*) i
		w = 5D-2*real(i)*Ef
		
		!write(*,*) i
		!write(9,*) w/Ef,ReEps(q,w),Eps(q,w,ReNLind)
		write(9,*) w/Ef,RePI(q,w),ReNLind(q,w),ImPi(q,w),ImNLind(q,w)!,RePiPlus(q,w),RePiMinus(q,w)

	enddo

	!write(*,*) PiPlus(q,w)

	!write(*,*) "kf=",kf,"a0^-1"
	!write(*,*) "D0=",Dnaught
	!write(*,*) "Hartee Ef=",Ef
	!write(*,*) "eV Ef=",Ef/eV2Eh
	
	!close(unit=8)
	close(unit=9)
	!close(unit=10)

end program Lindhard
