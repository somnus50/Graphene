program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene
use AuxiliaryFunctions
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,PlasmonArray(10,3),wstart,qhat(2),w0,w
real (kind=8), allocatable::LindLine(:),EpsLine(:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	call ConstInit(1D12/cmSq2a0sq,0D0,GlobEf,Globeta,Globfx,Globfc)
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	open(unit=19, file='ImLind.Diam.dat', status='old', action='read')
	open(unit=9, file='ImLindPlot.dat')
	!write(9,*) "# q/kf,wp/ef,low q limit"
	read(19,*)
	read(19,*)
	read(19,*)
	read(19,'(4I10)') NProc, IntSteps, qsteps, wsteps
	write(*,*) NProc, IntSteps, qsteps, wsteps
	read(19,*)
	read(19,*)

	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	write(*,*) fmt2
	allocate(LindLine(wsteps),EpsLine(wsteps))


	write(*,*) "kf",Globkf
	write(*,*) "Ef",GlobEf

	do i=1,qsteps
		write(*,*) "qstep:",i
		q=0D0
		read(19,fmt1) q, wstart, dw
		q=q*Globkf
		write(*,*) "q",q
		read(19,fmt2) LindLine
		w=wstart
		do j=1,wsteps
			write(9,*) q/Globkf,w/GlobEf,LindLine(j)/pi/pi
			w=w+dw
		enddo
	enddo

	!!! Close files
	close(unit=19)
	close(unit=9)

end program GraphenePlasmons
