program GrapheneNextNearest
	implicit none
	real (kind=8) ::k
	integer (kind=4) ::i,j,l,m,Nr,N,ksteps,blockstart
	real (kind=8) , allocatable:: kacplots(:),kzzplots(:),ACEns(:),ZZEns(:)
	real (kind=8) , allocatable:: ACEnplots(:),ZZEnplots(:),ZZGNPlot(:,:),ACGNPlot(:,:),ACPlots(:,:),ZZPlots(:,:)
	complex (kind=8), allocatable::Hc(:,:),Sc(:,:),Hz(:,:),Sz(:,:),ZZAlpha(:),ZZBeta(:),ACAlpha(:),ACBeta(:)
	character (len=30) :: NrStr,filename

	! This program constructs the Hamiltonian and Overlap matrices for the tight-binding model of Graphene Nanoribbons
	! and then passes thoses matrices to the LAPACK driver ZGGEV to solve for the energy eigenvalues of the 
	! generalized eigenvalue problem H*Psi = E*S*Psi (A*v = l*B*v). 
	! It also outputs data files and gnuplot scripts to generate Energy vs. k graphs (The point plots look much better than lines).
	! I'll come back later to fully flesh out some descriptive comments

	
	write(*,*) 'Nr = ? [1,Inf)'
	read(*,*) Nr
	write(*,*) 'Number of steps = ? (ex. 100)'
	read(*,*) ksteps

	write(NrStr,*) Nr
	N=Nr*2
!	allocate(kacplots(N*(ksteps+1)))
!	allocate(kzzplots(N*(ksteps+1)))
!	allocate(ACEnplots(N*(ksteps+1)))
!	allocate(ZZEnplots(N*(ksteps+1)))
!	allocate(ZZGNPlot(N*(ksteps+1),2))
!	allocate(ACGNPlot(N*(ksteps+1),2))
	allocate(Hc(N,N))
	allocate(Sc(N,N))
	allocate(Hz(N,N))
	allocate(Sz(N,N))
	allocate(ACEns(N))
	allocate(ZZEns(N))
	allocate(ZZAlpha(N))
	allocate(ZZBeta(N))
	allocate(ACAlpha(N))
	allocate(ACBeta(N))
	allocate(ACPlots(ksteps,N+1))
	allocate(ZZPlots(ksteps,N+1))




	

	do m=1,ksteps
		k=-1.27707+(m-1)*2.55414/ksteps
		call ACH(Nr,k,Hc)
		call ACS(Nr,k,Sc)
		call Energies(Nr,Hc,Sc,ACAlpha,ACBeta)
		ACPlots(m,1)=k
		do l=1,N
!			blockstart=(m-1)*N
!			kacplots(blockstart+l)=k
			ACEns(l)=ACAlpha(l)/ACBeta(l)
!			ACEnplots(blockstart+l)=ACEns(l)
			ACPlots(m,l+1)=ACEns(l)
		enddo
	enddo

	do m=1,ksteps
		k=-2.25+(m-1)*4.5/ksteps
		call ZZH(Nr,k,Hz)
		call ZZS(Nr,k,Sz)
		call Energies(Nr,Hz,Sz,ZZAlpha,ZZBeta)
		ZZPlots(m,1)=k
		do l=1,N
!			blockstart=(m-1)*N
!			kzzplots(blockstart+l)=k
			ZZEns(l)=ZZAlpha(l)/ZZBeta(l)
!			ZZEnplots(blockstart+l)=ZZEns(l)
			ZZPlots(m,l+1)=ZZEns(l)
		enddo
	enddo	
	

	filename='ACNNEnNr'//trim(adjustl(NrStr))//'.dat'
	call GNUPlotScript(N,filename,'L')
	call GNUPlotScript(N,filename,'P')
	open(10,access='sequential',file=filename)
	do i=1,ksteps
		write(10,*) ACPlots(i,:)
	enddo
	close(10,status='keep')

	filename='ZZNNEnNr'//trim(adjustl(NrStr))//'.dat'
	call GNUPlotScript(N,filename,'L')
	call GNUPlotScript(N,filename,'P')
	open(10,access='sequential',file=filename)
	do i=1,ksteps
		write(10,*) ZZPlots(i,:)
	enddo
	close(10,status='keep')

end program GrapheneNextNearest

module constantsNN
	implicit none
	real (kind=8), parameter :: a=2.46, e2p=0.0, t=-3.033, s=0.0, tp=0.6066
	complex (kind=8) , parameter :: ii=(0.0,1.0), zero=(0.0,0.0), one=(1.0,0.0)
end module constantsNN

complex (kind=8) function f1(k)
	use constantsNN	
	implicit none
	real (kind=8) :: k


	f1=cdexp(ii*k*a/(2.0*sqrt(3.0)))
	return
end function f1

complex (kind=8) function f3(k)
	use constantsNN
	implicit none
	real (kind=8) :: k


	f3=cdexp(ii*k*a/sqrt(3.0))
	return 
end function f3

complex (kind=8) function f5(k)
	use constantsNN
	implicit none
	real (kind=8) :: k


	f5=cdexp(ii*k*a*sqrt(3.0)/2.0)
	return 
end function f5

complex (kind=8) function f8(k)
	use constantsNN
	implicit none
	real (kind=8) :: k


	f8=cdexp(ii*k*a)
	return 
end function f8

complex (kind=8) function f10(k)
	use constantsNN
	implicit none
	real (kind=8) :: k


	f10=cdexp(ii*k*a/2.0)
	return 
end function f10

complex (kind=8) function InnerRow(i,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i
	complex (kind=8) :: f3,f5,inrow(4)

	inrow=(/t*conjg(f3(k)),(f5(k)+conjg(f5(k)))*tp,t*f3(k),(f5(k)+conjg(f5(k)))*tp/)
	InnerRow=inrow(mod(i,4)+1)
	return 
end function InnerRow

complex (kind=8) function OuterRow(i,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i
	complex (kind=8) :: f1,outrow(4)


	outrow=(/conjg(f1(k)),conjg(f1(k)),f1(k),f1(k)/)
	OuterRow=outrow(mod(i,4)+1)
	return 
end function OuterRow

complex (kind=8) function OuterErRow(i,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i
	complex (kind=8) :: f1,f5,outrow(4)


	outrow=(/f5(k)+conjg(f5(k)),zero,f5(k)+conjg(f5(k)),zero/)
	OuterErRow=outrow(mod(i,4)+1)
	return 
end function OuterErRow

complex (kind=8) function OffDiagRow(i,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i
	complex (kind=8) :: f1,diagrow(2)


	diagrow=(/(f1(k)+conjg(f1(k))),one/)
	OffDiagRow=diagrow(mod(i,2)+1)
	return 
end function OffDiagRow

complex (kind=8) function ACHConstr(i,j,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j
	complex (kind=8) :: InnerRow,OuterRow,OuterErRow,f7

	if (i.eq.j) then
		ACHConstr=(e2p,0.0)
	endif
	if ((i-j).eq.1) then
		ACHConstr=InnerRow(i+2,k)
	endif
	if ((i-j).eq.(-1)) then
		ACHConstr=conjg(InnerRow((i+3),k))
	endif
	if ((i-j).eq.2) then
		ACHConstr=t*OuterRow(i,k)
	endif
	if ((i-j).eq.(-2)) then
		ACHConstr=t*conjg(OuterRow(i+2,k))
	endif
	if ((i-j).eq.(-3)) then
		ACHConstr=tp*conjg(OuterErRow(i+3,k))
	endif
	if ((i-j).eq.(3)) then
		ACHConstr=tp*conjg(OuterErRow(i,k))
	endif
	if (abs(i-j).eq.(4)) then
		ACHConstr=tp
	endif
	if (abs(i-j).gt.4) then
		ACHConstr=zero
	endif
	return 
end function ACHConstr

complex (kind=8) function ACSConstr(i,j,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j
	complex (kind=8) :: InnerRow,OuterRow

	if ((i).eq.(j)) then
		ACSConstr=one
	endif
	if ((i-j).eq.(1)) then
		ACSConstr=s*InnerRow(i+2,k)
	endif
	if ((i-j).eq.(-1)) then
		ACSConstr=s*conjg(InnerRow((i+3),k))
	endif
	if ((i-j).eq.(-2)) then
		ACSConstr=s*OuterRow((i),k)
	endif
	if ((i-j).eq.(2)) then
		ACSConstr=s*conjg(OuterRow((i+2),k))
	endif
	if (abs(i-j).gt.(2)) then
		ACSConstr=zero
	endif
	return 
end function ACSConstr

complex (kind=8) function ZZHConstr(i,j,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j
	complex (kind=8) :: OffDiagRow,f8,f10


	if ((i).eq.(j)) then
		ZZHConstr=(e2p,0.0)+tp*2*real(f8(k))
	endif
	if ((i-j).eq.(-1)) then
		ZZHConstr=t*OffDiagRow((i+1),k)
	endif
	if ((i-j).eq.(1)) then
		ZZHConstr=t*conjg(OffDiagRow((i),k))
	endif
	if (abs(i-j).eq.(2)) then
		ZZHConstr=tp*2*real(f10(k))
	endif
	if ((abs(i-j)).gt.(2)) then
		ZZHConstr=zero
	endif
	return 
end function ZZHConstr

complex (kind=8) function ZZSConstr(i,j,k)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j
	complex (kind=8) :: OffDiagRow

	if ((i).eq.(j)) then
		ZZSConstr=one
	endif
	if ((i-j).eq.(-1)) then
		ZZSConstr=s*OffDiagRow((i+1),k)
	endif
	if ((i-j).eq.(1)) then
		ZZSConstr=s*conjg(OffDiagRow((i),k))
	endif
	if ((abs(i-j)).gt.(1)) then
		ZZSConstr=zero
	endif
	return 
end function ZZSConstr

subroutine ZZH(Nr,k,zzhmatrix)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j,Nr
	complex (kind=8) :: zzhmatrix(Nr*2,Nr*2),ZZHConstr


	do i=1,2*Nr
		do j=1,2*Nr
			zzhmatrix(i,j)=ZZHConstr(i,j,k)
		enddo
	enddo
	return 
end subroutine ZZH

subroutine ZZS(Nr,k,zzsmatrix)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j,Nr
	complex (kind=8) :: zzsmatrix(Nr*2,Nr*2),ZZSConstr


	do i=1,2*Nr
		do j=1,2*Nr
			zzsmatrix(i,j)=ZZSConstr(i,j,k)
		enddo
	enddo
	return 
end subroutine ZZS

subroutine ACH(Nr,k,achmatrix)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j,Nr
	complex (kind=8) :: achmatrix(Nr*2,Nr*2),ACHConstr


	do i=1,2*Nr
		do j=1,2*Nr
			achmatrix(i,j)=ACHConstr(i,j,k)
		enddo
	enddo
	return 
end subroutine ACH

subroutine ACS(Nr,k,acsmatrix)
	use constantsNN	
	implicit none
	real (kind=8) :: k
	integer (kind=4) :: i,j,Nr
	complex (kind=8) :: acsmatrix(Nr*2,Nr*2),ACSConstr


	do i=1,2*Nr
		do j=1,2*Nr
			acsmatrix(i,j)=ACSConstr(i,j,k)
		enddo
	enddo
	return 
end subroutine ACS

subroutine Energies(Nr,A,B,Alpha,Beta)
	implicit none
	integer (kind=4) ::info,Nr,N
	real (kind=8) :: R(16*Nr)
	complex (kind=8) ::A((2*Nr),*),B((2*Nr),*),Work(6*Nr),Alpha(2*Nr),Beta(2*Nr),VL(2*Nr),VR(2*Nr)


	N=2*Nr
	call ZGGEV('N','N',N,A,N,B,N,Alpha,Beta,VL,N,VR,N,Work,2*N,R,info)
	return
end subroutine Energies

subroutine GNUPlotScript(N,DataFile,LP)
	implicit none
	integer (kind=4) :: N,i
	character (len=30) :: Datafile,line,LPt,NStr
	character (len=1) :: LP

	if (LP.eq.'L') then
		LPt='lines'
	else 
		LPt='points'
	endif

	open(20,access='sequential',file=trim(adjustl(DataFile))//"."//trim(adjustl(LPt))//'.gnu')
	write(20,*) "set nokey"
	write(20,*) "plot '"//trim(adjustl(DataFile))//"' using 1:2 with "//trim(adjustl(LPt))//", \"
	do i=1,N-2
		write(NStr,*) i+2
		write(20,*) "'' using 1:"//trim(adjustl(NStr))//" with "//trim(adjustl(LPt))//", \"
	enddo
	write(NStr,*) N+1
	write(20,*) "'' using 1:"//trim(adjustl(NStr))//" with "//trim(adjustl(LPt))
	write(20,*) 'pause -1 "Hit any key to continue"'
	close(20,status='keep')
	return
end subroutine GNUPlotScript







