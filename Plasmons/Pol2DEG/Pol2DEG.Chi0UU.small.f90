program Pol2DEGChi0DD
use Constants
use Globals
use PhysicsPol2DEG
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2)
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	call ConstInit(2D0,-4D-2,GlobEf,Globeta)
	MPImaster=0
	IntSteps=3000  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=50
	wsteps=300
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps))
	w=0D0
	dw=(1.2D0*GlobZeem)/real(wsteps, kind=8)
	dq=3D-2*Globkf0/real(qsteps, kind=8)
	q=(/dq,0D0/)
	open(unit=9, file='Pol2DEG.Chi0UU.z4.small.dat')

		write(9,*) "### Chi0UU(q,w)"
		write(9,*) "###", Globrs, Globz
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	do i=1,qsteps
		w=0D0
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			wline(j)=PolIntegratek(ReChi0UUInt2,0D0,GlobkfMAX*1.2D0,IntSteps,q,w)/4D0/pi**2
			w=w+dw
		enddo
		write(9,fmt2) wline
		q(1)=q(1)+dq
	enddo


	close(unit=9)
end program Pol2DEGChi0DD
