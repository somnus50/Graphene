real (kind=8) function RootBrentsMethod(a,b,delta,f)
	implicit none
	real (kind=8) ::a,b,f,c,d,s,swap1,swap2,delta
	integer (kind=4) ::mflag


! This function finds the root of a function, f in [a,b] with the precision of delta
! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
! a and b must be REAL. f must be a function that takes one input.
! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method


	
	if (f(a)*f(b).gt.(0.0)) then
		call EXIT(1)
	endif

	if (abs(f(a)).gt.abs(f(b))) then
		swap1=a
		swap2=b
		a=swap2
		b=swap1
	endif

	c=a
	mflag=1
	s=a
	
	do while (abs(b-a).gt.delta).or.(abs(f(b)).gt.delta).or.(abs(f(s)).gt.delta)
		if (f(a).ne.f(c)).and.(f(b).ne.f(c)) then
			s=(a*f(b)*f(c))/((f(a)-f(b))*(f(a)-f(c)))+(b*f(a)*f(c))/((f(b)-f(a))*(f(b)-f(c)))+(c*f(a)*f(b))/((f(c)-f(a))*(f(c)-f(b)))
			!Inverse Quadratic Interpolation
		else
			s=b-f(b)*(b-a)/(f(b)-f(a))
			!Secant Method
		endif
		
		if ((s.lt.((3.0*a+b)/4.0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b)).gte.(abs(b-c)/2.0))).or.((mflag.eq.(0)).and.((abs(s-b)).gte.(abs(c-d)/2.0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta)).or.((mflag.eq.(0)).and.(abs(c-d).lt.delta)) then
			s=(a+b)/2
			!Bisection Method
			mflag=1
		else
			mflag=0
		endif
		
		d=c
		c=b
		
		if ((f(a)*f(s)).lt.(0.0)) then
			b=s
		else
			a=s
		endif

		if (abs(f(a)).gt.abs(f(b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
		
	enddo

	if ((f(b)).lt.(f(s))) then
		RootBrentsMethod=b
	else
		RootBrentsMethod=s
	endif

	return
end function RootBrentsMethod
