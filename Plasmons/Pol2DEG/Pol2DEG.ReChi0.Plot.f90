program Pol2DEGImPlot
use readlib
use Constants
use Globals
use PhysicsPol2DEG
use AuxiliaryFunctions
use Response
use GV
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,PlasmonArray(4,20,3),wstart,qhat(2),w0,Chi0(4),Chi(4,4),rs,z,w
real (kind=8), allocatable::LindLine(:),EpsLine(:),qwgrid(:,:)&
						   ,Chigrid(:,:),EV(:,:),EVwline(:,:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,labelnum,temp,k
integer (kind=4), allocatable::labels(:),info(:,:)
character (len=32)::fmt1,fmt2,star,fmt3
character (len=32),allocatable::empty(:)




	!!!! Initialize some situational constants
	rs=2D0
	z=-4D-2
	call ConstInit(rs,z,GlobEf,Globeta)
	call KernelConstr(rs,z,GlobKernVec)
	!write(*,*) GlobKernVec
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	labelnum=4
	star='(A)'
	fmt3='(4I10)'
	allocate(labels(labelnum), empty(labelnum), qwgrid(labelnum,3), info(labelnum,4),EV(2,labelnum)) 

	!!! Create labels for files
	temp=19
	do i=1,labelnum
		labels(i)=temp
		temp=temp+10
	enddo

	!!! Open files
	open(unit=labels(1), file='Pol2DEG.Chi0UU.z4.small.dat', status='old', action='read')
	open(unit=labels(2), file='Pol2DEG.Chi0DD.z4.small.dat', status='old', action='read')
	open(unit=labels(3), file='Pol2DEG.Chi0UD.z4.small.dat', status='old', action='read')
	open(unit=labels(4), file='Pol2DEG.Chi0DU.z4.small.dat', status='old', action='read')
	open(unit=9, file='Chi0.0.dat')
	write(*,*) "# q/kf,w/ef,Chi0UU,DD,UD,DU"
	write(9,*) "w/Ef, Chi0(q=0)"

	!!! Throw out comment lines
	call multiread(labels,star,empty)
	call multiread(labels,star,empty)
	call multiread(labels,star,empty)

	!!! Read the metadata at the top of the file
	call multiread(labels,fmt3,info)
	!!! Sanity check to make sure all the data files are compatible
	!do i=2,labelnum
	!	do j=1,4
	!		if ((info(1,j)).ne.(info(i,j))) then
	!			write(*,*) "Data file parameters don't match. Please investigate."
	!			call exit(-1)
	!		endif
	!	enddo
	!enddo
	NProc=info(1,1)
	IntSteps=info(1,2)
	qsteps=info(1,3)
	wsteps=info(1,4)

	!!! Throw out comment lines
	call multiread(labels,star,empty) 
	call multiread(labels,star,empty) 

	allocate(Chigrid(labelnum,wsteps),EVwline(labelnum,wsteps))
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	!write(*,*) fmt2
	allocate(LindLine(wsteps),EpsLine(wsteps))
	w0=4D0*GlobEf/2D0!/kappa*e**2
	w0=dsqrt(w0)
	qhat=(/0D0,-1D0/)

	!write(*,*) "kf",GlobkfMAX
	!write(*,*) "Ef",GlobEf

	do i=1,qsteps
		!write(*,*) "qstep:",i
		q=0D0
		call multiread(labels,fmt1,qwgrid)
		if (i.eq.1) then
			write(9,*)
		endif
		!Do something to readvals (q, wstart, dw)
		q=qwgrid(1,1)*Globkf0
		w=qwgrid(1,2)
		dw=qwgrid(1,3)
		!write(*,*) "q",q
		call multiread(labels,fmt2,Chigrid) !read(19,fmt2) LindLine
		do j=1,wsteps
			Chi0=Chigrid(:,j)!*(-1D0)!/2D0/pi
			if (i.eq.1) then
				write(*,*) q/Globkf0,w/GlobEf,Chi0(1), ReChi0s(q,w,1D0)!,ChiCalc(q,w)
			endif
			w=w+dw
		enddo
	enddo

	!!! Close files
	do i=1,labelnum
		close(unit=labels(i))
	enddo
	close(unit=9)

end program Pol2DEGImPlot















