module Physics2DEG
use Constants
use Globals
implicit none

contains

		subroutine ConstInit(rs,z)
			use fxc2DEG
			use Globals
			implicit none
			!integer (kind=4) ::
			real    (kind=8), intent(in)::rs,z
			real    (kind=8)::n,fx,fc


				! Easy Stuff
				!Ef=8D0*t*frac !Specific to how I calculated the Lindhard Matrix
				!Globn=rs2n(rs)

				Globkf=dsqrt(2D0)/rs
				Globkf0=Globkf
				Globn=Globkf**2/2D0/pi
				Globrs=rs
				GlobEf=pi*Globn

				Globeta=5D-6*GlobEf

				! Hard Stuff
				!fx=2D0*dnexht(Globn,z)+n*d2nexht(Globn,z)
				!fc=2D0*dnecht(Globrs,z)+n*d2necht(Globrs,z)
				!fc=echt(rs,z)*Globn
				!Debug

				!fx=n*echt(Globrs,z)
				!fx=exh(Globn,z)
				!fc=Numdnech(n,z)
				!Globw=fx+fc

				fx=fxn(Globn,z)
				fc=fcn(Globn,z)

				!fx=d2nexh(Globrs,z)
				!fc=d2nech(Globrs,z)

				!fx=-dsqrt(2D0/pi/Globn)
				!fc=2D0*dnalpha(0,Globrs)+d2nalpha(0,Globrs)
				!fc=0D0

				Globfxc=fx+fc

			contains

				real (kind=8) function fcn(n,z)
					use AuxiliaryFunctions
					implicit none
					real (kind=8)::n,z

					fcn=NumDeriv2(ecn,n,1D-4)
				end function fcn

				real (kind=8) function ecn(n)
					use fxc2DEG
					implicit none
					real (kind=8)::n

					ecn=echn(n,z)
				end function ecn

				real (kind=8) function fxn(n,z)
					use AuxiliaryFunctions
					implicit none
					real (kind=8)::n,z

					fxn=NumDeriv2(exn,n,1D-4)
				end function fxn

				real (kind=8) function exn(n)
					use fxc2DEG
					implicit none
					real (kind=8)::n

					exn=exhn(n,z)
				end function exn


		end subroutine ConstInit


		real (kind=8) function ImChi0Int2(q,w,k)
			use AuxiliaryFunctions
			implicit none
			real (kind=8)::q(2),k(2),kp(2),w,temp

			ImChi0Int2=0D0
			kp=PolAdd(q,k)
			temp=f(k)-f(kp)
			if (temp**2.gt.9D-1) then
				temp=temp*(w+En(k)-En(kp))
				ImChi0Int2=-Globeta/(temp**2+Globeta**2)
			endif
		end function ImChi0Int2







	real (kind=8) function ReNLind2(q,w)
		implicit none
		integer (kind=4):: i,steps
		real (kind=8):: q(2),w,dk,temp
		real (kind=8), allocatable:: k(:,:)

		steps=1000
		allocate(k(2*steps,2))
		dk=2D0*pi/alat/real(2*steps, kind=8)
		temp=-pi/alat
		do i=1,2*steps
			k(i,:)=(/temp,temp/)
			temp=temp+dk
		enddo

		ReNLind2=ExtSimpIntegrate(k,steps,ReNLindInt,q,w)



	end function ReNLind2

	real (kind=8) function ReNLindMPI(q,w)
		implicit none
		integer (kind=4):: i,steps
		real (kind=8):: q(2),w,dk,temp
		real (kind=8), allocatable:: k(:,:)


		ReNLindMPI=ExtSimpIntegrate(k,steps,ReNLindInt,q,w)



	end function ReNLindMPI

	real (kind=8) function LowQPlasmon(q)
		use Globals
		implicit none
		real (kind=8)::q

		LowQPlasmon=Globkf*dsqrt(q)
	end function LowQPlasmon

	real (kind=8) function AnalyticPlasmon(q)
		implicit none
		real (kind=8)::q

		AnalyticPlasmon=(1D0+q*5D-1)**2*(1D0+2.5D-1*q**3/Globkf**2+q**4/16D0/Globkf**2)/(1D0+q/4D0)
		AnalyticPlasmon=dsqrt(AnalyticPlasmon)*Globkf*dsqrt(q)
	end function AnalyticPlasmon

	real (kind=8) function PH(s,q)
		implicit none
		real (kind=8)::q,s

		PH=q**2/2D0+s*Globkf*q
	end function PH


	real (kind=8) function MPITest(i,j)
		implicit none
		integer (kind=4)::i,j

		MPITest=real(i+j,kind=8)
	end function MPITest

	real (kind=8) function MPITest2(q,w,k)
		implicit none
		real (kind=8)::q(2),w,k(2)

		MPITest2=Norm(k)
	end function MPITest2

	real (kind=8) function ExtSimpIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps
		real (kind=8):: q(2),k(2),w,dkx,dky,a,b,c,d,temp
		real (kind=8), allocatable:: kxodd(:),kyodd(:),kxeven(:),kyeven(:)
		real (kind=8), pointer::OnesVect(:)
		real (kind=8), target:: vector(steps),limits(2*steps,2)
		real (kind=8), external:: fn


			dkx=(limits(2,1)-limits(1,1))
			dky=(limits(2,2)-limits(1,2))
			a=limits(1,1)
			b=limits(2*steps,1)
			c=limits(1,2)
			d=limits(2*steps,2)
			kxodd=limits(1:2*steps:2,1)
			kyodd=limits(1:2*steps:2,2)
			kxeven=limits(2:2*steps:2,1)
			kyeven=limits(2:2*steps:2,2)
			OnesVect => vector
			OnesVect=1D0


			ExtSimpIntegrate=0D0
			temp=0D0
			ExtSimpIntegrate=fn(q,w,(/a,c/))+fn(q,w,(/a,d/))+fn(q,w,(/b,c/))+fn(q,w,(/b,d/))

			temp=TwoDSum(1,steps,fn,q,w,a*OnesVect,kyodd)+TwoDSum(1,steps,fn,q,w,b*OnesVect,kyodd)&
			    +TwoDSum(steps,1,fn,q,w,kxodd,c*OnesVect)+TwoDSum(steps,1,fn,q,w,kxodd,d*OnesVect)
			temp=temp*4D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(1,steps-1,fn,q,w,a*OnesVect,kyeven)+TwoDSum(1,steps-1,fn,q,w,b*OnesVect,kyeven)&
			     +TwoDSum(steps-1,1,fn,q,w,kxeven,c*OnesVect)+TwoDSum(steps-1,1,fn,q,w,kxeven,d*OnesVect)
			temp=temp*2D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(steps,steps,fn,q,w,kxodd,kyodd)
			temp=temp*16D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(steps,steps-1,fn,q,w,kxodd,kyeven)+TwoDSum(steps-1,steps,fn,q,w,kxeven,kyodd)
			temp=temp*8D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(steps-1,steps-1,fn,q,w,kxeven,kyeven)
			temp=temp*4D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			ExtSimpIntegrate=ExtSimpIntegrate*dkx*dky/(9D0*4D0*pi**2)
	end function ExtSimpIntegrate

	real (kind=8) function OneDExtSimpIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,i
		real (kind=8):: q(2),k(2),w,dk,dky,a,b,c,d,temp1
		real (kind=8), allocatable:: kxodd(:),kyodd(:),kxeven(:),kyeven(:)
		real (kind=8), pointer::OnesVect(:)
		real (kind=8), target:: vector(steps),limits(2*steps,2)
		real (kind=8), external:: fn


			dk=(limits(2,1)-limits(1,1))
			a=limits(1,1)
			b=limits(steps,1)
			c=limits(1,2)
			d=limits(steps,2)
			!allocate(kxodd())
			!kxodd=limits(1:steps:2,1)
			!kyodd=limits(1:steps:2,2)
			!kxeven=limits(2:steps:2,1)
			!kyeven=limits(2:steps:2,2)
			OnesVect => vector
			OnesVect=1D0

  			temp1=0D0
			do i=1,1
				temp1=0D0
			enddo

			OneDExtSimpIntegrate=OneDExtSimpIntegrate*dk/(3D0)
	end function OneDExtSimpIntegrate


	real (kind=8) function TwoDSum(m,n,fn,q,w,x,y)
		implicit none
		integer (kind=4):: i,j,m,n
		real (kind=8):: q(2),w,x(m),y(n),total
		real (kind=8), external:: fn

		total=0D0


		do i=1,m
			do j=1,n
				total=total+fn(q,w,(/x(i),y(j)/))
			enddo
		enddo


		TwoDSum=total
	end function TwoDSum


	real (kind=8) function CartIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),k(2),limits(4),w,dkx,dky
		real (kind=8), external:: fn
		! 2D Cartesian integrator over k
		!
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dkx=(limits(2)-limits(1))/real(steps)
			dky=(limits(4)-limits(3))/real(steps)
			CartIntegrate=0D0
			k=(/limits(1),limits(3)/)
			do i=0,steps
				do j=0,steps
				CartIntegrate=CartIntegrate+fn(q,w,k)
				k=k+(/0D0,dky/)
				enddo
				k=(/k(1)+dkx,limits(3)/)
			enddo
			CartIntegrate=CartIntegrate*dkx*dky/(4D0*pi**2)
	end function CartIntegrate


	real (kind=8) function PolIntegrate(fn,a,b,steps,q,w,G)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,G
		real (kind=8), external:: fn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps)
			dphi=2D0*pi/real(steps)
			PolIntegrate=0D0
			k=a
			phi=0D0
			do i=0,steps
				do j=0,steps
				PolIntegrate=PolIntegrate+fn(k,phi,q,w)
				phi=phi+dphi
				enddo
				k=k+dk
				phi=0D0
			enddo
			PolIntegrate=PolIntegrate*dk*dphi/(4D0*pi**2)
	end function PolIntegrate

	real (kind=8) function PolIntegrate2(a,b,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine,a,b,khat(2)
		real (kind=8), external:: fn

		! NCorder must match what was chosen in ReNLind2
		NCorder=1

		!bk=0D0
		PolIntegrate2=0D0
		dphi=2D0*pi/real(NCorder*steps, kind=8)
		phi=0D0
		k=a
		bk=b
		do phistep=1,NCorder*steps!+1
			!bk=min(FermiVec((/dcos(phi),dsin(phi)/),1D-6),b)
			!bk=a+1D0 !DEBUG
			!if (bk.le.a) cycle !if the integration range is above the Fermi Vector then cycle back since it will be 0
			philine=0D0
			dk=(bk-a)/real(NCorder*steps, kind=8)
			khat=(/dcos(phi),dsin(phi)/)
			!k=dk


	!!!NewtonCotes
			do kstep=1,NCorder*steps!+1
				philine=philine+dk*k*fn(q,w,k*khat)  									!NC1
				!philine=philine+IntCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC4
				!philine=philine+BooleCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC5
				!philine=philine+IntCoeff10(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC10
				!philine=philine+IntCoeff11(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC11
				k=k+dk
			enddo

			PolIntegrate2=PolIntegrate2+philine  									!1
			!PolIntegrate=PolIntegrate+IntCoeff(phistep,NCorder*steps+1)*philine  	!4
			!PolIntegrate=PolIntegrate+BooleCoeff(phistep,NCorder*steps+1)*philine  !5
			!PolIntegrate=PolIntegrate+IntCoeff10(phistep,NCorder*steps+1)*philine  !10
			!PolIntegrate=PolIntegrate+IntCoeff11(phistep,NCorder*steps+1)*philine  	!11
	!!!NewtonCotes


			phi=phi+dphi
		enddo



		PolIntegrate2=PolIntegrate2*dphi

	end function PolIntegrate2

	real (kind=8) function PolIntegrate3(a,b,rsteps,phisteps,fn,q,w)
		implicit none
		integer (kind=4):: rsteps,phisteps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine,a,b,khat(2)
		real (kind=8), external:: fn



		PolIntegrate3=0D0
		dphi=2D0*pi/real(phisteps, kind=8)
		phi=0D0
		k=a
		do phistep=1,phisteps!+1
			philine=0D0
			dk=(b-a)/real(rsteps, kind=8)
			khat=(/dcos(phi),dsin(phi)/)
			k=a

	!!!NewtonCotes
			do kstep=1,rsteps!+1
				philine=philine+dk*k*fn(q,w,k*khat)  									!NC1
				k=k+dk
			enddo

			PolIntegrate3=PolIntegrate3+philine  									!1
	!!!NewtonCotes
			phi=phi+dphi
		enddo



		PolIntegrate3=PolIntegrate3*dphi

	end function PolIntegrate3


	real (kind=8) function PolIntegratek3(fn,a,b,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: i,j,steps,ier,phisteps,t1,t2
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),res,temp
		real (kind=8),allocatable::temp1(:)
		real (kind=8), external:: fn
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			!phisteps=(steps/9)*9 +1
			phisteps=(100/9)*9 +1
			dk=(b-a)/real(steps, kind=8)
			dphi=twopi/real(phisteps, kind=8)
			PolIntegratek3=0D0
			k=a
			phi=0D0
			allocate(temp1(phisteps))
			do i=1,phisteps
				k=a
					res=0D0
					!write(*,*) "IntStep:",i,j,k/Globkf0, q/Globkf0, w
					call GaussAdaptQuad(anc,k,k+dk,1D-8,res)
					temp=temp+res
				temp1(i)=temp
				phi=phi+dphi
			enddo
			!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


			t1=1
			t2=10
			do i=1,(phisteps/9)
				PolIntegratek3=PolIntegratek3+NC10PolList(dphi,temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratek3=PolIntegratek3*dphi    !/fourpisq     !NC10


		contains

			real (kind=8) function anc(phi)
				implicit none
				real (kind=8)::phi

				anc=fn(q,w,(/k,phi/))
			end function anc



			real (kind=8) function NC10PolList(dx,flist)
				integer (kind=4)::i
				real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
				real (kind=8), external::fn
				real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
				!NC10PolList=NC10PolList*dx/9D0
			end function NC10PolList
	end function PolIntegratek3



	subroutine phirange(k,q,kf,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),kf
		real (kind=8), intent(out)::phis(2)
		real (kind=8)::temp,phi0
		real (kind=8), parameter::twopi=2D0*pi

		temp=(kf**2-k**2-q(1)**2)/(2D0*k*q(1))
		if (dabs(temp).le.1D0) then
			phi0=dacos(temp)
			phis=(/q(2)-phi0,q(2)+phi0/)
			if (k.ge.max(q(1)-kf,kf)) then
				phis=(/modulo(q(2)-phi0,twopi),modulo(q(2)+phi0,twopi)/)
			endif
		else
			phis=(/0D0,twopi/)
		endif

		if (phis(1).gt.phis(2)) then
			phis=(/phis(2),phis(1)/)
		endif

	end subroutine phirange






	real (kind=8) function Norm(v)
		implicit none
		real (kind=8) ::v(2)

		!do i=1,len(v)
		Norm=(v(1))**2+(v(2))**2
		Norm=dsqrt(Norm)
	end function

	real (kind=8) function En(q)
		implicit none
		real (kind=8) ::q(2)


			!En=(Norm(q))**2/2D0!/m*hbar**2 !Cartesian

			En=(q(1))**2/2D0!/m*hbar**2 !Polar
	end function En

	real (kind=8) function FermiVec(q,delta)
		implicit none
		real (kind=8)::q(2),delta

		FermiVec=Globkf
	end function FermiVec

	real (kind=8) function f(q)
		implicit none
		real (kind=8) ::q(2)
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector

			f=0D0
			!!!Zero Temperature
			if ((En(q)).le.(GlobEf)) then
				f=1D0
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(q)-GlobEf)/kT))
	end function f


	real (kind=8) function ReNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of
		! q   :: real, wavevector
		! w   :: real, frequency

			ReNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=temp/(temp**2+Globeta**2)
				ReNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=temp/(temp**2+Globeta**2)
				ReNLindInt=ReNlindInt-temp
				ReNLindInt=ReNLindInt*f(k)
			endif
	end function ReNLindInt

	real (kind=8) function ImNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of
		! q   :: real, wavevector
		! w   :: real, frequency

			ImNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=1D0/(temp**2+Globeta**2)
				ImNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=1D0/(temp**2+Globeta**2)
				ImNLindInt=ImNlindInt-temp
				ImNLindInt=-ImNLindInt*f(k)*Globeta
			endif
	end function ImNLindInt

	real (kind=8) function ReNLind(q,w)
		implicit none
		real (kind=8) ::q(2),limits(4),w
		! Real part of
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			limits=(/ -(Globkf+Norm(q)+1D-2),(Globkf-Norm(q)+1D-2),-(Globkf+Norm(q)+1D-2),(Globkf-Norm(q)+1D-2) /)
			ReNLind=CartIntegrate(limits,300,ReNLindInt,q,w)
	end function ReNLind


	real (kind=8) function ImNLind(q,w)
		implicit none
		real (kind=8) ::q(2),limits(4),w
		! Imaginary part of
		! q   :: real, wavevector
		! w   :: real, frequency

			limits=(/ -pi/alat,pi/alat,-pi/alat,pi/alat /)
			ImNLind=CartIntegrate(limits,1000,ImNLindInt,q,w)
	end function ImNLind

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector

		NVc=2D0*pi/Norm(q)!*e**2/kappa
	end function NVc

	real (kind=8) function KsiG(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		!
		!

		KsiG=(1D0+(1D0/(b**2))*(Norm(q+G))**2)**3
		KsiG=1D0/KsiG
	end function KsiG

	real (kind=8) function KsiGb(bparam,q,G)
		implicit none
		real (kind=8) ::q(2),G(2),bparam
		!
		!

		KsiGb=(1D0+(1D0/(bparam**2))*(Norm(q+G))**2)**3
		KsiGb=1D0/KsiGb
	end function KsiGb

	real (kind=8) function KsiGauss(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		!
		!

		KsiGauss=dexp(-bgauss**2*(Norm(q+G))**2)
	end function KsiGauss

	real (kind=8) function fxc(q)
		implicit none
		integer (kind=4)::flag
		real (kind=8) ::q(2)
		! Exchange
		! q :: real, wavevector
		! flag :: int, turns exchange on or off

		flag=1
		fxc=0D0
		if (flag.eq.1) then
			fxc=0D0!Globfx
			!fxc=Globfc
			!fxc=Globfx+Globfc
		endif

	end function fxc

	real (kind=8) function ReNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function


		ReNEps=1D0+(2D0*NVc(q+G)*ReNLind(q,w))!*KsiG(q,G))

	end function ReNEps

	real (kind=8) function ImNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

		ImNEps=2D0*NVc(q+G)*ImNLind(q,w)*KsiG(q,G)

	end function ImNEps

	subroutine ReNEpsLine(q,wstart,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,wstep
		real (kind=8) ::q,phi,dw,w,qv(2),wstart
		real (kind=8) ::LindLine(wsteps),EpsLine(wsteps)

		phi=0D0
		qv=q*(/dcos(phi),dsin(phi)/)
			w=wstart
		do wstep=1,wsteps
				EpsLine(wstep)=1D0+(2D0*(NVc(qv))*LindLine(wstep))
				w=w+dw
		enddo

	end subroutine ReNEpsLine

	subroutine ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2),bp
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs!(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*KsiGb(bp,qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLineb



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	real (kind=8) function fxcInt(q)
		use QuadPackDouble2
		use Elliptic
		use STLS
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8), singtol=1D-8
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,temp1,temp2,temp3

		th=0D0
		fxcInt=0D0

		temp1=0D0
		temp2=0D0
		temp3=0D0

		call dqage ( anc, 0d0, 1D0, 1D-10, 1D-10, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 50D0, 1D-10, 1D-10, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 50D0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		fxcInt=(2D0*q*(temp1+temp2+temp3))/Globn**2/pi

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x

				anc=(StructSC(q*x)-Globn)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)
			end function anc
	end function fxcInt





	real (kind=8) function StructSC(q)
		use STLS
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=100000,lenw=limit*5
		real (kind=8)   , parameter::minpi=-1D0/pi
		integer (kind=4)::neval,ier,last,iord(limit),iwork(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,work(lenw),epsabs,epsrel,ws(4)

		!write(*,*) lenw

		epsabs=1D-2
		epsrel=1D-2

		temp2=0D0
		ws(1)=-(q*Globkf+q**2/2D0)
		ws(2)=q*Globkf-q**2/2D0
		ws(3)=-q*Globkf+q**2/2D0
		ws(4)=q*Globkf+q**2/2D0
		temp1=0D0


		call StupidIntegrate(anc,max(0D0,min(ws(1),ws(3))),max(0D0,ws(1),ws(2),ws(3),ws(4)),1000,temp1,ier)



		StructSC=minpi*temp1

		contains
			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				anc=dimag(ChiSC2(q,w))
			end function
	end function StructSC

	real (kind=8) function StructSCbar(p)
		use STLS
		use QuadPackDouble
		implicit none
		real (kind=8)::p,q
		integer (kind=4), parameter::limit=100000,lenw=limit*5
		real (kind=8)   , parameter::minpi=-1D0/pi
		integer (kind=4)::neval,ier,last,iord(limit),iwork(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,work(lenw),epsabs,epsrel,ws(4)

		!write(*,*) lenw

		q=(1D0-p)/p

		epsabs=1D-2
		epsrel=1D-2

		temp2=0D0
		ws(1)=-(q*Globkf+q**2/2D0)
		ws(2)=q*Globkf-q**2/2D0
		ws(3)=-q*Globkf+q**2/2D0
		ws(4)=q*Globkf+q**2/2D0
		temp1=0D0


		call StupidIntegrate(anc,max(0D0,min(ws(1),ws(3))),max(0D0,ws(1),ws(2),ws(3),ws(4)),1000,temp1,ier)



		StructSCbar=minpi*temp1

		contains
			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				anc=dimag(ChiSC2(q,w))
			end function
	end function StructSCbar


	complex (kind=8) function ChiSC2(q,w)
		use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::s,sp
		real    (kind=8)::q,w,fxc
		complex (kind=8)::cz

		cz=dcmplx(Rechi0SC(q,w),ImChi0SC(q,w))

		call splint(qgrid,GSCold,GSCspline,qsteps,q,fxc)

		ChiSC2=cz/(one-fxc*cz)

	end function ChiSC2


	real (kind=8) function ReChi0SC(q,w)
		implicit none
		real (kind=8)::q,w,nup,num,sim,sip
		real (kind=8),parameter:: minpi=-pi**(-1)


      NUP = (w/Q + Q/2.D0)/GlobKF
      NUM = (w/Q - Q/2.D0)/GlobKF

      SIM = 1.D0
      IF (NUM.LT.0.D0) SIM = -1.D0
      SIP = 1.D0
      IF (NUP.LT.0.D0) SIP = -1.D0

      RECHI0SC = 0.D0

      IF (NUM**2.GT.1.D0) RECHI0SC = SIM*DSQRT(NUM**2-1.D0)
      IF (NUP**2.GT.1.D0) RECHI0SC = RECHI0SC - SIP*DSQRT(NUP**2-1.D0)

      RECHI0SC = -1.D0/PI -RECHI0SC*GlobKF/(PI*Q)

	end function ReChi0SC

	real (kind=8) function ImChi0SC(q,w)
		implicit none
		real (kind=8)::q,w,nup,num,temp


      nup = (w/q + q/2.D0)/Globkf
      num = (w/q - q/2.D0)/Globkf

      ImChi0SC = 0.D0

      if (num**2.lt.1.D0) ImChi0SC = dsqrt(1.D0-num**2)
      if (nup**2.lt.1.D0) ImChi0SC = ImChi0SC - dsqrt(1.D0-nup**2)

      ImChi0SC = -Globkf*ImChi0SC/(pi*q)


	end function ImChi0SC











end module Physics2DEG
