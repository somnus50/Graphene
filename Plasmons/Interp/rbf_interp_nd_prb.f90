program main

use RBFInterpND
use r8lib

!*****************************************************************************80
!
!! MAIN is the main program for RBF_INTERP_ND_PRB.
!
!  Discussion:
!
!    RBF_INTERP_ND_PRB tests the RBF_INTERP_ND library.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    05 October 2012
!
!  Author:
!
!    John Burkardt
!
  implicit none

  call timestamp ( )
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) 'RBF_INTERP_ND_TEST:'
  write ( *, '(a)' ) '  FORTRAN90 version'
  write ( *, '(a)' ) '  Test the RBF_INTERP_ND library.'
  write ( *, '(a)' ) '  The R8LIB library is also needed.'

  call rbf_interp_nd_test01 ( )
  call rbf_interp_nd_test02 ( )
  call rbf_interp_nd_test03 ( )
  call rbf_interp_nd_test04 ( )
!
!  Terminate.
!
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) 'RBF_INTERP_ND_TEST:'
  write ( *, '(a)' ) '  Normal end of execution.'
  write ( *, '(a)' ) ' '
  call timestamp ( )

  return
end
