# Graphene Status 02/11/19

## What we have
- Response function with infinite lower band. 
  - Has a contradictory integral. The direct way is divergent. The KK relation is finite.
  - Reproduces plasmons very well.
  - Impossible to construct a structure factor (integral diverges) and therefore is invalid for STLS.
  - Possible to introduce xc effects not using STLS.
- Response function with finite lower band.
  - All integrals are well behaved.
  - Completely breaks plasmons.
  - There are most likely some sign errors.
  - Right now, STLS0 does not reproduce PGG.
- PGG for graphene.
  - In an old calculation, PGG pushed the spin-wave down below the axis.  
    We were hoping that adding correlation would push it up enough.  
    This might be fixed by the finite lower band PGG.
  - Cannot make PGG with infinite lower band.
  - The FT of PGG must be numerically integrated (Not too difficult).
  - We also need spin resolved PGG with finite lower band (shouldn't be hard).
- Scheme for filling split bands with lower cutoff.
  - Need to redo the stiffness parameterization using this scheme.
- Very old numerical calculations of spin waves with LDA. Probably need double checking.




