program Lindhard
	use constants	
	use GLindFunctions
	use NGLindFunctions
	use HelperFunctions
	implicit none
	real    (kind=8):: q,w,a,b,ao,bo,wo,aon,bon,nl,el,k,kp,theta,phi,dk,dtheta
	integer (kind=4):: i,j,steps,stat
!	complex (kind=8):: Chi,ChiCalc

	q = 0.5D0*kf
	!w = Ef

	!open(unit=8, file='EpsPlot.dat')
	open(unit=9, file='RePiMinus.dat')
	!open(unit=10, file='Bracket.dat')
	!write(9,*) "# w/Ef                      RePI                      NLind"
	!write(8,*) "# w/Ef                      EpsNlind                    EpsRePi"

	w=0D0
	do i=1,100
		write(*,*) i
		w = 3D-2*real(i)
		
!		write(*,*) i
		write(9,*) w,RePiMinus(q,w*Ef),RePiPlus(q,w*Ef),ReNLind(q,w*Ef),NVc(q),ReNEps(q,w*Ef)
		!write(8,*) w/Ef,Eps(q,w,NLind),Eps(q,w,RePI)

		!write(9,*) w/kf**2,ImEpsLind(q,w)!,dimag(EpsLindCalc(q,w))
	enddo

	!write(*,*) PiPlus(q,w)

	!write(*,*) "kf=",kf,"a0^-1"
	!write(*,*) "D0=",Dnaught
	!write(*,*) "Hartee Ef=",Ef
	!write(*,*) "eV Ef=",Ef/eV2Eh
	
!	close(unit=8)
	close(unit=9)
	!close(unit=10)

end program Lindhard
