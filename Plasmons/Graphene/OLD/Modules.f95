!##############################################################################
!                                    References
!##############################################################################
!
! [1] Hwang and Sarma, Phys.Rev.B 75 205418
!
! [2] 
!
! [3] 
!
!###############################################################################
!                                    Constants
!###############################################################################

module constants
	implicit none
	real (kind=8), parameter :: pi=dacos(-1.0D0), gs=2D0, gv=2D0,& 
				    !n=1.D0/(pi*1.79D0),&
				    kappa=1D0, hbar=1D0, e=1D0, m=1D0, &
				    eV2Eh=1D0/(27.211D0), Ang2a0=1D0/(0.529D0), &
				    cm2Ang=1D8, &
				    gam=(6.5D0)*eV2Eh*Ang2a0, &
				    L=1D0, n=1D12/((cm2Ang*Ang2a0)**2)

	real (kind=8), parameter :: kf=dsqrt(4D0*pi*n/(gs*gv)),&
				    !Dnaught=dsqrt(gs*gv*n/(pi))/gam,&
				    Ef=gam*kf,&
				    Dnaught=gs*gv*Ef/(2D0*pi*gam**2),&
				    v0=dsqrt(gs*gv*(e**2)*Ef*kf/&
					(2D0*kappa))/Ef,&
				    kT=2.6D-2*eV2Eh,&
				    eta=Ef*1D-2
	!complex (kind=8) ::
	!x=q/kf
	!v=w/Ef
end module constants

!###############################################################################
!                          Numerical Lindhard Function
!###############################################################################

module NGLindFunctions
use constants
implicit none

contains

	real (kind=8) function Integrate(fn,a,b,steps,q,w)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q,w,k,dk,phi,dphi,a,b
		real (kind=8), external:: fn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of 
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps)     
			dphi=2D0*pi/real(steps)
			Integrate=0D0
			k=a
			phi=0D0
			do i=0,steps
				do j=0,steps
				Integrate=Integrate+fn(k,phi,q,w)
				!!!!!To find NaNs
				!write(*,*) q,w,k,phi,costh(q,k,phi),Integrate
				phi=phi+dphi
				enddo
				k=k+dk
				phi=0D0
			enddo
			Integrate=Integrate*dk*dphi/(4D0*pi**2)
	end function Integrate

	real (kind=8) function En(q,s)
		implicit none
		real (kind=8) ::q,s
		! Energy of a Dirac particle
		! q :: real, wavevector magnitude
		! s :: real, +/-1 (conduction/valence band)

			En=s*gam*dabs(q)
	end function En

	real (kind=8) function f(q,s)
		implicit none
		real (kind=8) ::q,s
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector magnitude
		! s :: real, +/-1 (conduction/valence band)
	
			f=0D0
			!!!Zero Temperature
			if (s.lt.(0D0)) then
				!if (En(q,s).gt.(-2D0*ev2Eh)) then
					f=1D0
				!endif
			else
				if (dabs(q).lt.(kf)) then
					f=1D0
				endif
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(q,s)-Ef)/kT))
	end function f

	real (kind=8) function RePiPlusInt(k,phi,q,w)
		implicit none
		real (kind=8) ::k,kp,q,w,d1,d2,d3,fk,fkp,ct,phi
		! Integrand of [1].
		! k   :: real, wavevector magnitude to be integrated over
		! phi :: real, angle of k
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			ct=costh(q,k,phi)
			kp=kprime(q,k,phi)
			fk=f(k,1D0)
			fkp=f(kp,1D0)
			d1=w+En(k,1D0)-En(kp,1D0)
			d1=d1/(d1**2+eta**2)
			d2=w+En(k,1D0)-En(kp,-1D0)
			d2=d2/(d2**2+eta**2)
			d3=w+En(k,-1D0)-En(kp,1D0)
			d3=d3/(d3**2+eta**2)
			RePiPlusInt=(fk-fkp)*(1D0+ct)*d1
			RePiPlusInt=RePiPlusInt+fk*(1D0-ct)*d2
			RePiPlusInt=RePiPlusInt+fkp*(-1D0+ct)*d3
	end function RePiPlusInt

	real (kind=8) function RePiMinusInt(k,phi,q,w)
		implicit none
		real (kind=8) ::k,kp,q,w,d1,d2,d3,fk,fkp,ct,phi
		! Integrand of [1].
		! k   :: real, wavevector magnitude to be integrated over
		! phi :: real, angle of k
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			ct=costh(q,k,phi)
			kp=kprime(q,k,phi)
			fk=f(k,-1D0)
			fkp=f(kp,-1D0)
			d1=w+En(k,-1D0)-En(kp,-1D0)
			d1=d1/(d1**2+eta**2)
			d2=w+En(k,-1D0)-En(kp,1D0)
			d2=d2/(d2**2+eta**2)
			d3=w+En(k,1D0)-En(kp,-1D0)
			d3=d3/(d3**2+eta**2)
			RePiMinusInt=(fk-fkp)*(1D0+ct)*d1
			RePiMinusInt=RePiMinusInt+fk*(1D0-ct)*d2
			RePiMinusInt=RePiMinusInt+fkp*(-1D0+ct)*d3
	end function RePiMinusInt

	real (kind=8) function ImPiPlusInt(k,phi,q,w)
		implicit none
		real (kind=8) ::k,kp,q,w,d1,d2,d3,fk,fkp,ct,phi
		! Integrand of [1].
		! k   :: real, wavevector magnitude to be integrated over
		! phi :: real, angle of k
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			ct=costh(q,k,phi)
			kp=kprime(q,k,phi)
			fk=f(k,1D0)
			fkp=f(kp,1D0)
			d1=eta**2/((w+En(k,1D0)-En(kp,1D0))**2+eta**2)
			d2=eta**2/((w+En(k,1D0)-En(kp,-1D0))**2+eta**2)
			d3=eta**2/((w+En(k,-1D0)-En(kp,1D0))**2+eta**2)
			ImPiPlusInt=(fk-fkp)*(1D0+ct)*d1
			ImPiPlusInt=ImPiPlusInt+fk*(1D0-ct)*d2
			ImPiPlusInt=ImPiPlusInt+fkp*(-1D0+ct)*d3
			ImPiPlusInt=-ImPiPlusInt
	end function ImPiPlusInt

	real (kind=8) function ImPiMinusInt(k,phi,q,w)
		implicit none
		real (kind=8) ::k,kp,q,w,d1,d2,d3,fk,fkp,ct,phi
		! Integrand of [1].
		! k   :: real, wavevector magnitude to be integrated over
		! phi :: real, angle of k
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			ct=costh(q,k,phi)
			kp=kprime(q,k,phi)
			fk=f(k,-1D0)
			fkp=f(kp,-1D0)
			d1=eta**2/((w+En(k,-1D0)-En(kp,-1D0))**2+eta**2)
			d2=eta**2/((w+En(k,-1D0)-En(kp,1D0))**2+eta**2)
			d3=eta**2/((w+En(k,1D0)-En(kp,-1D0))**2+eta**2)
			ImPiMinusInt=(fk-fkp)*(1D0+ct)*d1
			ImPiMinusInt=ImPiMinusInt+fk*(1D0-ct)*d2
			ImPiMinusInt=ImPiMinusInt+fkp*(-1D0+ct)*d3
			ImPiMinusInt=-ImPiMinusInt
	end function ImPiMinusInt

	real (kind=8) function RePiPlus(q,w)
		implicit none
		real (kind=8) ::q,w
		! Real part of [1].
		! q   :: real, wavevector magnitude
		! w   :: real, frequency
			
			RePiPlus=-gs*gv*Integrate(RePiPlusInt,0D0,2D0*kf,300,q,w)/(2D0*L**2)
	end function RePiPlus

	real (kind=8) function RePiMinus(q,w)
		implicit none
		real (kind=8) ::q,w
		! Real part of [1].
		! q   :: real, wavevector magnitude
		! w   :: real, frequency
			
			RePiMinus=-gs*gv*Integrate(RePiMinusInt,0D0,15D0*kf,600,q,w)/(2D0*L**2)
	end function RePiMinus

	real (kind=8) function ImPiPlus(q,w)
		implicit none
		real (kind=8) ::q,w
		! Imaginary part of [1].
		! q   :: real, wavevector magnitude
		! w   :: real, frequency
			
			ImPiPlus=-gs*gv*Integrate(ImPiPlusInt,0D0,2D0*kf,300,q,w)/(2D0*L**2)
	end function ImPiPlus

	real (kind=8) function ImPiMinus(q,w)
		implicit none
		real (kind=8) ::q,w
		! Imaginary part of [1].
		! q   :: real, wavevector magnitude
		! w   :: real, frequency
			
			ImPiMinus=-gs*gv*Integrate(ImPiMinusInt,0D0,15D0*kf,600,q,w)/(2D0*L**2)
	end function ImPiMinus

	real (kind=8) function ReNLind(q,w)
		implicit none
		real (kind=8) ::q,w
		! Real part of [1].
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			ReNLind=RePiPlus(q,w)+RePiMinus(q,w)
	end function ReNLind

	real (kind=8) function ImNLind(q,w)
		implicit none
		real (kind=8) ::q,w
		! Imaginary part of [1].
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			ImNLind=ImPiPlus(q,w)+ImPiMinus(q,w)
	end function ImNLind

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q
		! Fourier Transform of screened Coulomb Potential
		! q   :: real, wavevector magnitude
	
		NVc=2D0*pi*e**2/(kappa*q)
	end function NVc

	real (kind=8) function costh(q,k,phi)
		implicit none
		real (kind=8) ::q,k,phi,kp
		! Cosine of angle theta between k and kp. [2].
		! q   :: real, wavevector magnitude
		! k   :: real, wavevector magnitude to be integrated over
		! phi :: real, angle of k

		costh=q+k*dcos(phi)
		kp=kprime(q,k,phi)
		if (dabs(kp).gt.(1D-10)) then
			costh=costh/kp
			costh=dacos(costh)
			costh=phi-costh
			costh=-dcos(costh)
		else
			!!!Avoids a divide by zero
			costh=0D0	
		endif		

	end function costh

	real (kind=8) function costh2(q,k,phi)
		implicit none
		real (kind=8) ::q,k,phi,kp
		! TEST. Cosine angle between q and kp
	
		costh2=q+k*dcos(phi)
		kp=kprime(q,k,phi)
		if (dabs(kp).gt.(1D-10)) then
			costh2=costh2/kp
		else
			costh2=pi/2D0		
		endif		

	end function costh2

	real (kind=8) function kprime(q,k,phi)
		implicit none
		real (kind=8) ::q,k,phi
		! Magnitude of kprime=q+k [2]. 
		! q   :: real, wavevector magnitude
		! k   :: real, wavevector magnitude to be integrated over
		! phi :: real, angle of k

		kprime=(q+k*dcos(phi))**2+(k*dsin(phi))**2
		kprime=dsqrt(kprime)
	end function kprime	

	real (kind=8) function ReNEps(q,w)
		implicit none
		real (kind=8) ::q,w
		! Real part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

		ReNEps=1D0+(NVc(q)*ReNLind(q,w))

	end function ReNEps

end module NGLindFunctions

!###############################################################################
!                      Exact Lindhard Function from Paper
!###############################################################################

module GLindFunctions

use constants
implicit none

contains

	real (kind=8) function f1(x,v)
		implicit none
		real (kind=8) ::x,v,inlog
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
		! inlog is used to see if x,v are in the domain

		f1=0D0
	
		if (dabs(2D0+v).gt.dabs(x)) then
			f1=(2D0+v)*dsqrt((2D0+v)**2-x**2)
			if (dabs(v).gt.dabs(x)) then
				inlog=(dsqrt((2D0+v)**2-x**2)+(2D0+v))/(dabs(dsqrt(v**2-x**2)+v))
				if (inlog.gt.(0D0)) then
					!f1=(2D0+v)*dsqrt((2D0+v)**2-x**2)
					f1=f1-(x**2)*dlog(inlog)
				endif
			endif
		endif
		!return
	end function f1
	
	real (kind=8) function f2(x,v)
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		f2=0D0
		if ((dabs(v).gt.dabs(x)).and.(v.gt.(dsqrt(v**2-x**2)))) then
			f2=(x**2)*dlog((v-dsqrt(v**2-x**2))/(x))
		endif
		!return
	end function f2
	
	real (kind=8) function f3(x,v)
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		f3=0D0
		if ((x**2-(2D0+v)**2)+(x**2)*dasin((2D0+v)/x).ge.(0D0)) then
		f3=(2D0+v)*dsqrt(x**2-(2D0+v)**2)+(x**2)*dasin((2D0+v)/x)
		endif
		!return
	end function f3
	
	real (kind=8) function f4(x,v)
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		f4=0D0
		if (((2D0+v)**2-x**2)-(x**2)*dlog((dsqrt((2D0+v)**2-x**2)+(2D0+v))/x).ge.(0D0)) then
		f4=(2D0+v)*dsqrt((2D0+v)**2-x**2)-(x**2)*dlog((dsqrt((2D0+v)**2-x**2)+(2D0+v))/x)
		endif
		!return
	end function f4
	
	real (kind=8) function heaviside(x)
		implicit none
		real (kind=8) ::x
	
		heaviside=0D0
		if (x.gt.(0D0)) then
			heaviside=1D0
		endif
		!return
	end function heaviside
	
	
	real (kind=8) function RePItp1(x,v)
		implicit none
		real (kind=8) ::x,v,nv
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		nv=-1D0*v
		RePItp1=0D0
	
		if ((dabs(2D0+v)-x).gt.(0D0)) then
			RePItp1=RePItp1+f1(x,v)
		endif
		if ((dabs(2D0-v)-x).gt.(0D0)) then
			RePItp1=RePItp1+sign(1D0,v-2D0+x)*f1(x,nv)
		endif
		if (((x+2D0-v).gt.(0D0)).or.((2D0-x-v).gt.(0D0))) then
			RePItp1=RePItp1+f2(x,v)
		endif
		if (dabs(v).gt.dabs(x)) then
			RePItp1=0.125D0/dsqrt(v**2-x**2)*RePItp1
		else
			RePItp1=0D0
		endif
	
		RePitp1=1D0-RePItp1
	end function RePItp1
	
	real (kind=8) function RePItp2(x,v)
		use constants
		implicit none
		real (kind=8) ::x,v,nv
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
		
		nv=-1D0*v
		RePItp2=0D0
	
		if ((x-dabs(2D0+v)).gt.(0D0)) then
			RePItp2=RePItp2+f3(x,v)
		endif
		if ((x-dabs(v-2D0)).gt.(0D0)) then
			RePItp2=RePItp2+f3(x,nv)
		endif
	
		RePItp2=RePItp2+pi*x**2/2D0*(heaviside(dabs(v+2D0)-x)+heaviside(dabs(v-2D0)-x))
	
		if (dabs(x).gt.dabs(v)) then
			RePItp2=0.125D0/dsqrt(x**2-v**2)*RePItp2
		else
			RePItp2=0D0
		endif
	
		RePItp2=1D0-RePItp2
	end function RePItp2
	
	real (kind=8) function ImPItp1(x,v)
		use constants
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		if (dabs(x).gt.dabs(v)) then
			ImPItp1=0D0
		else
		ImPItp1=-0.125D0/(dsqrt(v**2-x**2))*(f3(x,-v)*heaviside(x-dabs(v-2D0))&
			+pi*x**2/2D0*(heaviside(x+2D0-v)+heaviside(2D0-x-v)))
		endif
	end function ImPItp1
	
	real (kind=8) function ImPItp2(x,v)
		implicit none
		real (kind=8) ::x,v,nv
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		nv=-1D0*v
		if (dabs(v).gt.dabs(x)) then
			ImPItp2=0D0
		else
		ImPItp2=(heaviside(v-x+2D0))/(8D0*dsqrt(x**2-v**2))*(f4(x,v)-f4(x,nv)*heaviside(2D0-x-v))
		endif
	end function ImPItp2
	
	real (kind=8) function RePItm(x,v)
		use constants
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		RePItm=0D0
		if (dabs(x).gt.dabs(v)) then
			RePItm=(pi*x**2)/(8D0*dsqrt(x**2-v**2))
		endif
	end function RePItm
	
	real (kind=8) function ImPItm(x,v)
		use constants
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		ImPItm=0D0
	
		if (dabs(v).gt.dabs(x)) then
			ImPItm=(pi*x**2)/(8D0*dsqrt(v**2-x**2))
		endif
	end function ImPItm
	
	real (kind=8) function RePItp(x,v)
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		if (v.ge.x) then
			RePItp=RePItp1(x,v)
		else
			RePItp=RePItp2(x,v)
		endif
	end function RePItp
	
	real (kind=8) function ImPItp(x,v)
		implicit none
		real (kind=8) ::x,v
		! [1].
		! x :: real, q/kf
		! v :: real, w/Ef
	
		if (v.ge.x) then
			ImPItp=ImPItp1(x,v)
		else
			ImPItp=ImPItp2(x,v)
		endif
	end function ImPItp
	
	real (kind=8) function RePI(q,w)
		use constants
		implicit none
		real (kind=8) ::x,v,q,w
		! [1].
		! q :: real, wavevector magnitude
		! w :: real, frequency
	
		x=q/kf
		v=w/Ef
		RePI=-Dnaught*(RePItp(x,v)+RePItm(x,v))
	end function RePI
	
	real (kind=8) function ImPI(q,w)
		use constants
		implicit none
		real (kind=8) ::q,w,x,v
		! [1].
		! q :: real, wavevector magnitude
		! w :: real, frequency
	
		x=q/kf
		v=w/Ef
		ImPI=-Dnaught*(ImPItp(x,v)+ImPItm(x,v))
	end function ImPI
	
	real (kind=8) function Vc(q)
		use constants
		implicit none
		real (kind=8) ::q
		! Fourier Transform of screened Coulomb Potential
		! q   :: real, wavevector magnitude
	
		Vc=2D0*pi*e**2/(kappa*q)
	end function Vc
	
	real (kind=8) function ReEps(q,w)
		implicit none
		real (kind=8) ::q,w
		! Real part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency
	
		ReEps=1D0-Vc(q)*RePI(q,w)
	end function ReEps
	
	real (kind=8) function ImEps(q,w)
		implicit none
		real (kind=8) ::q,w
		! Imaginary part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency
	
		ImEps=-Vc(q)*ImPI(q,w)
	end function ImEps

end module GLindFunctions

!###############################################################################
!                               Helper Functions
!###############################################################################

module HelperFunctions

implicit none

contains

	real (kind=8) function IntraBand(x)
		use constants
		implicit none
		real (kind=8) ::x
		! Boundary of the intraband SPE continuum
		! x :: real, q/kf
		
		IntraBand=gam*x*kf/Ef
	end function IntraBand

	real (kind=8) function HVc(q)
		use constants
		implicit none
		real (kind=8) ::q
		! Fourier Transform of screened Coulomb Potential
		! q   :: real, wavevector magnitude
	
		HVc=2D0*pi*e**2/(kappa*q)
	end function HVc

	real (kind=8) function Eps(q,w,Lind)
		implicit none
		real (kind=8) ::q,w
		real (kind=8), external :: Lind
		! Real part of Epsilon, dielectric function
		! Wrapper to accept any Lindhard function
		! q    :: real, wavevector magnitude
		! w    :: real, frequency
		! Lind :: real, external Lindhard function

		Eps=1D0-HVc(q)*Lind(q,w)
	end function Eps

	subroutine FindBracket(func,x,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4) :: steps,stat,i
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
	
		do while (((func(x,a)*func(x,b)).gt.(0D0)).and.(i.lt.steps))
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			i=i+1

		enddo
		stat=-1
		if (int(sign(1D0,(func(x,a)*func(x,b)))).eq.(-1)) then
			stat=1
		endif
		return
	end subroutine FindBracket

	subroutine FindBracketList(func,x,ao,bo,array,steps)
		implicit none
		integer (kind=4) :: steps,stat,i,j
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8) :: array(4,3)
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		j=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
	
		do while (i.lt.steps)
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			if ((func(x,a)*func(x,b)).lt.(0D0)) then
				array(j,:)=(/a,b,1D0/)
				j=j+1
				
			endif
			i=i+1

		enddo
		return
	end subroutine FindBracketList

	subroutine FindBracketList2(EpsList,q,dw,array)
		implicit none
		integer (kind=4) :: steps,i,j
		real    (kind=8) :: q,dw
		real    (kind=8) :: array(5,3),EpsList(100)
		!real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
		steps=size(EpsList)-1
		j=1
		do i=1,steps
			if ((EpsList(i)*EpsList(i+1)).lt.(0D0)) then
				!write(*,*) (/q,dw*real(i),dw*real(i+1)/)
				array(j,:)=(/q,dw*real(i),dw*real(i+1)/)
				j=j+1
			endif
		enddo



		return
	end subroutine FindBracketList2
	
	real (kind=8) function RootBrentsMethod(a,b,delta,f,x)
		implicit none
		real (kind=8) ::a,b,c,d,s,swap1,swap2,delta,x
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind
	
	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL. f must be a function that takes two inputs (x gets passed along to f).
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method
		
		if (f(x,a)*f(x,b).gt.(0D0)) then
			call EXIT(1)
		endif
	
		if (abs(f(x,a)).gt.abs(f(x,b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
	
		c=a
		mflag=1
		s=a
		ind=1
	
		do while ((abs(b-a).gt.delta).or.(abs(f(x,b)).gt.delta).or.(abs(f(x,s)).gt.delta).or.(ind.lt.1000))
			ind=ind+1
			if ((f(x,a).ne.f(x,c)).and.(f(x,b).ne.f(x,c))) then
				s=(a*f(x,b)*f(x,c))/((f(x,a)-f(x,b))*(f(x,a)-f(x,c)))+(b*f(x,a)&
				  *f(x,c))/((f(x,b)-f(x,a))*(f(x,b)-f(x,c)))+(c*f(x,a)*f(x,b))&
				  /((f(x,c)-f(x,a))*(f(x,c)-f(x,b)))
				!Inverse Quadratic Interpolation
			else
				s=b-f(x,b)*(b-a)/(f(x,b)-f(x,a))
				!Secant Method
			endif
			
			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b))&
			   .ge.(abs(b-c)/2D0))).or.((mflag.eq.(0)).and.((abs(s-b)).ge.&
			   (abs(c-d)/2D0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta)).or.&
			   ((mflag.eq.(0)).and.(abs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif
			
			d=c
			c=b
			
			if ((f(x,a)*f(x,s)).lt.(0D0)) then
				b=s
			else
				a=s
			endif
	
			if (abs(f(x,a)).gt.abs(f(x,b))) then
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif
			
		enddo
	
		if ((f(x,b)).lt.(f(x,s))) then
			RootBrentsMethod=b
		else
			RootBrentsMethod=s
		endif
	
		return
	end function RootBrentsMethod
	
end module HelperFunctions
