program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene3
use AuxiliaryFunctions
use fxc2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,w,dq,a,b1,bb,k1,k2,qs(12)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,valid
character (len=32)::fmt1,fmt2,fname




	!!!! Initialize some situational constants
	Globgs=2D0
	Globgv=2D0
	call ConstInit(2D13/cmSq2a0sq,0D0)
	!Globg=4D0*(cfx(Globrs)+cfc(Globrs))!/kappa

	!call ConstInit(0D0,0D0)
	! Globeta=1D-1*GlobEf
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	! open(unit=10, file='ReChi0kf.dat')



	!write(9,*) "# q/kf,wp/ef,low q limit"
	qs=(/0.5D0*Globkf,&
			Globkf,&
			1.5D0*Globkf,&
			2D0*Globkf,&
			Globkc-globkf,&
			Globkc-0.5D0*Globkf,&
			Globkc,&
			Globkc+0.5D0*Globkf,&
			globkc+Globkf,&
			1.5D0*globkc,&
			2D0*globkc,&
			2.5D0*globkc/)

	write(*,*) "kf",Globkf
	write(*,*) "Ef",GlobEf

	qsteps=12
	wsteps=200


	do i=1,qsteps
		q=qs(i)
		write(fname, '(a, i0, a )') 'Chi0-q',i,'.dat'
		open(unit=9, file=fname)
		write(9,*) "# kf,Ef,kc,Ec,q/kf "
		write(9,*) "# ",Globkf,GlobEf,Globkc,Globkc*gam,q/Globkf
		write(9,*) "# w/kf, ImChi0Ex, ImChiPeak, ImChiKK, ReChi0Ex, ReChiPeak, ReChiKK"
		write(*,*) "qstep:",i,"/",qsteps
		write(*,*) "q",q/Globkf
		dw=((2.1D0*Globkc+q)*gam)/real(wsteps, kind=8)
		w=dw!0D0!*GlobEf
		do j=1,wsteps
			write(*,*) "wstep:",j,"/",wsteps!, w, gam, globeta
			write(9,*) w/Globkf/gam, ImChi0Ex(q,w), ImChiPeak(q,w), ImChiKK(q,w), ReChi0Ex(q,w), ReChiPeak(q,w), ReChiKK(q,w)!, ReChiKK(q,w), ReNLindDumb(q,w)!, ReN2Lind(q,w)!, ImN2Lind(q,w)!, ImNLind(q,w), ImNLindHybrid(q,w)
			w=w+dw
		enddo
		close(unit=9)
	enddo

!write(*,*) '##########################'
!call krange(Globkf,globEf,1D0,1D0,1D0,k1,k2,valid)
!write(*,*) '##########################'
!write(*,*) k1,k2,valid
	!write(*,*) PolIntegratephi11(test,0D0,1D0,0D0,0D0,phirange0,2000,q,w), pi/4D0

	!!! Close files
	close(unit=9)
	close(unit=10)



end program GraphenePlasmons
