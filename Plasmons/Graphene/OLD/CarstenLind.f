      PROGRAM PLASMON
      IMPLICIT NONE

      INTEGER I
      DOUBLE PRECISION RS,PI,N,KF,Q,OMEGA
      DOUBLE COMPLEX CHI,CHICALC
      COMMON/PARMS/ KF,PI
      EXTERNAL CHI,CHICALC

      RS = 1.79D0 

      PI = 3.141592653589793D0
      N = 1.D0/(PI*RS)
      KF = DSQRT(2.D0*PI*N)

      Q = 0.5D0*KF

!      open(90, file="ReChi.dat")
!      open(91, file="ImChi.dat")

      DO 100 I=1,100
      WRITE(*,*)I

      OMEGA = 0.01D0*I*KF**2

      WRITE(90,*)OMEGA/KF**2,
     &           DREAL(CHI(Q,OMEGA)),DREAL(CHICALC(Q,OMEGA))
      WRITE(91,*)OMEGA/KF**2,
     &           DIMAG(CHI(Q,OMEGA)),DIMAG(CHICALC(Q,OMEGA))

100   CONTINUE


      END
C*************************************************************************
C*************************************************************************
      DOUBLE COMPLEX FUNCTION CHI(Q,OMEGA)
      IMPLICIT NONE

      DOUBLE PRECISION Q,OMEGA,KF,PI,VP,VM,SIP,SIM,DP,DM,RECHI,IMCHI

      COMMON/PARMS/ KF,PI

      VP = OMEGA/(Q*KF) + Q/(2.D0*KF)
      VM = OMEGA/(Q*KF) - Q/(2.D0*KF)

      IF (VP.GE.0.D0) THEN
         SIP = 1.D0
      ELSE
         SIP = -1.D0
      ENDIF

      IF (VM.GE.0.D0) THEN
         SIM = 1.D0
      ELSE
         SIM = -1.D0
      ENDIF

      DP = VP**2-1.D0 
      DM = VM**2-1.D0 

      RECHI = 0.D0

      IF (DM.GE.0.D0) RECHI = RECHI + SIM*DSQRT(DM)
      IF (DP.GE.0.D0) RECHI = RECHI - SIP*DSQRT(DP)

      RECHI = RECHI*KF/Q + 1.D0

      RECHI = -RECHI/PI

      IMCHI = 0.D0
  
      IF (DM.LE.0.D0) IMCHI = IMCHI + DSQRT(-DM)
      IF (DP.LE.0.D0) IMCHI = IMCHI - DSQRT(-DP)

      IMCHI = -IMCHI*KF/Q

      IMCHI = IMCHI/PI 

      CHI = RECHI*(1.D0,0.D0) + IMCHI*(0.D0,1.D0)

      END
C*************************************************************************
C*************************************************************************
      DOUBLE COMPLEX FUNCTION CHICALC(Q,OMEGA)
      IMPLICIT NONE

      INTEGER I,J,NPHI,NK
      PARAMETER (NPHI = 1000, NK = 1000)

      DOUBLE PRECISION Q,OMEGA,KF,PI,ETA,RECHI,IMCHI,K,DK,PHI,DPHI,
     &                 NM,NP

      COMMON/PARMS/ KF,PI
      ETA = 0.01D0*KF**2/2.D0
C      ETA = 2.D-3
      
      DPHI = 2.D0*PI/NPHI
      DK = KF/NK  

      RECHI = 0.D0
      IMCHI = 0.D0

      DO 10 I=1,NPHI
         PHI = I*DPHI - DPHI/2.D0

         DO 10 J=1,NK
            K = J*DK

            NM = OMEGA - K*Q*DCOS(PHI) - Q**2/2.D0
            NP = OMEGA + K*Q*DCOS(PHI) + Q**2/2.D0

            RECHI = RECHI 
     &            + NM*K/(NM**2 + ETA**2) - NP*K/(NP**2+ETA**2) 

            IMCHI = IMCHI 
     &            - ETA*K/(NM**2 + ETA**2) + ETA*K/(NP**2+ETA**2) 

10    CONTINUE

      RECHI = RECHI*DK*DPHI/(2.D0*PI)**2
      IMCHI = IMCHI*DK*DPHI/(2.D0*PI)**2

      CHICALC = RECHI*(2.D0,0.D0) + IMCHI*(0.D0,2.D0)

      RETURN
      END
