\documentclass [12pt] {article}
\usepackage{graphicx}
\topmargin -2cm
\oddsidemargin 0cm
\textheight 23.0cm
\textwidth 16.0cm
\begin{document}
\baselineskip 18pt
\parskip0pt
\parindent8mm
\noindent

\newcommand{\dy}{\displaystyle}
\newcommand{\bfk}{{\bf k}}
\newcommand{\bfK}{{\bf K}}
\newcommand{\bfq}{{\bf q}}
\newcommand{\bfa}{{\bf a}}
\newcommand{\bfb}{{\bf b}}
\newcommand{\bfr}{{\bf r}}

In the paper by B. Wunsch, T. Stauber, F. Sols and F. Guinea, New J. Phys. {\bf 8}, 301 (2006), the response function
of graphene is written as follows in their Eq. (5):
\begin{equation}\label{1}
\chi_D^\beta(\bfq,\omega) = \frac{g}{4\pi^2\hbar} \int_0^D kdk\int_0^{2\pi}d\varphi \sum_{\alpha=\pm1}
\frac{\alpha f^\beta(\bfk,\bfq)}{\omega + \alpha v_F(k-\beta|\bfk + \bfq|)+i\delta}
\end{equation}
where $\beta = \pm 1$ and
\begin{equation}
f^\beta(\bfk,\bfq) = \frac{1}{2}\left(1 + \beta\:\frac{k+q\cos\varphi}{|\bfk + \bfq|}\right).
\end{equation}
Let us now calculate the imaginary part of $\chi_D^\beta$ using the standard decomposition
\begin{displaymath}
\frac{1}{x+i\delta} = P(1/x)-i\pi \delta(x) \:.
\end{displaymath}
Substituting this into Eq. (\ref{1}) gives (setting $\hbar = v_F=1$)
\begin{equation}\label{2}
\Im \chi_D^\beta(\bfq,\omega) = -\frac{g}{4\pi} \int_0^D kdk\int_0^{2\pi}d\varphi \sum_{\alpha=\pm1}
\alpha f^\beta(\bfk,\bfq) \: \delta[\omega + \alpha (k-\beta|\bfk + \bfq|)].
\end{equation}
In their Appendix, Wunsch et al. define this as
\begin{equation}
\Im \chi_D^\beta(\bfq,\omega) = -\frac{g}{4\pi} \int_0^D dk \sum_{\alpha=\pm1} \alpha I^{\alpha\beta}(k,q,\omega)
\end{equation}
where
\begin{equation}
I^{\alpha\beta}(k,q,\omega) = k\int_0^{2\pi}d\varphi  f^\beta(\bfk,\bfq) \: \delta[\omega + \alpha (k-\beta|\bfk + \bfq|)].
\end{equation}
The task is now to carry out the $\varphi$-integration. Since the integrand only depends on $\varphi$ via $\cos \varphi$, we can rewrite this as
\begin{eqnarray}
I^{\alpha\beta}(k,q,\omega) &=&
k\int_0^{\pi}d\varphi  f^\beta(k,q,\cos\varphi) \: \delta[\omega + \alpha (k-\beta\sqrt{k^2+q^2+2kq\cos\varphi})]
\nonumber\\
&+&
k\int_0^{\pi}d\varphi  f^\beta(k,q,-\cos\varphi) \: \delta[\omega + \alpha (k-\beta\sqrt{k^2+q^2-2kq\cos\varphi})]
\end{eqnarray}
Let us substitute
\begin{displaymath}
x=\cos\varphi \qquad \Longrightarrow \qquad d\varphi = -\frac{dx}{\sqrt{1-x^2}}
\end{displaymath}
so that
\begin{eqnarray}
I^{\alpha\beta}(k,q,\omega) &=&
k\int_{-1}^{1} \frac{dx}{\sqrt{1-x^2}}\: f^\beta(k,q,x) \: \delta[\omega + \alpha k- \alpha\beta\sqrt{k^2+q^2+2kqx})]\nonumber\\
&+&
k\int_{-1}^{1} \frac{dx}{\sqrt{1-x^2}}\: f^\beta(k,q,-x) \: \delta[\omega + \alpha k- \alpha\beta\sqrt{k^2+q^2-2kqx})].
\end{eqnarray}
In the second integral we can substitute $x \to -x$, and the two terms can be combined. This gives, in detail,
\begin{equation}
I^{\alpha\beta}(k,q,\omega) = k\int_{-1}^{1} \frac{dx}{\sqrt{1-x^2}} \left(1 + \beta\:\frac{k+qx}{\sqrt{k^2+q^2+2kqx}}\right) \delta[\omega + \alpha k- \alpha\beta\sqrt{k^2+q^2+2kqx})]
\end{equation}
Next, let
\begin{displaymath}
y=\alpha\beta\sqrt{k^2+q^2+2kqx} \qquad \Longrightarrow \qquad dx = \frac{y}{kq} dy
\end{displaymath}
and we obtain
\begin{equation}
I^{\alpha\beta}(k,q,\omega) = \frac{1}{q}\int_{\alpha\beta|k-q|}^{\alpha\beta(k+q)} \frac{ydy}{\sqrt{1-x^2}} \left(1 + \alpha\:\frac{k+qx}{y}\right) \delta[\omega + \alpha k- y].
\end{equation}
We have
\begin{displaymath}
x=\frac{y^2 - k^2-q^2}{2kq}
\end{displaymath}
so
\begin{equation}\label{10}
I^{\alpha\beta}(k,q,\omega) = \frac{1}{q}\int_{\alpha\beta|k-q|}^{\alpha\beta(k+q)} \frac{ydy}{\sqrt{1-\left(\frac{y^2 - k^2-q^2}{2kq}\right)^2}}
\left(1 + \alpha\:\frac{k^2+y^2-q^2}{2ky}\right) \delta[\omega + \alpha k- y].
\end{equation}
This expression is zero if $y=\omega+\alpha k$ does not lie in the interval $[\alpha\beta|k-q|,\alpha\beta(k+q)]$. Furthermore,
if $\alpha\beta=-1$ then we need to turn the limits around and multiply with $-1$.

We need to work out the following algebraic expression:
\begin{eqnarray*}
Q&=& \frac{y}{\sqrt{1-\left(\frac{y^2 - k^2-q^2}{2kq}\right)^2}}
\left(1 + \alpha\:\frac{k^2+y^2-q^2}{2ky}\right)   \\
&=&
\frac{y}{\sqrt{1-\left(\frac{y^2 - k^2-q^2}{2kq}\right)^2}}
+
\frac{\alpha}{2k}\:\frac{y^2+k^2-q^2}{\sqrt{1-\left(\frac{y^2 - k^2-q^2}{2kq}\right)^2}}\\
&=&
\frac{\alpha}{2k}\: \frac{2k\alpha y + y^2 + k^2-q^2}{\sqrt{1-\left(\frac{y^2 - k^2-q^2}{2kq}\right)^2}}
\end{eqnarray*}
Now we set $y=\omega + \alpha k$ and get
\begin{eqnarray*}
Q
&=&
\frac{\alpha}{2k}\: \frac{2k\alpha (\omega + \alpha k) + (\omega + \alpha k)^2 + k^2-q^2}{\sqrt{1-\left(\frac{(\omega + \alpha k)^2 - k^2-q^2}{2kq}\right)^2}}\\
&=&
\frac{\alpha}{2k}\: \frac{4k^2 + \omega^2 + 4k\alpha \omega -q^2}{\sqrt{1-\left(\frac{\omega^2 + 2k\alpha\omega -q^2}{2kq}\right)^2}}\\
&=&
\alpha q\: \frac{4k^2 + \omega^2 + 4k\alpha \omega -q^2}{\sqrt{4k^2q^2- (\omega^2 + 2k\alpha\omega -q^2 )^2}}\\
&=&
\alpha q\: \frac{4k^2 + \omega^2 + 4k\alpha \omega -q^2}{\sqrt{(q^2-\omega^2)(\omega^2+4k^2-q^2+4\alpha k\omega)}}\\
&=&
\alpha q\: \frac{\sqrt{4k^2 + \omega^2 + 4k\alpha \omega -q^2}}{\sqrt{q^2-\omega^2}}\\
&=&
\alpha q\: \sqrt{\frac{(2\alpha k + \omega)^2 -q^2}{q^2-\omega^2} }
\end{eqnarray*}
Substituting this back into Eq. (\ref{10}) gives
\begin{equation}\label{11}
I^{\alpha\beta}(k,q,\omega) = \alpha \sqrt{\frac{(2\alpha k + \omega)^2 -q^2}{q^2-\omega^2} } \:.
\end{equation}
The integral vanishes unless
\begin{eqnarray} \label{12}
\alpha\beta|k-q|<\omega+\alpha k<   \alpha\beta(k+q) \:, \qquad \qquad \alpha\beta=+1
\\
\alpha\beta|k-q|>\omega+\alpha k>   \alpha\beta(k+q) \:, \qquad \qquad \alpha\beta=-1
\label{13}
\end{eqnarray}


%The prefactor $\alpha$ goes away because
%of the integration limits in Eq. (\ref{10}): if $\alpha=-1$ then the limits are turned around which gives an overall minus sign which cancels %$\alpha$.


Let us distinguish two cases:

\underline{1. $\beta=-1$.} Eqs. (\ref{12}), (\ref{13}) give
\begin{eqnarray} \label{14}
|k-q| + k<\omega <   2k+q \:, \qquad \qquad \alpha=-1
\\
-|k-q| -  k >\omega >   -(2k+q) \:, \qquad \qquad \alpha=+1
\end{eqnarray}\label{15}
Since the frequency is positive, this can only be satisfied for $\alpha=-1$, and the only remaining condition
is Eq. (\ref{14}. We need to distinguish $k<q$ and $k>q$:
\begin{eqnarray*}
q  <\omega< 2k+q &\mbox{for}& k<q \\
2k-q  <\omega< 2k+q &\mbox{for}& k>q
\end{eqnarray*}
or, expressed via step functions,
\begin{eqnarray*}
\theta(\omega-q)\theta(2k+q-\omega) &\mbox{for}& k<q \\
\theta(\omega-2k+q)\theta(2k+q-\omega) &\mbox{for}& k>q
\end{eqnarray*}
or, taken together,
\begin{displaymath}
\theta(-\beta)\theta(-\alpha)\theta(2k+q-\omega)\Big[\theta(\omega-q)\theta(q-k) + \theta(\omega-2k+q)\theta(k-q)\Big]
\end{displaymath}

\underline{2. $\beta=+1$.} Eqs. (\ref{12}), (\ref{13}) give
\begin{eqnarray} \label{16}
|k-q| - k <\omega <  q  \:, \qquad \qquad \alpha=+1
\\
-|k-q| + k >\omega >  -q  \:, \qquad \qquad \alpha=-1
\label{17}
\end{eqnarray}
Consider first Eq. (\ref{16}). Again, we  distinguish $k<q$ and $k>q$:
\begin{eqnarray*}
q-2k  <\omega< q &\mbox{for}& k<q \\
-q <\omega< q &\mbox{for}& k>q
\end{eqnarray*}
and the result is
\begin{displaymath}
\theta(\beta)\theta(\alpha)\theta(q-\omega)\Big[\theta(\omega-q+2k)\theta(q-k) + \theta(k-q)\Big]
\end{displaymath}

Now consider Eq. (\ref{17}):
\begin{eqnarray*}
2k -q > \omega >- q &\mbox{for}& k<q \\
q > \omega> -q &\mbox{for}& k>q
\end{eqnarray*}
and the result is
\begin{displaymath}
\theta(\beta)\theta(-\alpha)\Big[\theta(2k-q-\omega)\theta(q-k) + \theta(q-\omega)\theta(k-q)\Big]
\end{displaymath}
Remember that there is a prefactor of $\alpha$ in Eq. (\ref{11}). We thus get the result
\begin{eqnarray*}
I^{\alpha\beta}(k,q,\omega) &=& \sqrt{\frac{(2\alpha k + \omega)^2 -q^2}{q^2-\omega^2} } \\
&\times& \bigg\{
-\theta(-\beta)\theta(-\alpha)\theta(2k+q-\omega)\Big[\theta(\omega-q)\theta(q-k) + \theta(\omega-2k+q)\theta(k-q)\Big]\\
&&
{}+\theta(\beta)\theta(\alpha)\theta(q-\omega)\Big[\theta(\omega-q+2k)\theta(q-k) + \theta(k-q)\Big] \\
&&
{}- \theta(\beta)\theta(-\alpha)\Big[\theta(2k-q-\omega)\theta(q-k) + \theta(q-\omega)\theta(k-q)\Big] \bigg\}
\end{eqnarray*}
This doesn't quite agree with the result of Wunsch et al.....


\end{document}
