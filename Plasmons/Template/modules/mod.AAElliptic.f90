module Elliptic
implicit none
integer (kind=4), parameter::SPrec=4,DP=8


contains


	real (kind=8) function Kell(k)
		use constants
		implicit none
		real (kind=8)::k
        real(4), parameter::pi2=real(pi/2D0)


		!Kell=rf_s(0D0,1D0-k**2,1D0)
		Kell=ellf(pi2,real(k))
	end function Kell

	real (kind=8) function Eell(k)
		implicit none
		real (kind=8)::k,ksq,temp
		real (kind=8),parameter::onethird=1D0/3D0

		ksq=k**2
		temp=1D0-ksq

		!Eell=rf_s(0D0,temp,1D0)-onethird*ksq*rd_s(0D0,temp,1D0)
		Eell=rf(0e0,real(temp),1e0)-onethird*ksq*rd(0e0,real(temp),1e0)

	end function Eell



FUNCTION rf_s(x,y,z)
!USE nrtype; USE nrutil, ONLY : assert
IMPLICIT NONE
REAL(DP), INTENT(IN) :: x,y,z
REAL(DP) :: rf_s
REAL(DP), PARAMETER :: ERRTOL=0.08_dp,TINY=1.5e-38_dp,BIG=3.0e37_dp,&
THIRD=1.0_dp/3.0_dp,&
 C1=1.0_dp/24.0_dp,C2=0.1_dp,C3=3.0_dp/44.0_dp,C4=1.0_dp/14.0_dp
!Computes Carlson’s elliptic integral of the first kind, R F (x, y, z). x, y, and z must be
!nonnegative, and at most one can be zero. TINY must be at least 5 times the machine
!underflow limit, BIG at most one-fifth the machine overflow limit.
REAL(dp) :: alamb,ave,delx,dely,delz,e2,e3,sqrtx,sqrty,sqrtz,xt,yt,zt
call assert(min(x,y,z) >= 0.0_dp, min(x+y,x+z,y+z) >= TINY, max(x,y,z) <= BIG, 'rf_s args')
xt=x
yt=y
zt=z
do
	sqrtx=sqrt(xt)
	sqrty=sqrt(yt)
	sqrtz=sqrt(zt)
	alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
	xt=0.25_dp*(xt+alamb)
	yt=0.25_dp*(yt+alamb)
	zt=0.25_dp*(zt+alamb)
	ave=THIRD*(xt+yt+zt)
	delx=(ave-xt)/ave
	dely=(ave-yt)/ave
	delz=(ave-zt)/ave
	if (max(abs(delx),abs(dely),abs(delz)) <= ERRTOL) exit
end do
e2=delx*dely-delz**2
e3=delx*dely*delz
rf_s=(1.0_dp+(C1*e2-C2-C3*e3)*e2+C4*e3)/sqrt(ave)
END FUNCTION rf_s

FUNCTION rd_s(x,y,z)
!USE nrtype; USE nrutil, ONLY : assert
IMPLICIT NONE
REAL(dp), INTENT(IN) :: x,y,z
REAL(dp) :: rd_s
REAL(dp), PARAMETER :: ERRTOL=0.05_dp,TINY=1.0e-25_dp,BIG=4.5e21_dp,&
 C1=3.0_dp/14.0_dp,C2=1.0_dp/6.0_dp,C3=9.0_dp/22.0_dp,&
 C4=3.0_dp/26.0_dp,C5=0.25_dp*C3,C6=1.5_dp*C4
!Computes Carlson’s elliptic integral of the second kind, R D (x, y, z). x and y must be
!nonnegative, and at most one can be zero. z must be positive. TINY must be at least twice
!the negative 2/3 power of the machine overflow limit. BIG must be at most 0.1 × ERRTOL
!times the negative 2/3 power of the machine underflow limit.
REAL(dp) :: alamb,ave,delx,dely,delz,ea,eb,ec,ed,&
ee,fac,sqrtx,sqrty,sqrtz,sum,xt,yt,zt
call assert(min(x,y) >= 0.0_dp, min(x+y,z) >= TINY, max(x,y,z) <= BIG, &
'rd_s args')
xt=x
yt=y
zt=z
sum=0.0_dp
fac=1.0_dp
do
	sqrtx=sqrt(xt)
	sqrty=sqrt(yt)
	sqrtz=sqrt(zt)
	alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
	sum=sum+fac/(sqrtz*(zt+alamb))
	fac=0.25_dp*fac
	xt=0.25_dp*(xt+alamb)
	yt=0.25_dp*(yt+alamb)
	zt=0.25_dp*(zt+alamb)
	ave=0.2_dp*(xt+yt+3.0_dp*zt)
	delx=(ave-xt)/ave
	dely=(ave-yt)/ave
	delz=(ave-zt)/ave
	if (max(abs(delx),abs(dely),abs(delz)) <= ERRTOL) exit
end do
ea=delx*dely
eb=delz*delz
ec=ea-eb
ed=ea-6.0_dp*eb
ee=ed+ec+ec
rd_s=3.0_dp*sum+fac*(1.0_dp+ed*(-C1+C5*ed-C6*delz*ee)&
+delz*(C2*ee+delz*(-C3*ec+delz*C4*ea)))/(ave*sqrt(ave))
END FUNCTION rd_s

SUBROUTINE assert(n1,n2,n3,string)
CHARACTER(LEN=*), INTENT(IN) :: string
LOGICAL, INTENT(IN) :: n1,n2,n3
if (.not. (n1 .and. n2 .and. n3)) then
write (*,*) 'nrerror: an assertion failed with this tag:', &
string
STOP 'program terminated by assert3'
end if
END SUBROUTINE assert




      FUNCTION ellf(phi,ak)
      REAL ellf,ak,phi
!CU    USES rf
      REAL s!,rf
      s=sin(phi)
      ellf=s*rf(cos(phi)**2,(1.-s*ak)*(1.+s*ak),1.)
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
!C****************************************************************
      FUNCTION rf(x,y,z)
      REAL rf,x,y,z,ERRTOL,TINY,BIG,THIRD,C1,C2,C3,C4
      PARAMETER (ERRTOL=.08,TINY=1.5e-38,BIG=3.E37,THIRD=1./3.,&
     C1=1./24.,C2=.1,C3=3./44.,C4=1./14.)
      REAL alamb,ave,delx,dely,delz,e2,e3,sqrtx,sqrty,sqrtz,xt,yt,zt
      if(min(x,y,z).lt.0..or.min(x+y,x+z,y+z).lt.TINY.or.max(x,y,&
     z).gt.BIG)stop 'invalid arguments in rf'
      xt=x
      yt=y
      zt=z
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        zt=.25*(zt+alamb)
        ave=THIRD*(xt+yt+zt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
      if(max(abs(delx),abs(dely),abs(delz)).gt.ERRTOL)goto 1
      e2=delx*dely-delz**2
      e3=delx*dely*delz
      rf=(1.+(C1*e2-C2-C3*e3)*e2+C4*e3)/sqrt(ave)
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY
!C****************************************************************
      FUNCTION ellpi(phi,en,ak)
      REAL ellpi,ak,en,phi
!CU    USES rf,rj
      REAL cc,enss,q,s!,rf,rj
      s=sin(phi)
      enss=en*s*s
      cc=cos(phi)**2
      q=(1.-s*ak)*(1.+s*ak)
      ellpi=s*(rf(cc,q,1.)-enss*rj(cc,q,1.,1.+enss)/3.)
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
!C****************************************************************
      FUNCTION rj(x,y,z,p)
      REAL rj,p,x,y,z,ERRTOL,TINY,BIG,C1,C2,C3,C4,C5,C6,C7,C8
      PARAMETER (ERRTOL=.05,TINY=2.5e-13,BIG=9.E11,C1=3./14.,C2=1./3.,&
     C3=3./22.,C4=3./26.,C5=.75*C3,C6=1.5*C4,C7=.5*C2,C8=C3+C3)
!CU    USES rc,rf
      REAL a,alamb,alpha,ave,b,beta,delp,delx,dely,delz,ea,eb,ec,ed,ee,&
     fac,pt,rcx,rho,sqrtx,sqrty,sqrtz,sum,tau,xt,yt,zt!,rc,rf
      if(min(x,y,z).lt.0..or.min(x+y,x+z,y+z,abs(p)).lt.TINY.or.max(x,y,&
     z,abs(p)).gt.BIG)stop 'invalid arguments in rj'
      sum=0.
      fac=1.
      if(p.gt.0.)then
        xt=x
        yt=y
        zt=z
        pt=p
      else
        xt=min(x,y,z)
        zt=max(x,y,z)
        yt=x+y+z-xt-zt
        a=1./(yt-p)
        b=a*(zt-yt)*(yt-xt)
        pt=yt+b
        rho=xt*zt/yt
        tau=p*pt/yt
        rcx=rc(rho,tau)
      endif
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        alpha=(pt*(sqrtx+sqrty+sqrtz)+sqrtx*sqrty*sqrtz)**2
        beta=pt*(pt+alamb)**2
        sum=sum+fac*rc(alpha,beta)
        fac=.25*fac
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        zt=.25*(zt+alamb)
        pt=.25*(pt+alamb)
        ave=.2*(xt+yt+zt+pt+pt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
        delp=(ave-pt)/ave
      if(max(abs(delx),abs(dely),abs(delz),abs(delp)).gt.ERRTOL)goto 1
      ea=delx*(dely+delz)+dely*delz
      eb=delx*dely*delz
      ec=delp**2
      ed=ea-3.*ec
      ee=eb+2.*delp*(ea-ec)
      rj=3.*sum+fac*(1.+ed*(-C1+C5*ed-C6*ee)+eb*(C7+delp*(-C8+delp*C4))+&
     delp*ea*(C2-delp*C3)-C2*delp*ec)/(ave*sqrt(ave))
      if (p.le.0.) rj=a*(b*rj+3.*(rcx-rf(xt,yt,zt)))
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
!C****************************************************************
      FUNCTION rc(x,y)
      REAL rc,x,y,ERRTOL,TINY,SQRTNY,BIG,TNBG,COMP1,COMP2,THIRD,C1,C2,&
     C3,C4
      PARAMETER (ERRTOL=.04,TINY=1.69e-38,SQRTNY=1.3e-19,BIG=3.E37,&
     TNBG=TINY*BIG,COMP1=2.236/SQRTNY,COMP2=TNBG*TNBG/25.,THIRD=1./3.,&
     C1=.3,C2=1./7.,C3=.375,C4=9./22.)
      REAL alamb,ave,s,w,xt,yt
      if(x.lt.0..or.y.eq.0..or.(x+abs(y)).lt.TINY.or.(x+&
     abs(y)).gt.BIG.or.(y.lt.-COMP1.and.x.gt.0..and.x.lt.COMP2))stop &
     'invalid arguments in rc'
      if(y.gt.0.)then
        xt=x
        yt=y
        w=1.
      else
        xt=x-y
        yt=-y
        w=sqrt(x)/sqrt(xt)
      endif
1     continue
        alamb=2.*sqrt(xt)*sqrt(yt)+yt
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        ave=THIRD*(xt+yt+yt)
        s=(yt-ave)/ave
      if(abs(s).gt.ERRTOL)goto 1
      rc=w*(1.+s*s*(C1+s*(C2+s*(C3+s*C4))))/sqrt(ave)
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
!C*******************************************************************
      FUNCTION elle(phi,ak)
      REAL elle,ak,phi
!CU    USES rd,rf
      REAL cc,q,s!,rd,rf
      s=sin(phi)
      cc=cos(phi)**2
      q=(1.-s*ak)*(1.+s*ak)
      elle=s*(rf(cc,q,1.)-((s*ak)**2)*rd(cc,q,1.)/3.)
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
!C*********************************************************************
      FUNCTION rd(x,y,z)
      REAL rd,x,y,z,ERRTOL,TINY,BIG,C1,C2,C3,C4,C5,C6
      PARAMETER (ERRTOL=.05,TINY=1.e-25,BIG=4.5E21,C1=3./14.,C2=1./6.,&
     C3=9./22.,C4=3./26.,C5=.25*C3,C6=1.5*C4)
      REAL alamb,ave,delx,dely,delz,ea,eb,ec,ed,ee,fac,sqrtx,sqrty,&
     sqrtz,sum,xt,yt,zt
      if(min(x,y).lt.0..or.min(x+y,z).lt.TINY.or.max(x,y,&
     z).gt.BIG)stop 'invalid arguments in rd'
      xt=x
      yt=y
      zt=z
      sum=0.
      fac=1.
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        sum=sum+fac/(sqrtz*(zt+alamb))
        fac=.25*fac
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        zt=.25*(zt+alamb)
        ave=.2*(xt+yt+3.*zt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
      if(max(abs(delx),abs(dely),abs(delz)).gt.ERRTOL)goto 1
      ea=delx*dely
      eb=delz*delz
      ec=ea-eb
      ed=ea-6.*eb
      ee=ed+ec+ec
      rd=3.*sum+fac*(1.+ed*(-C1+C5*ed-C6*delz*ee)+delz*(C2*ee+delz*(-C3*&
     ec+delz*C4*ea)))/(ave*sqrt(ave))
      return
      END
!C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.


end module Elliptic
