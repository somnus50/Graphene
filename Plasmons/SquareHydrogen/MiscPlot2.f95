program MiscPlot
	use constants	
	use NSquareHLindFunctions
	use HelperFunctions
	implicit none
	real    (kind=8):: frac,dfrac
!	real (kind=8), allocatable :: vect(:)
	integer (kind=4):: i


		dfrac=1D-2
		frac=dfrac
			write(*,*) "#frac,    fx,    fc"
			do i=1,100
				call ConstInit(frac,0D0,Ef,eta,Globfx,Globfc)
				write(*,*) frac, Globfx, Globfc, Globfx+Globfc
				frac=frac+dfrac
			enddo

end program MiscPlot
