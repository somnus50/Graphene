program main
use QuadPack
!*****************************************************************************80
!
!! MAIN is the main program for QUADPACK_PRB.
!
!  Discussion:
!
!    QUADPACK_PRB tests the QUADPACK library.
!
!  Modified:
!
!    11 September 2015
!
!  Author:
!
!    John Burkardt
!
  implicit none

  call timestamp ( )
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) 'QUADPACK_PRB'
  write ( *, '(a)' ) '  FORTRAN90 version'
  write ( *, '(a)' ) '  Test the QUADPACK library.'

  call qag_test ( )
  call qagi_test ( )
  call qagp_test ( )
  call qags_test ( )
  call qawc_test ( )
  call qawf_test ( )
  call qawo_test ( )
  call qaws_test ( )
  call qk15_test ( )
  call qk21_test ( )
  call qk31_test ( )
  call qk41_test ( )
  call qk51_test ( )
  call qk61_test ( )
  call qng_test ( )
!
!  Terminate.
!
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) 'QUADPACK_PRB'
  write ( *, '(a)' ) '  Normal end of execution.'
  write ( *, '(a)' ) ' '
  call timestamp ( )
 
  stop
end

