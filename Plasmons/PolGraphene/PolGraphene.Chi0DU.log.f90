program PolGrapheneChi0DUSmall
use Constants
use Globals
use PhysicsPolGraphene
use fxc2DEG
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z,wa,wb
real (kind=8), allocatable::wline(:),qgrid(:),wgrid(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	n=1D12/cmSq2a0sq
	z=-4D-2
	Globgs=2D0
	Globgv=2D0
	call ConstInit(n,z)
	temp=Globgs*Globgv/2D0/4D0/pi**2/4D0
	MPImaster=0
	IntSteps=300  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=100
	wsteps=100
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps),qgrid(qsteps),wgrid(wsteps))
	!wa=0.9D0*GlobZeem
	!wb=1.01D0*GlobZeem
	!dw=(wb-wa)/real(wsteps, kind=8)
	!dq=2D0*(GlobZeem-wa)/gam/real(qsteps, kind=8)
	call LogGrid(LGdrSuggest(1D-1,qsteps),qsteps,qgrid)
	qgrid=qgrid*Globkf0*100D0
	call LogGrid(LGdrSuggest(1D-1,wsteps),wsteps,wgrid)
	wgrid=wgrid*GlobEf*100D0
	q=0D0!(/dq,0D0/)
	open(unit=9, file='PolGraphene.Chi0DU.z4.log.q1.dat')
	open(unit=10, file='PolGraphene.Chi0DU.z4.log.q1.qgrid.dat')
	open(unit=11, file='PolGraphene.Chi0DU.z4.log.q1.wgrid.dat')

	do i=1,qsteps
		write(10,*) qgrid(i)/Globkf0
	enddo

	do i=1,wsteps
		write(11,*) wgrid(i)/GlobEf
	enddo

	close(unit=10)
	close(unit=11)

		write(9,*) "### Chi0UDIntra(q,w) LogGrid"
		write(9,*) "###", n, z, Globrs
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	write(*,*) "Ef   kf0"
	write(*,*) GlobEf,Globkf0
	q=0D0
	q(1)=qgrid(1)
	do i=1,qsteps
		w=wgrid(1)
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			!wline(j)=PolIntegratek(ReChi0DUIntraU,max(q(1)-GlobkfMAX,0D0),q(1)+Globkf0,IntSteps,q,w)*temp
			wline(j)=PolIntegratek2(ReChi0DUIntraU,0D0,2D0*Globkf0,IntSteps,q,w)*temp
			w=wgrid(j)
		enddo
		write(9,fmt2) wline
		q(1)=qgrid(i)
	enddo


	close(unit=9)
end program PolGrapheneChi0DUSmall
