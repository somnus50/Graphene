module readlib
implicit none
private
public :: multiread
interface multiread
	module procedure multireadchar
	module procedure multireadint
	module procedure multireadreal
end interface multiread

contains


subroutine multireadreal(labels,readfmt,readvals)
implicit none
integer (kind=4),  intent(in)::labels(:)
real (kind=8),    intent(out)::readvals(:,:)
character (len=32) ,intent(in)::readfmt
integer (kind=4)::i

do i=1,size(labels)
	read(labels(i),readfmt) readvals(i,:)
enddo

end subroutine multireadreal

subroutine multireadint(labels,readfmt,readvals)
implicit none
integer (kind=4),  intent(in)::labels(:)
integer (kind=4),    intent(out)::readvals(:,:)
character (len=32),intent(in)::readfmt
integer (kind=4)::i

do i=1,size(labels)
	read(labels(i),readfmt) readvals(i,:)
enddo

end subroutine multireadint

subroutine multireadchar(labels,readfmt,readvals)
implicit none
integer (kind=4),  intent(in)::labels(:)
character (len=32),intent(out)::readvals(:)
character (len=32) ,intent(in)::readfmt
integer (kind=4)::i

do i=1,size(labels)
	read(labels(i),readfmt) readvals(i)
enddo

end subroutine multireadchar



end module readlib
