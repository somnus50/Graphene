module PhysicsGraphene3
use Constants
use Globals
implicit none

contains

	subroutine ConstInit(n,zp)
		use Globals
		use fxc2DEG
		implicit none
		real (8), intent(in) ::n,zp
		real (8)             ::rs

			Globn=n
			Globzp=zp
			Globn0=1D0/(dsqrt(3D0)*aGraphene**2)*2D0
			Globkc=dsqrt(pi*Globn0)
			Globntot=Globn+Globn0

			Globz=Globn*zp/Globntot
			if (dabs(Globz).gt.(Globn/Globntot)) then
				! I might rewrite this subroutine to calculate the total polarization
				! given just the upper band polarization. This would allow the variable
				! z to range from -1 to 1.
				write(*,*) "Polarization is out of bounds. z:[-n1/n,-n1/n]"
				write(*,*) "n1/n:",Globn/Globntot,"z:",Globz,"z+:",zp
				call exit(-1)
			endif


			! rs=1D0/kappa/gam
			Globkf0=dsqrt(4D0*pi*n/Globgs/Globgv) !dsqrt(4D0*pi*n/(gs*gv))
			Globkc=dsqrt(4D0*pi/Globgs/Globgv*2D0/dsqrt(3D0)/aGraphene**2)
			GlobEf=gam*Globkf0

			Globeta=1D-4*GlobEf
			GlobetaSQ=Globeta**2
			GlobkfU=Globkf0*dsqrt((1D0+zp))
			GlobkfD=Globkf0*dsqrt((1D0-zp))
			GlobvfU=hbar*GlobkfU/m
			GlobvfD=hbar*GlobkfD/m
			GlobkfMAX=max(GlobkfU,GlobkfD)
			Globrs=n2rs(n)!rs
			GLobZeem=GlobEf*dabs(dsqrt(1D0+zp)-dsqrt(1D0-zp))!-2D0*GlobEf*zp
			HalfGlobZeem=GlobZeem/2D0
			! Globz=z
			Globzt=dsqrt(1D0-zp)+dsqrt(1D0+zp)
			Globzt=Globzt/dsqrt(2D0)
			Globkf=Globkf0




			! Globfxc=cfxssp(n2rs(Globntot),Globz)+cfcssp(n2rs(Globntot),Globz)!0D0
			Globfxc=cfxssp(n2rs(Globn),Globzp)+cfcssp(n2rs(Globn),Globzp)!0D0



	end subroutine ConstInit

	real(8) function En(b,k)
		implicit none
		real (8) ::k(2),b,temp,temp2
		! Dirac energy band of Graphene
		! b :: real, band index, (+/-)1 => (upper/lower) band
		! k :: real, polar wavevector

			En=b*gam*k(1)
	end function En

	real(8) function f(b,k)
		implicit none
		real (8) ::k(2),b
		! Fermi-Dirac Distribution for electrons
		! b :: real, band index
		! q :: real, polar wavevector

			f=0D0
			!!!Zero Temperature
			if ((En(b,k)).le.(GlobEf)) then
				f=1D0
			endif
	end function f

	real(8) function ImChi0(q,w)
		real(8)::q,w,wt,k2

			wt=w/gam
			k2=2D0*Globkf

			ImChi0=0D0
			if (q.eq.0D0) return

			if ( q.gt.wt ) then ! *A
				if ((q-k2).ge.wt) then ! 3A
					ImChi0=0D0
				elseif ( (wt.ge.(k2-q)).and.(wt.ge.(q-k2)) ) then ! 2A
					ImChi0=gg((wt+k2)/q)
				!elseif ( (k2-q).ge.wt ) then ! 1A
				else
					ImChi0=gg((wt+k2)/q)-gg((k2-wt)/q)
				end if
			else ! *B
				if (wt.ge.(k2+q)) then ! 3B
					ImChi0=-pi
				elseif ( (wt.ge.(k2-q)).and.((k2+q).ge.wt) ) then ! 2B
					ImChi0=-gl((k2-wt)/q)
				!elseif ( (k2-q).ge.wt ) then ! 1B
				else
					ImChi0=0D0
				end if
			end if
			ImChi0=ImChi0*ChiBare(q,wt)
	end function ImChi0

	real(8) function ImChi0Ex(q,w)
		real(8)::q,w,wt,kf2,kc2

			wt=w/gam
			kf2=2D0*Globkf
			kc2=2D0*Globkc

			ImChi0Ex=0D0
			if (q.eq.0D0) return

			if ( q.gt.wt ) then ! *A
				if (wt.lt.(kc2-q)) then !123A
					if (wt.le.(q-kf2)) then ! 3A
						 ImChi0Ex=gg((kc2-wt)/q)-gg((wt+kc2)/q)
						! ImChi0Ex=-gg((kc2-wt)/q)+gg((wt+kc2)/q)
					elseif ( (wt.ge.(kf2-q)).and.(wt.ge.(q-kf2)) ) then ! 2A
						 ImChi0Ex=gg((wt+kf2)/q)+gg((kc2-wt)/q)-gg((wt+kc2)/q)
						! ImChi0Ex=gg((wt+kf2)/q)-gg((kc2-wt)/q)+gg((wt+kc2)/q)
					else ! 1A
						 ImChi0Ex=gg((wt+kf2)/q)-gg((kf2-wt)/q)+gg((kc2-wt)/q)-gg((wt+kc2)/q)
						! ImChi0Ex=gg((wt+kf2)/q)-gg((kf2-wt)/q)-gg((kc2-wt)/q)+gg((wt+kc2)/q)
					end if
				else ! 456A
					if ( wt.lt.(q-kc2) ) then ! 6A
						ImChi0Ex=0D0
					elseif ( wt.lt.(q-kf2) ) then ! 5A
						 ImChi0Ex=-gg((wt+kc2)/q)
						! ImChi0Ex=gg((wt+kc2)/q)
					else ! 4A
						 ImChi0Ex=gg((kf2+wt)/q)-gg((kc2+wt)/q)
						! ImChi0Ex=gg((kf2+wt)/q)+gg((kc2+wt)/q)
					end if
				endif

			else ! *B
				if ( wt.lt.(kc2-q) ) then ! 123B
					if (wt.ge.(kf2+q)) then ! 3B
						ImChi0Ex=-pi
						! ImChi0Ex=pi
					elseif ( (wt.ge.(kf2-q)).and.((kf2+q).ge.wt) ) then ! 2B
						 ImChi0Ex=-gl((kf2-wt)/q)
						! ImChi0Ex=+gl((kf2-wt)/q)
				else ! 1B
						ImChi0Ex=0D0
					end if
				else ! 456B
					if ( wt.gt.(kc2+q) ) then ! 6B
						ImChi0Ex=0D0
					elseif (wt.gt.(kf2+q)) then ! 5B
						ImChi0Ex=-gl((wt-kc2)/q)
					else ! 4B
						! ImChi0Ex=-gl((wt-kc2)/q)+gl((wt-kf2)/q)
						ImChi0Ex=gl((kc2-wt)/q)-gl((kf2-wt)/q)
					end if
				endif
			end if
			ImChi0Ex=ImChi0Ex*ChiBare(q,wt)
	end function ImChi0Ex

	real(8) function ReChi0Ex(q,w)
		real(8)::q,w,wt,kf2,kc2

			wt=w/gam
			kf2=2D0*Globkf
			kc2=2D0*Globkc

			ReChi0Ex=0D0
			if (q.eq.0D0) return

			! I've put the regions in order of size so that we statistically reduce the number of if evaluations
			if ( q.gt.wt ) then ! *A
				if (wt.lt.(kc2-q)) then !123A
					if (wt.le.(q-kf2)) then ! 3A
						ReChi0Ex=-gl((kf2-wt)/q)-gl((kf2+wt)/q)
					elseif ( (wt.ge.(kf2-q)).and.(wt.ge.(q-kf2)) ) then ! 2A
						ReChi0Ex=-gl((kf2-wt)/q)
					else ! 1A
						ReChi0Ex=0D0
					end if
				else ! 456A
					if ( wt.lt.(q-kc2) ) then ! 6A
						ReChi0Ex=gl((kc2+wt)/q)+gl((kc2-wt)/q)-gl((kf2+wt)/q)-gl((kf2-wt)/q)
					elseif ( wt.lt.(q-kf2) ) then ! 5A
						ReChi0Ex=gl((kc2-wt)/q)-gl((kf2+wt)/q)-gl((kf2-wt)/q)
					else ! 4A
						ReChi0Ex=gl((kc2-wt)/q)-gl((kf2-wt)/q)
					end if
				endif

			else ! *B
				if ( wt.lt.(kc2-q) ) then ! 123B
					if (wt.ge.(kf2+q)) then ! 3B
						ReChi0Ex=gg((kc2+wt)/q)-gg((kc2-wt)/q)-gg((kf2+wt)/q)+gg((wt-kf2)/q)
					elseif ( (wt.ge.(kf2-q)).and.((kf2+q).ge.wt) ) then ! 2B
						ReChi0Ex=-gg((kf2+wt)/q)+gg((kc2+wt)/q)-gg((kc2-wt)/q)
				else ! 1B
						ReChi0Ex=gg((kf2-wt)/q)-gg((kf2+wt)/q)+(gg((kc2+wt)/q)-gg((kc2-wt)/q))!+pi
						!ReChi0Ex=-gg((kf2+wt)/q)+gg((kf2-wt)/q)
					end if
				else ! 456B
					if ( wt.gt.(kc2+q) ) then ! 6B
						ReChi0Ex=gg((kc2+wt)/q)-gg((wt-kc2)/q)-gg((kf2+wt)/q)+gg((wt-kf2)/q)
					elseif (wt.gt.(kf2+q)) then ! 5B
						ReChi0Ex=gg((kc2+wt)/q)-gg((kf2+wt)/q)+gg((wt-kf2)/q)
					else ! 4B
						ReChi0Ex=gg((kc2+wt)/q)-gg((kf2+wt)/q)
					end if
				endif
			end if
			ReChi0Ex=ChiBare(q,wt)*ReChi0Ex+(Globkc-Globkf)/(2D0*pi*gam)
			!ReChi0Ex=ChiBare(q,wt)*ReChi0Ex-Globkf/(2D0*pi*gam)
	end function ReChi0Ex


	real(8) function ReChi0(q,w)
		real(8)::q,w,wt,k2

			wt=w/gam
			k2=2D0*Globkf

			ReChi0=0D0
			if (q.eq.0D0) return

			! I've put the regions in order of size so that we statistically reduce the number of if evaluations
			if ( q.gt.wt ) then ! *A
				if ((q-k2).ge.wt) then ! 3A
					ReChi0=-gl((k2-wt)/q)-gl((k2+wt)/q)
				elseif ( (wt.ge.(k2-q)).and.(wt.ge.(q-k2)) ) then ! 2A
					ReChi0=-gl((k2-wt)/q)
				!elseif ( (k2-q).ge.wt ) then ! 1A
				else
					ReChi0=0D0
				end if
			else ! *B
				if (wt.ge.(k2+q)) then ! 3B
					ReChi0=gg((wt-k2)/q)-gg((wt+k2)/q)
				elseif ( (wt.ge.(k2-q)).and.((k2+q).ge.wt) ) then ! 2B
					ReChi0=-gg((wt+k2)/q)
				!elseif ( (k2-q).ge.wt ) then ! 1B
				else
					ReChi0=gg((k2-wt)/q)-gg((k2+wt)/q)
				end if
			end if
			ReChi0=-Globkf/(2D0*pi*gam)+ChiBare(q,wt)*ReChi0
	end function ReChi0

	real(8) function gl(x)
		implicit none
		real(8)::x

		if (.not.( x**2.le.1D0)) then
			write(*,*) "Invalid value in gl (x^2<=1)"
			write(*,*) "x:",x
			call exit(-1)
		end if
		gl=dacos(x)-x*dsqrt(1D0-x**2)
	end function gl

	real(8) function gg(x)
		implicit none
		real(8)::x

		if (.not.( x.ge.1D0)) then
			write(*,*) "Invalid value in gg (x>=1)"
			write(*,*) "x:",x
			call exit(-1)
		end if
		gg=dacosh(x)-x*dsqrt(x**2-1D0)
	end function gg

	real(8) function ChiBare(q,wt)
		real(8)::q,wt
		real(8), parameter::temp=1D0/16D0/pi


		ChiBare=q**2*temp/(gam*dsqrt(dabs(q**2-wt**2)+(1D-10*Globkf)**2))
	end function ChiBare

	real(8) function epsRPA(q,w)
		implicit none
		real(8):: q,w

		! epsRPA=1D0-(Globgs+Globgv)*(Vq(q))*ReChi0(q,w)
		! epsRPA=1D0+(Vq(q))*(ReChi0Ex(q,w))*(Globgs+Globgv)!/kappa
		epsRPA=1D0-(Vq(q)+PGG(q))*(ReChiPeak(q,w))*(Globgs+Globgv)!/kappa
	end function epsRPA

	real(8) function epsPGG(q,w)
		implicit none
		real(8):: q,w

		! epsPGG=1D0-(Globgs+Globgv)*(Vq(q)+PGG(q))*ReChi0(q,w)
		! epsPGG=1D0-(Vq(q)+PGG(q))*ReChi0Ex(q,w)*(Globgs+Globgv)!/kappa

		! epsPGG=1D0+(Vq(q)+PGG(q))*(ReChi0Ex(q,w)-ReChitest2(q,w))*(Globgs+Globgv)!/kappa
		! epsPGG=1D0-(Vq(q)+PGG(q))*(ReChiPeak(q,w))*(Globgs+Globgv)!/kappa
		epsPGG=1D0-(Globgs+Globgv)*(Vq(q)+PGG(q))*ReChi0(q,w)


	end function epsPGG

	real(8) function wplasmonPGG(q,a,b)
 	 use AuxiliaryFunctions
	 implicit none
	 real(8)::q,a,b,resfn
 	 real(8),parameter::eps=1D-8

 	 !write(*,*) "a,b"
 	 !write(*,*) a,b
 	 call RootFinder(anc,a,b,eps,wplasmonPGG,resfn)
 	 !write(*,*) "Zero found"
 	 !write(*,*) "q,w,epsRPA(q,w)"
 	 !write(*,*) q/Globkf,wplasmonPGG/GlobEf,resfn


  contains
 	 real(8) function anc(w)
 	 		real(8)::w

 			anc=epsPGG(q,w)
 	 end function anc
 end function wplasmonPGG

 real(8) function wplasmonRPA(q,a,b)
	 use AuxiliaryFunctions
	 real(8)::q,a,b,resfn
	 real(8),parameter::eps=1D-9

	 write(*,*) "a,b"
	 write(*,*) a,b
	 call RootFinder(anc,a,b,eps,wplasmonRPA,resfn)
	 !write(*,*) "Zero found"
	 !write(*,*) "q,w,epsRPA(q,w)"
	 !write(*,*) q/Globkf,wplasmonRPA/GlobEf,resfn


 contains
	 real(8) function anc(w)
	 		real(8)::w

			anc=epsRPA(q,w)
	 end function anc
 end function wplasmonRPA

real(8) function Vq(q)
	implicit none
	real(8)::q
	real(8), parameter::a=2D0*pi

	Vq=a/q
 end function Vq


 real (kind=8) function PGG(q)
	use QuadPackDouble
	implicit none
	real (kind=8)::q,qt,kct
	integer (kind=4), parameter::limit=10000
	real (kind=8)   , parameter::minpi=-pi**(-1)
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),temp


	call dqagie ( anc, 0d0, 1, 1D-8, 1D-8, limit , temp, abserr, &
					 neval, ier, alist, blist, rlist, elist, iord, last )

	PGG=-16D0/pi/Globntot**2*temp

contains
	real(8) function anc(r)
		implicit none
		real(8)::r

		if ( r.eq.0D0 ) then
			anc=(Globkf+Globkc)**2/4D0
			return
		end if

		anc=bessel_jn(0,q*r)*(Globkf*bessel_jn(1,Globkf*r)+Globkc*bessel_jn(1,Globkc*r))**2/r**2
	end function anc
end function PGG

real(8) function Struct(q)
	use QuadPackDouble
	implicit none
	real(8)::q
	integer (kind=4), parameter::limit=10000,key=1
	real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),temp,temp2,dk


	! call dqagie ( anc, 0d0, 1, eps, eps, limit , temp2, abserr, &
	! 			 neval, ier, alist, blist, rlist, elist, iord, last )


	dk=1D-10*Globkf
	temp2=0D0
	if ( q.lt.Globkf ) then
		call dqage ( anc, 0D0, gam*(q-dk), eps, eps, key, limit, temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf-q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
		call dqagie ( anc, gam*(2D0*Globkf+q+dk), 1, eps, eps, limit , temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
	elseif ( q.lt.2D0*Globkf ) then
		call dqage ( anc, 0D0, gam*(2D0*Globkf-q-dk), eps, eps, key, limit, temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf-q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
		call dqagie ( anc, gam*(2D0*Globkf+q+dk), 1, eps, eps, limit , temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last )
		temp2=temp2+temp
	else
		call dqage ( anc, 0D0, gam*(q-2D0*Globkf-dk), eps, eps, key, limit, temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last ) ! Always equal to zero
		temp2=temp2+temp
		call dqage ( anc, gam*(q-2D0*Globkf+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! goes to a constant at high q
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! Keeps growing with q
		temp2=temp2+temp
		call dqagie ( anc, gam*(2D0*Globkf+q+dk), 1, eps, eps, limit , temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last ) ! huge and growing with q
		temp2=temp2+temp
	end if


	Struct=-temp2/pi/Globn

contains
	real(8) function anc(w)
		implicit none
		real(8)::w

		anc=ImChi0(q,w)
	end function anc
end function Struct

real(8) function StructExt(q)
	use QuadPackDouble
	implicit none
	real(8)::q
	integer (kind=4), parameter::limit=10000,key=1
	real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),temp,temp2,dk


	! call dqagie ( anc, 0d0, 1, eps, eps, limit , temp2, abserr, &
	! 			 neval, ier, alist, blist, rlist, elist, iord, last )


	dk=1D-10*Globkf
	temp2=0D0
	if ( q.lt.Globkf ) then
		call dqage ( anc, 0D0, gam*(q-dk), eps, eps, key, limit, temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last ) ! 1A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 1B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf-q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkc-q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		!   			 neval, ier, alist, blist, rlist, elist, iord, last )
		! temp2=temp2+temp


	elseif ( q.lt.2D0*Globkf ) then
		call dqage ( anc, 0D0, gam*(2D0*Globkf-q-dk), eps, eps, key, limit, temp, abserr, &
		  neval, ier, alist, blist, rlist, elist, iord, last ) ! 1A
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf-q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkc-q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
		! temp2=temp2+temp


	elseif ( q.lt.(Globkc-Globkf) ) then
		call dqage ( anc, 0D0, gam*(-2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3A
		temp2=temp2+temp
		call dqage ( anc, gam*(-2D0*Globkf+q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkc-q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
		! temp2=temp2+temp

	elseif ( q.lt.Globkc ) then
		call dqage ( anc, 0D0, gam*(-2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3A
		temp2=temp2+temp
		call dqage ( anc, gam*(-2D0*Globkf+q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkc-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkc-q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
		! temp2=temp2+temp

	elseif ( q.lt.(Globkc+Globkf) ) then
		call dqage ( anc, 0D0, gam*(-2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3A
		temp2=temp2+temp
		call dqage ( anc, gam*(-2D0*Globkf+q+dk), gam*(2D0*Globkc-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 2A
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkc-q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
		! temp2=temp2+temp

	elseif ( q.lt.2D0*Globkc ) then
		call dqage ( anc, 0D0, gam*(2D0*Globkc-q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 3A
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkc-q+dk), gam*(-2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5A
		temp2=temp2+temp
		call dqage ( anc, gam*(-2D0*Globkf+q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
		! temp2=temp2+temp

	else
		call dqage ( anc, 0D0, gam*(-2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 6A
		temp2=temp2+temp
		call dqage ( anc, gam*(-2D0*Globkc+q+dk), gam*(-2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5A
		temp2=temp2+temp
		call dqage ( anc, gam*(-2D0*Globkf+q+dk), gam*(q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4A
		temp2=temp2+temp
		call dqage ( anc, gam*(q+dk), gam*(2D0*Globkf+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 4B
		temp2=temp2+temp
		call dqage ( anc, gam*(2D0*Globkf+q+dk), gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
		temp2=temp2+temp
		! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
		! temp2=temp2+temp
	end if

	! call dqage ( anc, 0D0, gam*(2D0*Globkc+q-dk), eps, eps, key, limit, temp, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last ) ! 6A
	! temp2=temp2+temp
	! call dqagie ( anc, gam*(2D0*Globkc+q+dk), 1, eps, eps, limit , temp, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last ) ! 6B
	! temp2=temp2+temp

	StructExt=-temp2/pi/Globntot

contains
	real(8) function anc(w)
		implicit none
		real(8)::w

		anc=ImChi0Ex(q,w)
	end function anc
end function StructExt


real(8) function Gp0(q)
	use QuadPackDouble2
	real(8):: q
	integer (kind=4), parameter::limit=10000,key=1
	real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),temp,temp2,dk


	call dqage ( anc, 0D0, 2D0*Globkc-1D-10*Globkc, eps, eps, key, limit, temp, abserr, &
		neval, ier, alist, blist, rlist, elist, iord, last ) ! 5B
	!temp2=temp2+temp

	call dqagie ( anc, 2D0*Globkc+1D-10*Globkc, 1, eps, eps, limit , temp2, abserr, &
		neval, ier, alist, blist, rlist, elist, iord, last )

		Gp0=-(temp+temp2)/pi**2/Globntot
contains
	real(8) function anc(x)
		use Elliptic
		real(8):: x


		anc=(StructExt(q)-1D0)*(q*x)/(q+x)*Kell(2D0*dsqrt(q*x)/(q+x))
	end function anc
end function Gp0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!! Outdated stuff !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	real(8) function ImLindInt(q,w,k,a,b,bb)
		real(8)::q,w,k(2),a,b,bb,kp

		ImLindInt=0D0
		kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*dcos(k(2)))
		if (f(b,k).gt.0D0) then
			ImLindInt=-a*Globeta*0.5D0*(1D0+bb*(k(1)+a*q*dcos(k(2)))/(kp))
			ImLindInt=ImLindInt/((w+a*b*gam*(k(1)-bb*kp))**2+Globeta**2)
		endif
	end function ImLindInt

	real(8) function ReLindInt(q,w,k,a,b,bb)
		real(8)::q,w,k(2),a,b,bb,kp

		ReLindInt=0D0
		kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*dcos(k(2)))
		if (f(b,k).gt.0D0) then
			ReLindInt=(w+a*b*gam*(k(1)-bb*kp))
			ReLindInt=a*ReLindInt*0.5D0*(1D0+bb*(k(1)+a*q*dcos(k(2)))/(kp))&
								/(ReLindInt**2+GlobEta**2)
		endif
	end function ReLindInt


	real(8) function ReNLind(q,w)
		real(8)::q,w
		integer(4)::a,b,bb

		ReNLind=0D0
		do a = -1,1,2
			do b = -1,1,2
				do bb = -1,1,2
					ReNLind=ReNLind+ReNLindpart(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8))
				end do
			end do
		end do
		!ReNLind=ReNLind/(4D0*pi**2)
	end function ReNLind

	real(8) function ReNLindpart(q,w,a,b,bb)
		real(8)::q,w,a,b,bb
		integer(4), parameter::steps=1000
		real(8), parameter::infourpiSQ=1D0/(4D0*pi**2)

		ReNLindpart=0D0
		!ReNLindpart=PolIntegratephi12(anc,0D0,Globkf,0D0,0D0,phirange0,steps,q,w)*infourpiSQ
		if ( b.gt.0D0 ) then
			ReNLindpart=PolIntegratephi11(anc,0D0,Globkf0,0D0,0D0,phirange0,steps,q,w)*infourpiSQ
		else
			ReNLindpart=PolIntegratephi11(anc,0D0,Globkc,0D0,0D0,phirange0,5*steps,q,w)*infourpiSQ!int(Globkc/Globkf)
		end if

	contains
		real(8) function anc(q,w,k)
			real(8)::k(2),q,w

			anc=ReLindInt(q,w,k,a,b,bb)
		end function anc
	end function ReNLindpart

	real(8) function ImNLind(q,w)
		real(8)::q,w
		integer(4)::a,b,bb

		ImNLind=0D0
		do a = -1,1,2
			do b = -1,1,2
				do bb = -1,1,2
					ImNLind=ImNLind+ImNLindpart(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8))
					! ImNLind=ImNLind+ImNChiDelta(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8))
				end do
			end do
		end do
		ImNLind=ImNLind/(4D0*pi**2)
	end function ImNLind

	real(8) function ImNLindpart(q,w,a,b,bb)
		real(8)::q,w,a,b,bb
		integer(4), parameter::steps=100
		real(8), parameter::infourpiSQ=1D0/(4D0*pi**2)

		! write(*,*) "ImNLindpart a,b,bb",a,b,bb


		ImNLindpart=0D0
		if ( b.gt.0D0 ) then
			! ImNLindpart=PolIntegratephi12(anc,0D0,Globkf0,0D0,0D0,phirange0,steps,(/q,0D0/),w)*infourpiSQ
			ImNLindpart=PolIntegratek22(anc,0D0,Globkf0,0D0,2D0*pi,phirange0,steps,q,w)*infourpiSQ
		else
			! ImNLindpart=PolIntegratephi12(anc,0D0,Globkc,0D0,0D0,phirange0,10*steps,(/q,0D0/),w)*infourpiSQ
			ImNLindpart=PolIntegratek22(anc,0D0,Globkc,0D0,2D0*pi,phirange0,steps,q,w)*infourpiSQ
		end if
	contains
		real(8) function anc(q,w,k)
			use AuxiliaryFunctions
			real(8)::k(2),q,w,kp(2)

			kp=PolAdd(k,(/q,0D0/))

			anc=-Globeta*b/gam*0.5D0*(1D0+bb*dcos(kp(2)-k(2)))/((a*w/gam+k(1)-bb*kp(1))**2+GlobetaSQ)
		end function anc
	end function ImNLindpart

	real(8) function ImNChiDelta(q,w,a,b,bb)
		real(8)::q,w,a,b,bb
		integer(4), parameter::steps=3000


		if ( b.eq.1D0 ) then
			ImNChiDelta=PolIntegratephi12(anc,0D0,Globkf,0D0,0D0,phirange0,steps,(/q,0D0/),w)
		else
			ImNChiDelta=PolIntegratephi12(anc,0D0,Globkc,0D0,0D0,phirange0,steps,(/q,0D0/),w)
		end if

	contains
		real(8) function anc(q,w,k)
			use AuxiliaryFunctions
			real(8)::q,w,k(2),th,kp,sigma
			real(8), parameter::temp=-1D0/8D0/pi!,sigma=4D-4
			logical::kocc,kpunocc


			sigma=Globeta
			th=dcos(k(2))
			kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*th)

			!anc=0D0
			anc=temp*a*(1D0+bb*(k(1)+a*q*th)/kp)*NumDelta(w+a*b*gam*(k(1)-bb*kp),sigma)
		end function anc
	end function ImNChiDelta


	real(8) function PolIntegratephi11(fn,a,b,A1,B1,phisub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer(4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1
		real(8):: res,temp,phis(2),kf1,kf2,eps
		real(8),allocatable::temp1(:)
		real(8), external:: fn
		external :: phisub
		real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			phisteps=steps
			eps=0D-2
			ksteps=(steps/9)*9 +1
			dk=(b-a)/real(ksteps, kind=8)
			PolIntegratephi11=0D0
			k=a
			allocate(temp1(ksteps))
			do i=1,ksteps
					res=0D0
					call phisub(k,q,A1,B1,phis)
					!call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
					call StupidIntegrateNC2(anc,phis(1),phis(2),phisteps,res,ier)
				temp1(i)=res!temp
				k=k+dk
			enddo


			t1=1
			t2=10
			do i=1,(ksteps/9)
				PolIntegratephi11=PolIntegratephi11+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratephi11=PolIntegratephi11*dk    !/fourpisq     !NC10


		contains

			real(8) function anc(phi)
				implicit none
				real(8)::phi

				anc=k*fn(q,w,(/k,phi/))
			end function anc

			real(8) function NC10PolList(flist)
				integer(4)::i
				real(8)   ::flist(10)
				real(8), external::fn
				real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
			end function NC10PolList
	end function PolIntegratephi11

	real(8) function PolIntegratephi12(fn,a,b,A1,B1,phisub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer(4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real(8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),B1,A1
		real(8):: res,temp,phis(2),kf1,kf2,eps
		real(8),allocatable::temp1(:)
		real(8), external:: fn
		external :: phisub
		real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

		! th(k)

			phisteps=steps
			eps=0D-2
			ksteps=(steps/9)*9 +1
			dk=(b-a)/real(ksteps, kind=8)
			PolIntegratephi12=0D0
			k=a
			allocate(temp1(ksteps))
			do i=1,ksteps
					res=0D0
					call phisub(k,q,A1,B1,phis)
					!write(*,*) "phis",phis
					!write(*,*) "k", k

					call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
					!call StupidIntegrateNC2(anc,phis(1),phis(2),phisteps,res,ier)
				temp1(i)=res!temp
				k=k+dk
			enddo


			t1=1
			t2=10
			do i=1,(ksteps/9)
				PolIntegratephi12=PolIntegratephi12+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratephi12=PolIntegratephi12*dk    !/fourpisq     !NC10


		contains

			real(8) function anc(phi)
				implicit none
				real(8)::phi

				anc=k*fn(q,w,(/k,phi/))
			end function anc

			real(8) function NC10PolList(flist)
				integer(4)::i
				real(8)   ::flist(10)
				real(8), external::fn
				real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
			end function NC10PolList
	end function PolIntegratephi12

	real(8) function PolIntegratek22(fn,a,b,A1,B1,phisub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer(4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1
		real(8):: res,temp,phis(2),kf1,kf2,eps
		real(8),allocatable::temp1(:)
		real(8), external:: fn
		external :: phisub
		real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

		! th(k)

			ksteps=steps
			eps=0D-2
			phisteps=(steps/9)*9 +1
			dphi=(B1-A1)/real(phisteps, kind=8)
			PolIntegratek22=0D0
			phi=a
			allocate(temp1(phisteps))
			do i=1,phisteps
					res=0D0
					!write(*,*) "phis",phis
					! write(*,*) "phi", phi,"a",a,"b",b

					!call GaussAdaptQuad(anc,a,b,1D-8,res)
					call StupidIntegrateNC2(anc,a,b,ksteps,res,ier)
					! write(*,*) res
				temp1(i)=res!temp
				phi=phi+dphi
			enddo


			t1=1
			t2=10
			do i=1,(phisteps/9)
				PolIntegratek22=PolIntegratek22+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratek22=PolIntegratek22*dphi    !/fourpisq     !NC10


		contains

			real(8) function anc(k)
				implicit none
				real(8)::k

				anc=k*fn(q,w,(/k,phi/))
			end function anc

			real(8) function NC10PolList(flist)
				integer(4)::i
				real(8)   ::flist(10)
				real(8), external::fn
				real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
			end function NC10PolList
	end function PolIntegratek22


	subroutine phirange0(k,q,A,B,phis)
		implicit none
		real(8), intent(in)::k,q(2),A,B
		real(8), intent(out)::phis(2)
		real(8), parameter::twopi=2D0*pi


		phis(2)=twopi
		phis(1)=0D0
	end subroutine phirange0


	subroutine krange(q,w,a,b,bb,k1,k2,valid)
		real(8), intent(in) ::q,w,a,b,bb
		real(8), intent(out)::k1,k2
		integer(4),intent(out)::valid
		integer(4)::i
		real(8)::lims(16,7),test(4),qwp,qwm,wt
		logical::found

		wt=w/gam
		qwp=(q+wt)/2D0
		qwm=(q-wt)/2D0
		!  a,  b, bb,q>w,k1,k2,valid
		lims=reshape((/1D0,1D0,1D0,1D0,qwm,Globkf,1D0,&
		1D0,1D0,-1D0,1D0,0D0,0D0,-1D0,&
		1D0,-1D0,1D0,1D0,qwp,1000D0*Globkf,2D0,& ! technically should be infinity
		1D0,-1D0,-1D0,1D0,qwp,wt,-1D0,&
		-1D0,1D0,1D0,1D0,qwp,Globkf,1D0,&
		-1D0,1D0,-1D0,1D0,qwp,min(wt,Globkf),-1D0,&
		-1D0,-1D0,1D0,1D0,qwm,1000D0*Globkf,2D0,&! technically should be infinity
		-1D0,-1D0,-1D0,1D0,0D0,0D0,-1D0,&
		1D0,1D0,1D0,-1D0,0D0,min(qwp,Globkf),-1D0,&
		1D0,1D0,-1D0,-1D0,0D0,0D0,-1D0,&
		1D0,-1D0,1D0,-1D0,wt,qwp,-1D0,& ! Probably never valid
		1D0,-1D0,-1D0,-1D0,0D0,qwp,1D0,&
		-1D0,1D0,1D0,-1D0,wt,min(qwp,Globkf),1D0,&
		-1D0,1D0,-1D0,-1D0,-qwm,min(qwp,Globkf),1D0,&
		-1D0,-1D0,1D0,-1D0,0d0,0D0,-1D0,&
		-1D0,-1D0,-1D0,-1D0,0D0,0D0,-1D0/),shape(lims),order=(/ 2, 1 /))

		!do i=1,16
		!	write(*,*) lims(i,:)
		!enddo

		test=(/a,b,bb,sign(1D0,q-wt)/)

		do i=1,16
			if ( all(test.eq.lims(i,1:4)) ) then
				k1=lims(i,5)
				k2=lims(i,6)
				valid=int(lims(i,7),kind=4)
				found=.true.
				exit
			end if
		enddo

		if ( .not.found ) then
			valid=-3
			write(*,*) test, "not found in limits"
			call exit(valid)
		end if
	end subroutine krange

	real(8) function ReN2Lind(q,w)
		implicit none
		real(8)::q,w
		integer(4)::b,bp

		ReN2Lind=0D0
		do b=-1,1,2
			do bp=-1,1,2
				ReN2Lind=ReN2Lind+ReN2Lindpart(q,w,real(b,kind=8),real(bp,kind=8))
			end do
		end do

	end function ReN2Lind


	real(8) function ReN2Lindpart (q,w,b,bp)
		real(8):: bs,bp,q,w,temp1,temp2,temp3,A,B
		real(8),parameter::infourpiSQ=1D0/4D0/pi**2, eps=1D-8,eps2=1D-8
		integer(4),parameter::steps=1000,adj=100


	! 	ReN2Lindpart=0D0
	!
	! if (q.lt.2d0*GlobKf0) then
	! 		!k:  q-B<>A
	! 		!th: -th1<>th1
	!
	! 		write(*,*) "1.1"
	! 		ReN2Lindpart=ReN2Lindpart+PolIntegratephi12(anc,q-GlobKf0+eps,GlobKf0-eps,GlobKf0,GlobKf0,phirange22,steps,(/q,0D0/),w)
	!
	! 		!k:  A<>q+B
	! 		!th: th1<>2pi-th1
	!
	! 		write(*,*) "1.2"
	! 		ReN2Lindpart=ReN2Lindpart+PolIntegratephi12(anc,GlobKf0+eps,q+GlobKf0-eps,GlobKf0,GlobKf0,phirange21,steps,(/q,0D0/),w)
	!
	! 		if ( q.gt.GlobKf0 ) then
	! 			!th: 0<>2pi
	! 			!k:  0<>q-B
	! 			write(*,*) "1.3"
	!
	! 			ReN2Lindpart=ReN2Lindpart+PolIntegratephi12(anc,0D0+eps,q-GlobKf0-eps,GlobKf0,GlobKf0,phirange0,steps,(/q,0D0/),w)
	!
	!
	! 		end if
	! 	else
	! 		!th: 0<>2pi
	! 		!k:  0<>A
	! 		write(*,*) "2.1"
	!
	! 		ReN2Lindpart=ReN2Lindpart+PolIntegratephi12(anc,0D0+eps,GlobKf0-eps,GlobKf0,GlobKf0,phirange0,steps,(/q,0D0/),w)
	!
	! 		!k:  q-B<>q+B
	! 		!th: th1<>2pi-th1
	! 		write(*,*) "2.2"
	!
	! 		ReN2Lindpart=ReN2Lindpart+PolIntegratephi12(anc,q-GlobKf0+eps,q+GlobKf0-eps,GlobKf0,GlobKf0,phirange21,steps,(/q,0D0/),w)
	! 	end if
	!
	!
	!
	!
	! 	ReN2Lindpart=ReN2Lindpart*infourpiSQ
	if ( bs.eq.1D0 ) then
		A=Globkf0
	else
		A=Globkc
	end if

	if ( bp.eq.1D0 ) then
		B=Globkf0
	else
		B=Globkc
	end if

	temp1=0D0 ! Region 1
	temp2=0D0 ! Region 2
	temp3=0D0 ! Region 3

	if (q.lt.dabs(B-A)) then
		!write(*,*) "q<|B-A|"
		if (A.lt.B) then
			!write(*,*) "A<B"
			temp3=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange2,steps*max(adj/4,1),(/q,0D0/),w)
		elseif (q.lt.B) then
			!write(*,*) "q<B"
			temp1=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange3,steps*max(adj/4,1),(/q,0D0/),w)
		elseif (A.gt.2D0*B) then
				!write(*,*) "A>2B"
				temp1=PolIntegratephi12(anc,0D0,q-B,A,B,phirange0,steps*adj,(/q,0D0/),w)
				temp2=PolIntegratephi12(anc,q-B+eps2,q+B-eps2,A,B,phirange1,steps*adj,(/q,0D0/),w)
				temp3=PolIntegratephi12(anc,q+B,A,A,B,phirange0,steps*adj,(/q,0D0/),w) !This is actually region 4. I didn't want to make another variable
		else
			write(*,*) "This should never happen."
		endif
	elseif (q.lt.B) then
		!write(*,*) "|B-A|<q<B"
		temp1=PolIntegratephi12(anc,B-q+eps2,A-eps2,A,B,phirange1,steps*adj,(/q,0D0/),w) !Broken
		temp3=PolIntegratephi12(anc,A+eps2,B+q-eps2,A,B,phirange2,steps*adj,(/q,0D0/),w)
	elseif (q.lt.(A+B)) then
		!write(*,*) "B<q<B+A"
		temp1=PolIntegratephi12(anc,0D0,q-B,A,B,phirange0,steps*adj*2,(/q,0D0/),w)
		temp2=PolIntegratephi12(anc,q-B+eps2,A,A,B,phirange1,steps*adj*2,(/q,0D0/),w)
		temp3=PolIntegratephi12(anc,A+eps2,B+q-eps2,A,B,phirange2,steps*adj*2,(/q,0D0/),w)
	elseif (q.ge.(A+B)) then
		!write(*,*) "q>B+A"
		temp1=PolIntegratephi12(anc,0D0,A-eps2,A,B,phirange0,steps*adj,(/q,0D0/),w)
		temp3=PolIntegratephi12(anc,q-B+eps2,B+q-eps2,A,B,phirange2,steps*adj,(/q,0D0/),w)
	endif

	ReN2Lindpart=(temp1+temp2+temp3)*infourpiSQ


	contains

		real(8) function anc(q,w,k)
			use AuxiliaryFunctions
			implicit none
			real(8)::q(2),w,k(2),kp(2)

			kp=PolAdd(k,q)


			anc=w+gam*(bs*k(1)-bp*kp(1))
			anc=(fermi(k,bs)-fermi(kp,bp))*0.5D0*(1D0+bs*bp*dcos(kp(2)-k(2)))*anc/(anc**2+GlobetaSQ)
		end function anc

		real(8) function fermi(pv,b)
			!use AuxiliaryFunctions
			real(8):: pv(2),b

			fermi=0D0
			if ( b.eq.1D0 ) then
				if (pv(1).le.Globkf0) then
					fermi=1D0
				endif
			elseif (b.eq.-1D0) then
				if (pv(1).le.Globkc) then
					fermi=1D0
				endif
			end if
		end function fermi

	end function ReN2Lindpart

	real(8) function ReNLindDumb(q,w)
		real(8)    :: q,w
		integer(4) :: b,bp

		ReNLindDumb=0D0
		do b = -1,1,2
			do bp = -1,1,2
				ReNLindDumb=ReNLindDumb+ReNLindDumbPart(q,w,real(b,kind=8),real(bp,kind=8))
			end do
		end do
	end function ReNLindDumb

	real (8) function ReNLindDumbPart(q,w,b,bp)
		real (8)   :: q,w,b,bp,A1,B1
		integer(4),parameter :: steps=1500
		real(8), parameter   :: infourpiSQ=1D0/(4D0*pi**2)

		if ( b.eq.1D0 ) then
			A1=Globkf0
		else
			A1=Globkc
		end if

		if ( bp.eq.1D0 ) then
			B1=Globkf0
		else
			B1=Globkc
		end if


		ReNLindDumbPart=PolIntegratephi11(anc,0D0,max(q+B1,A1),A1,B1,phirange0,steps,q,w)*infourpiSQ


	contains
		real(8) function anc(q,w,k)
			use AuxiliaryFunctions
			implicit none
			real(8)::q,w,k(2),kp(2)

			kp=PolAdd(k,(/q,0D0/))


			anc=w+gam*(b*k(1)-bp*kp(1))
			anc=(fermi(k,b)-fermi(kp,bp))*0.5D0*(1D0+b*bp*dcos(kp(2)-k(2)))*anc/(anc**2+GlobetaSQ)
		end function anc

		real(8) function fermi(pv,b)
			!use AuxiliaryFunctions
			real(8):: pv(2),b

			fermi=0D0
			if ( b.eq.1D0 ) then
				if (pv(1).le.Globkf0) then
					fermi=1D0
				endif
			elseif (b.eq.-1D0) then
				if (pv(1).le.Globkc) then
					fermi=1D0
				endif
			end if
		end function fermi
	end function ReNLindDumbPart


	real (kind=8) function PolIntegratek12(fn,a,b,A1,B1,ksub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: steps
		real (kind=8):: q(2),w,B1,A1,res1,a,b
		real (kind=8), external:: fn
		external :: ksub
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

		! k(th), a and b are th

			PolIntegratek12=0D0
			call GaussAdaptQuad(anc2,a,b,1D-8,res1)
			PolIntegratek12=res1


		contains

			real (kind=8) function anc(k)
				implicit none
				real (kind=8)::k


				anc=k*fn(q,w,(/k,GlobphiInt/))
			end function anc

			real(8) function anc2(phi)
				implicit none
				real(8):: phi,res,ks(2)

				GlobphiInt=phi
				call ksub(phi,q,A1,B1,ks)
				call GaussAdaptQuad2(anc,ks(1),ks(2),1D-8,res)
				anc2=res
			end function anc2

	end function PolIntegratek12

	subroutine krange11(phi,q,A,B,ks)
		implicit none
		real (kind=8),intent(in)::phi,q(2),A,B
		real (kind=8),intent(out)::ks(2)

			ks(2)=A
			ks(1)=q(1)*dcos(pi-phi+q(2))+dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
	end subroutine krange11

	subroutine phirange22(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)


		phis(1)=(k**2+q(1)**2-B**2)/(2D0*k*q(1))
		!if (dabs(phis(1)).gt.1D0) then
		!	phis=0D0
		!	return
		!endif
		phis(2)=pi-dacos(phis(1))
		phis(1)=-phis(2)
		phis(:)=phis(:)+q(2)
	end subroutine phirange22

	subroutine phirange21(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)


		phis(1)=(k**2+q(1)**2-B**2)/(2D0*k*q(1))
		!if (dabs(phis(1)).gt.1D0) then
		!	phis=0D0
		!	return
		!endif
		phis(1)=pi-dacos(phis(1))
		phis(2)=2D0*pi-phis(1)
		!phis(:)=phis(:)+q(2)
	end subroutine phirange21



!!!!!!!!!!!!!!!!!!!!!!!!!
! subroutine phirange0(k,q,A,B,phis)
! 	implicit none
! 	real (kind=8), intent(in)::k,q(2),A,B
! 	real (kind=8), intent(out)::phis(2)
! 	real (kind=8), parameter::twopi=2D0*pi
!
!
! 	phis(2)=twopi
! 	phis(1)=0D0
! end subroutine phirange0

subroutine phirange1(k,q,A,B,phis)
	implicit none
	real (kind=8), intent(in)::k,q(2),A,B
	real (kind=8), intent(out)::phis(2)


	phis(1)=(k**2+q(1)**2-B**2)/(2D0*k*q(1))
	!if (dabs(phis(1)).gt.1D0) then
	!	phis=0D0
	!	return
	!endif
	phis(2)=pi-dacos(phis(1))
	phis(1)=-phis(2)
	phis(:)=phis(:)+q(2)
end subroutine phirange1

subroutine phirange2(k,q,A,B,phis)
	implicit none
	real (kind=8), intent(in)::k,q(2),A,B
	real (kind=8), intent(out)::phis(2)
	real (kind=8), parameter::twopi=2D0*pi

	phis(1)=(k**2+q(1)**2-B**2)/(2D0*k*q(1))
	!if (dabs(phis(1)).gt.1D0) then
	!	phis=0D0
	!	return
	!endif
	phis(1)=pi-dacos(phis(1))
	phis(2)=twopi-phis(1)
	phis(:)=phis(:)+q(2)
	!write(*,*) "phirange2"
	!write(*,*) "k,q,qphi,B,phis"
	!write(*,*) k,q,B,phis,(k**2+q(1)**2-B**2)/(2D0*k*q(1))
end subroutine phirange2

subroutine phirange3(k,q,A,B,phis)
	implicit none
	real (kind=8), intent(in)::k,q(2),A,B
	real (kind=8), intent(out)::phis(2)


	phis(2)=pi-dacos((A**2+q(1)**2-B**2)/(2D0*A*q(1)))
	phis(1)=-phis(2)
	phis(:)=phis(:)+q(2)
end subroutine phirange3

subroutine phirange4(k,q,A,B,phis)
	implicit none
	real (kind=8), intent(in)::k,q(2),A,B
	real (kind=8), intent(out)::phis(2)
	real (kind=8), parameter::twopi=2D0*pi

	phis(1)=pi-dacos((A**2+q(1)**2-B**2)/(2D0*A*q(1)))
	phis(2)=twopi-phis(1)
	phis(:)=phis(:)+q(2)
end subroutine phirange4

subroutine krange1(phi,q,A,B,ks)
	implicit none
	real (kind=8),intent(in)::phi,q(2),A,B
	real (kind=8),intent(out)::ks(2)

		ks(1)=0D0
		ks(2)=q(1)*dcos(pi-phi+q(2))-dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
end subroutine krange1

subroutine krange2(phi,q,A,B,ks)
	implicit none
	real (kind=8),intent(in)::phi,q(2),A,B
	real (kind=8),intent(out)::ks(2)

		ks(1)=A
		ks(2)=q(1)*dcos(pi-phi+q(2))+dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
end subroutine krange2

subroutine krange3(phi,q,A,B,ks)
	implicit none
	real (kind=8),intent(in)::phi,q(2),A,B
	real (kind=8),intent(out)::ks(2)

		ks(2)=A
		ks(1)=q(1)*dcos(pi-phi+q(2))+dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
end subroutine krange3

real(8) function ImN2Lind(q,w)
	real(8)::q,w
	integer(4)::b,bp


	ImN2Lind=0D0
	do b=-1,1,2
		do bp=-1,1,2
			ImN2Lind=ImN2Lind+ImN2Lindpart(q,w,real(b,kind=8),real(bp,kind=8))
		end do
	end do
end function ImN2Lind


real(8) function ImN2Lindpart(q,w,b,bp)
	real(8):: bs,bp,q,w,temp1,temp2,temp3,A,B
	real(8),parameter::infourpiSQ=1D0/4D0/pi**2, eps=1D-10,eps2=1D-10
	integer(4),parameter::steps=1000,adj=1


! 	ImN2Lindpart=0D0
!
! if (q.lt.2d0*GlobKf0) then
! 		!k:  q-B<>A
! 		!th: -th1<>th1
!
! 		write(*,*) "1.1"
! 		ImN2Lindpart=ImN2Lindpart+PolIntegratephi12(anc,q-GlobKf0+eps,GlobKf0-eps,GlobKf0,GlobKf0,phirange22,steps,(/q,0D0/),w)
!
! 		!k:  A<>q+B
! 		!th: th1<>2pi-th1
!
! 		write(*,*) "1.2"
! 		ImN2Lindpart=ImN2Lindpart+PolIntegratephi12(anc,GlobKf0+eps,q+GlobKf0-eps,GlobKf0,GlobKf0,phirange21,steps,(/q,0D0/),w)
!
! 		if ( q.gt.GlobKf0 ) then
! 			!th: 0<>2pi
! 			!k:  0<>q-B
! 			write(*,*) "1.3"
!
! 			ImN2Lindpart=ImN2Lindpart+PolIntegratephi12(anc,0D0+eps,q-GlobKf0-eps,GlobKf0,GlobKf0,phirange0,steps,(/q,0D0/),w)
!
!
! 		end if
! 	else
! 		!th: 0<>2pi
! 		!k:  0<>A
! 		write(*,*) "2.1"
!
! 		ImN2Lindpart=ImN2Lindpart+PolIntegratephi12(anc,0D0+eps,GlobKf0-eps,GlobKf0,GlobKf0,phirange0,steps,(/q,0D0/),w)
!
! 		!k:  q-B<>q+B
! 		!th: th1<>2pi-th1
! 		write(*,*) "2.2"
!
! 		ImN2Lindpart=ImN2Lindpart+PolIntegratephi12(anc,q-GlobKf0+eps,q+GlobKf0-eps,GlobKf0,GlobKf0,phirange21,steps,(/q,0D0/),w)
! 	end if
!
!
!
!
! 	ImN2Lindpart=ImN2Lindpart*infourpiSQ
if ( bs.eq.1D0 ) then
	A=Globkf0
else
	A=Globkc
end if

if ( bp.eq.1D0 ) then
	B=Globkf0
else
	B=Globkc
end if

temp1=0D0 ! Region 1
temp2=0D0 ! Region 2
temp3=0D0 ! Region 3

if (q.lt.dabs(B-A)) then
	!write(*,*) "q<|B-A|"
	if (A.lt.B) then
		!write(*,*) "A<B"
		temp3=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange2,steps*max(adj/4,1),(/q,0D0/),w)
	elseif (q.lt.B) then
		!write(*,*) "q<B"
		temp1=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange3,steps*max(adj/4,1),(/q,0D0/),w)
	elseif (A.gt.2D0*B) then
			!write(*,*) "A>2B"
			temp1=PolIntegratephi12(anc,0D0,q-B,A,B,phirange0,steps*adj,(/q,0D0/),w)
			temp2=PolIntegratephi12(anc,q-B+eps2,q+B-eps2,A,B,phirange1,steps*adj,(/q,0D0/),w)
			temp3=PolIntegratephi12(anc,q+B,A,A,B,phirange0,steps*adj,(/q,0D0/),w) !This is actually region 4. I didn't want to make another variable
	else
		write(*,*) "This should never happen."
	endif
elseif (q.lt.B) then
	!write(*,*) "|B-A|<q<B"
	temp1=PolIntegratephi12(anc,B-q+eps2,A-eps2,A,B,phirange1,steps*adj,(/q,0D0/),w) !Broken
	temp3=PolIntegratephi12(anc,A+eps2,B+q-eps2,A,B,phirange2,steps*adj,(/q,0D0/),w)
elseif (q.lt.(A+B)) then
	!write(*,*) "B<q<B+A"
	temp1=PolIntegratephi12(anc,0D0,q-B,A,B,phirange0,steps*adj*2,(/q,0D0/),w)
	temp2=PolIntegratephi12(anc,q-B+eps2,A,A,B,phirange1,steps*adj*2,(/q,0D0/),w)
	temp3=PolIntegratephi12(anc,A+eps2,B+q-eps2,A,B,phirange2,steps*adj*2,(/q,0D0/),w)
elseif (q.ge.(A+B)) then
	!write(*,*) "q>B+A"
	temp1=PolIntegratephi12(anc,0D0,A-eps2,A,B,phirange0,steps*adj,(/q,0D0/),w)
	temp3=PolIntegratephi12(anc,q-B+eps2,B+q-eps2,A,B,phirange2,steps*adj,(/q,0D0/),w)
endif

ImN2Lindpart=(temp1+temp2+temp3)*infourpiSQ


contains

	real(8) function anc(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real(8)::q(2),w,k(2),kp(2)

		kp=PolAdd(k,q)


		anc=w+gam*(bs*k(1)-bp*kp(1))
		anc=-Globeta*(fermi(k,bs)-fermi(kp,bp))*0.5D0*(1D0+bs*bp*dcos(kp(2)-k(2)))/(anc**2+GlobetaSQ)
	end function anc

	real(8) function fermi(pv,b)
		!use AuxiliaryFunctions
		real(8):: pv(2),b

		fermi=0D0
		if ( b.eq.1D0 ) then
			if (pv(1).le.Globkf0) then
				fermi=1D0
			endif
		elseif (b.eq.-1D0) then
			if (pv(1).le.Globkc) then
				fermi=1D0
			endif
		end if
	end function fermi

end function ImN2Lindpart

real(8) function ImNLindHybrid(q,w)
	real(8)::q,w
	integer(4)::a,b,bb

	ImNLindHybrid=0D0
	do a=-1,1,2
		do b=-1,1,2
			do bb=-1,1,2
				ImNLindHybrid=ImNLindHybrid+ImNLindHybridPart(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8))
			end do
		end do
	end do


end function ImNLindHybrid

real(8) function ImNLindHybridPart(q,w,a,b,bb)
	use AuxiliaryFunctions
	real(8)::q,w,a,b,bb,up,res,wt,up2,low
	integer(4)::ier
	integer(4),parameter::steps=3000

	wt=w/gam

	if ( b.eq.1D0 ) then
		up=Globkf0
	else
		up=Globkc
	end if
	ImNLindHybridPart=0D0

	up2=up
	if ( a.eq.1D0 ) then
		if ( wt.gt.q ) then
			up2=-(q+wt)*0.5D0
			low=(q-wt)*0.5D0
			! ImNLindHybridPart=0D0
			! return
		else
			low=max((q-wt)*0.5D0,-(q+wt)*0.5D0)
		end if
	else
		if ( wt.gt.q ) then
			up2=(q+wt)*0.5D0
			low=(wt-q)*0.5D0
		else
			low=max((q+wt)*0.5D0,(wt-q)*0.5D0)
		end if
	end if

	up=min(up,up2)
	low=max(0D0,low)

	res=0D0
	if ( low.lt.up ) then
		! call GaussAdaptQuad(anc,low,up,1D-8,res)
		call StupidIntegrateNC(anc,low,up,steps,res,ier)
	end if



	ImNLindHybridPart=-res*a*b*bb/(4D0*pi*gam*dsqrt(dabs(q**2-wt**2)))

	! ImNLindHybridPart=-a*b*res/(4D0*pi*gam)
contains
	real(8) function anc(k)
		real(8)::k,qSQ,z

		qSQ=q**2
		z=wt**2-qSQ+2D0*a*k*wt
		z=z/2D0/k/q
		anc=0D0
		if ( (z**2).le.1D0 ) then
			anc=(dabs(a*wt+k)+bb*k)**2-qSQ
			anc=anc/(dsqrt(dabs((a*wt+2D0*k)**2-qSQ))+GlobEtaSq)
		end if
	end function anc


	real(8) function anc2(k)
		real(8)::k,z

		z=wt**2-q**2+2D0*a*k*wt
		z=z*0.5D0/k/q
		anc2=0D0
		if ( z**2.le.1D0 ) then
			anc2=dsqrt(k**2+q**2+2D0*k*q*z)!dsqrt((a*wt+k)**2)
			anc2=(1D0+bb*(k+q*z)/anc2)/(q*dabs(dsqrt(1D0-z**2)/anc2))
		end if

	end function anc2
end function ImNLindHybridPart

real(8) function ImChiKK(q,w)
	use QuadPackDouble
	real(8)::q,w
	real(8),parameter::temp=-2D0/pi
	integer (kind=4), parameter::limit=10000,key=1
	real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),res1,res2,res3,up1,up2

	integer(4), parameter:: npts2=4
	real(8)::points(npts2)!,level(npts2)
	integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
	! real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::iwork(leniw)!,neval,ier,last
	real (kind=8)   ::work(lenw),up!,res1,res2,res3,up1,up2!,points(npts2)!,abserr

	! up= max(10D0*w,2D0*Globkc+gam*q)
	up1=2D0*Globkf+gam*q
	up2=2D0*Globkc+gam*q
	res2=0D0
	res1=0D0
	res3=0D0

	! call dqage ( anc, 0D0, w*0.99999D0, eps, eps, key, limit, res1, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last )

	! call dqage ( anc, w*1.00001D0, up, eps, eps, key, limit, res2, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last )

	up=max(w,gam*q)+Globkc*gam


	points=0d0
	points(1)=w
	points(2)=gam*q

	call	dqagp(anc,0D0,up,npts2,points,eps,eps,res1,abserr,&
		neval,ier,leniw,lenw,last,iwork,work)

	! call dqage ( anc, 0D0, up1, eps, eps, key, limit, res1, abserr, &
	 		! neval, ier, alist, blist, rlist, elist, iord, last )

	! call dqage ( anc, up1, up2, eps, eps, key, limit, res1, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last )

	call dqagie ( anc, up, 1, eps, eps, limit , res2, abserr, &
	 		neval, ier, alist, blist, rlist, elist, iord, last )

	ImChiKK=temp*w*(res1+res2+res3)

contains
	real(8) function anc(wp)
		real(8)::wp


		anc=ReChi0Ex(q,wp)/(wp**2-w**2)
	end function
end function ImChiKK

real(8) function ReChiKK(q,w)
	use QuadPackDouble
	real(8)::q,w
	real(8),parameter::temp=2D0/pi
	integer (kind=4), parameter::limit=10000,key=1
	real (kind=8)   , parameter::eps=1D-10
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),res1,res2,res3,up1,up2

	integer(4), parameter:: npts2=4
	real(8)::points(npts2)!,level(npts2)
	integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
	! real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::iwork(leniw)!,neval,ier,last
	real (kind=8)   ::work(lenw),up!,res1,res2,res3,up1,up2!,points(npts2)!,abserr

	! up= max(10D0*w,2D0*Globkc+gam*q)
	up1=2D0*Globkf+gam*q
	up2=2D0*Globkc+gam*q
	res2=0D0
	res1=0D0
	res3=0D0

	! call dqage ( anc, 0D0, w*0.99999D0, eps, eps, key, limit, res1, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last )

	! call dqage ( anc, w*1.00001D0, up, eps, eps, key, limit, res2, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last )

	up=max(w,gam*q)+Globkc*gam


	points=0d0
	points(1)=w
	points(2)=gam*q

	call	dqagp(anc,0D0,up,npts2,points,eps,eps,res1,abserr,&
		neval,ier,leniw,lenw,last,iwork,work)

	! call dqage ( anc, 0D0, up1, eps, eps, key, limit, res1, abserr, &
	 		! neval, ier, alist, blist, rlist, elist, iord, last )

	! call dqage ( anc, up1, up2, eps, eps, key, limit, res1, abserr, &
	!  		neval, ier, alist, blist, rlist, elist, iord, last )

	call dqagie ( anc, up, 1, eps, eps, limit , res2, abserr, &
	 		neval, ier, alist, blist, rlist, elist, iord, last )

	ReChiKK=temp*(res1+res2+res3)

contains
	real(8) function anc(wp)
		real(8)::wp


		anc=wp*ImChi0Ex(q,wp)/(wp**2-w**2)
	end function
end function ReChiKK


real(8) function ImChiPeak(q,w)
	real(8)::q,w
	integer(4):: a,b,bb

	ImChiPeak=0d0
	do a = -1,1,2
		do b = -1,1,2
			do bb = -1,1,2
				ImChiPeak=ImChiPeak+ImChiPeakPart2(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8))
			end do
		end do
	end do
end function ImChiPeak

real(8) function ImChiPeakPart(q,w,a,b,bb)
	real(8)::q,w,a,b,bb,up
	integer(4),parameter::steps=1000


	if ( b.gt.0D0 ) then
		up=Globkf
	else
		up=Globkc
	end if

	ImChiPeakPart=PolIntegratephi31(anc,0D0,up,0D0,0D0,pts,steps,q,w)/4D0/pi**2

contains

	real(8) function anc(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real(8)::q,w,k(2),kp

		kp=k(1)**2+q**2+2d0*k(1)*q*k(2)
		kp=dsqrt(kp)
		! wt=w/gam

		anc=-Globeta*a*b*(1D0+bb*(k(1)+q*k(2))/kp)/((a*w/gam+k(1)-bb*kp)**2+GlobetaSQ)/gam/dsqrt(1D0-k(2)**2)
	end function anc

	subroutine pts(k,q,w,res)
		real(8),intent(in) ::k,q,w
		real(8),intent(out)::res
		real(8)::x
		real(8),parameter::halfpi=pi/2D0

		x=(w**2/gam**2-q**2+2D0*a*w*k/gam)
		! write(*,*) x,k,q
		x=x/(k*q)*0.5D0
		res=x!dacos(x)
		! res=halfpi-2D0*datan2(x,1D0+dsqrt(1D0-x**2))
		! write(*,*) res
	end subroutine pts
end function ImChiPeakPart



real(8) function PolIntegratephi31(fn,a,b,A1,B1,ptssub,steps,q,w)
	use AuxiliaryFunctions
	use QuadPackDouble
	implicit none
	integer(4):: i,j,steps,ksteps,t1,t2,phisteps
	real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1
	real(8):: res,temp,phis(2),kf1,kf2,point!,eps
	real(8),allocatable::temp1(:)
	real(8), external:: fn
	external :: ptssub
	real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
	integer (kind=4), parameter::limit=1000,key=1
	real (kind=8)   , parameter::eps=1D-8
	integer(4), parameter:: npts2=3
	integer (kind=4)::neval,ier,last,iord(limit),level(limit),ndin(npts2)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit) !,res1,res2,res3,up1,up2

	real(8)::points(npts2)!,level(npts2)
	integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
	! real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::iwork(leniw)!,neval,ier,last
	real (kind=8)   ::work(lenw),res1,res2,res3,up1,up2!,points(npts2)!,abserr
	! 2D polar integrator over k and phi.
	! S fn(q,w)*dk*dphi
	! fn    :: real, external function to be integrated
	! steps :: integer, number of
	! q,w   :: real, arguments of fn


		phisteps=steps
		! eps=1D-8
		ksteps=(steps/9)*9 +1
		dk=(b-a)/real(ksteps, kind=8)
		PolIntegratephi31=0D0
		k=a
		allocate(temp1(ksteps))
		do i=1,ksteps
				res=0D0
				call ptssub(k,q,w,point)

				! write(*,*) point
				points=0D0

				if (point**2.lt.1D0) then
					points(1)=point
					points(2)=-1D0
					points(3)=1D0

					! write(*,*) npts2
					call	dqagp(anc,-1D0,1D0,npts2,points,eps,eps,res,abserr,&
		        neval,ier,leniw,lenw,last,iwork,work)
					else
						call dqage ( anc, -1D0, 1D0, eps, eps, key, limit, res, abserr, &
							neval, ier, alist, blist, rlist, elist, iord, last )
				endif

				! if (ier.ne.0) then
				! 	write(*,*) ier,points
				! endif
	! 			call dqagpe(anc,-1D0,1D0,npts2,points,eps,eps,limit,res, &
  ! abserr,neval,ier,alist,blist,rlist,elist,points,iord,level,ndin,last)

				! call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
				! call StupidIntegrateNC2(anc,phis(1),phis(2),phisteps,res,ier)
			temp1(i)=res!temp
			k=k+dk
		enddo


		t1=1
		t2=10
		do i=1,(ksteps/9)
			PolIntegratephi31=PolIntegratephi31+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
			t1=t2
			t2=t2+9
		enddo

		PolIntegratephi31=PolIntegratephi31*dk    !/fourpisq     !NC10


	contains

		real(8) function anc(x)
			implicit none
			real(8)::x

			anc=k*fn(q,w,(/k,x/))
		end function anc

		real(8) function NC10PolList(flist)
			integer(4)::i
			real(8)   ::flist(10)
			real(8), external::fn
			real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
													1080D0,19344D0,&
													5778D0,	5778D0,&
													19344D0,1080D0,&
													15741D0,2857D0&
													/)*9D0/89600D0

			NC10PolList=0D0
			do i=1,10
				NC10PolList=NC10PolList+Newt10(i)*flist(i)
			enddo
		end function NC10PolList
end function PolIntegratephi31


real(8) function ImChiPeakPart2(q,w,a,b,bb)
	real(8)::q,w,a,b,bb,up
	integer(4),parameter::steps=1000


	if ( b.gt.0D0 ) then
		up=Globkf
	else
		up=Globkc
	end if

	ImChiPeakPart2=PolIntegratephi32(anc,0D0,up,0D0,0D0,pts,steps,q,w)/4D0/pi**2

contains

	real(8) function anc(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real(8)::q,w,k(2),kp

		kp=k(1)**2+q**2+2d0*k(1)*q*dcos(k(2))
		kp=dsqrt(kp)
		! wt=w/gam

		anc=-Globeta*a*b*(1D0+bb*(k(1)+q*dcos(k(2)))/kp)/((a*w/gam+k(1)-bb*kp)**2+GlobetaSQ)/gam
	end function anc

	subroutine pts(k,q,w,res)
		real(8),intent(in) ::k,q,w
		real(8),intent(out)::res
		real(8)::x
		real(8),parameter::halfpi=pi/2D0

		x=(w**2/gam**2-q**2+2D0*a*w*k/gam)
		! write(*,*) x,k,q
		x=x/(k*q)*0.5D0
		res=dacos(x)
		! res=halfpi-2D0*datan2(x,1D0+dsqrt(1D0-x**2))
		! write(*,*) res
	end subroutine pts
end function ImChiPeakPart2



real(8) function PolIntegratephi32(fn,a,b,A1,B1,ptssub,steps,q,w)
	use AuxiliaryFunctions
	use QuadPackDouble
	implicit none
	integer(4):: i,j,steps,ksteps,t1,t2,phisteps
	real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1
	real(8):: res,temp,phis(2),kf1,kf2,point!,eps
	real(8),allocatable::temp1(:)
	real(8), external:: fn
	external :: ptssub
	real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
	integer (kind=4), parameter::limit=1000,key=1
	real (kind=8)   , parameter::eps=1D-10
	integer(4), parameter:: npts2=3
	integer (kind=4)::neval,ier,last,iord(limit),level(limit),ndin(npts2)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit) !,res1,res2,res3,up1,up2

	real(8)::points(npts2)!,level(npts2)
	integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
	! real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::iwork(leniw)!,neval,ier,last
	real (kind=8)   ::work(lenw),res1,res2,res3,up1,up2!,points(npts2)!,abserr
	! 2D polar integrator over k and phi.
	! S fn(q,w)*dk*dphi
	! fn    :: real, external function to be integrated
	! steps :: integer, number of
	! q,w   :: real, arguments of fn


		phisteps=steps
		! eps=1D-8
		ksteps=(steps/9)*9 +1
		dk=(b-a)/real(ksteps, kind=8)
		PolIntegratephi32=0D0
		k=a
		allocate(temp1(ksteps))
		do i=1,ksteps
				res=0D0
				call ptssub(k,q,w,point)

				! write(*,*) point
				points=0D0

				if (point.le.pi) then
					points(1)=point
					points(2)=0d0
					points(3)=pi

					! write(*,*) npts2
					call	dqagp(anc,0d0,pi,npts2,points,eps,eps,res,abserr,&
		        neval,ier,leniw,lenw,last,iwork,work)
					else
						call dqage ( anc, 0D0, pi, eps, eps, key, limit, res, abserr, &
							neval, ier, alist, blist, rlist, elist, iord, last )
				endif

				! if (ier.ne.0) then
				! 	write(*,*) ier,points
				! endif
	! 			call dqagpe(anc,-1D0,1D0,npts2,points,eps,eps,limit,res, &
  ! abserr,neval,ier,alist,blist,rlist,elist,points,iord,level,ndin,last)

				! call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
				! call StupidIntegrateNC2(anc,phis(1),phis(2),phisteps,res,ier)
			temp1(i)=res!temp
			k=k+dk
		enddo


		t1=1
		t2=10
		do i=1,(ksteps/9)
			PolIntegratephi32=PolIntegratephi32+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
			t1=t2
			t2=t2+9
		enddo

		PolIntegratephi32=PolIntegratephi32*dk    !/fourpisq     !NC10


	contains

		real(8) function anc(x)
			implicit none
			real(8)::x

			anc=k*fn(q,w,(/k,x/))
		end function anc

		real(8) function NC10PolList(flist)
			integer(4)::i
			real(8)   ::flist(10)
			real(8), external::fn
			real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
													1080D0,19344D0,&
													5778D0,	5778D0,&
													19344D0,1080D0,&
													15741D0,2857D0&
													/)*9D0/89600D0

			NC10PolList=0D0
			do i=1,10
				NC10PolList=NC10PolList+Newt10(i)*flist(i)
			enddo
		end function NC10PolList
end function PolIntegratephi32

real(8) function ReChiPeak(q,w)
	real(8)::q,w
	integer(4):: a,b,bb

	ReChiPeak=0d0
	do a = -1,1,2
		do b = -1,1,2
			do bb = -1,1,2
				if (.not.((b.eq.-1D0).and.(bb.eq.1D0))) then
				! if (.not.(b.eq.-1D0)) then
					ReChiPeak=ReChiPeak+ReChiPeakPart(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8))
				endif
			end do
		end do
	end do


	! ReChiPeak=ReChiPeakPart(q,w,1D0,1D0,1D0)+ReChiPeakPart(q,w,-1D0,1D0,1D0)&
	! 				 +ReChiPeakPart(q,w,-1D0,1D0,-1D0)+ReChiPeakPart(q,w,1D0,-1D0,-1D0)
end function ReChiPeak

real(8) function ReChiPeakPart(q,w,a,b,bb)
	real(8)::q,w,a,b,bb,up,wt
	integer(4),parameter::steps=20

	wt=w/gam

	if ( b.gt.0D0 ) then
		up=Globkf
	else
		up=Globkc
	end if

	! ReChiPeakPart=PolIntegratephi31(anc,0D0,up,0D0,0D0,pts,steps,q,w)/4D0/pi**2
	ReChiPeakPart=PolIntegratephi33(anc,0D0,up,0D0,0D0,pts,steps,q,w)/4D0/pi**2/gam!*2D0/2D0

contains

	real(8) function anc(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real(8)::q,w,k(2),kp,temp


		temp=dcos(k(2))
		kp=dsqrt(k(1)**2+q**2+2d0*k(1)*q*temp)
		! kp=dsqrt(kp)
		! wt=w/gam


		if ( dabs(k(1)-q).lt.1D-5 ) then
			temp=1D0+bb*dcos(k(2)*0.5D0)
		else
			temp=1D0+bb*(k(1)+q*temp)/kp
		end if

		anc=a*wt+k(1)-bb*kp

		anc=b*temp*anc/(anc**2+GlobetaSQ)
	end function anc

	subroutine pts(k,q,w,res)
		real(8),intent(in) ::k,q,w
		real(8),intent(out)::res
		real(8)::x
		real(8),parameter::halfpi=pi/2D0

		x=(wt**2-q**2+2D0*a*wt*k)
		! write(*,*) x,k,q
		x=x/(k*q)*0.5D0
		res=dacos(x)
		! res=halfpi-2D0*datan2(x,1D0+dsqrt(1D0-x**2))
		! write(*,*) res
	end subroutine pts
end function ReChiPeakPart

real(8) function ReChiFix(q,w)
	real(8) :: q,w
	integer(4)::a

	ReChiFix=0D0
	do a=-1,1,2
		ReChiFix=ReChiFix+ReChiFixPart(q,w,real(a, kind=8))
	end do
end function ReChiFix

real(8) function ReChiFixPart(q,w,a)
	real(8)::q,w,a,wt
	integer(4),parameter::steps=1000


	wt=w/gam
	! write(*,*) wt

	ReChiFixPart=PolIntegratephi32(anc,1D-10*Globkf,Globkc,0D0,0D0,pts,steps,q,w)/4D0/pi**2/gam
contains
	real(8) function anc(q,w,k)
		use AuxiliaryFunctions
		real(8)::q,w,k(2),kp(2)
			! kp=q**2+k(1)**2+2D0*k(1)*q*k(2)
			! kp=dsqrt(kp)

			kp=PolAdd(k,(/q,0D0/))

			anc=a*wt+k(1)
			! write(*,*) wt

			! if kp.lt.1D-10 then
			! 	anc=1D0/anc**2
			! 	return
			! endif



			! anc=(kp(1)+(k(1)**2+a*wt*k(1)+anc*q*dcos(k(2)))/kp(1))/(anc**2-kp(1)**2)!/dsqrt(1D0-k(2)**2)

			! anc=(q**2-(anc+k(1))**2)/anc*kp(1)/(anc**2-kp(1)**2)
			anc=(q**2-k(1)**2)/(anc*kp(1))
	end function anc

	subroutine pts (k,q,w,res)
		real(8),intent(in) ::k,q,w
		real(8),intent(out)::res


		! write(*,*) wt

		res=q**2-wt**2-2D0*a*k*wt
		res=res*0.5D0/k/q
		res=dacos(res)

	end subroutine pts
end function ReChiFixPart

real(8) function PolIntegratephi41(fn,a,b,A1,B1,ptssub,steps,q,w)
	use AuxiliaryFunctions
	use QuadPackDouble
	implicit none
	integer(4):: i,j,steps,ksteps,t1,t2,phisteps
	real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1
	real(8):: res,temp,phis(2),kf1,kf2,point!,eps
	real(8),allocatable::temp1(:)
	real(8), external:: fn
	external :: ptssub
	real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
	integer (kind=4), parameter::limit=10000,key=1
	real (kind=8)   , parameter::eps=1D-8
	integer(4), parameter:: npts2=3
	integer (kind=4)::neval,ier,last,iord(limit),level(limit),ndin(npts2)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit) !,res1,res2,res3,up1,up2

	real(8)::points(npts2)!,level(npts2)
	integer (kind=4), parameter::leniw=limit,lenw=limit*4
	! real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::iwork(leniw)!,neval,ier,last
	real (kind=8)   ::work(lenw),res1,res2,res3,up1,up2!,points(npts2)!,abserr
	! 2D polar integrator over k and phi.
	! S fn(q,w)*dk*dphi
	! fn    :: real, external function to be integrated
	! steps :: integer, number of
	! q,w   :: real, arguments of fn


		phisteps=steps
		! eps=1D-8
		ksteps=(steps/9)*9 +1
		dk=(b-a)/real(ksteps, kind=8)
		PolIntegratephi41=0D0
		k=a
		allocate(temp1(ksteps))
		do i=1,ksteps
				res=0D0
				call ptssub(k,q,w,point)

				! write(*,*) point
				points=0D0

				if (point**2.lt.1D0) then
					points(1)=point
					points(2)=-1D0
					points(3)=1D0

					! write(*,*) point
					call	dqawc(anc,-1D0+1D-10,1D0-1D-10,point,eps,eps,res,abserr,neval,ier, &
        							limit,lenw,last,iwork,work)
					else
						call dqage ( anc2, -1D0, 1D0, eps, eps, key, limit, res, abserr, &
							neval, ier, alist, blist, rlist, elist, iord, last )
				endif

				! if (ier.ne.0) then
				! 	write(*,*) ier,points
				! endif
	! 			call dqagpe(anc,-1D0,1D0,npts2,points,eps,eps,limit,res, &
  ! abserr,neval,ier,alist,blist,rlist,elist,points,iord,level,ndin,last)

				! call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
				! call StupidIntegrateNC2(anc,phis(1),phis(2),phisteps,res,ier)
			temp1(i)=res!temp
			k=k+dk
		enddo


		t1=1
		t2=10
		do i=1,(ksteps/9)
			PolIntegratephi41=PolIntegratephi41+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
			t1=t2
			t2=t2+9
		enddo

		PolIntegratephi41=PolIntegratephi41*dk    !/fourpisq     !NC10


	contains

		real(8) function anc(x)
			implicit none
			real(8)::x

			anc=k*fn(q,w,(/k,x/))
		end function anc

		real(8) function anc2(x)
			implicit none
			real(8)::x

			anc2=anc(x)/(x-point)
		end function anc2

		real(8) function NC10PolList(flist)
			integer(4)::i
			real(8)   ::flist(10)
			real(8), external::fn
			real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
													1080D0,19344D0,&
													5778D0,	5778D0,&
													19344D0,1080D0,&
													15741D0,2857D0&
													/)*9D0/89600D0

			NC10PolList=0D0
			do i=1,10
				NC10PolList=NC10PolList+Newt10(i)*flist(i)
			enddo
		end function NC10PolList
end function PolIntegratephi41

real(8) function ReChiCauchyPart(q,w,a)
	real(8)::q,w,a,wt
	integer(4),parameter::steps=1000

	wt=w/gam

	ReChiCauchyPart=PolIntegratephi41(anc,1D-10*Globkf,Globkc,0D0,0D0,pts,steps,q,w)

contains
	real(8) function anc(q,w,k)
		real(8)::q,w,k(2)


		anc=a*wt+k(1)
		anc=(q**2-(anc+k(1))**2)*dsqrt(k(1)**2+q**2+2D0*k(1)*q*k(2))/(k(1)*q*anc*dsqrt(1D0-k(2)**2))
	end function anc

	subroutine pts(k,q,w,point)
		real(8),intent(in)::k,q,w
		real(8),intent(out)::point

		point=(wt**2-q**2+2D0*wt*a*k)*0.5D0/k/q
	end subroutine pts

end function ReChiCauchyPart

real(8) function ReChiCauchy(q,w)
	real(8)::q,w
	integer(4)::a

	ReChiCauchy=0D0
	do a = -1,1,2
		ReChiCauchy=ReChiCauchy+ ReChiCauchyPart(q,w,real(a,kind=8))
	end do
end function ReChiCauchy


real(8) function ReChitest(q,w)
	real(8)::q,w,wt,a
	integer(4)::i
	integer(4), parameter::steps=1000

	wt=w/gam

	ReChitest=0D0
	do i = -1,1,2
		a=real(i,kind=8)

		! ReChitest=ReChitest+2D0*PolIntegratephi32(anc,0D0,Globkc,0D0,0D0,pts,steps,q,w)
		ReChitest=ReChitest-2D0*PolIntegratephi33(anc,0D0,Globkc,0D0,0D0,pts,steps,q,w)/4D0/pi**2
	end do

contains
	real(8) function anc(q,w,k)
		real(8)::q,w,k(2),kp,temp

		kp=dsqrt(k(1)**2+q**2+2D0*k(1)*q*dcos(k(2)))

		if ( dabs(k(1)-q).lt.1D-5 ) then
			temp=1D0-dcos(k(2)*0.5D0)
		else
			temp=1D0-(k(1)+q*dcos(k(2)))/kp
		end if

		anc=(a*wt+k(1)+kp)

		anc=temp*anc/(anc**2+GlobEtaSq)

		! write(*,*) "anc out", anc

	end function anc

	subroutine pts(k,q,w,point)
		real(8),intent(in)::k,q,w
		real(8),intent(out)::point

		point=wt**2-q**2+2D0*a*wt*k
		point=point*0.5D0/k/q
		point=dacos(point)
	end subroutine pts

	real(8) function anc2(k,q,th)
		real(8)::k,q,th

		if ( dabs(k-q).lt.1d-8 ) then
			anc2=1D0-dabs(dcos(th*0.5D0))
		else
			anc2=1D0-(k+q*dcos(th))/(dsqrt(k**2+q**2+2D0*k*q*dcos(th)))
		end if
	end function anc2
end function ReChitest

real(8) function ReChitest2(q,w)
	real(8)::q,w,wt,a
	integer(4)::i
	integer(4), parameter::steps=1000!30000

	wt=w/gam

	ReChitest2=0D0
	do i = -1,1,2
		a=real(i,kind=8)

		! ReChitest2=ReChitest2+2D0*PolIntegratephi32(anc,0D0,Globkc,0D0,0D0,pts,steps,q,w)
		ReChitest2=ReChitest2-2D0*PolIntegratephi33(anc,0D0,Globkc,0D0,0D0,pts,steps,q,w)/4D0/pi**2
	end do

contains
	real(8) function anc(q,w,k)
		real(8)::q,w,k(2),kp,temp

		kp=dsqrt(k(1)**2+q**2+2D0*k(1)*q*dcos(k(2)))

		if ( dabs(k(1)-q).lt.1D-5 ) then
			temp=1D0+dcos(k(2)*0.5D0)
		else
			temp=1D0+(k(1)+q*dcos(k(2)))/kp
		end if

		anc=(a*wt+k(1)-kp)

		anc=temp*anc/(anc**2+GlobEtaSq)

		! write(*,*) "anc out", anc

	end function anc

	subroutine pts(k,q,w,point)
		real(8),intent(in)::k,q,w
		real(8),intent(out)::point

			point=wt**2-q**2+2D0*a*wt*k
			point=point*0.5D0/k/q
			point=dacos(point)
	end subroutine pts
end function ReChitest2

real(8) function PolIntegratephi33(fn,a,b,A1,B1,ptssub,steps,q,w)
	use AuxiliaryFunctions
	use QuadPackDouble2
	implicit none
	integer(4):: i,j,steps,ksteps,t1,t2,phisteps
	real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1,aa
	real(8):: res,temp,phis(2),kf1,kf2,point!,eps
	real(8),allocatable::temp1(:)
	real(8), external:: fn
	external :: ptssub
	real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
	integer (kind=4), parameter::limit=100000,key=1
	real (kind=8)   , parameter::eps=1D-9
	integer(4), parameter:: npts2=3
	integer (kind=4)::neval,ier,last,iord(limit),level(limit),ndin(npts2)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit) !,res1,res2,res3,up1,up2

	real(8)::points(npts2)!,level(npts2)
	integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
	! real (kind=8)   , parameter::eps=1D-8
	integer (kind=4)::iwork(leniw)!,neval,ier,last
	real (kind=8)   ::work(lenw),res1,res2,res3,up1,up2!,point!,points(npts2)!,abserr

	! 2D polar integrator over k and phi.
	! S fn(q,w)*dk*dphi
	! fn    :: real, external function to be integrated
	! steps :: integer, number of
	! q,w   :: real, arguments of fn

	dk=(b-a)/real(steps, kind=8)
	aa=a
	PolIntegratephi33=0D0
	do i = 1, steps
		call dqage ( anc3, aa, aa+dk, eps, eps, key, limit, res, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last )
			! write(*,*) "k",res
		PolIntegratephi33=PolIntegratephi33+res
		aa=aa+dk
	end do

	contains

		real(8) function anc2(k)
			use QuadPackDouble
			real(8)::k
			integer (kind=4), parameter::limit=10000,key=1
			real (kind=8)   , parameter::eps=1D-10
			integer(4), parameter:: npts2=3
			integer (kind=4)::neval,ier,last,iord(limit),level(limit),ndin(npts2)
			real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
								,elist(limit) !,res1,res2,res3,up1,up2

			real(8)::points(npts2)!,level(npts2)
			integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
			! real (kind=8)   , parameter::eps=1D-8
			integer (kind=4)::iwork(leniw)!,neval,ier,last
			real (kind=8)   ::work(lenw),res1,res2,res3,up1,up2,point!,points(npts2)!,abserr

			call ptssub(k,q,w,point)

			! write(*,*) point
			points=0D0

			if (point.le.pi) then
				points(1)=point
				points(2)=0d0
				points(3)=pi

				 ! write(*,*) point
				call	dqagp(anc,0d0,pi,npts2,points,eps,eps,anc2,abserr,&
					neval,ier,leniw,lenw,last,iwork,work)
				else
					call dqage ( anc, 0D0, pi, eps, eps, key, limit, anc2, abserr, &
						neval, ier, alist, blist, rlist, elist, iord, last )
			endif
			! write(*,*) "phi",anc2
		end function anc2


		real(8) function anc(x)
			implicit none
			real(8)::x

			anc=k*fn(q,w,(/k,x/))
			! write(*,*) "anc", anc, k, fn(q,w,(/k,x/))
		end function anc

		real(8) function anc3(k)
			real(8)::k
			anc3=PIphi33(fn,k,ptssub,q,w)

		end function anc3

		real(8) function NC10PolList(flist)
			integer(4)::i
			real(8)   ::flist(10)
			real(8), external::fn
			real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
													1080D0,19344D0,&
													5778D0,	5778D0,&
													19344D0,1080D0,&
													15741D0,2857D0&
													/)*9D0/89600D0

			NC10PolList=0D0
			do i=1,10
				NC10PolList=NC10PolList+Newt10(i)*flist(i)
			enddo
		end function NC10PolList
end function PolIntegratephi33

real(8) function PIphi33(fn,k,ptssub,q,w)
		use QuadPackDouble
		real(8)::k,q,w
		real(8), external::fn
		external::ptssub
		integer (kind=4), parameter::limit=1000,key=1
		real (kind=8)   , parameter::eps=1D-10
		integer(4), parameter:: npts2=3
		integer (kind=4)::neval,ier,last,iord(limit),level(limit),ndin(npts2)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
							,elist(limit) !,res1,res2,res3,up1,up2

		real(8)::points(npts2)!,level(npts2)
		integer (kind=4), parameter::leniw=limit,lenw=leniw*2-npts2
		! real (kind=8)   , parameter::eps=1D-8
		integer (kind=4)::iwork(leniw)!,neval,ier,last
		real (kind=8)   ::work(lenw),res1,res2,res3,up1,up2,point!,points(npts2)!,abserr

		call ptssub(k,q,w,point)

		! write(*,*) point
		points=0D0

		if (point.le.pi) then
			points(1)=point
			points(2)=0d0
			points(3)=pi

			 ! write(*,*) point
			call	dqagp(anc,0d0,pi,npts2,points,eps,eps,PIphi33,abserr,&
				neval,ier,leniw,lenw,last,iwork,work)
			else
				call dqage ( anc, 0D0, pi, eps, eps, key, limit, PIphi33, abserr, &
					neval, ier, alist, blist, rlist, elist, iord, last )
		endif
		! write(*,*) "phi",PIphi33
contains

	real(8) function anc(x)
		implicit none
		real(8)::x

		anc=k*fn(q,w,(/k,x/))
		! write(*,*) "anc", anc, k, fn(q,w,(/k,x/))
	end function anc

end function PIphi33



!!!!!!!!!!!!!!!!!!!!!!!! Spin Stuff

real(8) function ReChiPeakst(q,w,s,t)
	real(8)::q,w,s,t
	integer(4):: a,b,bb

	ReChiPeakst=0d0
	do a = -1,1,2
		do b = -1,1,2
			do bb = -1,1,2
				if (.not.((b.eq.-1D0).and.(bb.eq.1D0))) then
				 if (b.eq.-1D0) cycle
				 ! if (bb.eq.-1D0) cycle
					ReChiPeakst=ReChiPeakst+ReChiPeakPartst(q,w,real(a,kind=8),real(b,kind=8),real(bb,kind=8),s,t)
				endif
			end do
		end do
	end do


	! ReChiPeak=ReChiPeakPart(q,w,1D0,1D0,1D0)+ReChiPeakPart(q,w,-1D0,1D0,1D0)&
	! 				 +ReChiPeakPart(q,w,-1D0,1D0,-1D0)+ReChiPeakPart(q,w,1D0,-1D0,-1D0)
end function ReChiPeakst




real(8) function ReChiPeakPartst(q,w,a,b,bb,s,t)
	real(8)::q,w,a,b,bb,up,wt,s,t
	integer(4),parameter::steps=20
	real(8)::infourpiSQ=1D0/4D0/pi**2

	wt=(w+(s-t)*HalfGlobZeem)/gam ! w gets shifted in the spin split case

	if ( b.gt.0D0 ) then ! b = +
		if ( a.gt.0D0 ) then ! a = +
			if ( s.gt.0D0 ) then ! s = +
				up=GlobkfU
			else ! s = -
				up=GlobkfD
			end if
		else ! a = -
			if ( t.gt.0D0 ) then ! t = +
				up=GlobkfU
			else ! t = -
				up=GlobkfD
			end if
		end if
	else ! b = -
		! The particles only redistribute in the upper band so we only have one k-limit for the lower band
		up=Globkc
	end if

	! ReChiPeakPart=PolIntegratephi31(anc,0D0,up,0D0,0D0,pts,steps,q,w)/4D0/pi**2
	ReChiPeakPartst=PolIntegratephi33(anc,0D0,up,0D0,0D0,pts,steps,q,w)*infourpiSQ/gam!*2D0/2D0!(gv/2)

contains

	real(8) function anc(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real(8)::q,w,k(2),kp,temp


		temp=dcos(k(2))
		kp=dsqrt(k(1)**2+q**2+2d0*k(1)*q*temp)
		! kp=dsqrt(kp)
		! wt=w/gam


		if ( dabs(k(1)-q).lt.1D-5 ) then
			temp=1D0+bb*dcos(k(2)*0.5D0)
		else
			temp=1D0+bb*(k(1)+q*temp)/kp
		end if

		anc=a*wt+k(1)-bb*kp

		anc=b*temp*anc/(anc**2+GlobetaSQ)
		! rewind 42
		! write(42,*) q,w,k,a,b,bb,s,t,anc
	end function anc

	subroutine pts(k,q,w,res)
		real(8),intent(in) ::k,q,w
		real(8),intent(out)::res
		real(8)::x
		real(8),parameter::halfpi=pi/2D0

		x=(wt**2-q**2+2D0*a*wt*k)
		! write(*,*) x,k,q
		x=x/(k*q)*0.5D0
		res=dacos(x)
		! res=halfpi-2D0*datan2(x,1D0+dsqrt(1D0-x**2))
		! write(*,*) res
	end subroutine pts
end function ReChiPeakPartst

real(8) function epsLDAst(q,w,s,t)
	real(8)::q,w,s,t


	! write(*,*) "eps",q,w,s,t
	! epsLDAst=1D0-Globfxc*ReChiPeakst(q,w,s,t)
	epsLDAst=1D0-Globfxc*ReChiAnDU(q,w)
end function epsLDAst


real(8) function wmagnonLDA(q,a,b,s,t)
 use AuxiliaryFunctions
 implicit none
 real(8)::q,a,b,resfn,s,t
 real(8),parameter::eps=5D-17

 !write(*,*) "a,b"
 !write(*,*) a,b
 call RootFinder(anc,a,b,eps,wmagnonLDA,resfn)
 !write(*,*) "Zero found"
 !write(*,*) "q,w,epsRPA(q,w)"
 !write(*,*) q/Globkf,wplasmonPGG/GlobEf,resfn


contains
 real(8) function anc(w)
		real(8)::w

		! write(*,*) "anc", q,w,s,t
		anc=epsLDAst(q,w,s,t)
 end function anc
end function wmagnonLDA

real(8) function wmagnonPGG(q,a,b,s,t)
 use AuxiliaryFunctions
 implicit none
 real(8)::q,a,b,resfn,s,t
 real(8),parameter::eps=1D-8

 !write(*,*) "a,b"
 !write(*,*) a,b
 call RootFinder(anc,a,b,eps,wmagnonPGG,resfn)
 !write(*,*) "Zero found"
 !write(*,*) "q,w,epsRPA(q,w)"
 !write(*,*) q/Globkf,wplasmonPGG/GlobEf,resfn


contains
 real(8) function anc(w)
		real(8)::w

		! write(*,*) "anc", q,w,s,t
		anc=1D0-PGGcst(q)*ReChiAnDU(q,w)
 end function anc
end function wmagnonPGG


real(8) function PGGcst(q)
	use QuadPackDouble
	real(8)::q
	integer (kind=4), parameter::limit=100
	real (kind=8)   , parameter::minpi=-pi**(-1),eps=1D-8
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit)

	call dqagie ( anc, 0D0, 1, eps, eps, limit , PGGcst, abserr, &
		neval, ier, alist, blist, rlist, elist, iord, last )

	PGGcst=-4D0*PGGcst/pi/(Globntot**2)
contains
	real(8) function anc(r)
		real(8)::r


		if ( r.eq.0D0 ) then
			anc=(GlobkfU+Globkc)*(GlobkfD+Globkc)/4D0
			return
		end if

		anc=Globkc*bessel_jn(1,Globkc*r)
		anc=(GlobkfU*bessel_jn(1,GlobkfU*r)+anc)*(GlobkfD*bessel_jn(1,GlobkfD*r)+anc)*bessel_jn(0,q*r)/r**2
	end function anc
end function PGGcst


real(8) function SpinQIntercept(a,b,s,t)
	use AuxiliaryFunctions
	real(8)::a,b,s,t
	real(8)::eps,resa,resfn



	call RootFinder1(anc,a,b,1D-8,SpinQIntercept,resfn)

	! call SimpleBisect(anc,a,b,1D-8,resa)

	! SpinQIntercept=resa

contains
	real(8) function anc(q)
		real(8)::q

		! anc=wmagnonLDA(q,0d0,GLobZeem-gam*q,s,t)-GLobZeem+gam*q
		! anc=epsLDAst(q,GLobZeem-gam*q,s,t)
		anc=1D0-Globfxc*ReChiPeakst(q,GLobZeem-gam*q,-1D0,1D0)
	end function anc
end function SpinQIntercept

real(8) function ReChiAnDU(q,w)
	real(8)::q,w,wt,wtt,wttsq,wts,wtssq



		wt=(w-GlobZeem)/gam


		! ReChiAnDU = gg((-wt+2D0*GlobkfU)/q)-gg((wt+2D0*GlobkfD)/q)
		! ReChiAnDU = ReChiAnDU*q**2/(16D0*pi*gam*dsqrt(wt**2-q**2))
		! ReChiAnDU = -(GlobkfU+GlobkfD)/(4D0*pi*gam)-ReChiAnDU

		wtt=-wt+2D0*Globkfu
		wttsq=dsqrt(wtt**2-q**2)
		wts=wt+2D0*Globkfd
		wtssq=dsqrt(wts**2-q**2)

		ReChiAnDU = q**2*dlog((wtt+wttsq)/(wts+wtssq))&
							 -wtt*wttsq+wts*wtssq
		ReChiAnDU = ReChiAnDU/(16D0*pi*gam*dsqrt(wt**2-q**2))
		ReChiAnDU = -(GlobkfU+GlobkfD)/(4D0*pi*gam)-ReChiAnDU
end function ReChiAnDU

real(8) function stiff(f0,f2)
	real(8)::f0,f2,sqp,sqm

	! stiff=f0*GlobKf0/4D0/pi/gam
	! sqp=dsqrt(1D0+Globzp)
	! sqm=dsqrt(1D0-Globzp)
	! stiff=f0/8D0/pi*dlog((sqp+Globzp*stiff)/(sqm-Globzp*stiff))
	! stiff=stiff-gam*0.5D0/GlobKf0/Globzp*(sqm+sqp)-f2*Globn0*Globzp-2D0*gam**2/f0/Globn0/Globzp

	sqm=GlobkfD**2-GlobkfU**2

	stiff=gam/(GlobkfD-GlobkfU)+f2*(sqm)/(2D0*pi)+4D0*pi*gam**2/(f0*sqm)&
			 +f0/(8D0*pi)*dlog((8D0*pi*gam*GlobkfU-f0*sqm)/(8D0*pi*gam*GlobkfD+f0*sqm))
end function stiff

real(8) function w0(f0)
	real(8)::f0

	w0=GLobZeem-f0*Globn*Globzp*0.5D0
end function w0

real(8) function wsw(q,f0,f2)
	real(8)::q,f0,f2

	wsw=w0(f0)+0.5D0*q**2*stiff(f0,f2)
end function wsw

real(8) function whyp(q,f0,f2)
	real(8)::q,f0,f2,s,wz

	wz=w0(f0)
	s=stiff(f0,f2)
	whyp=s*GlobZeem**2+2D0*wz*gam**2
	whyp=wz+(wz**2*gam**2-wz*gam*dsqrt(wz**2*gam**2+q**2*s*whyp))/dabs(whyp)

	! wz=dabs(GLobZeem-w0(f0))
	! s=dabs(stiff(f0,f2))

	! whyp=GLobZeem-wz*dsqrt(1D0+0.5D0*s/wz*q**2)
	! whyp=GLobZeem-dsqrt(wz**2+0.5D0*s*wz*q**2)

end function whyp



real(8) function PGGUD2(q)
	real(8):: q



end function PGGUD2

subroutine ReadVec (unt,size,vec)
	implicit none
	integer(4), intent(in)::unt,size
	real(8), intent(out)::vec(size)
	integer(4)::i

	! open(unit=99, file=filename, status='old', action='read')
	! read(unt,*) size

	! allocate(vec(size))

	do i = 1, size
		read(unt,*) vec(i)
	end do

end subroutine ReadVec

subroutine ReadBessJzs (fname0,fname1)
	use Globals
	character(len=*), intent(in) :: fname0,fname1
	integer(4)::i

	open(unit=99, file=fname0, status='old', action='read')
	open(unit=89, file=fname1, status='old', action='read')

	read(99,*) GlobJzsN
	read(89,*)

	allocate(GlobJ0zs(GlobJzsN),GlobJ1zs(GlobJzsN),GlobPGGSpinZs(2*GlobJzsN))

	do i = 1,GlobJzsN
		read(99,*) GlobJ0zs(i)
		read(89,*) GlobJ1zs(i)
	end do

	close(99)
	close(89)


end subroutine ReadBessJzs

subroutine SortPGGSpinZs ()
	use Globals
	integer(4)::i,pui,pdi
	real(8)::pu,pd
	! write(*,*) "Sorting"
	pui=1
	pdi=1
	do i = 1,2*GlobJzsN
		if ( pui.gt.GlobJzsN ) then
			pd=GlobJ1zs(pdi)/GlobkfD
			GlobPGGSpinZs(i)=pd
			pdi=pdi+1
			cycle
		elseif ( pdi.gt.GlobJzsN ) then
			pu=GlobJ1zs(pui)/GlobkfU
			GlobPGGSpinZs(i)=pu
			pui=pui+1
			cycle
		else
			pu=GlobJ1zs(pui)/GlobkfU
			pd=GlobJ1zs(pdi)/GlobkfD
		end if

		if ( pd.le.pu ) then
			GlobPGGSpinZs(i)=pd
			pdi=pdi+1
		else
			GlobPGGSpinZs(i)=pu
			pui=pui+1
		end if
	end do

	do i = 1,2*GlobJzsN-1
		if ( GlobPGGSpinZs(i).ge.GlobPGGSpinZs(i+1) ) then
			write(*,*) "GlobPGGSpinZs out of order."
			write(*,*) i,GlobPGGSpinZs(i),i+1,GlobPGGSpinZs(i+1)
			call exit(-1)
		end if
	end do

	! write(*,*) "GlobPGGSpinZs is in order."
end subroutine SortPGGSpinZs


subroutine SortPGGZs (a1,a2,v1,v2,num,vout)
	use Globals
	integer(4), intent(in)::num
	real(8), intent(in)::a1,a2,v1(num),v2(num)
	real(8), intent(out)::vout(2*num)
	integer(4)::i,p1i,p2i
	real(8)::p1,p2

	! write(*,*) "Sorting"
	p1i=1
	p2i=1
	do i = 1,2*num
		if ( p1i.gt.num ) then
			p2=v2(p2i)/a2
			vout(i)=p2
			p2i=p2i+1
			cycle
		elseif ( p2i.gt.num ) then
			p1=v1(p1i)/a1
			vout(i)=p1
			p1i=p1i+1
			cycle
		else
			p1=v1(p1i)/a1
			p2=v2(p2i)/a2
		end if

		if ( p2.le.p1 ) then
			vout(i)=p2
			p2i=p2i+1
		else
			vout(i)=p1
			p1i=p1i+1
		end if
	end do

	do i = 1,2*num-1
		if ( vout(i).ge.vout(i+1) ) then
			write(*,*) "vout out of order."
			write(*,*) i,vout(i),i+1,vout(i+1)
			call exit(-1)
		end if
	end do

	! write(*,*) "GlobPGGSpinZs is in order."
end subroutine SortPGGZs


real(8) function PGGUD20()
	! real(8)::
	use QuadPackDouble
	! real(8)::q
	integer (kind=4), parameter::limit=10000,key=7
	real (kind=8)   , parameter::minpi=-pi**(-1),eps=1D-12
	integer (kind=4)::neval,ier,last,iord(limit),i
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),a,b,temp

	temp=0D0
	a=0D0
	b=GlobPGGSpinZs(1)
	call dqage ( anc, a, b, eps, eps, key, limit, PGGUD20, abserr, &
		neval, ier, alist, blist, rlist, elist, iord, last )
	! call dqage ( anc, a, b, eps, eps, limit , PGGUD20, abserr, &
	! 	neval, ier, alist, blist, rlist, elist, iord, last )
	do i = 2, 2*GlobJzsN
		a=b
		b=GlobPGGSpinZs(i)
		! write(*,*) anc(b)
		call dqage ( anc, a, b, eps, eps, key, limit, temp, abserr, &
			neval, ier, alist, blist, rlist, elist, iord, last )
		! call dqage ( anc, a, b, eps, eps, limit , temp, abserr, &
		! 	neval, ier, alist, blist, rlist, elist, iord, last )
		PGGUD20=PGGUD20+temp
	end do

	! call dqagie ( anc, b, 1, eps, eps, limit , temp, abserr, &
		! neval, ier, alist, blist, rlist, elist, iord, last )

	! PGGUD20=PGGUD20+temp


	PGGUD20=-4D0*PGGUD20/pi/(Globntot**2)

contains
	real(8) function anc(r)
		real(8)::r


		if ( r.eq.0D0 ) then
			anc=(GlobkfU**2+Globkc**2)*(GlobkfD**2+Globkc**2)/4D0
			return
		end if

		anc=Globkc*bessel_jn(1,Globkc*r)
		anc=(GlobkfU*bessel_jn(1,GlobkfU*r)+anc)*(GlobkfD*bessel_jn(1,GlobkfD*r)+anc)/r**2
	end function anc
end function PGGUD20

subroutine IntegrateTYLR (a,b,fn,n,res)
	use TaylUR
	integer(4), intent(in)::n
	type(taylor), intent(in) ::a,b
	type(taylor), intent(out)::res
	type(taylor), external   ::fn
	integer(4)::i
	type(taylor):: dx,x


	dx = (b-a)/real(n, kind=8)

	res = 0D0

	x = a
	do i = 1,n
		res = res + fn(x)
		x = res + dx
	enddo

	res = res*dx
end subroutine IntegrateTYLR

subroutine StupidIntegrateNCtylr(f,a,b,steps,res,ier)
	use TaylUR
	implicit none
	type(taylor), intent (in)    ::a,b
	type(taylor), intent (out)   ::res
	integer (4), intent (in) ::steps
	integer (4), intent (out)::ier
	type(taylor), external:: f
	integer (4) ::i
	type(taylor) ::x,dx

	ier=0

	dx=(b-a)/real(steps, kind=8) !NC10 takes 10 steps

	x=a+dx/2D0
	res=0D0
	do i=1,steps
		res=res+NC10(x,x+dx,f)
		x=x+dx
	enddo
	!res=res*dx

contains
	function NC10(a,b,fn)
		integer (4)::i
		type(taylor) :: a,b,x,dx, NC10
		type(taylor), external::fn
		real (8), parameter:: Newt10(10)=(/2857D0,15741D0,&
												1080D0,19344D0,&
												5778D0,	5778D0,&
												19344D0,1080D0,&
												15741D0,2857D0&
												/)*9D0/89600D0

		NC10=0D0
		x=a
		dx=(b-a)/9D0
		do i=1,10
			NC10=NC10+fn(x)*Newt10(i)
			x=x+dx
		enddo
		NC10=NC10*dx
	end function NC10
end subroutine StupidIntegrateNCtylr

function PGGcstTYLR(q)
	use TaylUR
	! real(8)::q
	integer (4):: ier
	integer (4), parameter:: n=70000
	integer (4), parameter::limit=100
	real (8)   , parameter::minpi=-pi**(-1),eps=1D-8
  type(taylor) :: q,a,b,res,PGGcstTYLR

  a = 0D0
	b = 100D0!/GlobKf0
	! call dqagie ( anc, 0D0, 1, eps, eps, limit , PGGcst, abserr, &
	! 	neval, ier, alist, blist, rlist, elist, iord, last )

	! call IntegrateTYLR(a,b,anc,n,res)

	call StupidIntegrateNCtylr(anc,a,b,n/10,res,ier)


	PGGcstTYLR=res*(-4D0/pi/(Globntot**2))
contains
	function anc(r)
		type(taylor)::r,anc


		if ( value(r).eq.0D0 ) then
			anc=(GlobkfU+Globkc)*(GlobkfD+Globkc)/4D0
			return
		end if

		anc=BESSJ1(r*Globkc)*Globkc
		anc=(BESSJ1(r*GlobkfU)*GlobkfU+anc)*(BESSJ1(r*GlobkfD)*GlobkfD+anc)*BESSJ0(q*r)/r**2
	end function anc

	FUNCTION BESSJ0 (X)
	implicit none

!     THIS FUNCTION RETURNS THE VALUE OF THE FIRST KIND BESSEL FUNCTION
!     OF ORDER 0 FOR ANY REAL X. WE USE HERE THE POLYNOMIAL APPROXIMATION
!     BY SERIES OF CHEBYSHEV POLYNOMIALS FOR 0<X<8 AND 0<8/X<1.
!     REFERENCES :
!     M.ABRAMOWITZ,I.A.STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS, 1965.
!     C.W.CLENSHAW, NATIONAL PHYSICAL LABORATORY MATHEMATICAL TABLES,
!     VOL.5, 1962.
  type(taylor)::X,Y,AX,FR,FS,Z,FP,FQ,XX,BESSJ0
	REAL *8 P1,P2,P3,P4,P5,R1,R2,R3,R4,R5,R6,Q1,Q2,Q3,Q4,Q5,S1,S2,S3,S4,S5,S6
	DATA P1,P2,P3,P4,P5 /1.D0,-.1098628627D-2,.2734510407D-4,  &
	-.2073370639D-5,.2093887211D-6 /
	DATA Q1,Q2,Q3,Q4,Q5 /-.1562499995D-1,.1430488765D-3,   &
	-.6911147651D-5,.7621095161D-6,-.9349451520D-7 /
	DATA R1,R2,R3,R4,R5,R6 /57568490574.D0,-13362590354.D0,    &
	651619640.7D0,-11214424.18D0,77392.33017D0,-184.9052456D0 /
	DATA S1,S2,S3,S4,S5,S6 /57568490411.D0,1029532985.D0,  &
	9494680.718D0,59272.64853D0,267.8532712D0,1.D0 /
	IF(X.EQ.0.D0) GO TO 1
	AX = ABS (X)
	IF (AX.LT.8.) THEN
	Y = X*X
	FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))))
	FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))))
	BESSJ0 = FR/FS
	ELSE
	Z = 8.D0/AX
	Y = Z*Z
	XX = AX-.785398164D0
	FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)))
	FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)))
	BESSJ0 = SQRT(.636619772D0/AX)*(FP*COS(XX)-Z*FQ*SIN(XX))
	ENDIF
	RETURN
1 BESSJ0 = 1.D0
	RETURN
	END
! ----------------------------------------------------------------------
	FUNCTION BESSJ1 (X)

!     THIS FUNCTION RETURNS THE VALUE OF THE FIRST KIND BESSEL FUNCTION
!     OF ORDER 0 FOR ANY REAL X. WE USE HERE THE POLYNOMIAL APPROXIMATION
!     BY SERIES OF CHEBYSHEV POLYNOMIALS FOR 0<X<8 AND 0<8/X<1.
!     REFERENCES :
!     M.ABRAMOWITZ,I.A.STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS, 1965.
!     C.W.CLENSHAW, NATIONAL PHYSICAL LABORATORY MATHEMATICAL TABLES,
!     VOL.5, 1962.

	type(taylor) X,BESSJ1,AX,FR,FS,Z,FP,FQ,XX,Y
	REAL *8 P1,P2,P3,P4,P5,P6,R1,R2,R3,R4,R5,R6,Q1,Q2,Q3,Q4,Q5,S1,S2,S3,S4,S5,S6
	DATA P1,P2,P3,P4,P5 /1.D0,.183105D-2,-.3516396496D-4,  &
	.2457520174D-5,-.240337019D-6 /,P6 /.636619772D0 /
	DATA Q1,Q2,Q3,Q4,Q5 /.04687499995D0,-.2002690873D-3,   &
	.8449199096D-5,-.88228987D-6,.105787412D-6 /
	DATA R1,R2,R3,R4,R5,R6 /72362614232.D0,-7895059235.D0, &
	242396853.1D0,-2972611.439D0,15704.48260D0,-30.16036606D0 /
	DATA S1,S2,S3,S4,S5,S6 /144725228442.D0,2300535178.D0, &
	18583304.74D0,99447.43394D0,376.9991397D0,1.D0 /

	AX = ABS(X)
	IF (AX.LT.8.) THEN
	Y = X*X
	FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))))
	FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))))
	BESSJ1 = X*(FR/FS)
	ELSE
	Z = 8.D0/AX
	Y = Z*Z
	XX = AX-2.35619491D0
	FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)))
	FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)))
	BESSJ1 = SQRT(P6/AX)*(COS(XX)*FP-Z*SIN(XX)*FQ)*SIGN(S6,X)
	ENDIF
	RETURN
	END
end function PGGcstTYLR

function PGGcstTYLR2(q)
	use TaylUR
	! real(8)::q
	integer (4):: ier,imax,i
	integer (4), parameter:: n=100
	integer (4), parameter::limit=100
	real (8)   , parameter::minpi=-pi**(-1),eps=1D-8
  type(taylor) :: q,a,b,res,PGGcstTYLR2,temp

  a = 0D0
	b = 1D0!/GlobKf0
	! call dqagie ( anc, 0D0, 1, eps, eps, limit , PGGcst, abserr, &
	! 	neval, ier, alist, blist, rlist, elist, iord, last )

	! call IntegrateTYLR(a,b,anc,n,res)

	! call StupidIntegrateNCtylr(anc,a,b,n/10,res,ier)

	! call GaussAdaptQuadt(anc,a,b,1D-8,res)
  ! temp = res
	temp=0D0
	b=1D0

	imax = 2*GlobJzsN

	do i = 2, imax+1
		a = b
		if ( i.eq.imax+1 ) then
			b = 0D0
		else
			b=1D0/(GlobPGGSpinZs(i)+1D0)
		end if

		call GaussAdaptQuadt(anc1,a,b,1D-8,res)
		! call StupidIntegrateNCtylr(anc1,a,b,n/10,res,ier)
		temp = temp + res
		call GaussAdaptQuadt(anc2,a,b,1D-8,res)
		! call StupidIntegrateNCtylr(anc2,a,b,n/10,res,ier)
		temp = temp + res
		call GaussAdaptQuadt(anc3,a,b,1D-8,res)
		! call StupidIntegrateNCtylr(anc3,a,b,n/10,res,ier)
		temp = temp + res
		call GaussAdaptQuadt(anc4,a,b,1D-8,res)
		! call StupidIntegrateNCtylr(anc4,a,b,n/10,res,ier)
		temp = temp + res

	end do
	temp = temp*(-1D0)




	! temp = 0D0
	!
	! ! call GaussAdaptQuadt(anc1,a,b,1D-8,res)
	! call StupidIntegrateNCtylr(anc1,a,b,n/10,res,ier)
	! temp = res
	! ! call GaussAdaptQuadt(anc2,a,b,1D-8,res)
	! call StupidIntegrateNCtylr(anc2,a,b,n/10,res,ier)
	! temp = temp + res
	! ! call GaussAdaptQuadt(anc3,a,b,1D-8,res)
	! call StupidIntegrateNCtylr(anc3,a,b,n/10,res,ier)
	! temp = temp + res
	! ! call GaussAdaptQuadt(anc4,a,b,1D-8,res)
	! call StupidIntegrateNCtylr(anc4,a,b,n/10,res,ier)
	! temp = temp + res




	PGGcstTYLR2=temp*(-4D0/pi/(Globntot**2))
contains
	function anc(t)
		type(taylor)::t,anc,temp


		if ( value(t).eq.0D0 ) then
			anc=0D0
			return
		end if

		temp = (t*(-1D0)+1D0)/t

		anc=BESSJ1(temp*Globkc)*Globkc
		anc=(BESSJ1(temp*GlobkfU)*GlobkfU+anc)*(BESSJ1(temp*GlobkfD)*GlobkfD+anc)*BESSJ0(q*temp)/(t*(-1D0)+1D0)/(t*(-1D0)+1D0)
	end function anc

	function anc1(t)
		type(taylor)::t,anc1,temp


		if ( value(t).eq.0D0 ) then
			anc1=0D0
			return
		end if

		temp = (t*(-1D0)+1D0)/t

		anc1=(BESSJ1(temp*GlobkfU)*GlobkfU)*(BESSJ1(temp*GlobkfD)*GlobkfD)*BESSJ0(q*temp)/(t*(-1D0)+1D0)/(t*(-1D0)+1D0)
	end function anc1

	function anc2(t)
		type(taylor)::t,anc2,temp


		if ( value(t).eq.0D0 ) then
			anc2=0D0
			return
		end if

		temp = (t*(-1D0)+1D0)/t

		anc2=(BESSJ1(temp*GlobkfU)*GlobkfU)*(BESSJ1(temp*Globkc)*Globkc)*BESSJ0(q*temp)/(t*(-1D0)+1D0)/(t*(-1D0)+1D0)
	end function anc2

	function anc3(t)
		type(taylor)::t,anc3,temp


		if ( value(t).eq.0D0 ) then
			anc3=0D0
			return
		end if

		temp = (t*(-1D0)+1D0)/t

		anc3=(BESSJ1(temp*GlobkfD)*GlobkfD)*(BESSJ1(temp*Globkc)*Globkc)*BESSJ0(q*temp)/(t*(-1D0)+1D0)/(t*(-1D0)+1D0)
	end function anc3

	function anc4(t)
		type(taylor)::t,anc4,temp


		if ( value(t).eq.0D0 ) then
			anc4=0D0
			return
		end if

		temp = (t*(-1D0)+1D0)/t

		anc4=((BESSJ1(temp*Globkc)*Globkc)**2)*BESSJ0(q*temp)/(t*(-1D0)+1D0)/(t*(-1D0)+1D0)
	end function anc4

	FUNCTION BESSJ0 (X)
	implicit none

!     THIS FUNCTION RETURNS THE VALUE OF THE FIRST KIND BESSEL FUNCTION
!     OF ORDER 0 FOR ANY REAL X. WE USE HERE THE POLYNOMIAL APPROXIMATION
!     BY SERIES OF CHEBYSHEV POLYNOMIALS FOR 0<X<8 AND 0<8/X<1.
!     REFERENCES :
!     M.ABRAMOWITZ,I.A.STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS, 1965.
!     C.W.CLENSHAW, NATIONAL PHYSICAL LABORATORY MATHEMATICAL TABLES,
!     VOL.5, 1962.
  type(taylor)::X,Y,AX,FR,FS,Z,FP,FQ,XX,BESSJ0
	REAL *8 P1,P2,P3,P4,P5,R1,R2,R3,R4,R5,R6,Q1,Q2,Q3,Q4,Q5,S1,S2,S3,S4,S5,S6
	DATA P1,P2,P3,P4,P5 /1.D0,-.1098628627D-2,.2734510407D-4,  &
	-.2073370639D-5,.2093887211D-6 /
	DATA Q1,Q2,Q3,Q4,Q5 /-.1562499995D-1,.1430488765D-3,   &
	-.6911147651D-5,.7621095161D-6,-.9349451520D-7 /
	DATA R1,R2,R3,R4,R5,R6 /57568490574.D0,-13362590354.D0,    &
	651619640.7D0,-11214424.18D0,77392.33017D0,-184.9052456D0 /
	DATA S1,S2,S3,S4,S5,S6 /57568490411.D0,1029532985.D0,  &
	9494680.718D0,59272.64853D0,267.8532712D0,1.D0 /
	IF(X.EQ.0.D0) GO TO 1
	AX = ABS (X)
	IF (AX.LT.8.) THEN
	Y = X*X
	FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))))
	FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))))
	BESSJ0 = FR/FS
	ELSE
	Z = 8.D0/AX
	Y = Z*Z
	XX = AX-.785398164D0
	FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)))
	FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)))
	BESSJ0 = SQRT(.636619772D0/AX)*(FP*COS(XX)-Z*FQ*SIN(XX))
	ENDIF
	RETURN
1 BESSJ0 = 1.D0
	RETURN
	END
! ----------------------------------------------------------------------
	FUNCTION BESSJ1 (X)

!     THIS FUNCTION RETURNS THE VALUE OF THE FIRST KIND BESSEL FUNCTION
!     OF ORDER 0 FOR ANY REAL X. WE USE HERE THE POLYNOMIAL APPROXIMATION
!     BY SERIES OF CHEBYSHEV POLYNOMIALS FOR 0<X<8 AND 0<8/X<1.
!     REFERENCES :
!     M.ABRAMOWITZ,I.A.STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS, 1965.
!     C.W.CLENSHAW, NATIONAL PHYSICAL LABORATORY MATHEMATICAL TABLES,
!     VOL.5, 1962.

	type(taylor) X,BESSJ1,AX,FR,FS,Z,FP,FQ,XX,Y
	REAL *8 P1,P2,P3,P4,P5,P6,R1,R2,R3,R4,R5,R6,Q1,Q2,Q3,Q4,Q5,S1,S2,S3,S4,S5,S6
	DATA P1,P2,P3,P4,P5 /1.D0,.183105D-2,-.3516396496D-4,  &
	.2457520174D-5,-.240337019D-6 /,P6 /.636619772D0 /
	DATA Q1,Q2,Q3,Q4,Q5 /.04687499995D0,-.2002690873D-3,   &
	.8449199096D-5,-.88228987D-6,.105787412D-6 /
	DATA R1,R2,R3,R4,R5,R6 /72362614232.D0,-7895059235.D0, &
	242396853.1D0,-2972611.439D0,15704.48260D0,-30.16036606D0 /
	DATA S1,S2,S3,S4,S5,S6 /144725228442.D0,2300535178.D0, &
	18583304.74D0,99447.43394D0,376.9991397D0,1.D0 /

	AX = ABS(X)
	IF (AX.LT.8.) THEN
	Y = X*X
	FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))))
	FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))))
	BESSJ1 = X*(FR/FS)
	ELSE
	Z = 8.D0/AX
	Y = Z*Z
	XX = AX-2.35619491D0
	FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)))
	FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)))
	BESSJ1 = SQRT(P6/AX)*(COS(XX)*FP-Z*SIN(XX)*FQ)*SIGN(S6,X)
	ENDIF
	RETURN
	END
end function PGGcstTYLR2


real(8) function d2qpgg(q)
	use TaylUR
	implicit none
	real(8)::q
	type(taylor)::x,t

	diagonal_taylors=.true.
	Taylor_order = 3

	x = independent(1,q)
	t = PGGcstTYLR(x)
	d2qpgg = real(derivative(t,1,2))
	! d2qpgg = realvalue(t)
end function d2qpgg

subroutine PGGtylr (q,f,f1,f2)
	use TaylUR
	real(8), intent(in) ::q
	real(8), intent(out)::f,f1,f2
  type(taylor) :: x,t,r
	! real(8) ::r


	diagonal_taylors=.true.
	Taylor_order = 2

	x = independent(1,q)
	t = PGGcstTYLR2(x)
	! t = BESSJ1(x)*BESSJ0(x)

	! r=1D0

	! t = BESSJ1(r*Globkc)*Globkc
	! t = (BESSJ1(r*GlobkfU)*GlobkfU+t)*(BESSJ1(r*GlobkfD)*GlobkfD+t)*BESSJ0(x*r)/r**2


	f = realvalue(t)

	f1 = real(derivative(t,1,1))
	f2 = real(derivative(t,1,2))

end subroutine PGGtylr


recursive subroutine GaussAdaptQuadt(fn,a,b,eps,res)
	use TaylUR
	implicit none
	real (kind=8), intent(in) ::eps
	type(taylor),  intent(in) ::a,b
	type(taylor), intent(out) ::res
	type(taylor), external::fn
	type(taylor)::zero
	!Wrapper subroutine to properly initialize the the
	!auxiliary subroutine.
	!!!fn  :: real; function
	!!!a,b :: real; integration limits, a<b
	!!!eps :: real; local error tolerance
	!!!res :: real; integration result

	zero=0D0

	call GaussAdaptQuadAuxt(fn,a,b,eps,0,zero,res)

end subroutine GaussAdaptQuadt

recursive subroutine GaussAdaptQuadAuxt(fn,a,b,eps,ct,totin,totout)
	use TaylUR
	implicit none
	integer (kind=4), intent(in)::ct
	real (kind=8), intent(in) ::eps
	type(taylor),  intent(in) ::a,b,totin
	type(taylor), intent(out) ::totout
	type(taylor) ::lint,rint,m,tot1,tot2,totnew,totold
	type(taylor), external::fn
	!Adaptive integration subroutine
	!!!fn    :: real; function
	!!!a,b   :: real; integration limits
	!!!eps   :: real; local error tolerance
	!!!totin :: real; previous integration result
	!!!totout:: real; current integration result
	!!!ct    ::  int; subdivision counter



		!Don't go deeper than 1000 subdivisions
		if (ct.gt.1000) then
			write(*,*) "Integral didn't converge within 1000 subdivisions"
			write(*,*) "a,b,eps,|err|"
			write(*,*) a,b,eps,dabs(realvalue(totnew)-realvalue(totold))

			call EXIT(5)
		endif


		!Halve the interval and approximate each side with
		!8-point Gaussian-Legendre Quadrature
		m=(a+b)/2D0
		lint=G8(fn,a,m)
		rint=G8(fn,m,b)


		!If this is the first call then approximate the whole interval.
		!Otherwise use the previous approximation
		if (ct.eq.0) then
			totold=G8(fn,a,b)
		else
			totold=totin
		endif

		!New approximation
		totnew=lint+rint



		!Stopping conditions
		if (((dabs(realvalue(totnew)-realvalue(totold))).lt.eps).or.((dabs(realvalue(a)-realvalue(b))).lt.eps)) then
			totout=totnew
		else
			!Subdivide further
			call GaussAdaptQuadAuxt(fn,a,m,eps/2D0,ct+1,lint,tot1)
			call GaussAdaptQuadAuxt(fn,m,b,eps/2D0,ct+1,rint,tot2)
			totout=tot1+tot2
		endif


contains

	type(taylor) recursive function G8(fn,l,r)
		implicit none
		type(taylor)::l,r,x,h
		type(taylor), external::fn
		!Abscissae and weights for 8-point Gaussian-Legendre Quadrature over [-1,1]
		!https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
		real (kind=8), parameter::G8x1=1.83434642495649805D-01&
								 ,G8x2=5.25532409916328986D-01&
								 ,G8x3=7.96666477413626740D-01&
								 ,G8x4=9.60289856497536232D-01&
								 ,G8w1=3.62683783378361983D-01&
								 ,G8w2=3.13706645877887287D-01&
								 ,G8w3=2.22381034453374471D-01&
								 ,G8w4=1.01228536290376259D-01
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!8-point Gaussian-Legendre Approximation
		!!!fn    :: real; function
		!!!l,r   :: real; interval limits

			!x and h map [a,b] to [-1,1]
			x=(l+r)/2D0
			h=(r-l)/2D0
			G8=((fn(x+h*G8x1)+fn(x-h*G8x1))*G8w1&
				 +(fn(x+h*G8x2)+fn(x-h*G8x2))*G8w2&
				 +(fn(x+h*G8x3)+fn(x-h*G8x3))*G8w3&
				 +(fn(x+h*G8x4)+fn(x-h*G8x4))*G8w4)*h
	end function G8

end subroutine GaussAdaptQuadAuxt

end module PhysicsGraphene3
