module Response
implicit none
real (kind=8), parameter::  UUUU(4)=(/1D0,1D0,1D0,1D0/),&
							DDDD(4)=(/-1D0,-1D0,-1D0,-1D0/),&
							UDUD(4)=(/1D0,-1D0,1D0,-1D0/),&
							DUDU(4)=(/-1D0,1D0,-1D0,1D0/)
							

real (kind=8):: Globevinfo,fxcVec


contains

	subroutine KernelConstr(rs,z,KernVec)
		use fxc2DEG
		implicit none
		!integer (kind=4) :: 
		real    (kind=8), intent(in)::rs,z 
		real    (kind=8), intent(out)::KernVec(4)
		real    (kind=8)::n 
			

			n=rs2n(rs)

			! Hard Stuff
			! The kernel is actually a somewhat sparse matrix so I'll define it 
			! as a vector thusly:
			! KernVec=< fxc^^^^ , fxcvvvv , fxc^^vv=fxcvv^^ ,fxc^v^v=fxcv^v^ >
			! ^ : up ; v : down

			!KernVec(1)=2D0*Ndnexc(rs,z)+n*Nd2nexc(rs,z)+2D0*(1D0-z)*Nd2nzexc(rs,z)&    !fxcUUUU
			!		   +(1D0-z)**2/n*Nd2zexc(rs,z)
			!KernVec(2)=2D0*Ndnexc(rs,z)+n*Nd2nexc(rs,z)-2D0*(1D0+z)*Nd2nzexc(rs,z)& !fxcDDDD
			!		   +(1D0+z)**2/n*Nd2zexc(rs,z)
			!KernVec(3)=2D0*Ndnexc(rs,z)+n*Nd2nexc(rs,z)-2D0*z*Nd2nzexc(rs,z)&       !fxcUUDD & fxcDDUU
			!		   -(1D0-z**2)/n*Nd2zexc(rs,z)
			!KernVec(4)=2D0/n/z*Ndzexc(rs,z)                                           !fxcUDUD & fxcDUDU

			KernVec(1)=2D0*Ndnexct(rs,z)+n*Nd2nexct(rs,z)+2D0*(1D0-z)*Nd2nzexct(rs,z)&    !fxcUUUU
					   +(1D0-z)**2/n*Nd2zexct(rs,z)
			KernVec(2)=2D0*Ndnexct(rs,z)+n*Nd2nexct(rs,z)-2D0*(1D0+z)*Nd2nzexct(rs,z)& !fxcDDDD
					   +(1D0+z)**2/n*Nd2zexct(rs,z)
			KernVec(3)=2D0*Ndnexct(rs,z)+n*Nd2nexct(rs,z)-2D0*z*Nd2nzexct(rs,z)&       !fxcUUDD & fxcDDUU
					   -(1D0-z**2)/n*Nd2zexct(rs,z)
			KernVec(4)=2D0/n/z*Ndzexct(rs,z)                                           !fxcUDUD & fxcDUDU

			!KernVec=0D0

	end subroutine KernelConstr




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


	subroutine RespCnstr(q,w,Chi0,Chi)
		use Globals
		real (kind=8)::  Chi0UUUU,Chi0DDDD,Chi0UDUD,Chi0DUDU,fHxcUUUU,fHxcDDDD&
						,fHxcUUDD,fHxcDDUU,fxcUDUD,fxcDUDU,M,q,w
		real (kind=8), intent(out)::Chi(4,4)
		real (kind=8), intent(in)::Chi0(4)

			! Init
			Chi0UUUU=Chi0(1) !ReChi0(q,w,uu)
			Chi0DDDD=Chi0(2) !ReChi0(q,w,dd)
			Chi0UDUD=Chi0(3) !ReChi0(q,w,ud)
			Chi0DUDU=Chi0(4) !ReChi0(q,w,du)
			fHxcUUUU=Vc(q)+GlobKernVec(1)
			fHxcDDDD=Vc(q)+GlobKernVec(2)
			fHxcDDUU=Vc(q)+GlobKernVec(3)
			fHxcUUDD=Vc(q)+GlobKernVec(3)
			fxcUDUD=GlobKernVec(4)
			fxcDUDU=GlobKernVec(4)
			M=(1D0-Chi0UUUU*fHxcUUUU)*(1D0-Chi0DDDD*fHxcDDDD)&
			  -Chi0UUUU*Chi0DDDD*fHxcUUDD*fHxcDDUU
			Chi=0D0

			Chi(1,1)=1D0-Chi0UUUU*fHxcUUUU
			Chi(4,1)=-Chi0DDDD*fHxcDDUU
			Chi(1,4)=-Chi0UUUU*fHxcUUDD
			Chi(4,4)=1D0-Chi0DDDD*fHxcDDDD
			Chi(2,2)=1D0-Chi0UDUD*fxcUDUD
			Chi(3,3)=1D0-Chi0DUDU*fxcDUDU
			!Chi(1,1)=(1D0-Chi0DDDD*fHxcDDDD)*Chi0UUUU/M
			!Chi(4,1)=Chi0UUUU*fHxcUUDD*Chi0DDDD/M
			!Chi(1,4)=Chi0DDDD*fHxcDDUU*Chi0UUUU/M
			!Chi(4,4)=(1D0-Chi0UUUU*fHxcUUUU)*Chi0DDDD/M
			!Chi(2,2)=0D0!(1D0-Chi0UDUD*fxcUDUD)/Chi0UDUD
			!Chi(3,3)=0D0!(1D0-Chi0DUDU*fxcDUDU)/Chi0DUDU
			!write(*,*) 1D0/Chi(2,2)
		
		contains

			real (kind=8) function Vc(q)
				use Constants
				implicit none
				real (kind=8) ::q
				! Fourier Transform of screened Coulomb Potential
				! q   :: real wavevector magnitude
			
				Vc=2D0*pi/dabs(q)!*Globn !Norm(q)!*e**2/kappa
			end function Vc


	end subroutine RespCnstr

	subroutine RespCnstrPGG(q,w,Chi0,Chi)
		use Globals
		use fxcPGG
		real (kind=8)::  Chi0UUUU,Chi0DDDD,Chi0UDUD,Chi0DUDU,fHxcUUUU,fHxcDDDD&
						,fHxcUUDD,fHxcDDUU,fxcUDUD,fxcDUDU,M,q,w&
						,fxUUUU,fxDDDD,fxUDUD,fxDUDU,fxDDUU,fxUUDD,abserr
		!real (kind=4)::  qsp,fxUUUUsp,fxDDDDsp,fxUDUDsp,fxDUDUsp,fxDDUUssp,fxUUDDsp&
		!				,abserr,kfsp,kfusp,kfdsp
		real (kind=8), intent(out)::Chi(4,4)
		real (kind=8), intent(in)::Chi0(4)
		integer (kind=4)::neval,ier

			! Init
			Chi0UUUU=Chi0(1) !ReChi0(q,w,uu)
			Chi0DDDD=Chi0(2) !ReChi0(q,w,dd)
			Chi0UDUD=Chi0(3) !ReChi0(q,w,ud)
			Chi0DUDU=Chi0(4) !ReChi0(q,w,du)

			!qsp=real(q, kind=4)
			!kfUsp=real(GlobkfU, kind=4)
			!kfDsp=real(GlobkfD, kind=4)
			!kfsp=real(Globkf0, kind=4)
			!
			!call PGGuu(qsp,kfsp,fxUUUUsp,abserr,neval,ier)           !fxUUUU PGG
			!call PGGdd(qsp,kfsp,fxDDDDsp,abserr,neval,ier)           !fxDDDD PGG
			!call PGGud(qsp,kfUsp,kfDsp,fxUDUDsp,abserr,neval,ier)  !fxUDUD PGG
			!call PGGdu(qsp,kfUsp,kfDsp,fxDUDUsp,abserr,neval,ier)  !fxDUDU PGG
			!
			!fxUUUU=real(fxUUUUsp, kind=8)
			!fxDDDD=real(fxDDDDsp, kind=8)
			!fxUDUD=real(fxUDUDsp, kind=8)
			!fxDUDU=real(fxDUDUsp, kind=8)

			call DPPGGuu(q,Globkf0,fxUUUU,abserr,neval,ier)           !fxUUUU PGG
			call DPPGGdd(q,Globkf0,fxDDDD,abserr,neval,ier)           !fxDDDD PGG
			call DPPGGud(q,GlobkfU,GlobkfD,fxUDUD,abserr,neval,ier)  !fxUDUD PGG
			call DPPGGdu(q,GlobkfU,GlobkfD,fxDUDU,abserr,neval,ier)  !fxDUDU PGG


			fHxcUUUU=Vc(q)+fxUUUU
			fHxcDDDD=Vc(q)+fxDDDD
			fHxcDDUU=0D0!Vc(q)+GlobKernVec(3)
			fHxcUUDD=0D0!Vc(q)+GlobKernVec(3)
			fxcUDUD=fxUDUD!GlobKernVec(4)
			fxcDUDU=fxDUDU!GlobKernVec(4)
			M=(1D0-Chi0UUUU*fHxcUUUU)*(1D0-Chi0DDDD*fHxcDDDD)&
			  -Chi0UUUU*Chi0DDDD*fHxcUUDD*fHxcDDUU
			Chi=0D0

			Chi(1,1)=1D0-Chi0UUUU*fHxcUUUU
			Chi(4,1)=-Chi0DDDD*fHxcDDUU
			Chi(1,4)=-Chi0UUUU*fHxcUUDD
			Chi(4,4)=1D0-Chi0DDDD*fHxcDDDD
			Chi(2,2)=1D0-Chi0UDUD*fxcUDUD
			Chi(3,3)=1D0-Chi0DUDU*fxcDUDU
			!Chi(1,1)=(1D0-Chi0DDDD*fHxcDDDD)*Chi0UUUU/M
			!Chi(4,1)=Chi0UUUU*fHxcUUDD*Chi0DDDD/M
			!Chi(1,4)=Chi0DDDD*fHxcDDUU*Chi0UUUU/M
			!Chi(4,4)=(1D0-Chi0UUUU*fHxcUUUU)*Chi0DDDD/M
			!Chi(2,2)=0D0!(1D0-Chi0UDUD*fxcUDUD)/Chi0UDUD
			!Chi(3,3)=0D0!(1D0-Chi0DUDU*fxcDUDU)/Chi0DUDU
			!write(*,*) 1D0/Chi(2,2)
		
		contains

			real (kind=8) function Vc(q)
				use Constants
				implicit none
				real (kind=8) ::q
				! Fourier Transform of screened Coulomb Potential
				! q   :: real wavevector magnitude
			
				Vc=2D0*pi/dabs(q)!*Globn !Norm(q)!*e**2/kappa
			end function Vc


	end subroutine RespCnstrPGG






end module Response


























