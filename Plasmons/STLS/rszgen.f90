program rszgen
  implicit none
  integer(4) ::ios,i,j,rssteps,zsteps,k
  real(8) ::r,dr,ra,rb,z,dz,za,zb


  open(unit=12, file='rszgen.dat', iostat=ios, action="write")

  rssteps=100
  zsteps=50

  ra=0D0
  rb=20D0
  za=0D0
  zb=-1D0
  dr=(rb-ra)/real(rssteps, kind=8)
  dz=(zb-za)/real(zsteps, kind=8)

  r=ra
  ! r=ra-dr
  z=za
  k=1
  ! do i = 1,rssteps
  do i = 1,rssteps+1
    r=r+dr
    z=za
    do j = 1, zsteps-1
      z=z+dz
      write(12,*) r,z,k
      k=k+1
    end do
  end do

end program rszgen
