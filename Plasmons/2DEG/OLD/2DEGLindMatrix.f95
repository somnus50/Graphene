program SquareHLindMatrix
	use constants	
	use NSquareHLindFunctions
	implicit none
	real    (kind=8):: qv(2),G(2),q,phi,w,dq,dphi,dw,percent,dperc
	real (kind=8) , allocatable:: LindLine(:)
	integer (kind=4):: steps,gstep,qstep,phistep,wstep,phisteps,qsteps,wsteps


	qsteps=150
	wsteps=150
	phisteps=1
	write(*,*) "Caution, this will take ~", int(1D-3*qsteps*wsteps*phisteps/4D0), "minutes"
	read(*,*) steps


	open(unit=9,file='2DEGLindMatrix.dat')
	write(9,*)  "###             alat,                        t,        ",&
		    "          Ef/(4t),                   eta/Ef"
	write(9,*) alat, t, Ef/(4D0*t), eta/Ef
	write(9,*) "###  qsteps,   phisteps,     wsteps"
	write(9,*)  qsteps, phisteps, wsteps
	write(9,*) ""
	


	allocate(LindLine(wsteps))
	dperc=1D0/(qsteps*wsteps*phisteps)
	dq=5D0*kf/(real(qsteps))
	dphi=2D0*pi/real(phisteps)
	dw=5D0*Ef/real(wsteps)
	w=0D0
	percent=0D0
	q=0D0
	do qstep=1,qsteps
		write(9,*) "#                  q,", "                       dw,", "                     dphi"
		write(9,*) q, dw, dphi 
		write(9,*) "# NLind"
		phi=0D0
		qv=q*(/1D0,0D0/)
	!	do phistep=1,phisteps
			w=0D0
			do wstep=1,wsteps
				LindLine(wstep)=ReNLind(qv,w)
				w=w+dw
				percent=percent+dperc
				write(*,*) 1D2*percent, "%", qstep, phistep, wstep
			enddo
			write(9,*) LindLine
	!		phi=phi+dphi
	!		qv=q*(/dcos(phi),dsin(phi)/)
	!	enddo
		q=q+dq
	enddo
	close(9,status='keep')






end program SquareHLindMatrix
