program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene3
use AuxiliaryFunctions
use fxc2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,w,dq,a,b1,bb,k1,k2,ww,w2,ss,tt,bareZ0,bareZ1,hypa
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,valid
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	Globgs=2D0
	Globgv=2D0
	ss=-1D0
	tt=1D0
	call ConstInit(1D12/cmSq2a0sq,-0.5D0)
	write(*,*) "Z*:",GlobZeem,GlobEf,GLobZeem/GlobEf
	! Globfxc=0.2614251197550341D0
	GlobEta=min(1D-4*GLobZeem,GlobEta)
	GlobEtaSq=GlobEta**2
	! Globn=1D0*Globn0
	! Globkf=dsqrt(pi*Globn)
	! globkf0=globkf
	! Globntot=Globn+Globn0
	! Globfxc=200D0*(cfx(Globrs)+cfc(Globrs))

	!Globeta=1D-3*GlobEf
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	open(unit=9, file='Expansion.wmagnon.LDA.z10.dat')
	!write(9,*) "# q/kf,wp/ef,low q limit"


	write(*,*) "kf",Globkf
	write(*,*) "Ef",GlobEf
	write(*,*) "n1/n0",Globn/Globn0


	qsteps=100

	q=0D0!3D0*Globkf
	! write(*,*) 1D-2*GLobZeem/gam,GlobZeem*0.9D0/gam
	! w2 = SpinQIntercept(GlobZeem/gam*(1D0),0D0,ss,tt)
	! write(*,*) "q0:",w2*gam,(w2*gam)**2
	! write(*,*) "############################################"
	! call exit(0)
	dq=GlobZeem/gam/real(qsteps, kind=8)!*Globkf
	! dq=w2/real(qsteps, kind=8)!*Globkf
	!dw=5D0/real(wsteps, kind=8)!*GlobEf
	! q=dq!0.7D0*Globkf

	! bareZ0=-Globfxc*Globn0*Globz*0.5D0
	! bareZ1=wmagnonLDA(0D0,1D-10*GlobKf0*gam,GlobZeem-(0D0+1D-10*GlobKf0)*gam,ss,tt)-GlobZeem
	! bareZ1=wmagnonLDA(0D0,0D0,GlobZeem-(0D0+1D-10*GlobKf0)*gam,ss,tt)-GlobZeem
	! hypa=(-bareZ1)/(-bareZ0)
	! hypa=0.99989052930643973D0
	! write(*,*) "hypa:",hypa
	! call exit(-1)

	write(9,*) "# kf,Ef,n1,n0,z,zp"
	write(9,*) "# ",Globkf,GlobEf,Globn,Globn0,Globz,Globzp,GLobZeem,gam
	write(9,*) "# q/kf, wp/Ef (LDA), wp/Ef (PGG), Spin-flip Ctm, PGGud, LDA, LDAx, LDAc"
	! write(9,*) 0D0,0D0,0D0,0D0
	do i=1,qsteps
		write(*,*) "qstep:",i
		write(*,*) "q:",q!/Globkf
		! if ( Globstat.eq.0 ) then
			! ww=wmagnonLDA(q,0D0,GlobZeem-(q+1D-10*Globkf)*gam,ss,tt)
		! else
		! 	call exit(-1)
		! 	ww=GlobZeem-gam*q
		! end if
		if (Globstat.lt.0) EXIT
		ww=wmagnonPGG(q,1D-10*GlobKf0*gam,GlobZeem-(q+1D-10*GlobKf0)*gam,ss,tt)!+1D-10*Globkf!+1D-2*GlobEf
		! w2=wmagnonPGG(q,0D0,GlobZeem-(q+1D-10*Globkf)*gam,ss,tt)!+1D-2*GlobEf
		if (Globstat.lt.0) EXIT
		! w2=wplasmonPGG(q,q*gam+1D-3*GlobEf,2.5D0*GlobEf)!+1D-2*GlobEf
		! write(9,*) q/Globkf, ww/GlobZeem, 1D0-gam*q/GlobZeem!,dsqrt(4D0*GlobEf/2D0*q)/GlobEf!, ReChi0(q,ww), ReChi0Ex(q,ww)-Globkc/(2D0*pi*gam)!, Vq(q),PGG(q)
		! write(9,*) q, ww/GlobZeem, w2/GlobZeem, 1D0-gam*q/GlobZeem, PGGcst(q)&
		! 					, Globfxc,cfxssp(n2rs(Globntot),Globz),cfcssp(n2rs(Globntot),globz)
			! write(9,*) q/(GLobZeem/gam), 1D0-gam*q/GlobZeem, epsLDAst(q,GLobZeem-gam*q,ss,tt), ww/GlobZeem
			write(9,*) q, ww,whyp(q,Globfxc,0D0), wsw(q,Globfxc,0D0)&
			,ww-GlobZeem,GLobZeem-gam*q,w0(PGGcst(0D0)),stiff(PGGcst(0D0),0D0),Globfxc!, epsLDAst(q,GLobZeem-gam*q,ss,tt),
		! write(9,*) q/Globkf, Globfxc, PGGcst(q), PGG(q)
		! write(9,*) q/Globkc, PGG(q),StructExt(q)!,-Gp0(q)*2D0*pi/q!, ImChi0(q,10000000D0*GlobEf)
		write(*,*) "wp:", ww/GlobZeem,w2/GlobZeem,1D0-gam*q/GlobZeem!,dsqrt(4D0*GlobEf/2D0*q)/GlobEf
		q=q+dq
	enddo

	! dq=3D0*Globkc/real(qsteps, kind=8)
	! q=q+dq
	! do i=1,qsteps
	! 	write(*,*) "qstep:",i
	! 	write(*,*) "q:",q/Globkf
	! 	!ww=wplasmonRPA(q,q*gam+1D-10*GlobEf,3D0*GlobEf)
	! 	!write(9,*) q/Globkf, ww/GlobEf,dsqrt(4D0*GlobEf/2D0*q)/GlobEf, ReChi0(q,ww), ReChi0Ex(q,ww)-Globkc/(2D0*pi*gam)!, Vq(q),PGG(q)
	! 	write(9,*) q/Globkc, PGG(q), -Gp0(q)*2D0*pi/q,StructExt(q)!, ImChi0(q,10000000D0*GlobEf)
	! 	q=q+dq
	! enddo

	close(unit=9)

	write(*,*) "hypa:",GLobZeem

	write(*,*) "fx,fc",cfxssp(n2rs(Globn),Globzp),cfcssp(n2rs(Globn),Globzp)

end program GraphenePlasmons
