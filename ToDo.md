# To Do list

### Main Project
- [ ] Reproduce [Spin-Polarized 2DEG](https://gitlab.com/somnus50/Graphene/blob/master/Papers/Carsten%20Notes/magnetic_2Dbulk.pdf)
- [ ] Spin-Polarize Square H Lattice
- [ ] Spin-Polarize Graphene
  - [ ] Profit

### Extras
- [ ] Read Chapter 1 of QTEL
- [ ] Study Larmor's Theorem
- [ ] Thoroughly comment code
- [ ] Read Kittel section on Magnons
- [ ] Study Response Theory
- [ ] Study Group Theory SU(2)
