#!/bin/bash


filesfile='tmp.files'
baseinfile='bandstructure.in'
infile='tmp.in'
psp='6c-gga-uspp.paw'
touch $filesfile
touch $infile
i=1

# have to use an array because `while read` reads from stdin which was being messed with
declare -a myarr
readarray -t myarr < fields.txt 

let i=0
while (( ${#myarr[@]} > i )); do
#    printf "${myarr[i++]}\n"
	line="${myarr[i++]}"
	echo $i $line

outdir="out/$i"
mkdir -p $outdir
echo "$infile" > $filesfile
echo "$outdir/bands.out" >> $filesfile
echo "$outdir/bands_xi" >> $filesfile
echo "$outdir/bands_xo" >> $filesfile
echo "$outdir/bands_x" >> $filesfile
echo "$psp" >> $filesfile

cat $baseinfile > $infile
#echo "$line" >> $infile

#echo "$i \"$line\""

#cmd="mpirun -np 6 abinit -i $filesfile" 
cmd="abinit -i $filesfile"

echo $cmd

{
 $cmd 
} &> log
cp log $outdir/$i.log
#echo "" > log

#echo $?

#((i+=1))
done

rm $infile $filesfile
echo $i
echo "Out of loop"
