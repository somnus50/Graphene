program GraphenePlasmons
use Constants
use Globals
use PhysicsGraphene3
use AuxiliaryFunctions
use fxc2DEG
use readlib
!use SomeModule !As many as you need
implicit none
real (kind=8)::ss,tt,pgg0,pu,pd,pgg02,pgg0t,pggdt,pggd2t
integer (kind=4)::NProc,i,j,zsteps,nsteps,IntSteps,valid,l,p,pui,pdi
character (len=128)::fmt1,fmt2,cl,cp,fname
real(8), allocatable::zs(:),ns(:)


	ss=-1D0
	tt=1D0

	! zsteps=12
	! nsteps=9

	zsteps=100
	nsteps=1


	allocate(ns(nsteps),zs(zsteps))

	! zs=-(/1D-8,1D-4,1D-2,5D-2,1D-1,2D-1,3D-1,4D-1,5D-1,8D-1,95D-2,0.999999D0/)
	! zs=-(/1D-8,5D-8,1D-7,1D-4,5D-4,1D-5,1D-2,5D-2,1D-1,1.5D-1,2D-1,2.5D-1,3D-1,&
	! 3.5D-1,4D-1,4.5D-1,5D-1,5.5D-1,6D-1,6.5D-1,7D-1,7.5D-1,8D-1,8.5D-1,9D-1,95D-2,0.999999D0/)
	do l = 1, zsteps
		zs(l)=-real(l, kind=8)/real(zsteps, kind=8)
	end do

	ns=(/1D10,1D11,1D12,2D13,3.5D13,5D13,7D13,10D13,1D0/(dsqrt(3D0)*aGraphene**2)*2D0*cmSq2a0sq/)!,1D10,1D7

	open(unit=9, file='Stiffs.dat')

	call ReadBessJzs('j0zs.dat','j1zs.dat')

	! Globgs=2D0
	! Globgv=2D0
	! call ConstInit(ns(5)/cmSq2a0sq,zs(5))



	! call exit(0)




	write(9,*) "### Graphene Stiffness"
	write(9,*) "# n,zp,Z*,ZLDA,SLDA,ZPGG,SPGG0,fLDA,fPGG0"


	do l=1,nsteps
		do p=1,zsteps
			Globgs=2D0
			Globgv=2D0
			! call ConstInit(ns(l)/cmSq2a0sq,zs(p))
			call ConstInit(ns(4)/cmSq2a0sq,zs(p))
			call SortPGGSpinZs()
			call SortPGGZs (GlobkfD,GlobkfU,GlobJ1zs,GlobJ1zs,GlobJzsN,GlobPGGSpinZs)
			! Globkc=0D0
			! Globntot=Globn
			write(*,*) l,nsteps,p,zsteps
			pgg0=PGGcst(0D0)

			call PGGtylr(tiny(1D0),pgg0t,pggdt,pggd2t)
			! write(9,*) ns(l), zs(p), GLobZeem, w0(Globfxc), stiff(Globfxc,0D0), w0(pgg0), stiff(pgg0,0D0), Globfxc, pgg0, PGGUD20()
			write(9,*) ns(l), zs(p), pgg0, pgg0t,pggdt,pggd2t!, pgg02, GlobZeem, w0(pgg0), w0(pgg02), w0(Globfxc)

		end do
	end do
close(unit=9)


! write(*,*)

end program GraphenePlasmons
