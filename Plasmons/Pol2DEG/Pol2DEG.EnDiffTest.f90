program test
use Constants
use PhysicsPol2DEG
use Globals
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),k(2),dq(2),dk(2),phi,b2(2)
integer (kind=4)::i,j,steps

	call ConstInit(1.25D0,0D0,GlobEf,Globeta)

	
	q=(/5D-1*GlobkfU,0D0/)
	k=(/2.5D-1,0D0/)
	steps=100
	dk=(/0D0,2D0*pi/)/real(steps, kind=8)

	do i=1,steps
		b2=PolAdd(k,(/-q(1),q(2)/))
		write(*,*) k,En(0D0,k)-En(0D0,b2),DeltaEn2(k,q)
		k=k+dk



	enddo















end program test
