program PolGrapheneChi0UDSmall
use Constants
use Globals
use PhysicsPolGraphene
use fxc2DEG
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z,wa,wb&
		,qb,dqmin,dwmin,qmax
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	n=1D12/cmSq2a0sq
	z=-4D-2
	Globgs=2D0
	Globgv=2D0
	call ConstInit(n,z)
	temp=Globgv/2D0/4D0/pi**2
	MPImaster=0
	IntSteps=3000  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=30
	wsteps=100
	Nproc=1
	
	fmt1='(3ES24.15)'

	wa=0.945D0*GlobZeem
	wb=1.01D0*GlobZeem
	dw=(wb-wa)/real(wsteps, kind=8)
	dq=2D0*(GlobZeem-wa)/gam/real(qsteps, kind=8)
	q=0D0!(/dq,0D0/)

	dwmin=5D-8
	qmax=0.0002D0
	!dqmin=qmax/30D0
	dqmin=1D0

	write(*,*) dw,qb,qmax

	if (dwmin.lt.dw) then
		wsteps=int((wb-wa)/dwmin, kind=4)
		dw=dwmin
		write(*,*) "Warning: dw is too big. Adjusting wsteps to ", wsteps
	endif

	if (qmax.lt.(dq*real(qsteps, kind=8))) then
		qb=qmax
		dq=qb/real(qsteps, kind=8)
		write(*,*) "Warning: Changing q window to 0D0 - ", qmax
	endif

	if (dqmin.lt.dq) then
		qsteps=int((qb)/dqmin, kind=4)
		dq=qb/real(qsteps, kind=8)
		write(*,*) "Warning: dq is too big. Adjusting qsteps to ", qsteps
	endif

	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	allocate(wline(wsteps))

	open(unit=9, file='PolGraphene.Chi0UD.z4.xsmall.q1.dat')

		write(9,*) "### Chi0DUIntra(q,w)"
		write(9,*) "### ", n, z, GlobEf 
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	write(*,*) "Ef   kf0"
	write(*,*) GlobEf,Globkf0
	do i=1,qsteps
		w=wa-gam*q(1)
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			wline(j)=PolIntegratek(ReChi0UDIntraU,0D0,2D0*Globkf0,IntSteps,q,w)*temp
			w=w+dw
		enddo
		write(9,fmt2) wline
		q(1)=q(1)+dq
	enddo


	close(unit=9)
end program PolGrapheneChi0UDSmall
