program Pol2DEGChi0DD
use Constants
use Globals
use PhysicsPol2DEG
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),temp2,k,phi,temp3,phis(2),outs(2)
real (kind=8), allocatable::wline(:),qgrid(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	call ConstInit(2D0,-0D-1,GlobEf,Globeta)
	write(*,*) Globkf0,GLobkfU,GlobkfD

	qsteps=1000

	w=0D0*GlobEf

	allocate(qgrid(qsteps))

	call LogGrid(LGdrSuggest(10D0/50D0,qsteps),qsteps,qgrid)
	qgrid=qgrid*Globkf0*50D0
	!temp=qgrid(1)

	!dq=0.08D0*Globkf0/real(qsteps, kind=8)
	!qgrid(1)=dq
	!do i=2,qsteps
	!qgrid(i)=qgrid(i-1)+dq
	!enddo
	!qgrid(1)=0D0

	!qgrid(1)=1.9D0*(GlobkfD-GLobkfU)
	do i=1,qsteps
		write(*,*) i,qsteps !/Globkf0
		!call intcheck(qgrid(i),0D0,1D0,outs)
		write(56,*) qgrid(i)/Globkf0,real(Chi0ssp(1,1,qgrid(i),w),kind=8),ReChi0sspNum((/qgrid(i),0D0/),w,1D0,1D0,50)!,ImChi0num2(q,w,50,-1D0,1D0)!
		!q(1)=q(1)+dq
	enddo

	close(unit=56)

end program Pol2DEGChi0DD
