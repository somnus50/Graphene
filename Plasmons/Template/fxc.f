      PROGRAM STIFF
      IMPLICIT NONE

      INTEGER I
      DOUBLE PRECISION FXC,ZETA,RS
      EXTERNAL S 

      ZETA = 0.04D0

      DO 10 I=1,100
         RS = 0.1D0*I
         WRITE(92,*)real(RS),real(FXC(RS,ZETA))
10    CONTINUE

      END
C********************************************************************
      DOUBLE PRECISION FUNCTION FXC(RS,ZETA)
      IMPLICIT NONE

      DOUBLE PRECISION PI,RS,DEN,ZETA,DEX,DEXC
      DOUBLE PRECISION A1,B1,C1,D1,E1,G1,H1,A2,B2,C2,D2,E2,H2,
     &                 ALPHA1,ALPHA2,BETA

      PI = 3.141592653589793D0
      DEX = -DSQRT(2.D0)*( DSQRT(1.D0 + ZETA) - 
     &      DSQRT(1.D0 - ZETA) ) /(PI*RS)

      A1 = 0.117331D0
      A2 = 0.0234188D0
      B1 = -3.394D-2
      B2 = -0.037093D0
      C1 = -7.66765D-3
      C2 = 0.0163618D0
      E1 = 0.4133D0
      E2 = 1.424301D0
      G1 = 6.68467D-2
      H1 = 7.799D-4
      H2 = 1.163099D0
      D1 = -A1*H1
      D2 = -A2*H2

      ALPHA1 = A1 + (B1*RS + C1*RS**2 + D1*RS**3)*
     &         DLOG(1.D0 + 1.D0/(E1*RS + G1*RS**2 + H1*RS**3))
      ALPHA2 = A2 + (B2*RS + C2*RS**2 + D2*RS**3)*
     &         DLOG(1.D0 + 1.D0/(E2*RS + H2*RS**3))

      BETA = 1.3386D0

      DEXC = DEXP(-BETA*RS)*DEX + (DEXP(-BETA*RS)-1.D0)*
     &       (ZETA + ZETA**3/8.D0)*DSQRT(2.D0)/(PI*RS)
     &      + 2.D0*ALPHA1*ZETA + 4.D0*ALPHA2*ZETA**3

      DEN = 1.D0/(PI*RS**2)

      FXC = 2.D0*DEXC/(ZETA*DEN)
      

      RETURN
      END
