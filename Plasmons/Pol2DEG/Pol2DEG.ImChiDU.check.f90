program Pol2DEGChi0DULog
use Constants
use Globals
use PhysicsPol2DEG
!use fxc2DEG
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z,wa,wb
real (kind=8), allocatable::wline(:),qgrid(:),wgrid(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
  call ConstInit(30D0,0.255D0,GlobEf,Globeta)
	write(*,*) Globkf0,GLobkfU,GlobkfD

	MPImaster=0
	IntSteps=300  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=1
	wsteps=500
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps),qgrid(qsteps),wgrid(wsteps))
	!wa=0.9D0*GlobZeem
	!wb=1.01D0*GlobZeem
	!dw=(wb-wa)/real(wsteps, kind=8)
	!dq=2D0*(GlobZeem-wa)/gam/real(qsteps, kind=8)
	!call LogGrid(LGdrSuggest(7D0/50D0,qsteps),qsteps,qgrid)
	!qgrid=qgrid*Globkf0*50D0
	call LogGrid(LGdrSuggest(7D0/50D0,wsteps),wsteps,wgrid)
	wgrid=wgrid*50D0*GlobEf
	q=0D0!(/dq,0D0/)
	q(1)=6D0*Globkf0!0.99D0*(GlobkfD-GLobkfU)
	wgrid(1)=0D0

	!dw=wgrid(wsteps)/real(wsteps, kind=8)
	!do i=2,wsteps
	!	wgrid(i)=wgrid(i-1)+dw
	!enddo

	!qgrid(1)=1.9D0*(GlobkfD-GLobkfU)
	do i=1,wsteps
		write(*,*) i,wsteps !/Globkf0
		!call intcheck(qgrid(i),0D0,1D0,outs)
		write(56,*) wgrid(i)/GlobEf&!,dimag(ChiUD(q,wgrid(i),fxcDU2)),dimag(ChiUDA(q,wgrid(i),fxcDU2))&
								!,dimag(ChiUD(q,wgrid(i),PGGDU)),dimag(ChiUDA(q,wgrid(i),PGGDU))&
								,ImChi0sspNum(q,wgrid(i),1D0,-1D0,1),dimag(Chi0ssp(1,-1,q(1),wgrid(i)))
		!q(1)=q(1)+dq
	enddo

	close(unit=56)

	write(*,*) fxcDU2(q,0D0),PGGUD(q,0D0)

end program Pol2DEGChi0DULog
