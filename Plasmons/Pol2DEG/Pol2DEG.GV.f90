program GV2
use Constants
use Globals
use PhysicsPol2DEG
use GV
implicit none
real (kind=8) ::q,w,dq,dw,s
integer(kind=4)::wsteps,i


call ConstInit(1.25D0,0D0,GlobEf,Globeta)
wsteps=300
s=1D0
q=6D-2*GlobkfMax
w=0D0
dw=3D0*GlobEf/real(wsteps, kind=8)
dq=3D0*GlobkfU/real(wsteps, kind=8)
q=0D0
do i=1,wsteps
	write(*,*) q/GlobkfU,ReChi0s(q,w,s)
	q=q+dq
enddo












end program GV2
