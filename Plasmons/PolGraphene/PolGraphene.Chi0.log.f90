program PolGrapheneChi0log
use Constants
use Globals
use PhysicsPolGraphene
use fxc2DEG
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z,wa,wb,k,dk,phi,temp2,temp3!,dphi
real (kind=8), allocatable::wline(:),qgrid(:),wgrid(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	n=1D12/cmSq2a0sq
	z=0D0!-1D-1
	Globgs=2D0
	Globgv=2D0
	call ConstInit(n,z)
	temp=Globgs*Globgv/2D0/4D0/pi**2/4D0
	MPImaster=0
	IntSteps=50  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=300
	wsteps=300
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps),qgrid(qsteps),wgrid(wsteps))
	!wa=0.9D0*GlobZeem
	!wb=1.01D0*GlobZeem
	!dw=(wb-wa)/real(wsteps, kind=8)
	!dq=2D0*(GlobZeem-wa)/gam/real(qsteps, kind=8)
	call LogGrid(LGdrSuggest(1D-1,qsteps),qsteps,qgrid)
	qgrid=qgrid*Globkf0*100D0
	call LogGrid(LGdrSuggest(1D-1,wsteps),wsteps,wgrid)
	wgrid=wgrid*GlobEf*100D0
	q=0D0!(/dq,0D0/)
	open(unit=9, file='PolGraphene.ImChi0.log.q1.adapt.dat')
	open(unit=10, file='PolGraphene.ImChi0.log.q1.qgrid.adapt.dat')
	open(unit=11, file='PolGraphene.ImChi0.log.q1.wgrid.adapt.dat')

	do i=1,qsteps
		write(10,*) qgrid(i)/Globkf0
	enddo

	do i=1,wsteps
		write(11,*) wgrid(i)/GlobEf
	enddo

	close(unit=10)
	close(unit=11)

		write(9,*) "### ImChi0Intra(q,w) LogGrid"
		write(9,*) "###", n, z, Globrs
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	write(*,*) "Ef   kf0"
	write(*,*) GlobEf,Globkf0,GlobkfU,GlobkfD
	q=0D0
	q(1)=qgrid(1)

	dphi=2D0*pi/real(qsteps, kind=8)
	phi=-pi
	temp=0D0
	k=0.12D0*Globkf0
	temp2=1.2D0*Globkf0
	write(*,*) k, temp2
	w=0D0!1D0*GlobEf
	do i=1,qsteps
		write(55,*) phi,ImChi0UUIntraU((/temp2,0D0/),w,(/k,phi/))
		temp=temp+ImChi0UUIntraU((/temp2,0D0/),w,(/k,phi/))
		phi=phi+dphi
	enddo

	temp3=dacos((Globkf0**2-k**2-temp2**2)/(2D0*k*temp2))
	write(*,*) temp*dphi,-temp3,temp3

	close(unit=55)

	call exit(0)

	do i=1,qsteps
		w=wgrid(1)
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			!wline(j)=PolIntegratek(ReChi0DUIntraU,max(q(1)-GlobkfMAX,0D0),q(1)+Globkf0,IntSteps,q,w)*temp
			wline(j)=PolIntegratek4(ImChi0UUIntraU2,0D0,Globkf0,IntSteps,q,w)*temp*2D0
			w=wgrid(j)
		enddo
		write(9,fmt2) wline
		q(1)=qgrid(i)
	enddo


	close(unit=9)
	close(unit=100)
end program PolGrapheneChi0log
