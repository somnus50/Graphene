program Template
use Constants
use Globals
!use Elliptic !As many as you need
!use fxcPGG
use QuadPackDouble
use fxcPGG
implicit none
real (kind=8)::q,kf,PGGresult,abserr,kfu,kfd,z
integer (kind=4)::i,ier,neval,steps
!complex (kind=8)::

steps=50
kf=0.003_8
z=-0.04_8
kfu=kf*sqrt(1.0_8+z)
kfd=kf*sqrt(1.0_8-z)
write(*,*) "q, z, kf, PGG, abserr, neval, ier"
q=0._8
do i=1,steps
	call DPPGGuu(q,kf,PGGresult,abserr,neval,ier)
	write(*,*) q,z,kf,PGGresult,abserr,neval,ier
	call DPPGGud(q,kfu,kfd,PGGresult,abserr,neval,ier)
	write(*,*) q,z,kf,PGGresult,abserr,neval,ier
	q=q+0.0003_8
enddo





end program Template


