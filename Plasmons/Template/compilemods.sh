#!/bin/bash
set -e

usage() {
cat << EOF
Usage: compilemods.sh [OPTION]

   -g    Use gfortran compiler. Default is ifort.
   -h    Display this help.
EOF
exit 1
}

libdir=$(readlink -f $(git rev-parse --show-cdup))"/Plasmons/libs"
#libdir="../libs"
lpckdir="$HOME/bin/lib/lapack"
lpckopt=" -I$lpckdir -L$lpckdir -llapack -lblas"
#gcmd="gfortran -g -cpp -fbacktrace -fcheck=all -O3 -c -ffree-form -Wno-tabs -Dnonans -J$libdir $lpckopt"
gcmd="gfortran -g -cpp -fbacktrace -fcheck=all -c -ffree-form -Wno-tabs -Dnonans -J$libdir $lpckopt"
icmd="ifort -O3 -c -module $libdir/ $lpckopt"
cmd=$icmd

while getopts :g opts
do	case "$opts" in
	g)	cmd=$gcmd;;
	*)  usage;;
	esac
done
shift "$((OPTIND-1))"

echo "Compiling..."
for file in modules/mod.*.f90
do
   echo $cmd $file
   $cmd $file
done

if [ $? -eq 0 ]
then
	echo "ok..."
	echo ""
	mv mod.*.o $libdir/

	echo "Adding objects to library..."
	echo "$libdir/libmodules.a"
	echo ""
	ar rcvf $libdir/libmodules.a $libdir/mod.*.o
	rm $libdir/mod.*.o
else
	echo "Compilation errors. Exiting."
	rm ./*.o
fi
