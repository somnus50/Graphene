module STLS
use constants
use Globals
implicit none
real (kind=8),allocatable::GSC(:),ChiSC(:,:),SSC(:),qgrid(:),G1(:)
real (kind=8),allocatable::GSCold(:),ChiSCold(:,:),SSCold(:),Ggrid(:,:)
real (kind=8),allocatable::GSCspline(:),SSCspline(:),Sbargrid(:),Sbarspline(:)
integer (kind=4)::SCCount,qsteps,Ggridi



contains


	subroutine STLSSCInit(SCsteps,qstps,qs)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::qs(2),dq
		integer (kind=4)::SCsteps,qstps,i!,qsteps

		qsteps=qstps

		if (.not.allocated(qgrid)) then
			allocate(GSC(qsteps),SSC(qsteps),qgrid(qsteps))
			allocate(GSCold(qsteps),SSCold(qsteps),G1(qsteps))
			allocate(GSCspline(qsteps),SSCspline(qsteps))
			allocate(Sbargrid(qsteps),Sbarspline(qsteps))
			allocate(Ggrid(SCsteps+5,qsteps))
		endif

		dq=(qs(2)-qs(1))/real(qsteps+1, kind=8)
		if (dq.lt.0D0) then
			qs=(/qs(2),qs(1)/)
			dq=-dq
		endif

		do i=1,qsteps
			qgrid(i)=qs(1)+dq*real(i, kind=8)
		enddo

		! call LogGrid(LGdrSuggest(0.1D0,qsteps),qsteps,qgrid)
		! qgrid=qgrid*qs(2)

		Ggridi=0
		Ggrid=0D0


		GSC=1D0
		GSCold=GSC
		G1=GSC
		call spline(qgrid,G1,qsteps,1D31,1D31,GSCspline)
		SSC=1D0
		SSCold=SSC
		call spline(qgrid,SSC,qsteps,1D31,1D31,SSCspline)

		call StructbarsplineUpdate()

	end subroutine STLSSCInit


			subroutine STLSSCInit2(SCsteps,qstps,qs,UpdateSub)
				use AuxiliaryFunctions
				implicit none
				real (kind=8)::qs(2),dq
				integer (kind=4)::SCsteps,qstps,i!,qsteps
				external :: UpdateSub

				qsteps=qstps

				if (.not.allocated(qgrid)) then
					allocate(GSC(qsteps),SSC(qsteps),qgrid(qsteps))
					allocate(GSCold(qsteps),SSCold(qsteps),G1(qsteps))
					allocate(GSCspline(qsteps),SSCspline(qsteps))
					allocate(Sbargrid(qsteps),Sbarspline(qsteps))
					allocate(Ggrid(SCsteps+5,qsteps))
				endif

				dq=(qs(2)-qs(1))/real(qsteps-1, kind=8)
				if (dq.lt.0D0) then
					qs=(/qs(2),qs(1)/)
					dq=-dq
				endif


				qgrid(1)=qs(1)
				do i=2,qsteps-1
					qgrid(i)=qs(1)+dq*real(i-1, kind=8)
				enddo
				qgrid(qsteps)=qs(2)

				! call LogGrid(LGdrSuggest(0.1D0,qsteps),qsteps,qgrid)
				! qgrid=qgrid*qs(2)

				Ggridi=0
				Ggrid=0D0


				GSC=0D0
				GSCold=GSC
				G1=GSC
				call spline(qgrid,G1,qsteps,1D31,1D31,GSCspline)
				SSC=1D0
				SSCold=SSC
				call spline(qgrid,SSC,qsteps,1D31,1D31,SSCspline)

				call UpdateSub()

			end subroutine STLSSCInit2



	real (kind=8) function Vc(q)
		implicit none
		real (kind=8) ::q
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector, Cartesian

		Vc=2D0*pi/q!*e**2/kappa
	end function Vc


	real (kind=8) function ReChi0(q,w)
		implicit none
		real (kind=8)::q,w,NUP,NUM,sim,sip
		real (kind=8),parameter:: minpi=-pi**(-1)


      NUP = (w/Q + Q/2.D0)/GlobKF
      NUM = (w/Q - Q/2.D0)/GlobKF

      SIM = 1.D0
      IF (NUM.LT.0.D0) SIM = -1.D0
      SIP = 1.D0
      IF (NUP.LT.0.D0) SIP = -1.D0

      RECHI0 = 0.D0

      IF (NUM**2.GT.1.D0) RECHI0 = SIM*DSQRT(NUM**2-1.D0)
      IF (NUP**2.GT.1.D0) RECHI0 = RECHI0 - SIP*DSQRT(NUP**2-1.D0)

      RECHI0 = -1.D0/PI -RECHI0*GlobKF/(PI*Q)

	end function ReChi0

	real (kind=8) function ImChi0(q,w)
		implicit none
		real (kind=8)::q,w,nup,num,temp


      nup = (w/q + q/2.D0)/Globkf
      num = (w/q - q/2.D0)/Globkf

      ImChi0 = 0.D0

      if (num**2.lt.1.D0) ImChi0 = dsqrt(1.D0-num**2)
      if (nup**2.lt.1.D0) ImChi0 = ImChi0 - dsqrt(1.D0-nup**2)

      ImChi0 = -Globkf*ImChi0/(pi*q)


			if ( isnan(ImChi0) ) then
				write(*,*) "ImChi0 NaN"
				write(*,*) "q,w", q, w
				call exit()
			end if

	end function ImChi0


	real (kind=8) function Struct(q)
		use QuadPackDouble2
		use AuxiliaryFunctions
		implicit none
		integer (kind=4), parameter::limit=1000000
		real (kind=8)   , parameter::minpi=-pi**(-1)
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::q,abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp


		Struct=0D0

		!call dqagie ( anc, 0d0, 1, 1D-10, 1D-10, limit , temp, abserr, &
		!			  neval, ier, alist, blist, rlist, elist, iord, last )


		! call StupidIntegrateNC2(anc,0D0,200D0*GlobEf,100000,temp,ier)

		! call GaussAdaptQuad(anc,max(0D0,q**2/2D0-GlobKf*q),q**2/2D0+GlobKf*q,1D-6,temp)
		call GaussAdaptQuad(anc,0D0,q**2/2D0+GlobKf*q,1D-8,temp)
		ier=0

		if (ier.gt.0) then
			write(*,*) "Integration of ImChi within Struct has failed."
			write(*,*) "dqagie threw an error, ier:",ier
			write(*,*) "See documentation for more information."
			call exit(-ier)
		endif

		Struct=temp*minpi/Globn

		contains

			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				! anc=dimag(Chi(q,w))
				anc=ImChi0(q,w)

			end function anc

	end function Struct

	real (kind=8) function Structbar(p)
		use QuadPackDouble2
		use AuxiliaryFunctions
		implicit none
		integer (kind=4), parameter::limit=1000000
		real (kind=8)   , parameter::minpi=-pi**(-1)
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::p,q,abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,temp1,temp2,temp3


		q=(1D0-p)/p
		Structbar=0D0

		!call dqagie ( anc, 0d0, 1, 1D-10, 1D-10, limit , temp, abserr, &
		!			  neval, ier, alist, blist, rlist, elist, iord, last )

		! call GaussAdaptQuad(anc,0D0,1D0,1D-6,temp)


		temp1=0D0
		if ( q.lt.2D0*Globkf ) then
			call GaussAdaptQuad(anc,1D0/(-q**2/2D0+GlobKf*q+1D0),1D0,1D-8,temp1)
			call GaussAdaptQuad(anc,1D0/(q**2/2D0+GlobKf*q+1D0),1D0/(-q**2/2D0+GlobKf*q+1D0),1D-8,temp2)
		else
			call GaussAdaptQuad(anc,1D0/(q**2/2D0+GlobKf*q+1D0),1D0/(q**2/2D0-GlobKf*q+1D0),1D-8,temp2)
		end if

		temp=temp1+temp2

		! call StupidIntegrateNC2(anc,0D0,1D0,100,temp,ier)
		ier=0

		if (ier.gt.0) then
			write(*,*) "Integration of ImChi within Struct has failed."
			write(*,*) "dqagie threw an error, ier:",ier
			write(*,*) "See documentation for more information."
			call exit(-ier)
		endif

		Structbar=temp*minpi/Globn

		contains

			real (kind=8) function anc(t)
				implicit none
				real (kind=8)::t

				anc=dimag(Chibar(p,t))/t**2
				! anc=ImChi0((1D0-p)/p,(1D0-t)/t)/t**2

			end function anc

	end function Structbar

	subroutine StructbarsplineUpdate()
		! use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::i

		! GSCold=1D0
		! GSCspline=0D0

		! write(99,*) "SCstep", SCCount
		do i=1,qsteps
			write(*,*) "Sbar Spline qstep", i,qsteps, qgrid(i), (1D0-qgrid(i))/qgrid(i)/Globkf
			Sbargrid(i)=Structbar(qgrid(i))
			! write(99,*) qgrid(i), (1D0-qgrid(i))/qgrid(i)/Globkf, Sbargrid(i), Struct((1D0-qgrid(i))/qgrid(i)), GSCold(i)
		enddo

		Ggridi=Ggridi+1

		Ggrid(Ggridi,:)=GSCold



		call spline(qgrid,Sbargrid,qsteps,1D33,1D33,Sbarspline)
	end subroutine StructbarsplineUpdate

	complex (kind=8) function Chi(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)


		call splint(qgrid,GSCold,GSCspline,qsteps,q,Gps)
		Chi0=one*ReChi0(q,w)+ione*ImChi0(q,w)
		Chi=Chi0/(one-Vc(q)*(1D0-Gps)*Chi0)
	end function Chi

	complex (kind=8) function Chibar(p,t)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::t,p,q,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)

		q=(1D0-p)/p
		w=(1D0-t)/t
		call splint(qgrid,GSCold,GSCspline,qsteps,p,Gps)
		! Gps=1D0
		Chi0=one*ReChi0(q,w)+ione*ImChi0(q,w)
		Chibar=Chi0/(one-Chi0*Vc(q)*(1D0-Gps))
	end function Chibar



	real (kind=8) function Gp(q)
		use Globals
		use QuadPackDouble
		use AuxiliaryFunctions
		implicit none
		real (kind=8),parameter::minpisq=-1D0/pi**2
		integer (kind=4), parameter::limit=10000000
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::q,abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp


		!call dqagie ( anc, 1d-6, 1, 1D-6, 1D-6, limit , Gp, abserr, &
 		!			 neval, ier, alist, blist, rlist, elist, iord, last )

		!write(*,*) q

		call StupidIntegrateNC(anc,0D0,5D0*Globkf,600,temp,ier)

		if (ier.gt.0) then
			write(*,*) "Integration of Gp has failed."
			write(*,*) "dqagie threw an error, ier:",ier
			write(*,*) "See documentation for more information."
			!call exit(-ier)
		endif


		Gp=temp*minpisq/Globn!*2D0/(2D0*pi)**2

		contains
			real (kind=8) function anc(qp)
				use Elliptic
				implicit none
				real (kind=8)::qp,temp1,temp2


				temp1=(q*qp)
				temp2=q+qp
				!write(*,*) "q,qp,k,1-k**2"
				!write(*,*) q,qp,2D0*dsqrt(temp1)/temp2,1D0-(2D0*dsqrt(temp1)/temp2)**2
				!call exit()
				anc=temp1/temp2*Kell(2D0*dsqrt(temp1)/temp2)
				anc=anc*(Struct(qp)-1D0)
			end function anc
	end function Gp


		real (kind=8) function Gpbar(p)
			use Globals
			! use SelfConsistent
			use QuadPackDouble
			use AuxiliaryFunctions
			implicit none
			real (kind=8),parameter::minpisq=-1D0/pi**2
			integer (kind=4), parameter::limit=10000000
			integer (kind=4)::neval,ier,last,iord(limit)
			real (kind=8)   ::p,q,abserr,alist(limit),blist(limit),rlist(limit)&
							  ,elist(limit),temp,temp1


			! q=(1D0-p)/p

			!call dqagie ( anc, 1d-6, 1, 1D-6, 1D-6, limit , Gp, abserr, &
	 		!			 neval, ier, alist, blist, rlist, elist, iord, last )

			!write(*,*) q

			call StupidIntegrateNC(anc,qgrid(1),1D0-qgrid(1),10000,temp,ier)
			! call GaussAdaptQuad2(anc,qgrid(1),1D0-qgrid(1),1D-8,temp)
			! ier=0
			! temp1=0
			! call StupidIntegrateNC(anc,xgrid(1),p-xgrid(1),600,temp,ier)
			! temp1=temp1+temp
			! call StupidIntegrateNC(anc,p+xgrid(1),1D0-xgrid(1),600,temp,ier)
			! temp1=temp1+temp
			! temp=temp1
			! if (isnan(temp)) then
			! 	write(*,*) 'Found a NaN'
			! 	write(*,*) p,q
			! 	call exit()
			! endif

			if (ier.gt.0) then
				write(*,*) "Integration of Gp has failed."
				write(*,*) "dqagie threw an error, ier:",ier
				write(*,*) "See documentation for more information."
				!call exit(-ier)
			endif


			Gpbar=temp*minpisq/Globn*(1D0-p)!*2D0/(2D0*pi)**2

			contains
				real (kind=8) function anc(t)
					use Elliptic
					implicit none
					real (kind=8)::t,a,temp1,temp2


					! ! The single K method
					temp1=p+t-2D0*p*t
					call splint(qgrid,Sbargrid,Sbarspline,qsteps,t,temp2)
					anc=(1D0-t)/(t**2*temp1)
					anc=anc*Kell(2D0*dsqrt(p*t*(p-1D0)*(t-1D0))/temp1)
					anc=anc*(temp2-1D0)



					if ( isnan(anc) ) then
						write(*,*) 'Found a NaN'
						write(*,*) p,t,anc
						call exit()
					end if
				end function anc
		end function Gpbar

		real (8) function GpbarSplintq(q)
			use AuxiliaryFunctions
			implicit none
			real(8):: q,p

			p=1D0/(1D0+q)

			call splint(qgrid,GSCold,GSCspline,qsteps,p,GpbarSplintq)



		end function GpbarSplintq


	real (kind=8) function GpForce(q)
		use QuadPackDouble
		use AuxiliaryFunctions
		implicit none
		real (kind=8),parameter::mintwopisq=-1D0/pi**2/2D0
		integer (kind=4), parameter::limit=1000000
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::q,abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit)


		!call dqagie ( anc, 0d0, 1, 1D-6, 1D-6, limit , GpForce, abserr, &
 		!			 neval, ier, alist, blist, rlist, elist, iord, last )
		call StupidIntegrateNC(anc,0D0,5D0*Globkf,600,GpForce,ier)


		if (ier.gt.0) then
			write(*,*) "Integration of GpForce has failed."
			write(*,*) "dqagie threw an error, ier:",ier
			write(*,*) "See documentation for more information."
			!call exit(-ier)
		endif


		GpForce=GpForce*mintwopisq/Globn!*2D0/(2D0*pi)**2

		contains
			real (kind=8) function anc(qp)
				use Elliptic
				implicit none
				real (kind=8)::qp,temp

				temp=2D0*dsqrt(q*qp)/(q+qp)
				anc=(q+qp)*Eell(temp)+(q-qp)*Kell(temp)
				anc=anc*(Struct(qp)-1D0)*qp/q
			end function anc
	end function GpForce

	real (kind=8) function GpForcebar(p)
		use QuadPackDouble
		use AuxiliaryFunctions
		implicit none
		real (kind=8),parameter::mintwopisq=-1D0/pi**2/2D0
		integer (kind=4), parameter::limit=1000000
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::p,abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,dp


		dp = min(qgrid(1),p/100D0)

		!call dqagie ( anc, 0d0, 1, 1D-6, 1D-6, limit , GpForce, abserr, &
 		!			 neval, ier, alist, blist, rlist, elist, iord, last )
		! call StupidIntegrateNC(anc,dp,p-dp,10000,temp1,ier)
		! call StupidIntegrateNC(anc,p+dp,1D0-dp,10000,temp2,ier)
		! call StupidIntegrateNC(anc,dp,1D0-dp,30000,temp1,ier)
		call GaussAdaptQuad2(anc,dp,1D0-dp,1D-8,temp1)
		ier=0
		! temp2=0d0

		if (ier.gt.0) then
			write(*,*) "Integration of GpForcebar has failed."
			write(*,*) "dqagie threw an error, ier:",ier
			write(*,*) "See documentation for more information."
			call exit(-ier)
		endif


		! GpForcebar=GpForcebar*mintwopisq/Globn/(p-1D0)!*2D0/(2D0*pi)**2
		GpForcebar=(temp1)*mintwopisq/Globn/(p-1D0)!*2D0/(2D0*pi)**2

		contains
			real (kind=8) function anc(t)
				use Elliptic
				implicit none
				real (kind=8)::t,temp,temp2,temp3


				call splint(qgrid,Sbargrid,Sbarspline,qsteps,t,temp3)
				temp2=p+t-2D0*p*t
				temp=2D0*dsqrt(p*t*(p-1D0)*(t-1D0))/(temp2)
				! write(*,*) "p,t",p,t,temp

				if ( temp.ge.1D0 ) then
					anc=temp2
				else
					anc=(temp2)*Eell(temp)+(t-p)*Kell(temp)
				end if
				anc=anc*(temp3-1D0)*(t-1D0)/t**4



				if ( isnan(anc) ) then
					write(*,*) 'Found a NaN'
					write(*,*) p,t,anc
					call exit()
				end if
			end function anc
	end function GpForcebar


	subroutine StupidIntegrate(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8), external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8)

		x=a+dx/2D0
		res=0D0
		do i=1,steps
			res=res+f(x)
			x=x+dx
		enddo
		res=res*dx
	end subroutine StupidIntegrate


	subroutine StupidIntegrate2(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8),  external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8)

		x=a+dx/2D0
		res=0D0
		do i=1,steps
			res=res+f(x)
			x=x+dx
		enddo
		res=res*dx
	end subroutine StupidIntegrate2

	real (kind=8) function eps(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)
		! From GV eq. (5.104), pg 221.


		call splint(qgrid,GSCold,GSCspline,qsteps,q,Gps)
		!Chi0=(one*ReChi0(q,w)+ione*ImChi0(q,w))!/dsqrt(3.5D0)
		eps=1D0-Vc(q)*(1D0-Gps)*ReChi0(q,w)
	end function eps

	real (kind=8) function epst(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,p,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)
		! From GV eq. (5.104), pg 221.


		p=1D0/(q+1D0)
		call splint(qgrid,GSCold,GSCspline,qsteps,p,Gps)
		!Chi0=(one*ReChi0(q,w)+ione*ImChi0(q,w))!/dsqrt(3.5D0)
		epst=1D0-Vc(q)*(1D0-Gps)*ReChi0(q,w)
	end function epst

	subroutine wplast(q,ws,eps,wout,fnout)
		use AuxiliaryFunctions
		implicit none
		real(8), intent(in) ::q,ws(2),eps
		real(8), intent(out)::wout,fnout

			call RootFinder(anc,ws(2),ws(1),eps,wout,fnout)

			if (isnan(wout).or.isnan(fnout)) then
				write(*,*) 'Found a NaN'
				write(*,*) q,ws,eps,wout,fnout
				call exit()
			endif
	contains
		real(8) function anc(w)
			implicit none
			real(8):: w

			anc=epst(q,w)

			if (isnan(anc)) then
				write(*,*) 'Found a NaN in anc'
				write(*,*) q,ws,eps,anc
				call exit()
			endif
		end function anc
	end subroutine wplast

	real (kind=8) function eps2(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)
		! From GV eq. (5.104), pg 221.


		!call splint(qgrid,GSCold,GSCspline,qsteps,q,Gps)
		!Chi0=(one*ReChi0(q,w)+ione*ImChi0(q,w))!/dsqrt(3.5D0)
		eps2=1D0-Vc(q)*(1D0-GpA(q))*ReChi0(q,w)
	end function eps2

	real (kind=8) function eps3(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)
		! From GV eq. (5.104), pg 221.


		call splint(qgrid,GSCold,GSCspline,qsteps,q,Gps)
		!Chi0=(one*ReChi0(q,w)+ione*ImChi0(q,w))!/dsqrt(3.5D0)
		eps3=1D0-Gps*ReChi0(q,w)
	end function eps3

	real (kind=8) function epsGV(q,w)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q,w,Gps
		complex(kind=8)::Chi0
		complex (kind=8),parameter::one=(1D0,0D0),ione=(0D0,1D0)
		! From GV eq. (5.104), pg 221.


		call splint(qgrid,GSCold,GSCspline,qsteps,q,Gps)
		Chi0=(one*ReChi0(q,w)+ione*ImChi0(q,w))!/dsqrt(3.5D0)
		epsGV=1D0-Vc(q)*real(Chi0/(1D0+Vc(q)*Gps*Chi0))
	end function epsGV

	subroutine FnToFile(uno,nomo,params,fn,xs,ys)
		use Globals
		implicit none
		real      (kind=8), intent(in) ::xs(2),ys(2)
		integer   (kind=4), intent(in) ::uno,params(4)
		character (len=32), intent(in) ::nomo
		real      (kind=8), external   ::fn
		real      (kind=8), allocatable::wline(:)

		real      (kind=8)::wa,wb,dw,w,qa,qb,dq,qp
		integer   (kind=4)::NProc,IntSteps,qsteps,wsteps,i,j
		character (len=32)::fmt1,fmt2

		fmt1='(3ES24.15)'


		NProc=params(1)
		IntSteps=params(2)
		qsteps=params(3)
		wsteps=params(4)

		open(unit=uno,file=nomo)

		write(uno,*) "### ",nomo
		write(uno,*) "###"
		write(uno,*) "# NProc, IntSteps, qsteps, wsteps"
		write(uno,'(4I10)') NProc,IntSteps,qsteps,wsteps
		write(uno,*) "# q/kf,  wstart, dw"
		write(uno,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."



		write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'

		allocate(wline(wsteps))

		wa=ys(1)
		wb=ys(2)
		dw=(wb-wa)/real(wsteps ,kind=8)

		qa=xs(1)
		qb=xs(2)
		dq=(qb-qa)/real(qsteps ,kind=8)

		qp=qa
		do i=1,qsteps
			w=wa
			write(uno,fmt1) qp/Globkf, w, dw
			do j=1,wsteps
				wline(j)=fn(qp,w)
				w=w+dw
			enddo
			write(uno,fmt2) wline
			qp=qp+dq
		enddo

		close(unit=uno)

	end subroutine FnToFile


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!! Analytic G+ !!!!!!!!!!!!!!!!!!!
!!! DOI: 10.1103/PhysRevB.64.153101 !!!!!!!!!!!!!!

real (kind=8) function GpA(q)
	use Globals
	implicit none
	real (kind=8)::q,qb,rsb,qbsq,rs,er10

	qb=q/Globkf
	rsb=Globrs/10D0
	rs=Globrs

	qbsq=qb**2


	er10=dexp(rsb)

	GpA=(er10)/(dsqrt(1D0+(Ap(rs)*er10*qb/Bp(rs))**2))
	GpA=GpA+(1D0-er10)*dexp(-qbsq*0.25D0)
	GpA=GpA*Ap(rs)*qb+Cp(rs)*qb*(1D0-dexp(-qbsq))+Pp(qb,rsb)*dexp(-alphap(rsb)*qbsq)

	contains
		real (kind=8) function Pp(qbar,rsbar)
			implicit none
			real (kind=8):: qbar,rsbar

			Pp=g2(rsbar)*qbar**2+g4(rsbar)*qbar**4&
			  +g6(rsbar)*qbar**6+g8(rsbar)*qbar**8
		end function

		real (kind=8) function g2(r10)
			implicit none
			real (kind=8)::r10
			real (kind=8), parameter:: a=0.5824D0,b=0.4272D0

			g2=a*r10**2-b*r10
		end function g2

		real (kind=8) function g4(r10)
			implicit none
			real (kind=8)::r10
			real (kind=8), parameter:: a=0.2960D0,b=1.003D0,c=0.9466D0

			g4=a*r10-b*r10**2.5D0+c*r10**3
		end function g4

		real (kind=8) function g6(r10)
			implicit none
			real (kind=8)::r10
			real (kind=8), parameter:: a=0.0585D0

			g6=-a*r10**2
		end function g6

		real (kind=8) function g8(r10)
			implicit none
			real (kind=8)::r10
			real (kind=8), parameter:: a=0.0131D0

			g8=a*r10**2
		end function g8

		real (kind=8) function alphap(r10)
			implicit none
			real (kind=8)::r10
			real (kind=8), parameter:: a=0.1598D0,b=0.8931D0,c=0.8793D0,d=0.9218D0

			alphap=r10**d
			alphap=(a+b*alphap)/(1D0+c*alphap)
		end function alphap

		real (kind=8) function Ap(rs)
			implicit none
			real (kind=8)::rs
			real (kind=8), parameter::a=1D0/dsqrt(2D0)

			Ap=(1D0-k0k(rs))/rs*a
		end function Ap

		real (kind=8) function k0k(rs)
			use fxc2DEG
			implicit none
			real (kind=8)::rs
			real (kind=8), parameter::a=dsqrt(2D0)/pi

			k0k=Nd2rsec(rs,0D0)-Ndrsec(rs,0D0)/rs
			k0k=1D0-a*rs+0.125D0*rs**4*k0k
		end function k0k

		real (kind=8) function Bp(rs)
			implicit none
			real (kind=8)::rs
			real (kind=8), parameter::a=1.372D0,b=0.0830D0

			Bp=1D0-0.5D0/(1D0+a*rs+b*rs**2)
		end function Bp

		real (kind=8) function Cp(rs)
			use fxc2DEG
			implicit none
			real (kind=8)::rs
			real (kind=8), parameter::a=1D0/2D0/dsqrt(2D0)

			Cp=-a*rs*(EC(rs,0D0)+rs*Ndrsrec(rs,0D0))
		end function Cp

end function GpA

real (kind=8) function GpAbar(p)
	use Globals
	use fxc2DEG
	implicit none
	real (kind=8)::p,q

	q=(1D0-p)/p

	GpAbar=GpA(q)
end function GpAbar



real (kind=8) function GRPA(q)
	implicit none
	real (kind=8)::q

	GRPA=0D0
end function GRPA

real (kind=8) function fRPA(q)
	implicit none
	real (kind=8)::q

	fRPA=2D0*pi/q
end function fRPA

real (kind=8) function GLSDA(q)
	use Globals
	use fxc2DEG
	implicit none
	real (kind=8)::q

	GLSDA=-q/2D0/pi*(cfx(Globrs)+cfc(Globrs))
end function GLSDA

real (kind=8) function GLSDAbar(p)
	use Globals
	use fxc2DEG
	implicit none
	real (kind=8)::p,q

	q=(1D0-p)/p

	GLSDAbar=GLSDA(q)
end function GLSDAbar

real (kind=8) function GPGG(q)
	use Globals
	implicit none
	real (kind=8)::q

	GPGG=-q/2D0/pi*PGG(q)
end function GPGG

real (kind=8) function GPGGbar(p)
	use Globals
	implicit none
	real (kind=8)::p,q

	q=(1D0-p)/p

	GPGGbar=GPGG(q)
end function GPGGbar

real (kind=8) function G01(q)
	use Globals
	implicit none
	real (kind=8)::q

	G01=1D0
end function G01

real (kind=8) function GPGGC(q)
	use Globals
	use fxc2DEG
	implicit none
	real (kind=8)::q

	GPGGC=-q/2D0/pi*(PGG(q)-cfc(Globrs))
end function GPGGC

real (kind=8) function fLSDA(q)
	use Globals
	implicit none
	real (kind=8)::q

	fLSDA=2D0*pi/q+Globfx+Globfc
end function fLSDA

real (kind=8) function PGG(q)
	use QuadPackDouble
	implicit none
	real (kind=8)::q
	integer (kind=4), parameter::limit=10000
	real (kind=8)   , parameter::minpi=-pi**(-1)
	integer (kind=4)::neval,ier,last,iord(limit)
	real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						,elist(limit),temp

	call dqagie ( anc, 0d0, 1, 1D-8, 1D-8, limit , temp, abserr, &
					neval, ier, alist, blist, rlist, elist, iord, last )

	PGG=-2D0*temp/Globn

	contains
		real (kind=8) function anc(r)
			implicit none
			real (kind=8)::r

			if (r.eq.0D0) then
				anc=0D0
			endif
			anc=bessel_jn(0,q*r)*bessel_jn(1,Globkf*r)**2/r**2

		end function anc
end function PGG




















end module STLS
