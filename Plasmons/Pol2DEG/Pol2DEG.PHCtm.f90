program Pol2DEGChi0DD
use Constants
use Globals
use PhysicsPol2DEG
use Response
implicit none
real (kind=8)::q,a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),rs,z,temp2,temp3
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	rs=3.1318562176912983D0
	z=-1.6611445018979713D-003
	!!!! Initialize some situational constants
	call ConstInit(rs,z,GlobEf,Globeta)
	call KernelConstr(rs,z,GlobKernVec)
	!write(*,*) GlobStiff
	qsteps=25
	dq=1.5D-3*Globkf0/real(qsteps, kind=8)
	q=0D0
	do i=1,qsteps
			temp=GlobZeem-q*dsqrt(2D0*GlobEf+GlobZeem)+q**2/2D0
			!write(*,*) q/Globkf0, temp/GlobEf
			temp2=GlobZeem+q*dsqrt(2D0*GlobEf+GlobZeem)+q**2/2D0
			!write(*,*) q/Globkf0, temp/GlobEf
			temp3=GlobZeem-Globn*z*GlobKernVec(4)+GlobStiff*(q)**2/2D0
			write(*,*) q/Globkf0, temp/GlobEf, temp2/GlobEf, temp3/GlobEf
			q=q+dq
	enddo



end program Pol2DEGChi0DD
