module dgauss
implicit none
!Abscissae and weights for 8-point Gaussian-Legendre Quadrature over [-1,1]
!https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
real (kind=8), parameter::G8x1=1.83434642495649805D-01&
						 ,G8x2=5.25532409916328986D-01&
						 ,G8x3=7.96666477413626740D-01&
						 ,G8x4=9.60289856497536232D-01&
						 ,G8w1=3.62683783378361983D-01&
						 ,G8w2=3.13706645877887287D-01&
						 ,G8w3=2.22381034453374471D-01&
						 ,G8w4=1.01228536290376259D-01


contains


	recursive subroutine GaussAdaptQuad(fn,a,b,eps,res)
		implicit none
		real (kind=8), intent(in) ::a,b,eps
		real (kind=8), intent(out) ::res
		real (kind=8), external::fn
		!Wrapper subroutine to properly initialize the the 
		!auxiliary subroutine.
		!!!fn  :: real; function
		!!!a,b :: real; integration limits, a<b
		!!!eps :: real; local error tolerance
		!!!res :: real; integration result


		call GaussAdaptQuadAux(fn,a,b,eps,0,0D0,res)

	end subroutine GaussAdaptQuad


	recursive subroutine GaussAdaptQuadAux(fn,a,b,eps,ct,totin,totout)
		implicit none
		integer (kind=4), intent(in)::ct
		real (kind=8), intent(in) ::a,b,eps,totin
		real (kind=8), intent(out) ::totout
		real (kind=8) ::lint,rint,m,tot1,tot2,totnew,totold
		real (kind=8), external::fn
		!Adaptive integration subroutine
		!!!fn    :: real; function
		!!!a,b   :: real; integration limits
		!!!eps   :: real; local error tolerance
		!!!totin :: real; previous integration result
		!!!totout:: real; current integration result
		!!!ct    ::  int; subdivision counter

		

			!Don't go deeper than 1000 subdivisions
			if (ct.gt.1000) then
				write(*,*) "Integral didn't converge within 1000 subdivisions"
				write(*,*) "a,b"
				write(*,*) a,b
				
				call EXIT(5)
			endif


			!Halve the interval and approximate each side with
			!8-point Gaussian-Legendre Quadrature
			m=(a+b)/2D0
			lint=G8(fn,a,m)
			rint=G8(fn,m,b)


			!If this is the first call then approximate the whole interval.
			!Otherwise use the previous approximation
			if (ct.eq.0) then
				totold=G8(fn,a,b)
			else
				totold=totin
			endif

			!New approximation
			totnew=lint+rint



			!Stopping conditions
			if (((dabs(totnew-totold)).lt.eps).or.((dabs(a-b)).lt.eps)) then
				totout=totnew
			else
				!Subdivide further
				call GaussAdaptQuadAux(fn,a,m,eps/2D0,ct+1,lint,tot1)
				call GaussAdaptQuadAux(fn,m,b,eps/2D0,ct+1,rint,tot2)
				totout=tot1+tot2
			endif


	contains

		real (kind=8) recursive function G8(fn,l,r)
			implicit none
			real (kind=8)::l,r,x,h
			real (kind=8),external::fn
			!8-point Gaussian-Legendre Approximation
			!!!fn    :: real; function
			!!!l,r   :: real; interval limits

				!x and h map [a,b] to [-1,1]
				x=(l+r)/2D0
				h=(r-l)/2D0
				G8=h*(G8w1*(fn(x+h*G8x1)+fn(x-h*G8x1))&
					 +G8w2*(fn(x+h*G8x2)+fn(x-h*G8x2))&
					 +G8w3*(fn(x+h*G8x3)+fn(x-h*G8x3))&
					 +G8w4*(fn(x+h*G8x4)+fn(x-h*G8x4)))
		end function G8

	end subroutine GaussAdaptQuadAux


end module dgauss
