module fxc2DEG
use Constants
use Globals
implicit none
real (kind=8),parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
				  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
				  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
				  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
				  fi(3)=(/-2.069D-2,0D0,0D0/),&
				  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
				  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
				  di(3)=-ai(:)*hi(:)


contains

     DOUBLE PRECISION FUNCTION S(RS,ZETA)
      IMPLICIT NONE

      DOUBLE PRECISION RS,ZETA,DEX,DEXC
      DOUBLE PRECISION A1,B1,C1,D1,E1,G1,H1,A2,B2,C2,D2,E2,H2,&
                      ALPHA1,ALPHA2,BETA

      !PI = 3.141592653589793D0
      DEX = -DSQRT(2.D0)*( DSQRT(1.D0 + ZETA) -&
           DSQRT(1.D0 - ZETA) ) /(PI*RS)

      A1 = 0.117331D0
      A2 = 0.0234188D0
      B1 = -3.394D-2
      B2 = -0.037093D0
      C1 = -7.66765D-3
      C2 = 0.0163618D0
      E1 = 0.4133D0
      E2 = 1.424301D0
      G1 = 6.68467D-2
      H1 = 7.799D-4
      H2 = 1.163099D0
      D1 = -A1*H1
      D2 = -A2*H2

      ALPHA1 = A1 + (B1*RS + C1*RS**2 + D1*RS**3)*&
              DLOG(1.D0 + 1.D0/(E1*RS + G1*RS**2 + H1*RS**3))
      ALPHA2 = A2 + (B2*RS + C2*RS**2 + D2*RS**3)*&
              DLOG(1.D0 + 1.D0/(E2*RS + H2*RS**3))

      BETA = 1.3386D0

      DEXC = DEXP(-BETA*RS)*DEX + (DEXP(-BETA*RS)-1.D0)*&
            (ZETA + ZETA**3/8.D0)*DSQRT(2.D0)/(PI*RS)&
           + 2.D0*ALPHA1*ZETA + 4.D0*ALPHA2*ZETA**3

!C      S = 1.D0/ZETA + 1.D0/(RS**2*DEX)
      S = 1.D0/ZETA + 1.D0/(RS**2*DEXC)
      IF (ZETA.LE.0.D0) S = -S

      RETURN
      END

	real (kind=8) function rs2n(rs)
		implicit none
		real (kind=8)::rs

			rs2n=pi*rs**2
			rs2n=1D0/rs2n
	end function rs2n

	real (kind=8) function n2rs(n)
		implicit none
		real (kind=8)::n

			n2rs=1D0/dsqrt(pi*n)
	end function n2rs

	real (kind=8) function dnz(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)
			dnz=0D0
			!dnz=-z/n
	end function dnz

	real (kind=8) function dnrs(n,rs)
		implicit none
		real (kind=8)::n,rs

			dnrs=-1D0/(2D0*dsqrt(pi))*n**(-1.5D0)
	end function dnrs

	real (kind=8) function d2nrs(n,rs)
		implicit none
		real (kind=8)::n,rs

			d2nrs=1.5D0/(2D0*dsqrt(pi))*n**(-2.5D0)
	end function d2nrs

	real (kind=8) function fz(z)
		implicit none
		real (kind=8)::z

			fz=(1D0+z)**(1.5D0)+(1D0-z)**(1.5D0)
			fz=fz/2D0
	end function fz

	real (kind=8) function dzfz(z)
		implicit none
		real (kind=8)::z

			dzfz=(1D0+z)**(5D-1)-(1D0-z)**(5D-1)
			dzfz=dzfz*7.5D-1
	end function dzfz

	real (kind=8) function d2zfz(z)
		implicit none
		real (kind=8)::z

			d2zfz=(1D0+z)**(-5D-1)+(1D0-z)**(-5D-1)
			d2zfz=d2zfz*3D0/8D0
	end function d2zfz

	real (kind=8) function exh0(rs)
		implicit none
		real (kind=8)::nrs,rs

			nrs=rs2n(rs)
			exh0=-4D0*dsqrt(2D0/pi)/3D0*nrs**(1.5D0)
	end function exh0

	real (kind=8) function dnexh0(rs)
		implicit none
		real (kind=8)::nrs,rs

			nrs=rs2n(rs)
			dnexh0=-2D0*dsqrt(2D0/pi)*nrs**(5D-1)
	end function dnexh0

	real (kind=8) function d2nexh0(rs)
		implicit none
		real (kind=8)::nrs,rs

			nrs=rs2n(rs)
			d2nexh0=-dsqrt(2D0/pi)*nrs**(-5D-1)
	end function d2nexh0

	real (kind=8) function exh(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			exh=exh0(rs)*(1D0+(dsqrt(2D0)-1)*fz(z))
	end function exh

	real (kind=8) function exhn(n,z)
		implicit none
		real (kind=8)::rsn,n,z

			rsn=n2rs(n)
			exhn=exh(rsn,z)
	end function exhn

	real (kind=8) function dnexh(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			dnexh=dnexh0(rs)*fz(z)
	end function dnexh

	real (kind=8) function d2nexh(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			d2nexh=d2nexh0(rs)*fz(z)
	end function d2nexh

	real (kind=8) function d2nzexh(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			d2nzexh=dnexh0(rs)*dzfz(z)
	end function d2nzexh

	real (kind=8) function d2zexh(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			d2zexh=exh0(rs)*d2zfz(z)
	end function d2zexh

	real (kind=8) function dnexht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			dnexht=dnexh(rs,z)/nrs-exh(rs,z)/nrs**2
	end function dnexht

	real (kind=8) function dzexh(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			dzexh=exh0(rs)*dzfz(z)
	end function dzexh

	real (kind=8) function dzexht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			dzexht=dzexh(rs,z)/n
	end function dzexht

	real (kind=8) function d2nexht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			d2nexht=d2nexh(rs,z)/nrs-2D0*dnexh(rs,z)/nrs**2+2D0*exh(rs,z)/nrs**3
	end function d2nexht

	real (kind=8) function d2nzexht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			d2nzexht=d2nzexh(rs,z)/nrs-dzexh(rs,z)/nrs**2
	end function d2nzexht

	real (kind=8) function d2nzecht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			d2nzecht=(dexp(-beta*rs)-1D0)*d2nzex6(rs,z)&
			+dnalpha(1,rs)*2D0*z+dnalpha(2,rs)*4D0*z**3&
			-beta*dexp(-beta*rs)*dnrs(nrs,rs)*dzex6(rs,z)
	end function d2nzecht

	real (kind=8) function d2nzexct(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			d2nzexct=d2nzexht(rs,z)+d2nzecht(rs,z)
	end function d2nzexct

	real (kind=8) function d2nexct(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			d2nexct=d2nexht(rs,z)+d2necht(rs,z)
	end function d2nexct

	real (kind=8) function dnexct(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			dnexct=dnexht(rs,z)+dnecht(rs,z)
	end function dnexct

	real (kind=8) function d2zexht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs,n

			nrs=rs2n(rs)

			d2zexht=d2zexh(nrs,z)/nrs
	end function d2zexht

	real (kind=8) function alphay(indy,indalpha,rs)
		implicit none
		integer (kind=4)::indy,indalpha,ind
		real (kind=8)::rs


			alphay=0D0
			ind=indalpha+1
			if (indy.eq.1) then
		alphay=bi(ind)*rs+ci(ind)*rs**2&
			  +di(ind)*rs**3
			elseif (indy.eq.2) then
		alphay=ei(ind)*rs+fi(ind)*rs**(1.5D0)&
			  +gi(ind)*rs**2+hi(ind)*rs**3
			endif
	end function alphay

	real (kind=8) function drsalphay(indy,indalpha,rs)
		implicit none
		integer (kind=4)::indy,indalpha,ind
		real (kind=8)::rs


			drsalphay=0D0
			ind=indalpha+1
			if (indy.eq.1) then
		drsalphay=bi(ind)+2D0*ci(ind)*rs&
			  +3D0*di(ind)*rs**2
			elseif (indy.eq.2) then
		drsalphay=ei(ind)+1.5D0*fi(ind)*dsqrt(rs)&
			  +2D0*gi(ind)*rs+3D0*hi(ind)*rs**2
			endif
	end function drsalphay

	real (kind=8) function d2rsalphay(indy,indalpha,rs)
		implicit none
		integer (kind=4)::indy,indalpha,ind
		real (kind=8)::rs


			d2rsalphay=0D0
			ind=indalpha+1
			if (indy.eq.1) then
		d2rsalphay=2D0*ci(ind)+6D0*di(ind)*rs
			elseif (indy.eq.2) then
		d2rsalphay=0.75D0*fi(ind)/dsqrt(rs)&
				  +2D0*gi(ind)+6D0*hi(ind)*rs
			endif
	end function d2rsalphay

	real (kind=8) function alpha(ind,rs)
		implicit none
		integer (kind=4)::ind
		real (kind=8)::rs

			alpha=alphay(2,ind,rs)
			alpha=1D0+1D0/alpha
			alpha=dlog(alpha)
			alpha=alpha*alphay(1,ind,rs)+ai(ind+1)
	end function alpha

	real (kind=8) function dnalpha(ind,rs)
		implicit none
		integer (kind=4)::ind
		real (kind=8)::rs,temp,nrs

			nrs=rs2n(rs)

			dnalpha=dlog(1D0+1D0/alphay(2,ind,rs))*drsalphay(1,ind,rs)
			dnalpha=dnalpha-alphay(1,ind,rs)*drsalphay(2,ind,rs)&
			/alphay(2,ind,rs)/(alphay(2,ind,rs)+1D0)
			dnalpha=dnalpha*dnrs(nrs,rs)
	end function dnalpha

	real (kind=8) function d2nalpha(ind,rs)
		implicit none
		integer (kind=4)::ind
		real (kind=8)::rs,temp,nrs

			nrs=rs2n(rs)


			!dnf1aj
			d2nalpha=-1D0/(1D0+alphay(2,ind,rs))/(alphay(2,ind,rs)**2)&
			*drsalphay(1,ind,rs)*drsalphay(2,ind,rs)
			d2nalpha=d2nalpha+dlog(1D0/(1D0+alphay(2,ind,rs)))*d2rsalphay(1,ind,rs)
			!dnf2aj
			temp=-alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0)*(drsalphay(2,ind,rs)&
		 *drsalphay(1,ind,rs)+alphay(1,ind,rs)*d2rsalphay(2,ind,rs))&
		 +(2D0*alphay(2,ind,rs)+1D0)*alphay(1,ind,rs)*drsalphay(2,ind,rs)**2
			temp=temp/((alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0))**2)


			d2nalpha=d2nalpha+temp
			d2nalpha=d2nalpha*dnrs(nrs,rs)
			d2nalpha=d2nalpha+dnalpha(ind,rs)*d2nrs(nrs,rs)/dnrs(nrs,rs)
	end function d2nalpha

	real (kind=8) function ex6(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)
			ex6=exh(rs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*exh0(rs)
			ex6=ex6/nrs
	end function ex6

	real (kind=8) function dnex6(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)

			dnex6=dnexht(rs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*dnexht(rs,0D0)
	end function dnex6

	real (kind=8) function d2nex6(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)

			d2nex6=d2nexht(rs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*d2nexht(rs,0D0)
	end function d2nex6

	real (kind=8) function d2nzex6(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)

			d2nzex6=d2nzexht(rs,z)-(3D0*z/4D0+3D0*z**3/32D0)*dnexht(rs,0D0)
	end function d2nzex6

	real (kind=8) function dzex6(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)

			dzex6=dzexht(nrs,z)-(3D0*z/4D0+3D0*z**3/32D0)*exh0(nrs)/nrs
	end function dzex6

	real (kind=8) function d2zex6(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)

			d2zex6=d2zexht(rs,z)-(3D0/4D0+9D0*z**2/32D0)*exh0(rs)/nrs
	end function d2zex6


	real (kind=8) function echt(rs,z)
		implicit none
		real (kind=8)::rs,z
		real (kind=8), parameter::beta=1.3386D0

			echt=(dexp(-beta*rs)-1D0)*ex6(rs,z)
			echt=echt+alpha(0,rs)+alpha(1,rs)*z**2+alpha(2,rs)*z**4
	end function echt

	real (kind=8) function ech(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs

			nrs=rs2n(rs)
			ech=echt(rs,z)*nrs
	end function ech

	real (kind=8) function echn(n,z)
		implicit none
		real (kind=8)::rsn,n,z

			rsn=n2rs(n)
			echn=ech(rsn,z)
	end function echn

	real (kind=8) function dnecht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			dnecht=(dexp(-beta*rs)-1D0)*dnex6(rs,z)+dnalpha(0,rs)&
			+dnalpha(1,rs)*z**2+dnalpha(2,rs)*z**4&
			-beta*dexp(-beta*rs)*dnrs(nrs,rs)*ex6(rs,z)

			!Debug
			!dnecht=dnalpha(0,rs)
	end function dnecht

	real (kind=8) function d2necht(rs,z)
		implicit none
		real (kind=8)::nrs,z,rs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			d2necht=(dexp(-beta*rs)-1D0)*d2nex6(rs,z)&
			-2D0*beta*dexp(-beta*rs)*dnrs(nrs,rs)*dnex6(rs,z)&
			-beta*dexp(-beta*rs)*d2nrs(nrs,rs)*ex6(rs,z)&
			+ex6(rs,z)*dexp(-beta*rs)*beta*(beta*(dnrs(nrs,rs)**2)-d2nrs(nrs,rs))&
			+d2nalpha(0,rs)+d2nalpha(1,rs)*z**2+d2nalpha(2,rs)*z**4


			!Debug
	end function d2necht

	real (kind=8) function d2nech(rs,z)
		implicit none
		real (kind=8)::nrs,z,rs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			d2nech=2D0*dnecht(rs,z)+d2necht(rs,z)

			!Debug
	end function d2nech

	real (kind=8) function dzecht(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			dzecht=(dexp(-beta*rs)-1D0)*dzex6(rs,z)+2D0*alpha(1,rs)*z&
					+4D0*alpha(2,rs)*z**3
	end function dzecht

	real (kind=8) function d2zecht(rs,z)
		implicit none
		real (kind=8)::nrs,z,rs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			d2zecht=(dexp(-beta*rs)-1D0)*d2zex6(rs,z)+2D0*alpha(1,rs)&
					+12D0*alpha(2,rs)*z**2
	end function d2zecht

	real (kind=8) function dzexct(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			dzexct=dzexht(rs,z)+dzecht(rs,z)
	end function dzexct

	real (kind=8) function d2zexct(rs,z)
		implicit none
		real (kind=8)::rs,z,nrs
		real (kind=8), parameter::beta=1.3386D0

			nrs=rs2n(rs)

			d2zexct=d2zexht(rs,z)+d2zecht(rs,z)
	end function d2zexct

	real (kind=8) function NumDeriv1(f,x,h)
		real (kind=8)::x,h
		real (kind=8), external::f

			NumDeriv1=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
			NumDeriv1=NumDeriv1/12D0/h

	end function NumDeriv1

	real (kind=8) function NumDeriv12(f,x,h)
		real (kind=8)::x,h
		real (kind=8), external::f

			NumDeriv12=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
			NumDeriv12=NumDeriv12/12D0/h

	end function NumDeriv12

	real (kind=8) function NumDeriv2(f,x,h)
		real (kind=8)::x,h
		real (kind=8), external::f

		NumDeriv2=f(x-h)-2D0*f(x)+f(x+h)
		NumDeriv2=NumDeriv2/h**2

	end function NumDeriv2

	real (kind=8) function Ndnexct(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Ndnexct=NumDeriv1(anc,nrs,1D-4)

		contains
				real (kind=8) function anc(n) !needs to be n since that is the derivative
					real (kind=8)::n,rsn
					rsn=n2rs(n)
					anc=excht(rsn,Globzvar)!*n
				end function anc
	end function Ndnexct

	real (kind=8) function Ndzexct(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Ndzexct=NumDeriv1(anc,z,1D-4)

		contains
				real (kind=8) function anc(z) !needs to be z since that is the derivative
					real (kind=8)::n,rsn,z
					rsn=n2rs(Globnvar)
					anc=excht(rsn,z)!*Globnvar
				end function anc
	end function Ndzexct

	real (kind=8) function Nd2nzexct(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Nd2nzexct=NumDeriv12(anc,z,1D-4)

		contains
				real (kind=8) function anc(z) !needs to be z since that is the derivative
					real (kind=8)::n,rsn,z
					rsn=n2rs(Globnvar)
					anc=Ndnexct(rsn,z)
				end function anc
	end function Nd2nzexct

	real (kind=8) function Nd2nexct(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Nd2nexct=NumDeriv2(anc,nrs,1D-4)

		contains
				real (kind=8) function anc(n) !needs to be n since that is the derivative
					real (kind=8)::n,rsn
					rsn=n2rs(n)
					anc=excht(rsn,Globz)!*n
				end function anc
	end function Nd2nexct

	real (kind=8) function Nd2zexct(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Nd2zexct=NumDeriv2(anc,z,1D-4)

		contains
				real (kind=8) function anc(z) !needs to be n since that is the derivative
					real (kind=8)::z,rsn
					rsn=n2rs(Globnvar)
					anc=excht(rsn,z)!*Globnvar
				end function anc
	end function Nd2zexct




!-----------------------------------------------------------------------------------------


	real (kind=8) function Ndnexc(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Ndnexc=NumDeriv1(anc,nrs,1D-4)

		contains
				real (kind=8) function anc(n) !needs to be n since that is the derivative
					real (kind=8)::n,rsn
					rsn=n2rs(n)
					anc=exch(rsn,Globzvar)
				end function anc
	end function Ndnexc

	real (kind=8) function Ndzexc(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Ndzexc=NumDeriv1(anc,z,1D-4)

		contains
				real (kind=8) function anc(z) !needs to be z since that is the derivative
					real (kind=8)::n,rsn,z
					rsn=n2rs(Globnvar)
					anc=exch(rsn,z)
				end function anc
	end function Ndzexc

	real (kind=8) function Nd2nzexc(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Nd2nzexc=NumDeriv12(anc,z,1D-4)

		contains
				real (kind=8) function anc(z) !needs to be z since that is the derivative
					real (kind=8)::n,rsn,z
					rsn=n2rs(Globnvar)
					anc=Ndnexc(rsn,z)
				end function anc
	end function Nd2nzexc

	real (kind=8) function Nd2nexc(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Nd2nexc=NumDeriv2(anc,nrs,1D-4)

		contains
				real (kind=8) function anc(n) !needs to be n since that is the derivative
					real (kind=8)::n,rsn
					rsn=n2rs(n)
					anc=exch(rsn,Globz)
				end function anc
	end function Nd2nexc

	real (kind=8) function Nd2zexc(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			Globnvar=nrs
			Globzvar=z
			Nd2zexc=NumDeriv2(anc,z,1D-4)

		contains
				real (kind=8) function anc(z) !needs to be n since that is the derivative
					real (kind=8)::z,rsn
					rsn=n2rs(Globnvar)
					anc=exch(rsn,z)
				end function anc
	end function Nd2zexc



	real (kind=8) function exch(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			exch=exh(rs,z)+echt(rs,z)*nrs
	end function exch

	real (kind=8) function exchn(n,z)
		implicit none
		real (kind=8)::rsn,n,z

			rsn=n2rs(n)
			exchn=exch(rsn,z)
	end function exchn

	real (kind=8) function excht(rs,z)
		implicit none
		real (kind=8)::nrs,rs,z

			nrs=rs2n(rs)
			excht=exh(rs,z)/nrs+echt(rs,z)
	end function excht



	real (kind=8) function Ndrsec(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			Ndrsec=NumDeriv1(anc,rs,1D-6)

		contains
				real (kind=8) function anc(rs) !needs to be n since that is the derivative
					real (kind=8)::rs,nrs
					nrs=rs2n(rs)
					anc=EC(rs,z)
					anc=echt(rs,z)
				end function anc
	end function Ndrsec

	real (kind=8) function Ndrsrec(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			Ndrsrec=NumDeriv1(anc,rs,1D-8)

		contains
				real (kind=8) function anc(rs) !needs to be n since that is the derivative
					real (kind=8)::rs,nrs
					nrs=rs2n(rs)
					anc=EC(rs,z)
					anc=echt(rs,z)
				end function anc
	end function Ndrsrec

	real (kind=8) function Nd2rsec(rs,z)
		use Globals
		implicit none
		real (kind=8)::nrs,rs,z

			Nd2rsec=NumDeriv2(anc,rs,1D-8)

		contains
				real (kind=8) function anc(rs) !needs to be n since that is the derivative
					real (kind=8)::rs,nrs
					nrs=rs2n(rs)
					anc=EC(rs,z)
					anc=echt(rs,z)
				end function anc
	end function Nd2rsec


	DOUBLE PRECISION FUNCTION EC(RS,ZETA)
	IMPLICIT NONE

	DOUBLE PRECISION RS,ZETA,EX6,myEX,EX0
	DOUBLE PRECISION A0,B0,C0,D0,E0,F0,G0,H0,&
                   A1,B1,C1,D1,E1,G1,H1,&
                   A2,B2,C2,D2,E2,H2,&
                   ALPHA0,ALPHA1,ALPHA2,BETA

	!PI = 3.141592653589793D0

	A0 = -0.1925D0
	A1 = 0.117331D0
	A2 = 0.0234188D0

	B0 = 0.0863136D0
	B1 = -3.394D-2
	B2 = -0.037093D0

	C0 = 0.057234D0
	C1 = -7.66765D-3
	C2 = 0.0163618D0

	E0 = 1.0022D0
	E1 = 0.4133D0
	E2 = 1.424301D0

	F0 = -0.02069D0

	G0 = 0.34D0
	G1 = 6.68467D-2

	H0 = 1.747D-2
	H1 = 7.799D-4
	H2 = 1.163099D0

	D0 = -A0*H0
	D1 = -A1*H1
	D2 = -A2*H2

	ALPHA0 = A0 + (B0*RS + C0*RS**2 + D0*RS**3)*&
          DLOG(1.D0 + 1.D0/(E0*RS + F0*RS**1.5D0 + &
                                    G0*RS**2 + H0*RS**3))
	ALPHA1 = A1 + (B1*RS + C1*RS**2 + D1*RS**3)*&
          DLOG(1.D0 + 1.D0/(E1*RS + G1*RS**2 + H1*RS**3))
	ALPHA2 = A2 + (B2*RS + C2*RS**2 + D2*RS**3)*&
          DLOG(1.D0 + 1.D0/(E2*RS + H2*RS**3))

	BETA = 1.3386D0


	EX0 = EX(RS,0D0)

	myEX = EX(RS,ZETA)

	EX6 = myEX - (1.D0 + 3.D0*ZETA**2/8D0 + 3.D0*ZETA**4/128.D0)*EX0

	EC = (DEXP(-BETA*RS)-1.D0)*EX6&
       + ALPHA0 + ALPHA1*ZETA**2 + ALPHA2*ZETA**4

	RETURN
	END


	DOUBLE PRECISION FUNCTION EX(RS,ZETA)
	IMPLICIT NONE

	DOUBLE PRECISION RS,ZETA,EX0

	!PI = 3.141592653589793D0

	EX0 = -4.D0*DSQRT(2.D0)/(3.D0*PI*RS)

	EX = EX0*( (1.D0+ZETA)**1.5D0 + (1.D0-ZETA)**1.5D0)/2.D0

	RETURN
	END

	real (kind=8) function cfxssp(rs,z)
		implicit none
		real (kind=8)::rs,z

		cfxssp=NumDeriv1(anc,z,1D-8)
		cfxssp=2D0*cfxssp/rs2n(rs)/z

		contains
			real (kind=8) function anc(z)
				implicit none
				real (kind=8)::z

				anc=exh(rs,z)/rs2n(rs)
			end function anc
	end function cfxssp

	real (kind=8) function cfcssp(rs,z)
		implicit none
		real (kind=8)::rs,z

		cfcssp=NumDeriv1(anc,z,1D-8)
		cfcssp=2D0*cfcssp/rs2n(rs)/z

		contains
			real (kind=8) function anc(z)
				implicit none
				real (kind=8)::z

				anc=echt(rs,z)
			end function anc
	end function cfcssp

	real (kind=8) function cfx(rs)
		implicit none
		real (kind=8)::rs

		cfx=NumDeriv2(anc,rs2n(rs),1D-8)

		contains
			real (kind=8) function anc(n)
				implicit none
				real (kind=8)::n

				anc=n*EX(n2rs(n),0D0)
				!anc=exh(n2rs(n),0D0)
			end function anc
	end function cfx

	real (kind=8) function cfc(rs)
		implicit none
		real (kind=8)::rs

		cfc=NumDeriv2(anc,rs2n(rs),1D-8)
		contains
			real (kind=8) function anc(n)
				implicit none
				real (kind=8)::n

				anc=n*EC(n2rs(n),0D0)
				!anc=n*echt(n2rs(n),0D0)
			end function anc
	end function cfc


end module fxc2DEG
