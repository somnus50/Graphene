!##############################################################################
!                                    References
!##############################################################################
!
! [1] 
!
! [2] 
!
! [3] 
!
!###############################################################################
!                                    Constants
!###############################################################################

module constants
	implicit none
	real (kind=8), parameter :: pi=dacos(-1.0D0), alat=1D0, & 
				    kappa=1D0, hbar=1D0, e=1D0, m=1D0, &
				    eV2Eh=1D0/(27.211D0), Ang2a0=1D0/(0.529D0),&
				    cm2Ang=1D8, FillingFrac=2.5D-2, &
				    bgauss=1D-2

	real (kind=8), parameter :: t=0.5D0/(alat**2), & 
				    !Ef=FillingFrac*(8D0*t), eta=Ef*5D-2, &
				    Glat=2D0*pi/alat, b=1D0/alat
	

	real (kind=8), parameter :: &
	Gs(5,2)=transpose(reshape((/0D0,0D0,Glat,0D0,0D0,Glat,-Glat,0D0,0D0,-Glat/), (/2,5/)))



	!!!Global variables to help pass parameters in the 1D integrators
	real (kind=8)::Globkphi,Globq(2),Globqphi,Globw,Globkhat(2),&
				    Ef,eta,Globfx,Globfc,kf,Globn,Globrs,Globz

	contains

		subroutine ConstInit(frac,z,Ef,eta,fx,fc)
			implicit none
			!integer (kind=4) :: 
			real    (kind=8), intent(in)::frac,z 
			real    (kind=8), intent(out)::Ef,eta,fx,fc
			real    (kind=8)::n
				

				! Easy Stuff
				Ef=8D0*t*frac !Specific to how I calculated the Lindhard Matrix
				n=Ef/pi
				Globn=n
				eta=5D-2*Ef
				kf=dsqrt(2D0*Ef)
				Globrs=n2rs(n)

				! Hard Stuff
				fx=2D0*dnexht(n,z)+n*d2nexht(n,z)
				fc=2D0*dnecht(Globrs,z)+n*d2necht(Globrs,z)

				!Debug

				!fx=n*echt(Globrs,z)
				!fc=Numdnech(n,z)
				!Globw=Numfc(n,z)

				fc=Numfc(n,z)

				

			contains

				real (kind=8) function rs2n(rs)
					implicit none
					real (kind=8)::rs

						rs2n=pi*rs**2
						rs2n=1D0/rs2n
				end function rs2n

				real (kind=8) function n2rs(n)
					implicit none
					real (kind=8)::n

						n2rs=1D0/dsqrt(pi*n)
				end function n2rs

				real (kind=8) function dnz(n,z)
					implicit none
					real (kind=8)::n,z
		
						dnz=0D0
						!dnz=-z/n
				end function dnz

				real (kind=8) function dnrs(n,rs)
					implicit none
					real (kind=8)::n,rs

						dnrs=-1D0/(2D0*dsqrt(pi))*n**(-1.5D0)
				end function dnrs

				real (kind=8) function d2nrs(n,rs)
					implicit none
					real (kind=8)::n,rs

						d2nrs=1.5D0/(2D0*dsqrt(pi))*n**(-2.5D0)
				end function d2nrs

				real (kind=8) function fz(z)
					implicit none
					real (kind=8)::z

						fz=(1D0+z)**(4D0/3D0)+(1D0-z)**(4D0/3D0)-2D0
						fz=fz/(2D0*(2D0**(1D0/3D0)-1D0))
				end function fz

				real (kind=8) function dzfz(z)
					implicit none
					real (kind=8)::z

						dzfz=(1D0+z)**(1D0/3D0)-(1D0-z)**(1D0/3D0)
						dzfz=dzfz*2D0/3D0/(2D0**(1D0/3D0)-1D0)
				end function dzfz

				real (kind=8) function exh(n,z)
					implicit none
					real (kind=8)::n,z

						exh=1D0+(dsqrt(2D0)-1D0)*fz(z)
						exh=-exh*4D0*dsqrt(2D0/pi)/3D0*n**(1.5D0)
				end function exh

				real (kind=8) function dnexh(n,z)
					implicit none
					real (kind=8)::n,z

						dnexh=-2D0*dsqrt(2D0*n/pi)
						dnexh=dnexh*(1D0+(dsqrt(2D0)-1D0)*fz(z))
				end function dnexh

				real (kind=8) function d2nexh(n,z)
					implicit none
					real (kind=8)::n,z

						d2nexh=-dsqrt(2D0/(pi*n))
						d2nexh=d2nexh*(1D0+(dsqrt(2D0)-1D0)*fz(z))
				end function d2nexh

				real (kind=8) function alphay(indy,indalpha,rs)
					implicit none
					integer (kind=4)::indy,indalpha,ind
					real (kind=8)::rs
					real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
											  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
											  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
											  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
											  fi(3)=(/-2.069D-2,0D0,0D0/),&
											  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
											  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
											  di(3)=-ai(:)*hi(:)

						alphay=0D0
						ind=indalpha+1
						if (indy.eq.1) then
							alphay=bi(ind)*rs+ci(ind)*rs**2&
								  +di(ind)*rs**3
						elseif (indy.eq.2) then
							alphay=ei(ind)*rs+fi(ind)*rs**(1.5D0)&
								  +gi(ind)*rs**2+hi(ind)*rs**3
						endif
				end function alphay

				real (kind=8) function drsalphay(indy,indalpha,rs)
					implicit none
					integer (kind=4)::indy,indalpha,ind
					real (kind=8)::rs
					real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
											  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
											  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
											  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
											  fi(3)=(/-2.069D-2,0D0,0D0/),&
											  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
											  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
											  di(3)=-ai(:)*hi(:)

						drsalphay=0D0
						ind=indalpha+1
						if (indy.eq.1) then
							drsalphay=bi(ind)+2D0*ci(ind)*rs&
								  +3D0*di(ind)*rs**2
						elseif (indy.eq.2) then
							drsalphay=ei(ind)+1.5D0*fi(ind)*dsqrt(rs)&
								  +2D0*gi(ind)*rs+3D0*hi(ind)*rs**2
						endif
				end function drsalphay

				real (kind=8) function d2rsalphay(indy,indalpha,rs)
					implicit none
					integer (kind=4)::indy,indalpha,ind
					real (kind=8)::rs
					real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
											  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
											  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
											  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
											  fi(3)=(/-2.069D-2,0D0,0D0/),&
											  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
											  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
											  di(3)=-ai(:)*hi(:)

						d2rsalphay=0D0
						ind=indalpha+1
						if (indy.eq.1) then
							d2rsalphay=2D0*ci(ind)+6D0*di(ind)*rs
						elseif (indy.eq.2) then
							d2rsalphay=0.75D0*fi(ind)/dsqrt(rs)&
									  +2D0*gi(ind)+6D0*hi(ind)*rs
						endif
				end function d2rsalphay

				real (kind=8) function alpha(ind,rs)
					implicit none
					integer (kind=4)::ind
					real (kind=8)::rs
					real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/)

						alpha=alphay(2,ind,rs)
						alpha=1D0+1D0/alpha
						alpha=dlog(alpha)
						alpha=alpha*alphay(1,ind,rs)+ai(ind+1)
				end function alpha

				real (kind=8) function dnalpha(ind,rs)
					implicit none
					integer (kind=4)::ind
					real (kind=8)::rs,temp


						dnalpha=dlog(1D0+1D0/alphay(2,ind,rs))*drsalphay(1,ind,rs)
						dnalpha=dnalpha-alphay(1,ind,rs)*drsalphay(2,ind,rs)&
								/alphay(2,ind,rs)/(alphay(2,ind,rs)+1D0)
						dnalpha=dnalpha*dnrs(n,rs)


						!Debug
						!dnalpha=1D0+1D0/alphay(1,ind,rs)
				end function dnalpha

				real (kind=8) function d2nalpha(ind,rs)
					implicit none
					integer (kind=4)::ind
					real (kind=8)::rs,temp


						!dnf1aj
						d2nalpha=-1D0/(1D0+alphay(2,ind,rs))/(alphay(2,ind,rs)**2)&
								*drsalphay(1,ind,rs)*drsalphay(2,ind,rs)
						d2nalpha=d2nalpha+dlog(1D0/(1D0+alphay(2,ind,rs)))*d2rsalphay(1,ind,rs)
						!dnf2aj
						temp=-alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0)*(drsalphay(2,ind,rs)&
							 *drsalphay(1,ind,rs)+alphay(1,ind,rs)*d2rsalphay(2,ind,rs))&
							 +(2D0*alphay(2,ind,rs)+1D0)*alphay(1,ind,rs)*drsalphay(2,ind,rs)**2
						temp=temp/((alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0))**2)


						d2nalpha=d2nalpha+temp
						d2nalpha=d2nalpha*dnrs(n,rs)
						d2nalpha=d2nalpha+dnalpha(ind,rs)*d2nrs(n,rs)/dnrs(n,rs)						
				end function d2nalpha

				real (kind=8) function ex6(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs

						nrs=rs2n(rs)
						ex6=exh(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*exh(nrs,0D0)
						ex6=ex6/nrs
				end function ex6

				real (kind=8) function dnex6(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs

						nrs=rs2n(rs)
						
						dnex6=dnexht(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*dnexht(nrs,0D0)
				end function dnex6

				real (kind=8) function d2nex6(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs

						nrs=rs2n(rs)
						
						d2nex6=d2nexht(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*d2nexht(nrs,0D0)
				end function d2nex6

				real (kind=8) function dnexht(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs

						nrs=rs2n(rs)
						
						dnexht=dnexh(nrs,z)/n-exh(nrs,z)/n**2
				end function dnexht

				real (kind=8) function d2nexht(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs

						nrs=rs2n(rs)
						
						d2nexht=d2nexh(nrs,z)/n-2D0*dnexh(nrs,z)/n**2+2D0*exh(nrs,z)/n**3
				end function d2nexht

				real (kind=8) function echt(rs,z)
					implicit none
					real (kind=8)::rs,z
					real (kind=8), parameter::beta=1.3386D0

						echt=(dexp(-beta*rs)-1D0)*ex6(rs,z)
						echt=echt+alpha(0,rs)+alpha(1,rs)*z**2+alpha(2,rs)*z**4
				end function echt

				real (kind=8) function dnecht(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs
					real (kind=8), parameter::beta=1.3386D0

						nrs=rs2n(rs)

						dnecht=(dexp(-beta*rs)-1D0)*dnex6(rs,z)+dnalpha(0,rs)&
						+dnalpha(1,rs)*z**2+dnalpha(2,rs)*z**4&
						-beta*dexp(-beta*rs)*dnrs(nrs,rs)*ex6(rs,z)

						!Debug
						!dnecht=dnalpha(0,rs)
				end function dnecht

				real (kind=8) function d2necht(rs,z)
					implicit none
					real (kind=8)::nrs,z,rs
					real (kind=8), parameter::beta=1.3386D0

						nrs=rs2n(rs)

						d2necht=(dexp(-beta*rs)-1D0)*d2nex6(rs,z)&
						-2D0*beta*dexp(-beta*rs)*dnrs(nrs,rs)*dnex6(rs,z)&
						-beta*dexp(-beta*rs)*d2nrs(nrs,rs)*ex6(rs,z)&
						+ex6(rs,z)*dexp(-beta*rs)*beta*(beta*(dnrs(nrs,rs)**2)-d2nrs(nrs,rs))&
						+d2nalpha(0,rs)+d2nalpha(1,rs)*z**2+d2nalpha(2,rs)*z**4


						!Debug
				end function d2necht

				real (kind=8) function NumDeriv1(f,x,h)
					real (kind=8)::x,h
					real (kind=8), external::f

						NumDeriv1=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
						NumDeriv1=NumDeriv1/12D0/h

				end function NumDeriv1

				real (kind=8) function NumDeriv2(f,x,h)
					real (kind=8)::x,h
					real (kind=8), external::f

					NumDeriv2=f(x-h)-2D0*f(x)+f(x+h)
					NumDeriv2=NumDeriv2/h**2

				end function NumDeriv2

				real (kind=8) function Numfc(n,z)
					implicit none
					real (kind=8)::n,z

						Globz=z
						Numfc=NumDeriv2(help,n,1D-4)
				end function Numfc

				real (kind=8) function Numdnech(n,z)
					implicit none
					real (kind=8)::n,z

						Globz=z
						Numdnech=NumDeriv1(help,n,1D-4)
				end function Numdnech

				real (kind=8) function help(n)
					real (kind=8)::n,rsn
						rsn=n2rs(n)
						help=n*echt(rsn,Globz)
				end function help

		end subroutine ConstInit
end module constants

!###############################################################################
!                          Numerical Lindhard Function
!###############################################################################

module NSquareHLindFunctions
use constants
implicit none

contains



	real (kind=8) function ReNLind2(q,w)
		implicit none
		integer (kind=4):: i,steps
		real (kind=8):: q(2),w,dk,temp
		real (kind=8), allocatable:: k(:,:)

		steps=1000
		allocate(k(2*steps,2))
		dk=2D0*pi/alat/real(2*steps, kind=8)		
		temp=-pi/alat
		do i=1,2*steps
			k(i,:)=(/temp,temp/)
			temp=temp+dk
		enddo

		ReNLind2=ExtSimpIntegrate(k,steps,ReNLindInt,q,w)

		
		
	end function ReNLind2


	real (kind=8) function ExtSimpIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps
		real (kind=8):: q(2),k(2),w,dkx,dky,a,b,c,d,temp
		real (kind=8), allocatable:: kxodd(:),kyodd(:),kxeven(:),kyeven(:)
		real (kind=8), pointer::OnesVect(:)
		real (kind=8), target:: vector(steps),limits(2*steps,2)
		real (kind=8), external:: fn


			dkx=(limits(2,1)-limits(1,1))  
			dky=(limits(2,2)-limits(1,2))
			a=limits(1,1)
			b=limits(2*steps,1)
			c=limits(1,2)
			d=limits(2*steps,2)
			kxodd=limits(1:2*steps:2,1)
			kyodd=limits(1:2*steps:2,2)
			kxeven=limits(2:2*steps:2,1)
			kyeven=limits(2:2*steps:2,2)
			OnesVect => vector
			OnesVect=1D0
			
  
			ExtSimpIntegrate=0D0
			temp=0D0
			ExtSimpIntegrate=fn(q,w,(/a,c/))+fn(q,w,(/a,d/))+fn(q,w,(/b,c/))+fn(q,w,(/b,d/))

			temp=TwoDSum(1,steps,fn,q,w,a*OnesVect,kyodd)+TwoDSum(1,steps,fn,q,w,b*OnesVect,kyodd)&
			    +TwoDSum(steps,1,fn,q,w,kxodd,c*OnesVect)+TwoDSum(steps,1,fn,q,w,kxodd,d*OnesVect)
			temp=temp*4D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(1,steps-1,fn,q,w,a*OnesVect,kyeven)+TwoDSum(1,steps-1,fn,q,w,b*OnesVect,kyeven)&
			     +TwoDSum(steps-1,1,fn,q,w,kxeven,c*OnesVect)+TwoDSum(steps-1,1,fn,q,w,kxeven,d*OnesVect)
			temp=temp*2D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(steps,steps,fn,q,w,kxodd,kyodd)
			temp=temp*16D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(steps,steps-1,fn,q,w,kxodd,kyeven)+TwoDSum(steps-1,steps,fn,q,w,kxeven,kyodd)
			temp=temp*8D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			temp=TwoDSum(steps-1,steps-1,fn,q,w,kxeven,kyeven)
			temp=temp*4D0
			ExtSimpIntegrate=ExtSimpIntegrate+temp

			ExtSimpIntegrate=ExtSimpIntegrate*dkx*dky/(9D0*4D0*pi**2)
	end function ExtSimpIntegrate

	real (kind=8) function OneDExtSimpIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,i
		real (kind=8):: q(2),k(2),w,dk,dky,a,b,c,d,temp1
		real (kind=8), allocatable:: kxodd(:),kyodd(:),kxeven(:),kyeven(:)
		real (kind=8), pointer::OnesVect(:) 
		real (kind=8), target:: vector(steps),limits(2*steps,2)
		real (kind=8), external:: fn


			dk=(limits(2,1)-limits(1,1))  
			a=limits(1,1)
			b=limits(steps,1)
			c=limits(1,2)
			d=limits(steps,2)
			!allocate(kxodd())
			!kxodd=limits(1:steps:2,1)
			!kyodd=limits(1:steps:2,2)
			!kxeven=limits(2:steps:2,1)
			!kyeven=limits(2:steps:2,2)
			OnesVect => vector
			OnesVect=1D0
			
  			temp1=0D0
			do i=1,1
				temp1=0D0
			enddo		

			OneDExtSimpIntegrate=OneDExtSimpIntegrate*dk/(3D0)
	end function OneDExtSimpIntegrate


	real (kind=8) function TwoDSum(m,n,fn,q,w,x,y)
		implicit none
		integer (kind=4):: i,j,m,n
		real (kind=8):: q(2),w,x(m),y(n),total
		real (kind=8), external:: fn

		total=0D0


		do i=1,m
			do j=1,n
				total=total+fn(q,w,(/x(i),y(j)/))			
			enddo
		enddo
		
		
		TwoDSum=total
	end function TwoDSum


	real (kind=8) function CartIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),k(2),limits(4),w,dkx,dky
		real (kind=8), external:: fn
		! 2D Cartesian integrator over k
		! 
		! fn    :: real, external function to be integrated
		! steps :: integer, number of 
		! q,w   :: real, arguments of fn

			dkx=(limits(2)-limits(1))/real(steps)     
			dky=(limits(4)-limits(3))/real(steps)
			CartIntegrate=0D0
			k=(/limits(1),limits(3)/)
			do i=0,steps
				do j=0,steps
				CartIntegrate=CartIntegrate+fn(q,w,k)
				k=k+(/0D0,dky/)
				enddo
				k=(/k(1)+dkx,limits(3)/)
			enddo
			CartIntegrate=CartIntegrate*dkx*dky/(4D0*pi**2)
	end function CartIntegrate


	real (kind=8) function PolIntegrate(fn,a,b,steps,q,w,G)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,G
		real (kind=8), external:: fn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of 
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps)     
			dphi=2D0*pi/real(steps)
			PolIntegrate=0D0
			k=a
			phi=0D0
			do i=0,steps
				do j=0,steps
				PolIntegrate=PolIntegrate+fn(k,phi,q,w)
				phi=phi+dphi
				enddo
				k=k+dk
				phi=0D0
			enddo
			PolIntegrate=PolIntegrate*dk*dphi/(4D0*pi**2)
	end function PolIntegrate

	real (kind=8) function Norm(v)
		implicit none
		real (kind=8) ::v(2)

		!do i=1,len(v)
		Norm=(v(1))**2+(v(2))**2
		Norm=dsqrt(Norm)
	end function 

	real (kind=8) function En(q)
		implicit none
		real (kind=8) ::q(2)


			En=(Norm(q))**2/2D0!/m*hbar**2
	end function En

	real (kind=8) function f(q)
		implicit none
		real (kind=8) ::q(2)
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector 
	
			f=0D0
			!!!Zero Temperature
			if ((En(q)).lt.(Ef)) then
				f=1D0
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(q)-Ef)/kT))
	end function f


	real (kind=8) function ReNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of 
		! q   :: real, wavevector 
		! w   :: real, frequency

			ReNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=temp/(temp**2+eta**2)
				ReNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=temp/(temp**2+eta**2)
				ReNLindInt=ReNlindInt-temp
				ReNLindInt=ReNLindInt*f(k)
			endif
	end function ReNLindInt

	real (kind=8) function ImNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of 
		! q   :: real, wavevector 
		! w   :: real, frequency

			ImNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=1D0/(temp**2+eta**2)
				ImNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=1D0/(temp**2+eta**2)
				ImNLindInt=ImNlindInt-temp
				ImNLindInt=-ImNLindInt*f(k)*eta
			endif
	end function ImNLindInt

	real (kind=8) function ReNLind(q,w)
		implicit none
		real (kind=8) ::q(2),limits(4),w
		! Real part of 
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

			limits=(/ -(kf+Norm(q)+1D-2),(kf-Norm(q)+1D-2),-(kf+Norm(q)+1D-2),(kf-Norm(q)+1D-2) /)
			ReNLind=CartIntegrate(limits,300,ReNLindInt,q,w)
	end function ReNLind


	real (kind=8) function ImNLind(q,w)
		implicit none
		real (kind=8) ::q(2),limits(4),w
		! Imaginary part of 
		! q   :: real, wavevector
		! w   :: real, frequency

			limits=(/ -pi/alat,pi/alat,-pi/alat,pi/alat /)
			ImNLind=CartIntegrate(limits,1000,ImNLindInt,q,w)
	end function ImNLind

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector 
	
		NVc=2D0*pi*e**2/(kappa*Norm(q))
	end function NVc

	real (kind=8) function KsiG(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		! 
		! 
	
		KsiG=(1D0+(1D0/(b**2))*(Norm(q+G))**2)**3
		KsiG=1D0/KsiG
	end function KsiG

	real (kind=8) function KsiGb(bparam,q,G)
		implicit none
		real (kind=8) ::q(2),G(2),bparam
		! 
		! 
	
		KsiGb=(1D0+(1D0/(bparam**2))*(Norm(q+G))**2)**3
		KsiGb=1D0/KsiGb
	end function KsiGb

	real (kind=8) function KsiGauss(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		! 
		! 
	
		KsiGauss=dexp(-bgauss**2*(Norm(q+G))**2)
	end function KsiGauss

	real (kind=8) function fxc(q)
		implicit none
		integer (kind=4)::flag
		real (kind=8) ::q(2)
		! Exchange
		! q :: real, wavevector 
		! flag :: int, turns exchange on or off

		flag=1
		fxc=0D0
		if (flag.eq.1) then
			fxc=Globfx
			!fxc=Globfc	
			!fxc=Globfx+Globfc
		endif

	end function fxc

	real (kind=8) function ReNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function


		ReNEps=1D0+(2D0*NVc(q+G)*ReNLind(q,w))!*KsiG(q,G))

	end function ReNEps

	real (kind=8) function ImNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

		ImNEps=2D0*NVc(q+G)*ImNLind(q,w)*KsiG(q,G)

	end function ImNEps

	subroutine ReNEpsLine(q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2)
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,1
				G=Gs(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLine

	subroutine ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2),bp
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*KsiGb(bp,qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLineb

end module NSquareHLindFunctions



!###############################################################################
!                               Helper Functions
!###############################################################################

module HelperFunctions

implicit none

contains


	subroutine FindBracket(func,x,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4) :: steps,stat,i
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
	
		do while (((func(x,a)*func(x,b)).gt.(0D0)).and.(i.lt.steps))
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			i=i+1

		enddo
		stat=-1
		if (int(sign(1D0,(func(x,a)*func(x,b)))).eq.(-1)) then
			stat=1
		endif
		return
	end subroutine FindBracket

	subroutine FindBracketList(func,x,ao,bo,array,steps)
		implicit none
		integer (kind=4) :: steps,stat,i,j
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8) :: array(4,3)
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		j=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
	
		do while (i.lt.steps)
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			if ((func(x,a)*func(x,b)).lt.(0D0)) then
				array(j,:)=(/a,b,1D0/)
				j=j+1
				
			endif
			i=i+1

		enddo
		return
	end subroutine FindBracketList

	subroutine FindBracketList2(EpsList,n,q,dw,array)
		implicit none
		integer (kind=4) :: steps,i,j,n
		real    (kind=8) :: q,dw
		real    (kind=8) :: array(10,3),EpsList(n)
		!real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,&
			-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,&
			-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
		steps=n-1
		j=1
		do i=1,steps
			array(j,1)=q
			if ((EpsList(i)*EpsList(i+1)).lt.(0D0)) then
				!write(*,*) (/q,dw*real(i),dw*real(i+1)/)
				array(j,:)=(/q,dw*real(i),dw*real(i+1)/)
				j=j+1
			endif
		enddo



		return
	end subroutine FindBracketList2
	
	real (kind=8) function RootBrentsMethod(a,b,delta,f,x)
		implicit none
		real (kind=8) ::a,b,c,d,s,swap1,swap2,delta,x
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind
	
	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL. f must be a function that takes two inputs (x gets passed along to f).
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method
		
		if (f(x,a)*f(x,b).gt.(0D0)) then
			call EXIT(1)
		endif
	
		if (abs(f(x,a)).gt.abs(f(x,b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
	
		c=a
		mflag=1
		s=a
		ind=1
	
		do while ((abs(b-a).gt.delta).or.(abs(f(x,b)).gt.delta).or.(abs(f(x,s)).gt.delta).or.(ind.lt.1000))
			ind=ind+1
			if ((f(x,a).ne.f(x,c)).and.(f(x,b).ne.f(x,c))) then
				s=(a*f(x,b)*f(x,c))/((f(x,a)-f(x,b))*(f(x,a)-f(x,c)))+(b*f(x,a)&
				  *f(x,c))/((f(x,b)-f(x,a))*(f(x,b)-f(x,c)))+(c*f(x,a)*f(x,b))&
				  /((f(x,c)-f(x,a))*(f(x,c)-f(x,b)))
				!Inverse Quadratic Interpolation
			else
				s=b-f(x,b)*(b-a)/(f(x,b)-f(x,a))
				!Secant Method
			endif
			
			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b))&
			   .ge.(abs(b-c)/2D0))).or.((mflag.eq.(0)).and.((abs(s-b)).ge.&
			   (abs(c-d)/2D0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta)).or.&
			   ((mflag.eq.(0)).and.(abs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif
			
			d=c
			c=b
			
			if ((f(x,a)*f(x,s)).lt.(0D0)) then
				b=s
			else
				a=s
			endif
	
			if (abs(f(x,a)).gt.abs(f(x,b))) then
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif
			
		enddo
	
		if ((f(x,b)).lt.(f(x,s))) then
			RootBrentsMethod=b
		else
			RootBrentsMethod=s
		endif
	
		return
	end function RootBrentsMethod

	real (kind=8) function NumDeriv1(f,x,h)
		real (kind=8)::x,h
		real (kind=8), external::f

		NumDeriv1=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
		NumDeriv1=NumDeriv1/12D0/h

	end function NumDeriv1

	real (kind=8) function NumDeriv2(f,x,h)
		real (kind=8)::x,h
		real (kind=8), external::f

		NumDeriv2=f(x-h)-2D0*f(x)+f(x-h)+f(x+h)
		NumDeriv2=NumDeriv2/h**2

	end function NumDeriv2
	
end module HelperFunctions































