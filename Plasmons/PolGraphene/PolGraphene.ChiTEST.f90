program PolGrapheneChiTEST
use Constants
use Globals
use PhysicsPolGraphene
use fxc2DEG
implicit none
real (kind=8)::n,z,w,dw,q(2),dq
integer (kind=4)::qsteps,wsteps,IntSteps,i,j

	Globgs=2D0
	Globgv=2D0
	n=1D12/cmSq2a0sq
	z=-4D-2
	call ConstInit(n,z)
	qsteps=1
	wsteps=30
	IntSteps=3000
	w=0D0
	dw=(1.5D0*GlobZeem)/real(wsteps, kind=8)
	dq=2D0*dabs(Globz)*Globkf0/real(qsteps, kind=8)
	q=(/dq/2D0,0D0/)
	!q=0D0



	!write(*,*) Globkf0, GlobkfU, GlobkfD, GlobkfMAX



	do i=1,qsteps
		w=0D0
		do j=1,wsteps
			write(*,*) q(1), w, ReChi0UDLowq(q(1),w)*4D0 &
			, PolIntegratek(ReChi0UDIntraU,0D0,2D0*Globkf0,IntSteps,q,w)/2D0*Globgs*Globgv/4D0/pi**2 !&
			!, PolIntegratek(ReChi0UDIntAux1,0D0,2D0*Globkf0,IntSteps,q,w)/2D0*Globgs*Globgv/4D0/pi**2 &
			!+ PolIntegratek(ReChi0UDIntAux2,0D0,2D0*Globkf0,IntSteps,q,w)/2D0*Globgs*Globgv/4D0/pi**2 
			w=w+dw
		enddo
		q(1)=q(1)+dq
	enddo










end program
