!##############################################################################
!                                    References
!##############################################################################
!
! [1] 
!
! [2] 
!
! [3] 
!
!###############################################################################
!                                    Constants
!###############################################################################

module constants
	implicit none
	real (kind=8), parameter :: pi=dacos(-1.0D0), alat=1D0, & 
				    kappa=1D0, hbar=1D0, e=1D0, m=1D0, &
				    eV2Eh=1D0/(27.211D0), Ang2a0=1D0/(0.529D0),&
				    cm2Ang=1D8, FillingFrac=4D-1, &
				    bgauss=1D-2

	real (kind=8), parameter :: t=0.5D0/(alat**2), & 
				    Glat=2D0*pi/alat, b=8D-1/alat!,n=1D0/alat**2

	real (kind=8), parameter :: &
	Gs(5,2)=transpose(reshape((/0D0,0D0,Glat,0D0,0D0,Glat,-Glat,0D0,0D0,-Glat/), (/2,5/)))


	!!!Global variables to help pass parameters in the 1D integrators
	real (kind=8)::Globkphi,Globq(2),Globqphi,Globw,Globkhat(2),&
				    Ef,eta,Globfx,Globfc

	contains

		subroutine ConstInit(frac,z,Ef,eta,fx,fc)
			implicit none
			!integer (kind=4) :: 
			real    (kind=8), intent(in)::frac,z 
			real    (kind=8), intent(out)::Ef,eta,fx,fc
			real    (kind=8)::n 
				

				! Easy Stuff
				Ef=8D0*t*frac
				eta=5D-2*Ef
				n=2D0*frac

				! Hard Stuff
				fx=dnexh(n,z)
				fc=dnech(n,z)


			contains

				real (kind=8) function rs2n(rs)
					implicit none
					real (kind=8)::rs

						rs2n=pi*rs**2
						rs2n=1D0/rs2n
				end function rs2n

				real (kind=8) function n2rs(n)
					implicit none
					real (kind=8)::n

						n2rs=1D0/dsqrt(pi*n)
				end function n2rs

				real (kind=8) function dnz(n,z)
					implicit none
					real (kind=8)::n,z

						dnz=-z/n
				end function dnz

				real (kind=8) function dnrs(n,rs)
					implicit none
					real (kind=8)::n,rs

						dnrs=-1D0/(2D0*dsqrt(pi))*n**(-1.5D0)
				end function dnrs

				real (kind=8) function fz(z)
					implicit none
					real (kind=8)::z

						fz=(1D0+z)**(4D0/3D0)+(1D0-z)**(4D0/3D0)-2D0
						fz=fz/(2D0*(2D0**(1D0/3D0)-1D0))
				end function fz

				real (kind=8) function dzfz(z)
					implicit none
					real (kind=8)::z

						dzfz=(1D0+z)**(1D0/3D0)-(1D0-z)**(1D0/3D0)
						dzfz=dzfz*2D0/3D0/(2D0**(1D0/3D0)-1D0)
				end function dzfz

				real (kind=8) function exh(n,z)
					implicit none
					real (kind=8)::n,z

						exh=1D0+(dsqrt(2D0)-1D0)*fz(z)
						exh=-exh*4D0*dsqrt(2D0/pi)/3D0*n**(1.5D0)
				end function exh

				real (kind=8) function dnexh(n,z)
					implicit none
					real (kind=8)::n,z

						dnexh=-2D0*dsqrt(2D0*n/pi)
						dnexh=dnexh-4D0*dsqrt(2D0/pi)/3D0*n**(1.5D0)*&
							  (dsqrt(2D0)-1D0)*dnz(n,z)*dzfz(z)
				end function dnexh

				real (kind=8) function alpha(ind,rs)
					implicit none
					integer (kind=4)::ind
					real (kind=8)::rs
					real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
											  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
											  ci(3)=(/5.7234D-2,-7.66765D-3,1.63618D-2/),&
											  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
											  fi(3)=(/-2.069D-2,0D0,0D0/),&
											  gi(3)=(/3.4D-1,6.68467D-2,0D0/),&
											  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
											  di(3)=-ai(:)*hi(:)

						alpha=ei(ind+1)*rs+fi(ind+1)*rs**1.5D0+gi(ind+1)*rs**2+hi(ind+1)*rs**3
						alpha=1D0+1D0/alpha
						alpha=dlog(alpha)
						alpha=alpha*(bi(ind+1)*rs+ci(ind+1)*rs**2+di(ind+1)*rs**3)+ai(ind+1)
				end function alpha

				real (kind=8) function drsalpha(ind,rs)
					implicit none
					integer (kind=4)::ind
					real (kind=8)::rs,temp
					real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
											  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
											  ci(3)=(/5.7234D-2,-7.66765D-3,1.63618D-2/),&
											  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
											  fi(3)=(/-2.069D-2,0D0,0D0/),&
											  gi(3)=(/3.4D-1,6.68467D-2,0D0/),&
											  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
											  di(3)=-ai(:)*hi(:)

						temp=ei(ind+1)*rs+fi(ind+1)*rs**1.5D0+gi(ind+1)*rs**2+hi(ind+1)*rs**3
						drsalpha=dlog(1D0+1D0/temp)*(bi(ind+1)+2D0*ci(ind+1)*rs+3D0*di(ind+1)*rs**2)
						temp=1D0/(temp*(temp+1D0))
						temp=temp*(bi(ind+1)*rs+ci(ind+1)*rs**2+di(ind+1)*rs**3)
						temp=temp*(ei(ind+1)+1.5D0*fi(ind+1)*dsqrt(rs)+2D0*gi(ind+1)*rs+3D0*hi(ind+1)*rs**2)
						drsalpha=drsalpha-temp
				end function drsalpha

				real (kind=8) function ex6(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs

						nrs=rs2n(rs)
						ex6=exh(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*exh(nrs,0D0)
						ex6=ex6/nrs
				end function ex6

				real (kind=8) function dnex6(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs,dnexht,dnexht0

						nrs=rs2n(rs)
						dnexht=nrs*dnexh(nrs,z)-exh(nrs,z)
						dnexht0=nrs*dnexh(nrs,0D0)-exh(nrs,0D0)
						dnexht=dnexht/nrs**2
						dnexht0=dnexht0/nrs**2

						dnex6=-(3D0*z/4D0+3D0*z**3/32D0)*exh(nrs,0D0)/nrs&
							  -(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*dnexht0&
							  +dnexht
						
				end function dnex6

				real (kind=8) function echt(rs,z)
					implicit none
					real (kind=8)::rs,z
					real (kind=8), parameter::beta=1.3386D0

						echt=(dexp(-beta*rs)-1D0)*ex6(rs,z)
						echt=echt+alpha(0,rs)+alpha(1,rs)*z**2+alpha(2,rs)*z**4
				end function echt

				real (kind=8) function dnecht(rs,z)
					implicit none
					real (kind=8)::rs,z,nrs
					real (kind=8), parameter::beta=1.3386D0

						nrs=rs2n(rs)
						dnecht=(dexp(-beta*rs)-1D0)*dnex6(rs,z)
						dnecht=dnecht-beta*dexp(-beta*rs)*ex6(rs,z)*dnrs(nrs,rs)
						dnecht=dnecht+dnrs(nrs,rs)*(drsalpha(0,rs)+drsalpha(1,rs)*z**2+drsalpha(2,rs)*z**4)
						dnecht=dnecht+2D0*z*dnz(nrs,z)*alpha(1,rs)+4D0*z**3*dnz(nrs,z)*alpha(2,rs)
				end function dnecht

				real (kind=8) function dnech(n,z)
					implicit none
					real (kind=8)::n,z,rsn

						rsn=n2rs(n)

						dnech=n*dnecht(rsn,z)+echt(rsn,z)
				
				end function dnech

		end subroutine ConstInit
end module constants



!###############################################################################
!                               Helper Functions
!###############################################################################

module HelperFunctions

implicit none

contains


	subroutine FindBracket(func,x,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4) :: steps,stat,i
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
	
		do while (((func(x,a)*func(x,b)).gt.(0D0)).and.(i.lt.steps))
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			i=i+1

		enddo
		stat=-1
		if (int(sign(1D0,(func(x,a)*func(x,b)))).eq.(-1)) then
			stat=1
		endif
		return
	end subroutine FindBracket

	subroutine FindBracketList(func,x,ao,bo,array,steps)
		implicit none
		integer (kind=4) :: steps,stat,i,j
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8) :: array(4,3)
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		j=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
	
		do while (i.lt.steps)
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			if ((func(x,a)*func(x,b)).lt.(0D0)) then
				array(j,:)=(/a,b,1D0/)
				j=j+1
				
			endif
			i=i+1

		enddo
		return
	end subroutine FindBracketList

	subroutine FindBracketList2(EpsList,n,q,dw,array)
		implicit none
		integer (kind=4) :: steps,i,j,n
		real    (kind=8) :: q,dw
		real    (kind=8) :: array(10,3),EpsList(n)
		!real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,&
			-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,&
			-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
		steps=n-1
		j=1
		do i=1,steps
			array(j,1)=q
			if ((EpsList(i)*EpsList(i+1)).lt.(0D0)) then
				!write(*,*) (/q,dw*real(i),dw*real(i+1)/)
				array(j,:)=(/q,dw*real(i),dw*real(i+1)/)
				j=j+1
			endif
		enddo



		return
	end subroutine FindBracketList2
	
	real (kind=8) function RootBrentsMethod(a,b,delta,f,x)
		implicit none
		real (kind=8) ::a,b,c,d,s,swap1,swap2,delta,x
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind
	
	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL. f must be a function that takes two inputs (x gets passed along to f).
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method
		
		if (f(x,a)*f(x,b).gt.(0D0)) then
			call EXIT(1)
		endif
	
		if (abs(f(x,a)).gt.abs(f(x,b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
	
		c=a
		mflag=1
		s=a
		ind=1
	
		do while ((abs(b-a).gt.delta).or.(abs(f(x,b)).gt.delta).or.(abs(f(x,s)).gt.delta).or.(ind.lt.1000))
			ind=ind+1
			if ((f(x,a).ne.f(x,c)).and.(f(x,b).ne.f(x,c))) then
				s=(a*f(x,b)*f(x,c))/((f(x,a)-f(x,b))*(f(x,a)-f(x,c)))+(b*f(x,a)&
				  *f(x,c))/((f(x,b)-f(x,a))*(f(x,b)-f(x,c)))+(c*f(x,a)*f(x,b))&
				  /((f(x,c)-f(x,a))*(f(x,c)-f(x,b)))
				!Inverse Quadratic Interpolation
			else
				s=b-f(x,b)*(b-a)/(f(x,b)-f(x,a))
				!Secant Method
			endif
			
			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b))&
			   .ge.(abs(b-c)/2D0))).or.((mflag.eq.(0)).and.((abs(s-b)).ge.&
			   (abs(c-d)/2D0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta)).or.&
			   ((mflag.eq.(0)).and.(abs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif
			
			d=c
			c=b
			
			if ((f(x,a)*f(x,s)).lt.(0D0)) then
				b=s
			else
				a=s
			endif
	
			if (abs(f(x,a)).gt.abs(f(x,b))) then
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif
			
		enddo
	
		if ((f(x,b)).lt.(f(x,s))) then
			RootBrentsMethod=b
		else
			RootBrentsMethod=s
		endif
	
		return
	end function RootBrentsMethod
	
end module HelperFunctions



!###############################################################################
!                               Helper Functions
!###############################################################################

module HelperFunctions

implicit none

contains


	subroutine FindBracket(func,x,ao,bo,a,b,steps,stat)
		implicit none
		integer (kind=4) :: steps,stat,i
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
	
		do while (((func(x,a)*func(x,b)).gt.(0D0)).and.(i.lt.steps))
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			i=i+1

		enddo
		stat=-1
		if (int(sign(1D0,(func(x,a)*func(x,b)))).eq.(-1)) then
			stat=1
		endif
		return
	end subroutine FindBracket

	subroutine FindBracketList(func,x,ao,bo,array,steps)
		implicit none
		integer (kind=4) :: steps,stat,i,j
		real    (kind=8) :: ao,bo,a,b,x,v
		real    (kind=8) :: array(4,3)
		real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 

		i=1
		j=1
		b=bo
		a=bo+real(1)*(ao-bo)/(real(steps))
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
	
		do while (i.lt.steps)
			!write(10,*) i, func(x,a)*func(x,b)
			a=bo+real(i+1)*(ao-bo)/(real(steps))
			b=bo+real(i)*(ao-bo)/(real(steps))
			if ((func(x,a)*func(x,b)).lt.(0D0)) then
				array(j,:)=(/a,b,1D0/)
				j=j+1
				
			endif
			i=i+1

		enddo
		return
	end subroutine FindBracketList

	subroutine FindBracketList2(EpsList,n,q,dw,array)
		implicit none
		integer (kind=4) :: steps,i,j,n
		real    (kind=8) :: q,dw
		real    (kind=8) :: array(10,3),EpsList(n)
		!real    (kind=8), external:: func
	
	!Finds an interval that brackets a zero of a given function. 
	!Starts from b and moves toward a. 
		array=reshape((/-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,&
			-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,-1D0,&
			-1D0,-1D0,-1D0,-1D0,-1D0/),shape(array))
		steps=n-1
		j=1
		do i=1,steps
			array(j,1)=q
			if ((EpsList(i)*EpsList(i+1)).lt.(0D0)) then
				!write(*,*) (/q,dw*real(i),dw*real(i+1)/)
				array(j,:)=(/q,dw*real(i),dw*real(i+1)/)
				j=j+1
			endif
		enddo



		return
	end subroutine FindBracketList2
	
	real (kind=8) function RootBrentsMethod(aa,ba,delta,f,x)
		implicit none
		real (kind=8) ::a,b,aa,ba,c,d,s,swap1,swap2,delta,x
		real (kind=8), external:: f
		integer (kind=4) ::mflag,ind
	
	! This function finds the root of a function, f in [a,b] with the precision of delta
	! This function relies on the Intermediate Value Theorem so it will exit if f(a) and f(b) are the same sign.
	! a and b must be REAL. f must be a function that takes two inputs (x gets passed along to f).
	! For more details on Brent's Method, see: https://en.wikipedia.org/wiki/Brent%27s_method
		
		a=aa
		b=ba
		
		if (f(x,a)*f(x,b).gt.(0D0)) then
			write(*,*) "rootfinder problems"
			write(*,*) a,b,f(x,a),f(x,b)
			call EXIT(1)
		endif
	
		if (abs(f(x,a)).gt.abs(f(x,b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
	
		c=a
		mflag=1
		s=a
		ind=1
	
		do while ((abs(b-a).gt.delta).and.(abs(f(x,b)).gt.delta).and.(abs(f(x,s)).gt.delta).and.(ind.lt.1000))
			ind=ind+1
			if ((f(x,a).ne.f(x,c)).and.(f(x,b).ne.f(x,c))) then
				s=(a*f(x,b)*f(x,c))/((f(x,a)-f(x,b))*(f(x,a)-f(x,c)))+(b*f(x,a)&
				  *f(x,c))/((f(x,b)-f(x,a))*(f(x,b)-f(x,c)))+(c*f(x,a)*f(x,b))&
				  /((f(x,c)-f(x,a))*(f(x,c)-f(x,b)))
				!Inverse Quadratic Interpolation
			else
				s=b-f(x,b)*(b-a)/(f(x,b)-f(x,a))
				!Secant Method
			endif
			
			if (((s.lt.((3D0*a+b)/4D0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b))&
			   .ge.(abs(b-c)/2D0))).or.((mflag.eq.(0)).and.((abs(s-b)).ge.&
			   (abs(c-d)/2D0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta)).or.&
			   ((mflag.eq.(0)).and.(abs(c-d).lt.delta))) then
				s=(a+b)/2
				!Bisection Method
				mflag=1
			else
				mflag=0
			endif
			
			d=c
			c=b
			
			if ((f(x,a)*f(x,s)).lt.(0D0)) then
				b=s
			else
				a=s
			endif
	
			if (abs(f(x,a)).gt.abs(f(x,b))) then
				swap1=a
				swap2=b
				a=swap2
				b=swap1
			endif
			
		enddo
	
		if ((f(x,b)).lt.(f(x,s))) then
			RootBrentsMethod=b
		else
			RootBrentsMethod=s
		endif
		!write(*,*) "Root steps", ind, f(x,RootBrentsMethod)
		return
	end function RootBrentsMethod
	
end module HelperFunctions


!###############################################################################
!                          Numerical Lindhard Function
!###############################################################################

module NSquareHLindFunctions
use constants
use HelperFunctions
use dgauss
implicit none

contains

	real (kind=8) function ReNLind3(q,w)
		implicit none
		real (kind=8):: q(2),w
		!Calculates the Lindhard function using 
		!Gaussian Adaptive Quadrature.
		
		!Set some global variables for integrating
		Globq=q
		Globw=w

!		call GaussAdaptQuad(Lindphi,0D0,2D0*pi,1D-8,ReNLind3)

		contains

		!Helper functions to allow 2D integration

			real (kind=8) function Lindphi(phi)
				implicit none
				real (kind=8)::phi,rb
	
				Globkhat=(/dcos(phi),dsin(phi)/)
				rb=FermiVec(Globkhat,1D-8)
!				call GaussAdaptQuad(Lindr,0D0,rb,1D-8,Lindphi)		
	
			end function Lindphi
	
			real (kind=8) function Lindr(r)
				implicit none
				real (kind=8)::r,k(2)
	
					k=r*Globkhat
!					Lindr=ReNLindIntPol(Globq,Globw,k)			
	
			end function Lindr

	end function ReNLind3


	real (kind=8) function ReNLind2(q,w)
		implicit none
		real (kind=8):: q(2),w
		! Calculates Lindhard function using a prescribed method.
		! Uncomment the line that corresponds to the method you 
		! want to use. (Right now, NC1 is the best)
		! NCi: Newton-Cotes formula of order i
		!   http://mathworld.wolfram.com/Newton-CotesFormulas.html
		! AQ: Rectangular Adaptive Quadrature (very slow/doesn't work)
		

			ReNLind2=PolIntegrate(200,ReNLindInt,q,w)/4D0/pi**2  	!NC1
			!ReNLind2=PolIntegrate(50,ReNLindInt,q,w)/4D0/pi**2  	!NC4
			!ReNLind2=PolIntegrate(40,ReNLindInt,q,w)/4D0/pi**2  	!NC5
			!ReNLind2=PolIntegrate(20,ReNLindInt,q,w)/4D0/pi**2  	!NC10
			!ReNLind2=PolIntegrate(18,ReNLindInt,q,w)/4D0/pi**2  	!NC11
			!ReNLind2=PolIntegrate(15,ReNLindIntPol,q,w)/4D0/pi**2	!AQ

	end function ReNLind2

	real (kind=8) function ImNLind2(q,w)
		implicit none
		real (kind=8):: q(2),w
		! Calculates Lindhard function using a prescribed method.
		! Uncomment the line that corresponds to the method you 
		! want to use. (Right now, NC1 is the best)
		! NCi: Newton-Cotes formula of order i
		!   http://mathworld.wolfram.com/Newton-CotesFormulas.html
		! AQ: Rectangular Adaptive Quadrature (very slow/doesn't work)
		

			ImNLind2=PolIntegrate(200,ImNLindInt,q,w)/4D0/pi**2  	!NC1
			!ImNLind2=PolIntegrate(50,ImNLindInt,q,w)/4D0/pi**2  	!NC4
			!ImNLind2=PolIntegrate(40,ImNLindInt,q,w)/4D0/pi**2  	!NC5
			!ImNLind2=PolIntegrate(20,ImNLindInt,q,w)/4D0/pi**2  	!NC10
			!ImNLind2=PolIntegrate(18,ImNLindInt,q,w)/4D0/pi**2  	!NC11
			!ImNLind2=PolIntegrate(15,ImNLindIntPol,q,w)/4D0/pi**2	!AQ

	end function ImNLind2


	real (kind=8) function ReNLindIntPol(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! ReNLindInt*|k|
		! q   :: real, wavevector 
		! w   :: real, frequency
		! This is a workaround for integrators that can't 
		! internally multiply |k| for polar coordinates

			ReNLindIntPol=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=temp/(temp**2+eta**2)
				ReNLindIntPol=temp
				temp=w-En(k-q)+En(k)
				temp=temp/(temp**2+eta**2)
				ReNLindIntPol=ReNlindIntPol-temp
				ReNLindIntPol=ReNLindIntPol*f(k)*Norm(k)
			endif
	end function ReNLindIntPol

	real (kind=8) function PolIntegrate(steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine
		real (kind=8), external:: fn
		
		! NCorder must match what was chosen in ReNLind2
		NCorder=11

		bk=0D0
		PolIntegrate=0D0
		dphi=2D0*pi/real(NCorder*steps, kind=8)
		phi=0D0
		k=0D0
		! omp is still a work in progress
		! add $ before omp to use these
		!omp parallel shared () private() 
		!omp do reduction( + : philine, PolIntegrate )
		do phistep=1,NCorder*steps+1
			philine=0D0
			bk=FermiVec((/dcos(phi),dsin(phi)/),1D-6)
			dk=bk/real(NCorder*steps, kind=8)
			k=dk


	!!!AdaptQuad (this one sucks)
	!		dk=(bk-1D-4)/5D0
	!		k=dk
	!		do kstep=1,5
	!			philine=philine+AdaptQuad(fn,k-dk,k,phi,q,w,5D-6,1)
	!			k=k+dk
	!		enddo
	!		PolIntegrate=PolIntegrate+philine
	!!!AdaptQuad

	!!!NewtonCotes
			do kstep=1,NCorder*steps+1
				!philine=philine+dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  									!NC1
				!philine=philine+IntCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC4
				!philine=philine+BooleCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC5
				!philine=philine+IntCoeff10(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC10
				philine=philine+IntCoeff11(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC11
				k=k+dk
			enddo
  
			!PolIntegrate=PolIntegrate+philine  									!1
			!PolIntegrate=PolIntegrate+IntCoeff(phistep,NCorder*steps+1)*philine  	!4
			!PolIntegrate=PolIntegrate+BooleCoeff(phistep,NCorder*steps+1)*philine  !5
			!PolIntegrate=PolIntegrate+IntCoeff10(phistep,NCorder*steps+1)*philine  !10
			PolIntegrate=PolIntegrate+IntCoeff11(phistep,NCorder*steps+1)*philine  	!11
	!!!NewtonCotes


			phi=phi+dphi
		enddo
		!omp end do
		!omp end parallel


		PolIntegrate=PolIntegrate*dphi

	end function PolIntegrate



	real (kind=8) function Norm(v)
		implicit none
		real (kind=8) ::v(2)
		! Magnitude of a vector

		Norm=(v(1))**2+(v(2))**2
		Norm=dsqrt(Norm)
	end function 

	real (kind=8) function En(q)
		implicit none
		real (kind=8) ::q(2)
		! Energy of a square hydrogen lattice


			En=2D0*t*(2D0-(dcos(q(1)*alat)+dcos(q(2)*alat)))
	end function En

	real (kind=8) function f(q)
		implicit none
		real (kind=8) ::q(2)
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector 
	
			f=0D0
			!!!Zero Temperature
			if ((En(q)).lt.(Ef)) then
				f=1D0
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(q)-Ef)/kT))
	end function f

	real (kind=8) function FermiTest(phi,q)
		implicit none
		real (kind=8) ::q,phi
		
			FermiTest=-Ef+En(q*(/dcos(phi),dsin(phi)/))
	end function FermiTest

	real (kind=8) function FermiVec(q,tol)
		use HelperFunctions
		implicit none
		real (kind=8) ::q(2),phi,maxq,tol
		

			!Calculate the zone boundary (square)
			phi=datan(q(2)/q(1))
			maxq=pi/alat/dcos(modulo(phi+pi/4D0,pi/2D0)+7D0*pi/4D0)
			FermiVec=maxq
!			if ((En()*En()).le.(0D0)) then
				FermiVec=RootBrentsMethod(0D0,maxq,tol,FermiTest,phi)
!			endif
	end function FermiVec

	real (kind=8) function ReNLindInt(q,w,k,G)
		implicit none
		real (kind=8) ::q,phi
		
			FermiTest=-Ef+En(q*(/dcos(phi),dsin(phi)/))
	end function FermiTest

	real (kind=8) function FermiVec(q,tol)
		implicit none
		real (kind=8) ::q(2),phi,maxq,tol
		

			!Calculate the zone boundary (square)
			phi=datan(q(2)/q(1))
			maxq=pi/alat/dcos(modulo(phi+pi/4D0,pi/2D0)+7D0*pi/4D0)
			FermiVec=maxq
!			if ((En()*En()).le.(0D0)) then
				FermiVec=RootBrentsMethod(0D0,maxq,tol,FermiTest,phi)
!			endif
	end function FermiVec

	real (kind=8) function ReNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of 
		! q   :: real, wavevector 
		! w   :: real, frequency

			ReNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=temp/(temp**2+eta**2)
				ReNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=temp/(temp**2+eta**2)
				ReNLindInt=ReNlindInt-temp
				ReNLindInt=ReNLindInt*f(k)
			endif
	end function ReNLindInt


	real (kind=8) function ImNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of 
		! q   :: real, wavevector 
		! w   :: real, frequency

			ImNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=1D0/(temp**2+eta**2)
				ImNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=1D0/(temp**2+eta**2)
				ImNLindInt=ImNlindInt-temp
				ImNLindInt=-ImNLindInt*f(k)*eta
			endif
	end function ImNLindInt

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector 
	
		NVc=2D0*pi*e**2/(kappa*Norm(q))
	end function NVc

	real (kind=8) function KsiG(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		! 
		! 
	
		KsiG=(1D0+(1D0/(b**2))*(Norm(q+G))**2)**3
		KsiG=1D0/KsiG
	end function KsiG

	real (kind=8) function KsiGb(bparam,q,G)
		implicit none
		real (kind=8) ::q(2),G(2),bparam
		! 
		! 
	
		KsiGb=(1D0+(1D0/(bparam**2))*(Norm(q+G))**2)**3
		KsiGb=1D0/KsiGb
	end function KsiGb

	real (kind=8) function KsiGauss(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		! 
		! 
	
		KsiGauss=dexp(-bgauss**2*(Norm(q+G))**2)
	end function KsiGauss

	real (kind=8) function ReNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function


		ReNEps=1D0+(2D0*(NVc(q+G)+fxc(q))*ReNLind2(q,w)*KsiG(q,G))

	end function ReNEps

	real (kind=8) function ImNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

		ImNEps=2D0*NVc(q+G)*ImNLind2(q,w)*KsiG(q,G)

	end function ImNEps

	subroutine ReNEpsLine(q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2)
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)
		! Calculates ReNEps from a tabulated Lindhard function

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*KsiG(qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLine

	subroutine ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2),bp
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)
		! Calculates ReNEps from a tabulated Lindhard function
		! and also allows for the b parameter of Ksi to be changed

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*KsiGb(bp,qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLineb

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!Newton-Cotes Coefficients
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	real (kind=8) function BooleCoeff(x,n)
		implicit none
		integer (kind=4):: x,n,i
		real    (kind=8), parameter:: temp(4)=&
			(/32D0,12D0,32D0,14D0/)

