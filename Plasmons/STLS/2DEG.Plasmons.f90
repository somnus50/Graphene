program TWODEGPlasmons
use Constants
use Globals
use Physics2DEG
use AuxiliaryFunctions
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,w,dw,PlasmonArray(20,3),wstart
real (kind=8), allocatable::LindLine(:),EpsLine(:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	call ConstInit(2D0,0D0,GlobEf,Globeta,Globfx,Globfc)
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	open(unit=19, file='Eps10.dat', status='old', action='read')
	open(unit=9, file='Plasmons.10.dat')
	write(9,*) "# q/kf,wp/ef,low q limit"
	read(19,*)
	read(19,*)
	read(19,*)
	read(19,'(4I10)') NProc, IntSteps, qsteps, wsteps
	read(19,*)
	read(19,*)

	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	!write(*,*) fmt2
	allocate(LindLine(wsteps),EpsLine(wsteps))

	do i=1,qsteps
		read(19,fmt1) q, wstart, dw
		q=q*Globkf
		read(19,fmt2) LindLine
!		call ReNEpsLine(q,wstart,dw,wsteps,LindLine/4D0/pi/pi,EpsLine)
		EpsLine=LindLine
		call FindBracketList2(EpsLine,wsteps,q,wstart,dw,PlasmonArray)
		do j=1,10
			if ((PlasmonArray(j,2)).gt.(0D0)) then
				write(9,fmt1) PlasmonArray(j,1)/Globkf,&
						(PlasmonArray(j,2)+PlasmonArray(j,3))/(2D0*GlobEf),&
						Globkf*dsqrt(q)/GlobEf
			endif
		enddo
	enddo

	!!! Close files
	close(unit=19)
	close(unit=9)

end program TWODEGPlasmons
