a=2.46
e2p=0
t=-3.033
s=.129
def f1(k):
    return complex(np.cos(k*a/(2*np.sqrt(3))),np.sin(k*a/(2*np.sqrt(3))))
def f3(k):
    return complex(np.cos(k*a/np.sqrt(3)),np.sin(k*a/np.sqrt(3)))
def ACHConstr(i,j,Nr,k,inrow,outrow):
    if i==j:
        return e2p
    elif (np.abs(i-j)==1) and (i<j):
        return t*inrow[i]
    elif (np.abs(i-j)==1) and (i>j):
        return t*inrow[i-1].conjugate()
    elif (np.abs(i-j)==2) and (i < j):
        return t*outrow[i]
    elif (np.abs(i-j)==2) and (i > j):
        return t*outrow[i-2].conjugate()
    else:
        return 0
def ACSConstr(i,j,Nr,k,inrow,outrow):
    if i==j:
        return 1
    elif (np.abs(i-j)==1) and (i<j):
        return s*inrow[i]
    elif (np.abs(i-j)==1) and (i>j):
        return s*inrow[i-1].conjugate()
    elif (np.abs(i-j)==2) and (i < j):
        return s*outrow[i]
    elif (np.abs(i-j)==2) and (i > j):
        return s*outrow[i-2].conjugate()
    else:
        return 0
def ACH(Nr,k):
    achlist=[]
    InnerRow=[f3(k).conjugate(),0,f3(k),0]*Nr
    OuterRow=[f1(k).conjugate(),f1(k).conjugate(),f1(k),f1(k)]*Nr
    for i in xrange(0,2*Nr):
        row=[]
        for j in xrange(0,2*Nr):
            row.append(ACHConstr(i,j,Nr,k,InnerRow,OuterRow))
        achlist.append(row)
    ach=np.array(achlist)
    return ach
def ACS(Nr,k):
    acslist=[]
    InnerRow=[f3(k).conjugate(),0,f3(k),0]*Nr
    OuterRow=[f1(k).conjugate(),f1(k).conjugate(),f1(k),f1(k)]*Nr
    for i in xrange(0,2*Nr):
        row=[]
        for j in xrange(0,2*Nr):
            row.append(ACSConstr(i,j,Nr,k,InnerRow,OuterRow))
        acslist.append(row)
    acs=np.array(acslist)
    return acs
def ACEnergies(Nr,k):
    (En,Ev)=eig(ACH(Nr,k),ACS(Nr,k))
    return En
def ZZHConstr(i,j,Nr,k,offdrow):
    if i==j:
        return e2p
    elif i-j==-1:
        return t*offdrow[i]
    elif i-j==1:
        return t*offdrow[i-1].conjugate()
    else:
        return 0
def ZZSConstr(i,j,Nr,k,offdrow):
    if i==j:
        return 1
    elif i-j==-1:
        return s*offdrow[i]
    elif i-j==1:
        return s*offdrow[i-1].conjugate()
    else:
        return 0
def ZZH(Nr,k):
    zzhlist=[]
    OffDiagRow=[2*f1(k).real,1]*Nr
    for i in xrange(0,2*Nr):
        row=[]
        for j in xrange(0,2*Nr):
            row.append(ZZHConstr(i,j,Nr,k,OffDiagRow))
        zzhlist.append(row)
    zzh=np.array(zzhlist)
    return zzh
def ZZS(Nr,k):
    zzslist=[]
    OffDiagRow=[2*f1(k).real,1]*Nr
    for i in xrange(0,2*Nr):
        row=[]
        for j in xrange(0,2*Nr):
            row.append(ZZSConstr(i,j,Nr,k,OffDiagRow))
        zzslist.append(row)
    zzs=np.array(zzslist)
    return zzs
def ZZEnergies(Nr,k):
    (En,Ev)=eig(ZZH(Nr,k),ZZS(Nr,k))
    return En
