module SelfConsistent
use AuxiliaryFunctions
implicit none
real (kind=8),allocatable::fngrid(:),xgrid(:),splinegrid(:),fngridOLD(:)
integer (kind=4)::SCCount,xsteps
logical ::hasinit=.false.


!hasinit=.false.



contains


	subroutine SCSolve(fn,IterMax,mix,tol,ress,UpdateSub,fnout,splineout,stat)
		use PhysicsPol2DEG
		implicit none
		integer (kind=4), intent(in) ::IterMax
		integer (kind=4), intent(out)::stat
		real    (kind=8), intent(in) ::tol,mix
		real    (kind=8), intent(out)::fnout(xsteps),splineout(xsteps),ress(IterMax)
		real    (kind=8), external   ::fn
		external::UpdateSub
		integer (kind=4)::ConvFlag,i,iter,k
		real    (kind=8)::res

		ress=-1D0

		if (.not.hasinit) then
			write(*,*) "SCInit has not been called before SCSolve. Exiting"
			call exit(-1)
		endif

		write(*,*) "Start SCsolve"

		fnout=fngrid
		splineout=splinegrid

		ConvFlag=0
		iter=1
		do while ((ConvFlag.lt.2).and.(iter.le.IterMax))

			call UpdateSub()

			fngridOLD=fngrid

			write(*,*) "SC step:",iter

			! write(199,*) "SC step", iter-1
			! do k = 1, xsteps
			! 	write(199,*) xgrid(k),fngrid(k)
			! end do



			do i=1,xsteps
				write(*,*) "SC xstep:",i,xsteps
				fngrid(i)=fn(xgrid(i))
				! write(199,*) xgrid(i),fngrid(i)
				if ( isnan(fngrid(i)) ) then
					write(*,*) "fngrid NaN"
					write(*,*) "i,xgrid(i)", i,xgrid(i)
					call exit()
				end if
			enddo

			! write(*,*) fngrid


			fngrid=((1D0-mix)*fngrid+mix*fngridOLD)!/2D0

			res=residue(xsteps)
			ress(iter)=res
			if ((ConvFlag.gt.0).and.(res.gt.tol)) then
				ConvFlag=0
			endif
			if (res.lt.tol) then
				ConvFlag=ConvFlag+1
			endif

			fnout=fngrid
			call spline(xgrid,fngrid,xsteps,1D31,1D31,splinegrid)
			splineout=splinegrid



			! write(199,*) "SC step", iter
			! do k = 1, xsteps
			! 	write(199,*) xgrid(k),fngrid(k)
			! end do

			write(*,*) "   Residue,                   Tolerance,                Times Sequentially Converged"
			write(*,*) res,tol,ConvFlag

!   Residue,                   Tolerance,                Times Sequentially Converged
!   0.13223238265043055        1.0000000000000000E-004           0

			!Small Workaround

			! call UpdateSub()


			iter=iter+1
		enddo

		stat=iter-1

		if (res.gt.tol) then
			stat=-stat
		endif


		contains
			real (kind=8) function residue(xst)
				implicit none
				integer (kind=4)::i,xst
				real (kind=8)::temp(xst),temp1



				do i=1,xst
					temp(i)=(fngrid(i)-fngridOLD(i))**2!/dabs((fngrid(i)+fngridOLD(i))/2D0)
				enddo

				call ListIntegrate(temp,xst,temp1)

				residue=temp1

				do i=1,xst
					temp(i)=(fngrid(i)+fngridOLD(i))/2D0
				enddo

				call ListIntegrate(temp,xst,temp1)

				residue=residue/dabs(temp1)

			end function residue
	end subroutine SCSolve

	subroutine SCInit(xstps,xs,InitFn)
		! use AuxiliaryFunctions
		implicit none
		real (kind=8)::xs(2),dx
		integer (kind=4)::xstps,i
		real (kind=8), external:: InitFn

		xsteps=xstps

		if (.not.allocated(xgrid)) then
			allocate(xgrid(xsteps),fngrid(xsteps),splinegrid(xsteps),fngridOLD(xsteps))
		endif

		! write(*,*) xs

		dx=(xs(2)-xs(1))/real(xsteps+1, kind=8)
		if (dx.lt.0D0) then
			xs=(/xs(2),xs(1)/)
			dx=-dx
		endif

		do i=1,xsteps
			xgrid(i)=xs(1)+dx*real(i, kind=8)
		enddo

		! call LogGrid(0.02D0,xsteps,xgrid)
		! xgrid=xgrid*xs(2)

		do i=1,xsteps
			fngrid(i)=InitFn(xgrid(i))
		enddo
		! write(*,*) fngrid
		! call exit()
		call spline(xgrid,fngrid,xsteps,1D31,1D31,splinegrid)
		fngridOLD=fngrid

		hasinit=.true.

		write(*,*) "Init finished"

	end subroutine SCInit


	subroutine ListIntegrate(temp,n,res)
		implicit none
		integer (kind=4), intent(in) ::n
		real    (kind=8), intent(in) ::temp(n)
		real    (kind=8), intent(out)::res
		integer (kind=4)::i

		res=0D0
		do i=1,n
			res=res+temp(i)
		enddo

	end subroutine


	subroutine StupidIntegrate(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8), external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8)

		x=a+dx/2D0
		res=0D0
		do i=1,steps
			res=res+f(x)
			x=x+dx
		enddo
		res=res*dx
	end subroutine StupidIntegrate


	subroutine StupidIntegrate2(f,a,b,steps,res,ier)
		implicit none
		real (kind=8), intent (in)    ::a,b
		real (kind=8), intent (out)   ::res
		integer (kind=4), intent (in) ::steps
		integer (kind=4), intent (out)::ier
		real (kind=8),  external:: f
		integer (kind=4)::i
		real (kind=8)   ::x,dx

		ier=0

		dx=(b-a)/real(steps, kind=8)

		x=a+dx/2D0
		res=0D0
		do i=1,steps
			res=res+f(x)
			x=x+dx
		enddo
		res=res*dx
	end subroutine StupidIntegrate2


	subroutine FnToFile(uno,nomo,params,fn,xs,ys)
		use Globals
		implicit none
		real      (kind=8), intent(in) ::xs(2),ys(2)
		integer   (kind=4), intent(in) ::uno,params(4)
		character (len=32), intent(in) ::nomo
		real      (kind=8), external   ::fn
		real      (kind=8), allocatable::wline(:)

		real      (kind=8)::wa,wb,dw,w,qa,qb,dq,qp
		integer   (kind=4)::NProc,IntSteps,qsteps,wsteps,i,j
		character (len=32)::fmt1,fmt2

		fmt1='(3ES24.15)'


		NProc=params(1)
		IntSteps=params(2)
		qsteps=params(3)
		wsteps=params(4)

		open(unit=uno,file=nomo)

		write(uno,*) "### ",nomo
		write(uno,*) "###"
		write(uno,*) "# NProc, IntSteps, qsteps, wsteps"
		write(uno,'(4I10)') NProc,IntSteps,qsteps,wsteps
		write(uno,*) "# q/kf,  wstart, dw"
		write(uno,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."



		write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'

		allocate(wline(wsteps))

		wa=ys(1)
		wb=ys(2)
		dw=(wb-wa)/real(wsteps ,kind=8)

		qa=xs(1)
		qb=xs(2)
		dq=(qb-qa)/real(qsteps ,kind=8)

		qp=qa
		do i=1,qsteps
			w=wa
			write(uno,fmt1) qp/Globkf, w, dw
			do j=1,wsteps
				wline(j)=fn(qp,w)
				w=w+dw
			enddo
			write(uno,fmt2) wline
			qp=qp+dq
		enddo

		close(unit=uno)

	end subroutine FnToFile

	real (kind=8) function fn1(x)
		implicit none
		real (kind=8)::x

		fn1=1D0

	end function fn1


	real (kind=8) function fn0(x)
		implicit none
		real (kind=8)::x

		fn0=0D0

	end function fn0

	real (kind=8) function fn2(x,y)
		implicit none
		real (kind=8)::x,y

		fn2=x+y

	end function fn2


end module SelfConsistent
