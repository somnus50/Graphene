module PhysicsPol2DEG
use Constants
use Globals
implicit none
real (kind=8), parameter::  UUUU(4)=(/1D0,1D0,1D0,1D0/),&      !Svec=1
							DDDD(4)=(/-1D0,-1D0,-1D0,-1D0/),&  !Svec=2
							UDUD(4)=(/1D0,-1D0,1D0,-1D0/),&    !Svec=3
							DUDU(4)=(/-1D0,1D0,-1D0,1D0/)      !Svec=4

real(8), allocatable::SUDgrid(:),SUDspline(:),ReChiGrid(:,:),ImChiGrid(:,:),Modqgrid(:),Modwgrid(:)&
											,GSCold2(:),GSCspline2(:),ReChi0Grid(:,:),ImChi0Grid(:,:),fxcgrid(:),fxcspline(:)&
											,SDUgrid(:),SDUspline(:),SDUbargrid(:),SDUbarspline(:),Ssbargrid(:),Ssbarspline(:)
integer(4)::Modqsteps,Modwsteps



contains

	subroutine ConstInit(rs,z,Ef,eta)
		use Globals
		use fxc2DEG
		implicit none
		!integer (kind=4) ::
		real    (kind=8), intent(in) ::rs,z
		real    (kind=8), intent(out)::Ef,eta
		real    (kind=8)             ::n
		!real    (kind=8)::

			n=rs2n(rs)
			Globkf0=dsqrt(2D0*pi*n)
			Globkf=Globkf0
			Ef=(Globkf0**2)/2D0! = pi*n
			Globn=n
			eta=1D-4!*Ef
			GlobkfU=Globkf0*dsqrt(1D0+z)
			GlobkfD=Globkf0*dsqrt(1D0-z)
			GlobvfU=hbar*GlobkfU/m
			GlobvfD=hbar*GlobkfD/m
			Globnu=Globn*(1D0+z)/2D0
			Globnd=Globn*(1D0-z)/2D0
			GlobkfMAX=max(GlobkfU,GlobkfD)
			Globrs=rs
			GLobZeem=-2D0*Ef*z
			GlobZeemHalf=GlobZeem/2D0
			Globz=z
			!GlobStiff=S(rs,z)
			Globfx=-cfxssp(Globrs,Globz)
			Globfc=-cfcssp(Globrs,Globz)
	end subroutine ConstInit

	real (kind=8) function Norm(v)
		implicit none
		real (kind=8) ::v(2)

		!do i=1,len(v)
		Norm=(v(1))**2+(v(2))**2
		Norm=dsqrt(Norm)
	end function

	real (kind=8) function En(s,q)
		implicit none
		real (kind=8) ::q(2),s


			En=s*GlobZeem+(q(1))**2!(Norm(q))**2/2D0!/m*hbar**2
			En=En/2D0
	end function En

	real (kind=8) function FermiVec(q,delta)
		implicit none
		real (kind=8)::q(2),delta

		!FermiVec=Globkf
	end function FermiVec

	real (kind=8) function f(s,q)
		implicit none
		real (kind=8) ::q(2),s
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector

			f=0D0
			!!!Zero Temperature
			if ((En(s,q)).le.(GlobEf)) then
				f=1D0
			endif

			!f=0d0
			!if (q(1).lt.kfs2(s)) f=1D0


			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(s,q)-GlobEf)/kT))
	end function f

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector

		NVc=2D0*pi/Norm(q)!*e**2/kappa
	end function NVc

	real (kind=8) function Vc(q)
		implicit none
		real (kind=8) ::q
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector magnitude

		Vc=2D0*pi/abs(q)!*e**2/kappa
	end function Vc

	real (kind=8) function ReChi0sspInt(q,w,k,s,sp)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp,s,sp

		ReChi0sspInt=0D0
		kp=PolAdd(q,k)
		temp=f(s,k)-f(sp,kp)
		if (temp**2.gt.9D-1) then
			temp=temp*(w-En(s,k)+En(sp,kp))
			ReChi0sspInt=temp/(temp**2+Globeta**2)!*(-1D0) ! Not sure where the extra minus is coming from
		endif
	end function ReChi0sspInt

	real (kind=8) function ImChi0sspInt(q,w,k,s,sp)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),k(2),kp(2),w,temp,s,sp,temp2(2)

		ImChi0sspInt=0D0
		kp=PolAdd(q,k)
		temp=f(s,k)-f(sp,kp)
		!call Pol2Cart(k,temp2)
		if (temp**2.gt.9D-1) then
			!write(100,*) temp2/Globkf0,temp
			ImChi0sspInt=-temp*Globeta/((w-En(s,k)+En(sp,kp))**2+Globeta**2)!*(-1D0) ! Not sure where the extra minus is coming from
			return
		endif
		!write(100,*) temp2/Globkf0,0D0
	end function ImChi0sspInt

	real(8) function ReChiDU(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		real(8),external::fxc

		a=ReChi0sspNum(q,w,-1D0,1D0,50)
		b=ImChi0sspNum(q,w,-1D0,1D0,50)
		c=fxc(q,w)

		ReChiDU=(a+c*(a**2+b**2))/((1D0+c*a)**2+(c*b)**2)
	end function ReChiDU

	complex(8) function ChiUD(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		complex(8)::temp
		real(8),external::fxc

		temp=dcmplx(ReChi0sspNum(q,w,1D0,-1D0,50) , ImChi0sspNum(q,w,1D0,-1D0,50))
		c=fxc(q,w)

		ChiUD=temp/(1D0+c*temp)
	end function ChiUD

	complex(8) function ChiDU(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		complex(8)::temp
		real(8),external::fxc

		temp=dcmplx(ReChi0sspNum(q,w,1D0,-1D0,50) , ImChi0sspNum(q,w,1D0,-1D0,50))
		c=fxc(q,w)

		ChiDU=temp/(1D0+c*temp)
	end function ChiDU

	complex(8) function ChiUDA(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		complex(8)::temp
		real(8),external::fxc

		temp=-Chi0ssp(1,-1,q(1),w)
		c=fxc(q,w)

		ChiUDA=temp/(1D0+c*temp)
	end function ChiUDA

	complex(8) function ChiDUA(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		complex(8)::temp
		real(8),external::fxc

		temp=-Chi0ssp(-1,1,q(1),w)
		c=fxc(q,w)

		ChiDUA=temp/(1D0+c*temp)
	end function ChiDUA

	real(8) function ImChiDU(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		real(8),external::fxc

		a=ReChi0sspNum(q,w,-1D0,1D0,50)
		b=ImChi0sspNum(q,w,-1D0,1D0,50)
		c=fxc(q,w)

		ImChiDU=b/((1D0+c*a)**2+(c*b)**2)
	end function ImChiDU

	real(8) function ReChiUD(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		real(8),external::fxc

		a=ReChi0sspNum(q,w,1D0,-1D0,50)
		b=ImChi0sspNum(q,w,1D0,-1D0,50)
		c=fxc(q,w)

		ReChiUD=(a+c*(a**2+b**2))/((1D0+c*a)**2+(c*b)**2)
	end function ReChiUD

	real(8) function ImChiUD(q,w,fxc)
		implicit none
		real(8)::q(2),w,a,b,c
		real(8),external::fxc

		a=ReChi0sspNum(q,w,1D0,-1D0,50)
		b=ImChi0sspNum(q,w,1D0,-1D0,50)
		c=fxc(q,w)

		ImChiUD=b/((1D0+c*a)**2+(c*b)**2)
	end function ImChiUD


	real (kind=8) function PolIntegratek(fn,a,b,steps,q,w)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2)
		real (kind=8), external:: fn
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps, kind=8)
			dphi=twopi/real(steps, kind=8)
			PolIntegratek=0D0
			k=a
			phi=0D0
			do i=1,steps+1
				k=a
				do j=1,steps
					!kv=(/k,phi/)
					!PolIntegratek=PolIntegratek+k*fn(q,w,(/k,phi/))
					PolIntegratek=PolIntegratek+NC10Pol(k,k+dk,phi,fn,q,w)
					k=k+dk
				enddo
				phi=phi+dphi
			enddo
			PolIntegratek=PolIntegratek*dphi!*dk!/fourpisq

		contains

				real (kind=8) function NC10Pol(a,b,phi,fn,q,w)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,phi,q(2),w
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10Pol=0D0
					x=a
					dx=(b-a)/9D0
					do i=1,10
						NC10Pol=NC10Pol+Newt10(i)*x*fn(q,w,(/x,phi/))
						x=x+dx
					enddo
					NC10Pol=NC10Pol*dx
				end function NC10Pol
	end function PolIntegratek

	real (kind=8) function ImChi0num2(q,w,steps,s,sp)
		implicit none
		real (kind=8)::q(2),w,temp,s,sp
		integer (kind=4)::steps

		! q(1)+
		ImChi0num2=PolIntegratek(anc,0D0,GlobkfMAX,steps,q,w)/4D0/pi**2

		contains
		real (kind=8) function anc(q,w,k)
			implicit none
			real (kind=8)::q(2),w,k(2)

			anc=ImChi0sspInt(q,w,k,s,sp)
		end function anc
	end function ImChi0num2

	subroutine phirange0(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)
		real (kind=8), parameter::twopi=2D0*pi


		phis(2)=twopi
		phis(1)=0D0
	end subroutine phirange0

	subroutine phirange1(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)


		phis(1)=(k**2+q(1)**2-B**2)/(2D0*k*q(1))
		!if (dabs(phis(1)).gt.1D0) then
		!	phis=0D0
		!	return
		!endif
		phis(2)=pi-dacos(phis(1))
		phis(1)=-phis(2)
		phis(:)=phis(:)+q(2)
	end subroutine phirange1

	subroutine phirange2(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)
		real (kind=8), parameter::twopi=2D0*pi

		phis(1)=(k**2+q(1)**2-B**2)/(2D0*k*q(1))
		!if (dabs(phis(1)).gt.1D0) then
		!	phis=0D0
		!	return
		!endif
		phis(1)=pi-dacos(phis(1))
		phis(2)=twopi-phis(1)
		phis(:)=phis(:)+q(2)
		!write(*,*) "phirange2"
		!write(*,*) "k,q,qphi,B,phis"
		!write(*,*) k,q,B,phis,(k**2+q(1)**2-B**2)/(2D0*k*q(1))
	end subroutine phirange2

	subroutine phirange3(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)


		phis(2)=pi-dacos((A**2+q(1)**2-B**2)/(2D0*A*q(1)))
		phis(1)=-phis(2)
		phis(:)=phis(:)+q(2)
	end subroutine phirange3

	subroutine phirange4(k,q,A,B,phis)
		implicit none
		real (kind=8), intent(in)::k,q(2),A,B
		real (kind=8), intent(out)::phis(2)
		real (kind=8), parameter::twopi=2D0*pi

		phis(1)=pi-dacos((A**2+q(1)**2-B**2)/(2D0*A*q(1)))
		phis(2)=twopi-phis(1)
		phis(:)=phis(:)+q(2)
	end subroutine phirange4

	subroutine krange1(phi,q,A,B,ks)
		implicit none
		real (kind=8),intent(in)::phi,q(2),A,B
		real (kind=8),intent(out)::ks(2)

			ks(1)=0D0
			ks(2)=q(1)*dcos(pi-phi+q(2))-dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
	end subroutine krange1

	subroutine krange2(phi,q,A,B,ks)
		implicit none
		real (kind=8),intent(in)::phi,q(2),A,B
		real (kind=8),intent(out)::ks(2)

			ks(1)=A
			ks(2)=q(1)*dcos(pi-phi+q(2))+dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
	end subroutine krange2

	subroutine krange3(phi,q,A,B,ks)
		implicit none
		real (kind=8),intent(in)::phi,q(2),A,B
		real (kind=8),intent(out)::ks(2)

			ks(2)=A
			ks(1)=q(1)*dcos(pi-phi+q(2))+dsqrt(B**2-q(1)**2*dsin(pi-phi+q(2))**2)
	end subroutine krange3

	real (kind=8) function ImChi0sspNum(q,w,s,sp,steps)
		implicit none
		real (kind=8)::q(2),w,eps2,A,B,temp1,temp2,temp3,phis(2),s,sp
		integer (kind=4)::steps
		integer (kind=4),parameter::adj=5
		real (kind=8), parameter::temp=1D0/4D0/pi**2

		A=kfs(s)
		B=kfs(sp)
		!write(*,*) "A,B:", A, B
		eps2=1D-8
		ImChi0sspNum=0D0

		temp1=0D0 ! Region 1
		temp2=0D0 ! Region 2
		temp3=0D0 ! Region 3

		if (q(1).lt.dabs(B-A)) then
			!write(*,*) "q<|B-A|"
			if (A.lt.B) then
				!write(*,*) "A<B"
				temp3=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange2,steps*max(adj/4,1),q,w)
			elseif (q(1).lt.B) then
				!write(*,*) "q<B"
				temp1=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange3,steps*max(adj/4,1),q,w)
			elseif (A.gt.2D0*B) then
					!write(*,*) "A>2B"
					temp1=PolIntegratephi12(anc,0D0,q(1)-B,A,B,phirange0,steps*adj,q,w)
					temp2=PolIntegratephi12(anc,q(1)-B+eps2,q(1)+B-eps2,A,B,phirange1,steps*adj,q,w)
					temp3=PolIntegratephi12(anc,q(1)+B,A,A,B,phirange0,steps*adj,q,w) !This is actually region 4. I didn't want to make another variable
			else
				!write(*,*) "This should never happen."
			endif
		elseif (q(1).lt.B) then
			!write(*,*) "|B-A|<q<B"
			temp1=PolIntegratephi12(anc,B-q(1)+eps2,A-eps2,A,B,phirange1,steps*adj,q,w) !Broken
			temp3=PolIntegratephi12(anc,A+eps2,B+q(1)-eps2,A,B,phirange2,steps*adj,q,w)
		elseif (q(1).lt.(A+B)) then
			!write(*,*) "B<q<B+A"
			temp1=PolIntegratephi12(anc,0D0,q(1)-B,A,B,phirange0,steps*adj*2,q,w)
			temp2=PolIntegratephi12(anc,q(1)-B+eps2,A,A,B,phirange1,steps*adj*2,q,w)
			temp3=PolIntegratephi12(anc,A+eps2,B+q(1)-eps2,A,B,phirange2,steps*adj*2,q,w)
		elseif (q(1).ge.(A+B)) then
			!write(*,*) "q>B+A"
			temp1=PolIntegratephi12(anc,0D0,A-eps2,A,B,phirange0,steps*adj,q,w)
			temp3=PolIntegratephi12(anc,q(1)-B+eps2,B+q(1)-eps2,A,B,phirange2,steps*adj,q,w)
		endif

		ImChi0sspNum=(temp1+temp2+temp3)*temp
		contains
			real (kind=8) function anc(q,w,k)
				implicit none
				real (kind=8)::q(2),w,k(2)

				anc=ImChi0sspInt(q,w,k,s,sp)
			end function anc
			real (kind=8) function kfs(s)
				implicit none
				real (kind=8)::s

					if (s.eq.1D0) then
						kfs=GLobkfU
					elseif (s.eq.-1D0) then
						kfs=GlobkfD
					else
						write(*,*) "Improper value passed to ImChi0sspNum.kfs(s)"
						write(*,*) "s=+-1D0, given:",s
						call exit(- int(s))
					endif
			end function kfs
	end function ImChi0sspNum

	real (kind=8) function ReChi0sspNum(q,w,s,sp,steps)
		implicit none
		real (kind=8)::q(2),w,eps2,A,B,temp1,temp2,temp3,phis(2),s,sp
		integer (kind=4)::steps
		integer (kind=4),parameter::adj=5
		real (kind=8), parameter::temp=1D0/4D0/pi**2

		A=kfs(s)
		B=kfs(sp)
		!write(*,*) "A,B:", A, B
		eps2=1D-5
		ReChi0sspNum=0D0

		temp1=0D0 ! Region 1
		temp2=0D0 ! Region 2
		temp3=0D0 ! Region 3

		if (q(1).lt.dabs(B-A)) then
			!write(*,*) "q<|B-A|"
			if (A.lt.B) then
				!write(*,*) "A<B"
				temp3=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange2,steps*max(adj/4,1),q,w)
			elseif (q(1).lt.B) then
				!write(*,*) "q<B"
				temp1=PolIntegratek12(anc,0D0,2D0*pi,A,B,krange3,steps*max(adj/4,1),q,w)
			elseif (A.gt.2D0*B) then
					!write(*,*) "A>2B"
					temp1=PolIntegratephi12(anc,0D0,q(1)-B,A,B,phirange0,steps*adj,q,w)
					temp2=PolIntegratephi12(anc,q(1)-B+eps2,q(1)+B-eps2,A,B,phirange1,steps*adj,q,w)
					temp3=PolIntegratephi12(anc,q(1)+B,A,A,B,phirange0,steps*adj,q,w) !This is actually region 4. I didn't want to make another variable
			else
				!write(*,*) "This should never happen."
			endif
		elseif (q(1).lt.B) then
			!write(*,*) "|B-A|<q<B"
			temp1=PolIntegratephi12(anc,B-q(1)+eps2,A-eps2,A,B,phirange1,steps*adj,q,w) !Broken
			temp3=PolIntegratephi12(anc,A+eps2,B+q(1)-eps2,A,B,phirange2,steps*adj,q,w)
		elseif (q(1).lt.(A+B)) then
			!write(*,*) "B<q<B+A"
			temp1=PolIntegratephi12(anc,0D0,q(1)-B,A,B,phirange0,steps*adj*2,q,w)
			temp2=PolIntegratephi12(anc,q(1)-B+eps2,A,A,B,phirange1,steps*adj*2,q,w)
			temp3=PolIntegratephi12(anc,A+eps2,B+q(1)-eps2,A,B,phirange2,steps*adj*2,q,w)
		elseif (q(1).ge.(A+B)) then
			!write(*,*) "q>B+A"
			temp1=PolIntegratephi12(anc,0D0,A-eps2,A,B,phirange0,steps*adj,q,w)
			temp3=PolIntegratephi12(anc,q(1)-B+eps2,B+q(1)-eps2,A,B,phirange2,steps*adj,q,w)
		endif

		ReChi0sspNum=(temp1+temp2+temp3)*temp
		contains
			real (kind=8) function anc(q,w,k)
				implicit none
				real (kind=8)::q(2),w,k(2)

				anc=ReChi0sspInt(q,w,k,s,sp)
			end function anc
			real (kind=8) function kfs(s)
				implicit none
				real (kind=8)::s

					if (s.eq.1D0) then
						kfs=GLobkfU
					elseif (s.eq.-1D0) then
						kfs=GlobkfD
					else
						write(*,*) "Improper value passed to ReChi0sspNum.kfs(s)"
						write(*,*) "s=+-1D0, given:",s
						call exit(- int(s))
					endif
			end function kfs
	end function ReChi0sspNum

	real (kind=8) function PolIntegratephi11(fn,a,b,A1,B1,phisub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),B1,A1
		real (kind=8):: res,temp,phis(2),kf1,kf2,eps
		real (kind=8),allocatable::temp1(:)
		real (kind=8), external:: fn
		external :: phisub
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			phisteps=2
			eps=0D-2
			ksteps=(steps/9)*9 +1
			dk=(b-a)/real(ksteps, kind=8)
			PolIntegratephi11=0D0
			k=a
			allocate(temp1(ksteps))
			do i=1,ksteps
					res=0D0
					!write(*,*) "IntStep:",i,j,k/Globkf0, q/Globkf0, w
					!call phirange(k,q,kf1,kf2,phis)
					!phis(1)=-phi1(k,q(1),B1)
					!phis(2)=-phis(1)
					call phisub(k,q,A1,B1,phis)
					!write(*,*) "phis",phis
					!dphi=(phis(2)-phis(1))/real(phisteps, kind=8)
					!do j=1,phisteps
						!if (anc(phis(2)).ne.0D0) then
					!		exit
						!endif
						!phis(2)=phis(2)-dphi
					!enddo
					!call StupidIntegrateNC(anc,phis(1),phis(2)-eps,steps,res,ier)
					call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
					!temp=temp+res
				temp1(i)=res!temp
				k=k+dk
			enddo
			!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


			t1=1
			t2=10
			do i=1,(ksteps/9)
				PolIntegratephi11=PolIntegratephi11+NC10PolList(dk,temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratephi11=PolIntegratephi11*dk    !/fourpisq     !NC10


		contains

			real (kind=8) function anc(phi)
				implicit none
				real (kind=8)::phi

				anc=k*fn(q,w,(/k,phi/))
			end function anc

			real (kind=8) function phi1(k,q,B1)
				implicit none
				real (kind=8)::k,q,B1

				phi1=dacos((k**2+q**2-B1**2)/(2D0*k*q))
			end function phi1

			real (kind=8) function NC10PolList(dx,flist)
				integer (kind=4)::i
				real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
				real (kind=8), external::fn
				real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
				!NC10PolList=NC10PolList*dx/9D0
			end function NC10PolList
	end function PolIntegratephi11


	real (kind=8) function PolIntegratephi12(fn,a,b,A1,B1,phisub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer(4)::steps
		real (kind=8):: q(2),w,B1,A1,a,b,res1
		real (kind=8), external:: fn
		external :: phisub
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			PolIntegratephi12=0D0

			call GaussAdaptQuad(anc2,a,b,1D-8,res1)
			PolIntegratephi12=res1

		contains

			real (kind=8) function anc(phi)
				implicit none
				real (kind=8)::phi

				anc=GlobkInt*fn(q,w,(/GlobkInt,phi/))
			end function anc

			real (kind=8) function anc2(k)
				use AuxiliaryFunctions
				implicit none
				real (kind=8)::k,phis(2),res

				GlobkInt=k
				call phisub(k,q,A1,B1,phis)
				call GaussAdaptQuad2(anc,phis(1),phis(2),1D-8,res)
				anc2=res
			end function anc2

	end function PolIntegratephi12


	real (kind=8) function PolIntegratek11(fn,a,b,A1,B1,ksub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),B1,A1
		real (kind=8):: res,temp,ks(2),kf1,kf2,eps
		real (kind=8),allocatable::temp1(:)
		real (kind=8), external:: fn
		external :: ksub
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			phisteps=2
			eps=0D-2
			phisteps=(steps/9)*9 +1
			dphi=(b-a)/real(phisteps, kind=8)
			PolIntegratek11=0D0
			phi=a
			allocate(temp1(phisteps))
			do i=1,phisteps
					res=0D0
					!write(*,*) "IntStep:",i,j,k/Globkf0, q/Globkf0, w
					!call phirange(k,q,kf1,kf2,phis)
					!phis(1)=-phi1(k,q(1),B1)
					!phis(2)=-phis(1)
					call ksub(phi,q,A1,B1,ks)
					!write(*,*) "phis",phis
					!dphi=(phis(2)-phis(1))/real(phisteps, kind=8)
					!do j=1,phisteps
						!if (anc(phis(2)).ne.0D0) then
					!		exit
						!endif
						!phis(2)=phis(2)-dphi
					!enddo
					!call StupidIntegrateNC(anc,phis(1),phis(2)-eps,steps,res,ier)
					call GaussAdaptQuad(anc,ks(1),ks(2),1D-8,res)
					!temp=temp+res
				temp1(i)=res!temp
				phi=phi+dphi
			enddo
			!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


			t1=1
			t2=10
			do i=1,(phisteps/9)
				PolIntegratek11=PolIntegratek11+NC10PolList(dphi,temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratek11=PolIntegratek11*dphi    !/fourpisq     !NC10


		contains

			real (kind=8) function anc(k)
				implicit none
				real (kind=8)::k

				anc=k*fn(q,w,(/k,phi/))
			end function anc

			real (kind=8) function NC10PolList(dx,flist)
				integer (kind=4)::i
				real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
				real (kind=8), external::fn
				real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
				!NC10PolList=NC10PolList*dx/9D0
			end function NC10PolList
	end function PolIntegratek11

	real (kind=8) function PolIntegratek12(fn,a,b,A1,B1,ksub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: steps
		real (kind=8):: q(2),w,B1,A1,res1,a,b
		real (kind=8), external:: fn
		external :: ksub
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			PolIntegratek12=0D0
			call GaussAdaptQuad(anc2,a,b,1D-8,res1)
			PolIntegratek12=res1


		contains

			real (kind=8) function anc(k)
				implicit none
				real (kind=8)::k


				anc=k*fn(q,w,(/k,GlobphiInt/))
			end function anc

			real(8) function anc2(phi)
				implicit none
				real(8):: phi,res,ks(2)

				GlobphiInt=phi
				call ksub(phi,q,A1,B1,ks)
				call GaussAdaptQuad2(anc,ks(1),ks(2),1D-8,res)
				anc2=res
			end function anc2

	end function PolIntegratek12


	real (kind=8) function PolIntegratek3(fn,a,b,kf1,kf2,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer (kind=4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,kv(2),res,temp,phis(2),kf1,kf2,eps
		real (kind=8),allocatable::temp1(:)
		real (kind=8), external:: fn
		real (kind=8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			phisteps=2
			eps=0D-2
			ksteps=(steps/9)*9 +1
			dk=(b-a)/real(ksteps, kind=8)
			PolIntegratek3=0D0
			k=a
			allocate(temp1(ksteps))
			do i=1,ksteps
					res=0D0
					!write(*,*) "IntStep:",i,j,k/Globkf0, q/Globkf0, w
					!call phirange(k,q,kf1,kf2,phis)

					dphi=(phis(2)-phis(1))/real(phisteps, kind=8)
					do j=1,phisteps
						!if (anc(phis(2)).ne.0D0) then
							exit
						!endif
						!phis(2)=phis(2)-dphi
					enddo
					!call StupidIntegrateNC(anc,phis(1),phis(2)-eps,steps,res,ier)
					call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
					!temp=temp+res
				temp1(i)=res!temp
				k=k+dk
			enddo
			!PolIntegratek=PolIntegratek*dphi*dk!/fourpisq !Stupid Mid-Point


			t1=1
			t2=10
			do i=1,(ksteps/9)
				PolIntegratek3=PolIntegratek3+NC10PolList(dk,temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratek3=PolIntegratek3*dk    !/fourpisq     !NC10


		contains

			real (kind=8) function anc(phi)
				implicit none
				real (kind=8)::phi

				anc=fn(q,w,(/k,phi/))
			end function anc

			real (kind=8) function NC10PolList(dx,flist)
				integer (kind=4)::i
				real (kind=8) ::a,b,x,dx,phi,q(2),w,flist(10)
				real (kind=8), external::fn
				real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
				!NC10PolList=NC10PolList*dx/9D0
			end function NC10PolList
	end function PolIntegratek3






	subroutine ReNEpsLine(q,wstart,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,wstep
		real (kind=8) ::q,phi,dw,w,qv(2),wstart
		real (kind=8) ::LindLine(wsteps),EpsLine(wsteps)

		phi=0D0
		qv=q*(/dcos(phi),dsin(phi)/)
			w=wstart
		do wstep=1,wsteps
				EpsLine(wstep)=1D0+(2D0*(NVc(qv))*LindLine(wstep))
				w=w+dw
		enddo
	end subroutine ReNEpsLine

	subroutine ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2),bp
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs!(Gstep,:)
				!EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G))*KsiGb(bp,qv,G)*LindLine(wstep))
			enddo
		enddo
	end subroutine ReNEpsLineb




	real (kind=8) function PolIntegrate(fn,a,b,steps,q,w,G)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,G
		real (kind=8), external:: fn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps)
			dphi=2D0*pi/real(steps)
			PolIntegrate=0D0
			k=a
			phi=0D0
			do i=0,steps
				do j=0,steps
				PolIntegrate=PolIntegrate+fn(k,phi,q,w)
				phi=phi+dphi
				enddo
				k=k+dk
				phi=0D0
			enddo
			PolIntegrate=PolIntegrate*dk*dphi/(4D0*pi**2)
	end function PolIntegrate


	real (kind=8) function PolIntegrate2(a,b,steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine,a,b,khat(2)
		real (kind=8), external:: fn

		! NCorder must match what was chosen in ReNLind2
		NCorder=1

		!bk=0D0
		PolIntegrate2=0D0
		dphi=2D0*pi/real(NCorder*steps, kind=8)
		phi=0D0
		k=a
		bk=b
		do phistep=1,NCorder*steps!+1
			!bk=min(FermiVec((/dcos(phi),dsin(phi)/),1D-6),b)
			!bk=a+1D0 !DEBUG
			!if (bk.le.a) cycle !if the integration range is above the Fermi Vector then cycle back since it will be 0
			philine=0D0
			dk=(bk-a)/real(NCorder*steps, kind=8)
			khat=(/dcos(phi),dsin(phi)/)
			!k=dk


			!!!NewtonCotes
			do kstep=1,NCorder*steps!+1
				philine=philine+dk*k*fn(q,w,k*khat)  									!NC1
				!philine=philine+IntCoeff(s,kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC4
				!philine=philine+BooleCoeff(s,kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC5
				!philine=philine+IntCoeff10(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC10
				!philine=philine+IntCoeff11(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC11
				k=k+dk
			enddo

			PolIntegrate2=PolIntegrate2+philine  									!1
			!PolIntegrate=PolIntegrate+IntCoeff(s,phistep,NCorder*steps+1)*philine  	!4
			!PolIntegrate=PolIntegrate+BooleCoeff(s,phistep,NCorder*steps+1)*philine  !5
			!PolIntegrate=PolIntegrate+IntCoeff10(phistep,NCorder*steps+1)*philine  !10
			!PolIntegrate=PolIntegrate+IntCoeff11(phistep,NCorder*steps+1)*philine  	!11
			!!!NewtonCotes
			phi=phi+dphi
		enddo
		PolIntegrate2=PolIntegrate2*dphi
	end function PolIntegrate2

	real (kind=8) function PolIntegrate3(a,b,rsteps,phisteps,fn,q,w)
		implicit none
		integer (kind=4):: rsteps,phisteps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine,a,b,khat(2)
		real (kind=8), external:: fn



		PolIntegrate3=0D0
		dphi=2D0*pi/real(phisteps, kind=8)
		phi=0D0
		k=a
		do phistep=1,phisteps!+1
			philine=0D0
			dk=(b-a)/real(rsteps, kind=8)
			khat=(/dcos(phi),dsin(phi)/)
			k=a

			!!!NewtonCotes
			do kstep=1,rsteps!+1
				philine=philine+dk*k*fn(q,w,k*khat)  									!NC1
				k=k+dk
			enddo

			PolIntegrate3=PolIntegrate3+philine  									!1
			!!!NewtonCotes
			phi=phi+dphi
		enddo



		PolIntegrate3=PolIntegrate3*dphi
	end function PolIntegrate3


    complex (kind=8) function Chi0ssp(s,sp,q,w)
        implicit none
        real    (kind=8)::q,w,v(2),qbs(2),temp1,temp2,temp3,temp4,wssp
        integer (kind=4)::s,sp
        real (kind=8), parameter::nzero=1D0/2D0/pi, fix=1D-20


        qbs=(/q/kfs(s),q/kfs(sp)/)

				wssp=w-GlobZeem

        v(1)=wssp/q/kfs(sp)-qbs(2)/2D0
        v(2)=wssp/q/kfs(s)+qbs(1)/2D0


				! Real Part
        temp1=v(1)
        if ((v(1)**2-1D0).gt.(0D0)) then
            temp1=(temp1-sign(1D0,v(1))*dsqrt(v(1)**2-1D0))
        endif
        temp1=temp1/qbs(2)
        temp2=v(2)
        if ((v(2)**2-1D0).gt.(0D0)) then
            temp2=-(temp2-sign(1D0,v(2))*dsqrt(v(2)**2-1D0))
        endif
        temp2=temp2/qbs(1)

				! Imaginary Part
        temp3=0D0!fix
        if ((1D0-v(1)**2).gt.0D0) then
            temp3=-dsqrt(1D0-v(1)**2)/qbs(2)
        endif
        ! temp3=temp3/qbs(2)
        temp4=0D0!fix
        if ((1D0-v(2)**2).gt.0D0) then
            temp4=dsqrt(1D0-v(2)**2)/qbs(1)
        endif
        ! temp4=temp4/qbs(1)

        Chi0ssp=dcmplx(nzero*(temp1+temp2),nzero*(temp3+temp4))


    end function Chi0ssp

		complex (kind=8) function Chi0ssp2(s,sp,q,w)
        implicit none
        real    (kind=8)::q,w,v(2),qbs(2),temp1,temp2,temp3,temp4
        integer (kind=4)::s,sp
        real (kind=8), parameter::nzero=1D0/2D0/pi, fix=1D-20


				Chi0ssp2=(0D0,0D0)

				if ( q.eq.0D0 ) return

        qbs=(/q/kfs(s),q/kfs(sp)/)

        v(1)=wssp(s,sp,w)/kfs(sp)-q*qbs(2)/2D0
        v(2)=wssp(s,sp,w)/kfs(s)+q*qbs(1)/2D0


				! Real Part
        temp1=0D0!v(1)
        if ((v(1)**2-q**2).gt.(0D0)) then
						temp1=v(1)/q
            temp1=(temp1-sign(1D0,temp1)*dsqrt(temp1**2-1D0))/qbs(2)
        endif
        ! temp1=temp1/qbs(2)
        temp2=0D0!v(2)
        if ((v(2)**2-q**2).gt.(0D0)) then
						temp2=v(2)/q
            temp2=-(temp2-sign(1D0,temp2)*dsqrt(temp2**2-1D0))/qbs(1)
        endif
        ! temp2=-temp2/qbs(1)

				! Imaginary Part
        temp3=0D0!fix
        if ((q**2-v(1)**2).gt.0D0) then
						temp3=v(1)/q
            temp3=-dsqrt(1D0-temp3**2)/qbs(2)
        endif
        ! temp3=temp3/qbs(2)
        temp4=0D0!fix
        if ((q**2-v(2)**2).gt.0D0) then
						temp4=v(2)/q
            temp4=dsqrt(1D0-temp4**2)/qbs(1)
        endif
        ! temp4=temp4/qbs(1)

        Chi0ssp2=dcmplx(nzero*(temp1+temp2),nzero*(temp3+temp4))

        contains

            real (kind=8) function wssp(s,sp,w)
                implicit none
                integer (kind=4)::s,sp
                real    (kind=8)::w
                real    (kind=8), parameter:: zero(2)=0D0

                wssp=w+En(real(sp, kind=8),zero)-En(real(s, kind=8),zero)
            end function wssp
    end function Chi0ssp2


	complex (kind=8) function Chissp(s,sp,q,w)
		use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::s,sp
		real    (kind=8)::q,w,scvar,qp
		complex (kind=8)::cz

		!write(*,*) "c0",cz,q,w
		if (q.lt.qgrid(qsteps)) then
			qp=q
		else
			qp=qgrid(qsteps)
		endif

		cz=Chi0ssp(s,sp,qp,w)
		!cz=dcmplx(ReChi0sspNum((/qp,0D0/),w,real(s,kind=8),real(sp,kind=8),1)&
		!				 ,ImChi0sspNum((/qp,0D0/),w,real(s,kind=8),real(sp,kind=8),1))



		call splint(qgrid,GSCold,GSCspline,qsteps,qp,scvar)

		!fxc=fxcssp(s,sp,q)


		!if (s.ne.sp) then ! UDUD or DUDU
			!Chissp=cz/(one-scvar*cz)  ! fxc is self-consistently solved
			Chissp=cz/(one+Vc(qp)*scvar*cz)  ! G is self-consistently solved
		!else              ! UUUU, DDDD, UUDD, DDUU. Will get to this later.
		!	Chissp=0D0
		!endif
		!write(*,*) "c",Chissp
	end function Chissp

	complex (kind=8) function Chisspbar(s,sp,p,tt)
		use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::s,sp
		real    (kind=8)::p,tt,q,w,scvar
		complex (kind=8)::cz

		q=(1D0-p)/p
		w=(1D0-tt)/tt


		Chisspbar=(0D0,0D0)
		if ( (p.eq.0D0).or.(p.eq.1D0) ) return

		cz=Chi0ssp2(s,sp,q,w)
		!cz=dcmplx(ReChi0sspNum((/qp,0D0/),w,real(s,kind=8),real(sp,kind=8),1)&
		!				 ,ImChi0sspNum((/qp,0D0/),w,real(s,kind=8),real(sp,kind=8),1))



		call splint(qgrid,GSCold,GSCspline,qsteps,p,scvar)

		!fxc=fxcssp(s,sp,q)


		!if (s.ne.sp) then ! UDUD or DUDU
			!Chissp=cz/(one-scvar*cz)  ! fxc is self-consistently solved
			Chisspbar=cz/(one+Vc(q)*scvar*cz)  ! G is self-consistently solved
		!else              ! UUUU, DDDD, UUDD, DDUU. Will get to this later.
		!	Chissp=0D0
		!endif
		!write(*,*) "c",Chissp
	end function Chisspbar

	real (kind=8) function fxcssp(s,sp,q)
		use QuadPackDouble
		use STLS
		implicit none
		integer (kind=4)::s,sp
		real    (kind=8)::q,temp1,temp2,temp3,a,nsp
		integer (kind=4), parameter::limit=10000
		real (kind=8)   , parameter::minpi=1D0/2D0/pi, tol=1D-9
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit)

		nsp=ns(sp)

		if (s.ne.sp) then
			a=4D0/Globn**2
		else
			a=1D0/ns(s)**2
		endif

		fxcssp=0D0

		!call StupidIntegrate(anc,1D-12,10D0*Globkf0,1000,temp,ier)
		call dqage ( anc, 0d0, 1D0, tol, tol, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 20D0, tol, tol, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 20D0, 1, tol, tol, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		fxcssp=a*(temp1+temp2+temp3)*q*8D0*pi

		contains
			real (kind=8) function anc(x)
				use Elliptic
				implicit none
				real (kind=8)::x

				anc=(Structssp(s,sp,(q*x))-nsp)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)

			end function anc

			real (kind=8) function anc2(qp,t)
				implicit none
				real (kind=8)::qp,t,temp

				temp=dsqrt(q**2+qp**2-2D0*q*qp*dcos(t))
				anc2=Structssp(s,sp,temp)-nsp

			end function anc2

			real (kind=8) function ns(s)
				implicit none
				integer (kind=4)::s

				if (s.eq.1) then
					ns=Globnu
				elseif (s.eq.-1) then
					ns=Globnd
				else
					write(*,*) "Bad spin index value. s=+-1"
					write(*,*) "s:",s
					call exit(-s)
				endif
			end function ns
	end function fxcssp



	real (kind=8) function fxcUD(q)
		implicit none
		real (kind=8)::q

		fxcUD=fxcssp(1,-1,q)
	end function fxcUD

	real (kind=8) function fxcDU(q)
		implicit none
		real (kind=8)::q

		fxcDU=fxcssp(-1,1,q)
	end function fxcDU

	real (kind=8) function fxcDU2(q,w)
		use fxc2DEG
		implicit none
		real (kind=8)::q(2),w

		fxcDU2=(cfxssp(Globrs,Globz)+cfcssp(Globrs,Globz))
	end function fxcDU2

	real(8) function PGGUD(q,w)
		use fxc2DEG
		implicit none
		real(8)::q(2),w

		PGGUD=PGGDU(q(1))!+cfcssp(Globrs,Globz)
	end function PGGUD

	real (kind=8) function Structssp(s,sp,q)
		use STLS
		use AuxiliaryFunctions
		use QuadPackDouble2
		implicit none
		integer (kind=4)::s,sp
		real    (kind=8)::q
		real (kind=8), parameter::a=-1D0/pi
		integer (kind=4), parameter::limit=10000
		real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-8
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,k1,k2,en1,high,low,v(2),qbs(2),temp1&
							,temp2,temp3,temp4,temp5,lows(2),highs(2)



		Structssp=0D0

		if (q.eq.0D0) return

		k1=kfs(s)
		k2=kfs(sp)
		en1=(Ens(sp)-Ens(s))

		qbs=(/q/k1,q/k2/)

		v(1)=en1/q/k2-qbs(2)/2D0
		v(2)=en1/q/k1+qbs(1)/2D0

		low=max(min(-(q*k1+q**2/2D0+en1),(-q*k2+q**2/2D0-en1)),0D0)
		high=max(q*k1-q**2/2D0-en1,q*k2+q**2/2D0-en1,0D0)

		lows=(/max(-(q*k1+q**2/2D0+en1),0D0),max(0D0,-q*k2+q**2/2D0-en1)/)
		!highs=(/max(q*k1-q**2/2D0-en1,0D0),max(q*k2+q**2/2D0-en1,0D0)/)
		highs=(/q*k1-q**2/2D0-en1,q*k2+q**2/2D0-en1/)



		!!! Sort the limits
		if (lows(2).lt.lows(1)) then
			lows=(/lows(2),lows(1)/)
			highs=(/highs(2),highs(1)/)
		endif


		!!! Check if limits overlap and adjust accordingly
		if (lows(2).lt.highs(1)) then
			lows(2)=highs(1)
		endif


		!!! Check if second interval is contained by first
		if (highs(2).lt.highs(1)) then
			highs(2)=lows(2)
		endif

		!call dqage ( anc, low, high, tol, tol, 2, limit , temp, abserr, &
		!			  neval, ier, alist, blist, rlist, elist, iord, last )


		temp1=0D0
		temp2=0D0
		temp3=0D0
		temp4=0D0
		temp5=0D0

		if (lows(1).ne.highs(1)) then
			! call dqage ( anc, lows(1), highs(1), tol, tol, 2, limit , temp1, abserr, &
			! 			  neval, ier, alist, blist, rlist, elist, iord, last )
			call GaussAdaptQuad(anc,lows(1), highs(1),tol,temp1)
		endif

		if (lows(2).ne.highs(2)) then
			! call dqage ( anc, lows(2), highs(2), tol, tol, 2, limit , temp2, abserr, &
			! 			  neval, ier, alist, blist, rlist, elist, iord, last )
							call GaussAdaptQuad(anc,lows(2), highs(2),tol,temp2)
		endif

		if (lows(1).gt.0D0) then
			! call dqage ( anc, 0d0, lows(1), tol, tol, 2, limit , temp3, abserr, &
			! 				neval, ier, alist, blist, rlist, elist, iord, last )
							call GaussAdaptQuad(anc,0d0, lows(1),tol,temp3)
		endif

		if (highs(1).lt.lows(2)) then
			! call dqage ( anc, highs(1), lows(2), tol, tol, 2, limit , temp4, abserr, &
			! 				neval, ier, alist, blist, rlist, elist, iord, last )
							call GaussAdaptQuad(anc,highs(1), lows(2),tol,temp4)
		endif

		call dqagie ( anc, highs(2), 1, tol, tol, limit , temp5, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )


!		call dqagie ( anc, 0D0, 1, tol, tol, limit , temp5, abserr, &
!					  neval, ier, alist, blist, rlist, elist, iord, last )

		!call StupidIntegrate2(anc,low,high,300,temp,ier)

		!call StupidIntegrateNC2(anc,lows(1),highs(1),5000,temp,ier)

		!call StupidIntegrateNC2(anc,lows(2),highs(2),5000,temp,ier)

		temp=temp1+temp2+temp3+temp4+temp5

		!write(*,*) low,high,temp


		!!!! Explicitly integrating each part of ImChi0

		!call dqage ( anc2, max((-q*k2+q**2/2D0-en1),0D0), q*k2+q**2/2D0-en1, tol, tol, 2, limit , temp1, abserr, &
		!			  neval, ier, alist, blist, rlist, elist, iord, last )

		!call dqage ( anc3,max(-(q*k1+q**2/2D0+en1),0D0) , q*k1-q**2/2D0-en1, tol, tol, 2, limit , temp2, abserr, &
		!				neval, ier, alist, blist, rlist, elist, iord, last )

		!Structssp=a*(temp1+temp2)/2D0/pi

		if (ier.gt.0) then
			write(*,*) "Integration of ImChi within Struct has failed."
			write(*,*) "dqagie threw an error, ier:",ier
			write(*,*) "See documentation for more information."
			call exit(-ier)
		endif

		Structssp=a*temp

		contains

			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				anc=dimag(Chissp(s,sp,q,w))
				!write(*,*) q,w,anc

			end function anc

			real (kind=8) function anc2(w)
				implicit none
				real (kind=8)::w

				anc2=0D0
				if ((1D0-v(1)**2).gt.0D0) then
						anc2=-dsqrt(1D0-v(1)**2)
				endif
				anc2=anc2/qbs(2)

			end function anc2

			real (kind=8) function anc3(w)
				implicit none
				real (kind=8)::w

				anc3=0D0
				if ((1D0-v(2)**2).gt.0D0) then
						anc3=dsqrt(1D0-v(2)**2)
				endif
				anc3=anc3/qbs(1)
			end function anc3
	end function Structssp


		real (kind=8) function Structssp2(s,sp,q)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble2
			implicit none
			integer (kind=4)::s,sp
			real    (kind=8)::q
			real (kind=8), parameter::a=-1D0/pi
			integer (kind=4), parameter::limit=10000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-8
			integer (kind=4)::neval,ier,last,iord(limit)
			real (kind=8)   ::temp,k1,k2,en1,high,low,v(2),qbs(2),temp1&
								,temp2,temp3,temp4,temp5,lows(2),highs(2),q22,bb,aa



			Structssp2=0D0

			k1=kfs(s)
			k2=kfs(sp)
			en1=-(Ens(sp)-Ens(s))

			qbs=(/q/k1,q/k2/)

			v(1)=en1/q/k2-qbs(2)/2D0
			v(2)=en1/q/k1+qbs(1)/2D0

			q22=q**2/2D0


			temp1=0D0
			temp2=0D0
			temp3=0D0
			temp4=0D0
			! temp5=0D0

			bb=q22+q*k2+en1
			if ( bb.gt.0D0 ) then
				aa=max(0D0,q22+en1)
				write(*,*) "Structssp2.1 a,b:",aa,bb,q
				call GaussAdaptQuad(anc,aa,bb,tol,temp1)
			end if

			bb=q22+en1
			if ( bb.gt.0D0 ) then
				aa=max(0D0,q22-q*k2+en1)
				write(*,*) "Structssp2.2 a,b:",aa,bb,q
				call GaussAdaptQuad(anc,aa,bb,tol,temp2)
			end if

			bb=-q22+q*k1+en1
			if ( bb.gt.0D0 ) then
				aa=max(0D0,-q22+en1)
				write(*,*) "Structssp2.3 a,b:",aa,bb,q
				call GaussAdaptQuad(anc,aa,bb,tol,temp3)
			end if

			bb=-q22+en1
			if ( bb.gt.0D0 ) then
				aa=max(0D0,-q22-q*k1+en1)
				write(*,*) "Structssp2.4 a,b:",aa,bb,q
				call GaussAdaptQuad(anc,aa,bb,tol,temp4)
			end if


			temp=temp1+temp2+temp3+temp4!+temp5

			ier=0

			if (ier.gt.0) then
				write(*,*) "Integration of ImChi within Struct has failed."
				write(*,*) "dqagie threw an error, ier:",ier
				write(*,*) "See documentation for more information."
				call exit(-ier)
			endif

			Structssp2=a*temp

			contains

				real (kind=8) function anc(w)
					implicit none
					real (kind=8)::w

					anc=dimag(Chissp(s,sp,q,w))
					!write(*,*) q,w,anc

				end function anc

		end function Structssp2

		real (kind=8) function Structssp2bar(s,sp,p)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble2
			implicit none
			integer (kind=4)::s,sp
			real    (kind=8)::q,p
			real (kind=8), parameter::a=-1D0/pi
			integer (kind=4), parameter::limit=10000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-8
			integer (kind=4)::neval,ier,last,iord(limit),bounds(8),inds(8),closed,i
			real (kind=8)   ::temp,k1,k2,en1,high,low,v(2),qbs(2),temp1&
								,temp2,temp3,temp4,temp5,lows(2),highs(2),q22,bb,aa,lims(8),d


			q=(1D0-p)/p

			Structssp2bar=0D0

			k1=kfs(s)
			k2=kfs(sp)
			en1=-(Ens(sp)-Ens(s))

			qbs=(/q/k1,q/k2/)

			! v(1)=en1/q/k2-qbs(2)/2D0
			! v(2)=en1/q/k1+qbs(1)/2D0

			q22=q**2/2D0


			temp=0D0
			temp1=0D0
			! temp5=0D0

			! lims=(/max(0D0,q22+en1),max(0D0,q22-q*k2+en1),max(0D0,-q22+en1),max(0D0,-q22-q*k1+en1),&
			! 			q22+q*k2+en1,q22+en1,max(0D0,-q22+q*k1+en1),max(0D0,-q22+en1)/)
			d=0D0!(1D0-qgrid(qsteps)/1000D0)/(qgrid(qsteps)/1000D0)
			lims=(/max(d,q22+en1),max(d,q22-q*k2+en1),max(d,-q22+en1),max(d,-q22-q*k1+en1),&
						max(d,q22+q*k2+en1),max(d,q22+en1),max(d,-q22+q*k1+en1),max(d,-q22+en1)/)
			bounds=(/-1,-1,-1,-1,1,1,1,1/)

			call indexArrayReal(8,lims,inds)

			lims=lims(inds)
			bounds=bounds(inds)

			closed=bounds(1)
			do i = 1,7
				if ( closed.gt.0 ) then
					write(*,*) "Something went very wrong. closed > 0"
					write(*,*) lims
					write(*,*) bounds
					call exit()
				elseif ((closed.lt.0).and.(lims(i+1).ne.lims(i))) then
					! write(*,*) "lims: ", lims
					call GaussAdaptQuad(anc,1D0/(lims(i+1)+1D0),1D0/(lims(i)+1D0),tol,temp1)
					temp=temp+temp1
				end if
				closed=closed+bounds(i+1)
			end do


			! temp=temp1+temp2+temp3+temp4!+temp5

			ier=0

			if (ier.gt.0) then
				write(*,*) "Integration of ImChi within Struct has failed."
				write(*,*) "dqagie threw an error, ier:",ier
				write(*,*) "See documentation for more information."
				call exit(-ier)
			endif

			Structssp2bar=a*temp

			contains

				real (kind=8) function anc(t)
					implicit none
					real (kind=8)::t,w

					! w=(1D0-t)/t

					anc=dimag(Chisspbar(s,sp,p,t))/t**2
					!write(*,*) q,w,anc

				end function anc

		end function Structssp2bar

		real (kind=8) function Structssp2bar0(s,sp,p)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble2
			implicit none
			integer (kind=4)::s,sp
			real    (kind=8)::q,p
			real (kind=8), parameter::a=-1D0/pi
			integer (kind=4), parameter::limit=10000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-8
			integer (kind=4)::neval,ier,last,iord(limit)
			real (kind=8)   ::temp,k1,k2,en1,high,low,v(2),qbs(2),temp1&
								,temp2,temp3,temp4,temp5,lows(2),highs(2),q22,bb,aa


			q=(1D0-p)/p

			Structssp2bar0=0D0

			k1=kfs(s)
			k2=kfs(sp)
			en1=-(Ens(sp)-Ens(s))

			qbs=(/q/k1,q/k2/)

			v(1)=en1/q/k2-qbs(2)/2D0
			v(2)=en1/q/k1+qbs(1)/2D0

			q22=q**2/2D0


			temp1=0D0
			temp2=0D0
			temp3=0D0
			temp4=0D0
			! temp5=0D0


			bb=q22+q*k2+en1
			if ( bb.gt.0D0 ) then
				aa=max(0D0,q22+en1)
				! write(*,*) "S1"
				call GaussAdaptQuad(anc,1D0/(bb+1D0),1D0/(aa+1D0),tol,temp1)
			end if

			bb=q22+en1
			if ( bb.gt.0D0 ) then
				! write(*,*) "S2"
				aa=max(0D0,q22-q*k2+en1)
				call GaussAdaptQuad(anc,1D0/(bb+1D0),1D0/(aa+1D0),tol,temp2)
			end if

			bb=-q22+q*k1+en1
			if ( bb.gt.0D0 ) then
				! write(*,*) "S3"
				aa=max(0D0,-q22+en1)
				call GaussAdaptQuad(anc,1D0/(bb+1D0),1D0/(aa+1D0),tol,temp3)
			end if

			bb=-q22+en1
			if ( bb.gt.0D0 ) then
				! write(*,*) "S4"
				aa=max(0D0,-q22-q*k1+en1)
				call GaussAdaptQuad(anc,1D0/(bb+1D0),1D0/(aa+1D0),tol,temp4)
			end if


			temp=temp1+temp2+temp3+temp4!+temp5

			ier=0

			if (ier.gt.0) then
				write(*,*) "Integration of ImChi within Struct has failed."
				write(*,*) "dqagie threw an error, ier:",ier
				write(*,*) "See documentation for more information."
				call exit(-ier)
			endif

			Structssp2bar0=a*temp

			contains

				real (kind=8) function anc(t)
					implicit none
					real (kind=8)::t,w

					! w=(1D0-t)/t

					anc=dimag(Chisspbar(s,sp,p,t))/t**2
					!write(*,*) q,w,anc

				end function anc

		end function Structssp2bar0



	real (kind=8) function SDUsplint(q)
		use AuxiliaryFunctions
		use STLS
		implicit none
		real (kind=8)::q,sdu


		if (q.lt.qgrid(qsteps)) then
			call splint(qgrid,SDUgrid,SDUspline,qsteps,q,sdu)
		else
			!call splint(qgrid,SDUgrid,SDUspline,qsteps,qgrid(qsteps),sdu)
			sdu=0D0
		endif
		SDUsplint=sdu
	end function SDUsplint

	real (kind=8) function SDUbarsplint(p)
		use AuxiliaryFunctions
		use STLS
		implicit none
		real (kind=8)::p,sdu


		! if (q.lt.qgrid(qsteps)) then
		if ( (p.lt.0D0).or.(p.gt.1D0) ) then
			write(*,*) "SDUbarsplint, p",p
			call exit()
		end if
			call splint(qgrid,SDUbargrid,SDUbarspline,qsteps,p,sdu)
		! else
			!call splint(qgrid,SDUgrid,SDUspline,qsteps,qgrid(qsteps),sdu)
			! sdu=0D0
		! endif
		SDUbarsplint=sdu
	end function SDUbarsplint

	real (kind=8) function SUDbarsplint(p)
		use AuxiliaryFunctions
		use STLS
		implicit none
		real (kind=8)::p,sdu


		! if (q.lt.qgrid(qsteps)) then
		if ( (p.lt.0D0).or.(p.gt.1D0) ) then
			write(*,*) "SUDbarsplint, p",p
			call exit()
		end if
			call splint(qgrid,Ssbargrid,Ssbarspline,qsteps,p,sdu)
		! else
			!call splint(qgrid,SDUgrid,SDUspline,qsteps,qgrid(qsteps),sdu)
			! sdu=0D0
		! endif
		SUDbarsplint=sdu
	end function SUDbarsplint

	subroutine SDUsplineUpdate()
		use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::i


		! write(99,*) "SCstep", SCCount
		do i=1,qsteps
			SDUgrid(i)=Structssp2(-1,1,qgrid(i))-Globnu
			! write(*,*) "S update", i,qsteps, qgrid(i)/GlobKf0, SDUgrid(i)
			! write(99,*) qgrid(i), (1D0-qgrid(i))/qgrid(i)/Globkf, SDUgrid(i), Struct((1D0-qgrid(i))/qgrid(i)), GSCold(i)
		enddo

		Ggridi=Ggridi+1

		Ggrid(Ggridi,:)=GSCold

		call spline(qgrid,SDUgrid,qsteps,1D33,1D33,SDUspline)
	end subroutine SDUsplineUpdate

	subroutine SDUbarsplineUpdate()
		use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::i

		! write(99,*) "SCstep", Ggridi
		SDUbargrid(qsteps)=-Globnu
		SDUbargrid(1)=0D0
		! write(99,*) qgrid(1), (1D0-qgrid(1))/qgrid(1)/Globkf0, SDUbargrid(1)+Globnu, GSCold(1), -2D0*pi*qgrid(1)/(1D0-qgrid(1))*GSCold(1)
		do i=2,qsteps-1
			!write(*,*) "q", i,qsteps, qgrid(i)/GlobKf0
			SDUbargrid(i)=Structssp2bar(-1,1,qgrid(i))-Globnu
			! write(*,*) "Sbar update", i,qsteps, qgrid(i), (1D0-qgrid(i))/qgrid(i)/GlobKf0, SDUbargrid(i)
			! write(99,*) qgrid(i), (1D0-qgrid(i))/qgrid(i)/Globkf0, SDUbargrid(i)+Globnu, GSCold(i), -2D0*pi*qgrid(i)/(1D0-qgrid(i))*GSCold(i)
		enddo

		! write(99,*) qgrid(qsteps), (1D0-qgrid(qsteps))/qgrid(qsteps)/Globkf0, SDUbargrid(qsteps)+Globnu, GSCold(qsteps), &
								! -2D0*pi*qgrid(qsteps)/(1D0-qgrid(qsteps))*GSCold(qsteps)





		Ggridi=Ggridi+1

		Ggrid(Ggridi,:)=GSCold
		call spline(qgrid,SDUbargrid,qsteps,1D33,1D33,SDUbarspline)
	end subroutine SDUbarsplineUpdate

	subroutine SUDbarsplineUpdate()
		use STLS
		use AuxiliaryFunctions
		implicit none
		integer (kind=4)::i

		! write(99,*) "SCstep", Ggridi
		Ssbargrid(qsteps)=-Globnd
		Ssbargrid(1)=0D0
		! write(99,*) qgrid(1), (1D0-qgrid(1))/qgrid(1)/Globkf0, SDUbargrid(1)+Globnu, GSCold(1), -2D0*pi*qgrid(1)/(1D0-qgrid(1))*GSCold(1)
		do i=2,qsteps-1
			!write(*,*) "q", i,qsteps, qgrid(i)/GlobKf0
			Ssbargrid(i)=Structssp2bar(1,-1,qgrid(i))-Globnd
			! write(*,*) "Sbar update", i,qsteps, qgrid(i), (1D0-qgrid(i))/qgrid(i)/GlobKf0, SDUbargrid(i)
			! write(99,*) qgrid(i), (1D0-qgrid(i))/qgrid(i)/Globkf0, SDUbargrid(i)+Globnu, GSCold(i), -2D0*pi*qgrid(i)/(1D0-qgrid(i))*GSCold(i)
		enddo

		! write(99,*) qgrid(qsteps), (1D0-qgrid(qsteps))/qgrid(qsteps)/Globkf0, SDUbargrid(qsteps)+Globnu, GSCold(qsteps), &
								! -2D0*pi*qgrid(qsteps)/(1D0-qgrid(qsteps))*GSCold(qsteps)





		Ggridi=Ggridi+1

		Ggrid(Ggridi,:)=GSCold
		call spline(qgrid,Ssbargrid,qsteps,1D33,1D33,Ssbarspline)
	end subroutine SUDbarsplineUpdate

	real (kind=8) function GDU(q)
		use STLS
		use AuxiliaryFunctions
		use QuadPackDouble
		real (kind=8)::q
		integer (kind=4), parameter::limit=1000
		real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-10
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
							,elist(limit),temp1,temp2,temp3
		real(8), parameter::xa=0D0,xb=1D0,xc=100D0



							temp1=0D0
							temp2=0D0
							temp3=0D0


							call dqage ( anc, xa, xb, tol, tol, 6, limit , temp1, abserr, &
										  neval, ier, alist, blist, rlist, elist, iord, last )

							call dqage ( anc, xb, xc, tol, tol, 6, limit , temp2, abserr, &
										  neval, ier, alist, blist, rlist, elist, iord, last )

							call dqagie ( anc, xc, 1, tol, tol, limit , temp3, abserr, &
										  neval, ier, alist, blist, rlist, elist, iord, last )



							!call StupidIntegrateNC(anc,xa,xb,5000,temp1,ier)

							!call StupidIntegrateNC(anc,xb,xc,5000,temp2,ier)

							!call StupidIntegrateNC(anc,xc,100D0,1000,temp3,ier)

							!call GaussAdaptQuad(anc,xc,1000D0,tol,temp3)

							!call dqagie ( anc, xc, 1, tol, tol, limit , temp3, abserr, &
							!				neval, ier, alist, blist, rlist, elist, iord, last )

							!call dqagie ( anc, xa, 1, tol, tol, limit , temp2, abserr, &
							!			  neval, ier, alist, blist, rlist, elist, iord, last )

		GDU=-8D0*(temp1+temp2+temp3)/Globn**2*q**2*(2D0/(2D0*pi)**2)

		contains
		real (kind=8) function anc(x)
			use Elliptic
			real (kind=8)::x

			!anc=(Structssp(-1,1,q*x)-Globnu)*x*Kell((2D0*dsqrt(x))/(x+1))/(x+1)
			!anc=(S0ssp(-1,1,q*x)-Globnu)*x*Kell((2D0*dsqrt(x))/(x+1))/(x+1)
			anc=(SDUsplint(q*x))*x*Kell((2D0*dsqrt(x))/(x+1))/(x+1)


		end function anc
	end function GDU


	real(8) function GDUbar(p)
		implicit none
		real(8)::p



		! GDUbar=GDUscalebar(p)
		GDUbar=GDUforcebar(p)
		! GDUbar=GDUForcebar2(p)
	end function GDUbar

	real(8) function GUDbar(p)
		implicit none
		real(8)::p



		! GDUbar=GDUscalebar(p)
		GUDbar=GUDforcebar(p)
		! GDUbar=GDUForcebar2(p)
	end function GUDbar

	real(8) function GDUbarsplint(p)
		use AuxiliaryFunctions
		use STLS
		implicit none
		real(8)::p

		if ( (p.lt.0D0).or.(p.gt.1D0) ) then
			write(*,*) "GDUbarsplint"
			write(*,*) "p =", p
			call exit()
		end if


		call splint(qgrid,GSCold,GSCspline,qsteps,p,GDUbarsplint)
	end function GDUbarsplint


	real(8) function fDUbarsplint(p)
		use AuxiliaryFunctions
		use STLS
		implicit none
		real(8)::p

		if ( (p.lt.0D0).or.(p.gt.1D0) ) then
			write(*,*) "fDUbarsplint"
			write(*,*) "p =", p
			call exit()
		end if


		call splint(qgrid,fxcgrid,fxcspline,qsteps,p,fDUbarsplint)
	end function fDUbarsplint

	subroutine GDUqDerivs7(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: gs(16),dgdt(3),t
		! This is a 7 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 7] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		gs=(/GDUbarsplint(t),&						! 1
				GDUbarsplint(t-3D0*h),&				! 2
				GDUbarsplint(t-2D0*h),&				! 3
				GDUbarsplint(t-1.5D0*h),&			! 4
				GDUbarsplint(t-1D0*h),&				! 5
				GDUbarsplint(t-0.75D0*h),&		! 6
				GDUbarsplint(t-0.5D0*h),&			! 7
				GDUbarsplint(t-0.375D0*h),&		! 8
				GDUbarsplint(t-0.25D0*h),&		! 9
				GDUbarsplint(t-0.1875D0*h),&	! 10
				GDUbarsplint(t-0.125D0*h),&		! 11
				GDUbarsplint(t-0.09375D0*h),&	! 12
				GDUbarsplint(t-0.0625D0*h),&	! 13
				GDUbarsplint(t-0.046875D0*h),&! 14
				GDUbarsplint(t-0.03125D0*h),&	! 15
				GDUbarsplint(t-0.015625D0*h)/)! 16


		dgdt(1)=(127D0*gs(1) - 1.6255008574517023D-6*gs(5) + 0.000409626216077829D0*gs(7) - &
         0.03386243386243386D0*gs(9) + 1.160997732426304D0*gs(11) - &
         17.33756613756614D0*gs(13) + 107.38105478750639D0*gs(15) - &
         218.17103194921935D0*gs(16))/h

		dgdt(2)=(10795D0*gs(1) + 1.6255008574517023D-6*gs(3) - &
         0.0008225034338705614D0*gs(5) + 0.13708824031404676D0*gs(7) - &
         9.558881330309902D0*gs(9) + 295.9770219198791D0*gs(11) - &
         3990.9958696023227D0*gs(13) + 20835.33355115045D0*gs(15) - &
         27925.892089500077D0*gs(16))/h**2

		dgdt(3)=(788035D0*gs(1) - 1.6255008574517023D-6*gs(2) + &
         4.876502572355106D-6*gs(3) + 0.001638504864311316D0*gs(4) - &
         0.004920391095506302D0*gs(5) - 0.5417989417989417D0*gs(6) + &
         1.6303123399897594D0*gs(7) + 74.30385487528345D0*gs(8) - &
         224.53696145124715D0*gs(9) - 4438.41693121693D0*gs(10) + &
         13538.162358276646D0*gs(11) + 109958.20010240652D0*gs(12) - &
         343189.8511008704D0*gs(13) - 893628.5468640025D0*gs(14) + &
         3.0107602408992276D6*gs(15) - 2.680885640592007D6*gs(16))/h**3



		! x=(q+1D0)
		! dgdq(1)=-dgdt(1)/x**2
		! dgdq(2)=2D0*dgdt/x**3+dgdt(2)/x**4
		! dgdq(3)=-(6D0*dgdt(1)/x**4+6D0*dgdt(2)/x**5+dgdt(3)/x**6)

		! x=(q+1D0)
		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine GDUqDerivs7

	subroutine GDUqDerivs10(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: gs(22),dgdt(3),t
		! This is a 10 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 10] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		gs=(/GDUbarsplint(t),&						! 1
				GDUbarsplint(t-3D0*h),&				! 2
				GDUbarsplint(t-2D0*h),&				! 3
				GDUbarsplint(t-1.5D0*h),&			! 4
				GDUbarsplint(t-1D0*h),&				! 5
				GDUbarsplint(t-0.75D0*h),&		! 6
				GDUbarsplint(t-0.5D0*h),&			! 7
				GDUbarsplint(t-0.375D0*h),&		! 8
				GDUbarsplint(t-0.25D0*h),&		! 9
				GDUbarsplint(t-0.1875D0*h),&	! 10
				GDUbarsplint(t-0.125D0*h),&		! 11
				GDUbarsplint(t-0.09375D0*h),&	! 12
				GDUbarsplint(t-0.0625D0*h),&	! 13
				GDUbarsplint(t-0.046875D0*h),&! 14
				GDUbarsplint(t-0.03125D0*h),&	! 15
				GDUbarsplint(t-0.0234375D0*h),&! 16
				GDUbarsplint(t-0.015625D0*h),&! 17
				GDUbarsplint(t-0.01171875D0*h),&! 18
				GDUbarsplint(t-0.0078125D0*h),&! 19
				GDUbarsplint(t-0.005859375D0*h),&! 20
				GDUbarsplint(t-0.00390625D0*h),&! 21
				GDUbarsplint(t-0.001953125D0*h)/)! 22


		dgdt(1)=(1023D0*gs(1) + 9.822508230699823D-14*gs(5) - &
         2.0077206823550437D-10*gs(7) + 1.3652500640014298D-7*gs(9) - &
         0.00003963125900072722D0*gs(11) + 0.005326441209697739D0*gs(13) - &
         0.3408922374206553D0*gs(15) + 10.38909675948664D0*gs(17) - &
         146.5926093937012D0*gs(19) + 883.0048942302942D0*gs(21) - &
         1769.465776304934D0*gs(22))/h

		 dgdt(2)=(698027D0*gs(1) - 9.822508230699821D-14*gs(3) + &
          4.0174058663562276D-10*gs(5) - 5.469031138735139D-7*gs(7) + &
          0.00031814227205701883D0*gs(9) - 0.08585715949917544D0*gs(11) + &
          11.078997716171296D0*gs(13) - 686.7192958020667D0*gs(15) + &
          20093.658387608048D0*gs(17) - 263576.9609277429D0*gs(19) + &
          1.3580649833140369D6*gs(21) - 1.8119329549362522D6*gs(22))/h**2

		dgdt(3)=(4.08345795D8*gs(1) + 9.822508230699823D-14*gs(2) - &
         2.946752469209946D-13*gs(3) - 8.030882729420175D-10*gs(4) + &
         2.4095594940729733D-9*gs(5) + 2.1844001024022872D-6*gs(6) - &
         6.55560957202569D-6*gs(7) - 0.0025364005760465415D0*gs(8) + &
         0.007615754928446832D0*gs(9) + 1.3635689496826209D0*gs(10) - &
         4.098316050776002D0*gs(11) - 349.07365111875094D0*gs(12) + &
         1051.3116602053008D0*gs(13) + 42553.74032685726D0*gs(14) - &
         128708.44193392801D0*gs(15) - 2.4017733123063995D6*gs(16) + &
         7.332981157899774D6*gs(17) + 5.786860874827654D7*gs(18) - &
         1.8081114618174884D8*gs(19) - 4.638548364636806D8*gs(20) + &
         1.5651703356358716D9*gs(21) - 1.3915645093910415D9*gs(22))/h**3


		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine GDUqDerivs10

	subroutine GDUqDerivs12(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: dgdt(3),t!,gs(22)
		! This is a 12 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 12] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		! gs=(/GDUbarsplint(t),&						! 1
		! 		GDUbarsplint(t-3D0*h),&				! 2
		! 		GDUbarsplint(t-2D0*h),&				! 3
		! 		GDUbarsplint(t-1.5D0*h),&			! 4
		! 		GDUbarsplint(t-1D0*h),&				! 5
		! 		GDUbarsplint(t-0.75D0*h),&		! 6
		! 		GDUbarsplint(t-0.5D0*h),&			! 7
		! 		GDUbarsplint(t-0.375D0*h),&		! 8
		! 		GDUbarsplint(t-0.25D0*h),&		! 9
		! 		GDUbarsplint(t-0.1875D0*h),&	! 10
		! 		GDUbarsplint(t-0.125D0*h),&		! 11
		! 		GDUbarsplint(t-0.09375D0*h),&	! 12
		! 		GDUbarsplint(t-0.0625D0*h),&	! 13
		! 		GDUbarsplint(t-0.046875D0*h),&! 14
		! 		GDUbarsplint(t-0.03125D0*h),&	! 15
		! 		GDUbarsplint(t-0.0234375D0*h),&! 16
		! 		GDUbarsplint(t-0.015625D0*h),&! 17
		! 		GDUbarsplint(t-0.01171875D0*h),&! 18
		! 		GDUbarsplint(t-0.0078125D0*h),&! 19
		! 		GDUbarsplint(t-0.005859375D0*h),&! 20
		! 		GDUbarsplint(t-0.00390625D0*h),&! 21
		! 		GDUbarsplint(t-0.001953125D0*h)/)! 22


		dgdt(1)=(4095.D0*GDUbarsplint(t) + 4.690605678911093D-20*GDUbarsplint(-h + t) - &
         3.840667929892403D-16*GDUbarsplint(-0.5D0*h + t) + &
         1.0477342112746479D-12*GDUbarsplint(-0.25D0*h + t) - &
         1.2237535587687886D-9*GDUbarsplint(-0.125D0*h + t) + &
         6.657219359702211D-7*GDUbarsplint(-0.0625D0*h + t) - &
         0.00017454799663115986D0*GDUbarsplint(-0.03125D0*h + t) + &
         0.02234214356878847D0*GDUbarsplint(-0.015625D0*h + t) - &
         1.3961200894638213D0*GDUbarsplint(-0.0078125D0*h + t) + &
         42.047852106204495D0*GDUbarsplint(-0.00390625D0*h + t) - &
         589.8219254349781D0*GDUbarsplint(-0.001953125D0*h + t) + &
         3542.390918741451D0*GDUbarsplint(-0.0009765625D0*h + t) - &
         7088.242893583284D0*GDUbarsplint(-0.00048828125D0*h + t))/h

		 dgdt(2)=(1.1180714999999998D7*GDUbarsplint(t) - &
         4.690605678911093D-20*GDUbarsplint(-2.D0*h + t) + &
         7.68227398092059D-16*GDUbarsplint(-h + t) - &
         4.1924731122705474D-12*GDUbarsplint(-0.5D0*h + t) + &
         9.798410343840506D-9*GDUbarsplint(-0.25D0*h + t) - &
         0.000010671131032463835D0*GDUbarsplint(-0.125D0*h + t) + &
         0.005606838994148162D0*GDUbarsplint(-0.0625D0*h + t) - &
         1.4410682601868559D0*GDUbarsplint(-0.03125D0*h + t) + &
         181.563165828174D0*GDUbarsplint(-0.015625D0*h + t) - &
         11121.656882091089D0*GDUbarsplint(-0.0078125D0*h + t) + &
         323517.32610108546D0*GDUbarsplint(-0.00390625D0*h + t) - &
         4.231385952436665D6*GDUbarsplint(-0.001953125D0*h + t) + &
         2.1771538047641065D7*GDUbarsplint(-0.0009765625D0*h + t) - &
         2.903344289211713D7*GDUbarsplint(-0.00048828125D0*h + t))/h**2

		dgdt(3)=(2.6167664835D10*GDUbarsplint(t) + 4.690605678911093D-20*GDUbarsplint(-3.D0*h + t) - &
         1.4071817036733277D-19*GDUbarsplint(-2.D0*h + t) - &
         1.5362671719569611D-15*GDUbarsplint(-1.5D0*h + t) + &
        4.608942234041251D-15*GDUbarsplint(-h + t) + &
        1.676374738039436D-11*GDUbarsplint(-0.75D0*h + t) - &
        5.029585094269895D-11*GDUbarsplint(-0.5D0*h + t) - &
        7.832022776120246D-8*GDUbarsplint(-0.375D0*h + t) + &
        2.350109745257485D-7*GDUbarsplint(-0.25D0*h + t) + &
        0.00017042481560837657D0*GDUbarsplint(-0.1875D0*h + t) - &
        0.0005115094075084134D0*GDUbarsplint(-0.125D0*h + t) - &
        0.1787371485503077D0*GDUbarsplint(-0.09375D0*h + t) + &
        0.5367227200977482D0*GDUbarsplint(-0.0625D0*h + t) + &
        91.51342005775754D0*GDUbarsplint(-0.046875D0*h + t) - &
        275.07647161892356D0*GDUbarsplint(-0.03125D0*h + t) - &
        22874.03154577524D0*GDUbarsplint(-0.0234375D0*h + t) + &
        68896.634897499D0*GDUbarsplint(-0.015625D0*h + t) + &
        2.7556480356322164D6*GDUbarsplint(-0.01171875D0*h + t) - &
        8.335566201533977D6*GDUbarsplint(-0.0078125D0*h + t) - &
        1.5461827882122684D8*GDUbarsplint(-0.005859375D0*h + t) + &
        4.721217805705772D8*GDUbarsplint(-0.00390625D0*h + t) + &
        3.7144661000102353D9*GDUbarsplint(-0.0029296875D0*h + t) - &
        1.1607253136494389D10*GDUbarsplint(-0.001953125D0*h + t) - &
        2.9730245521527943D10*GDUbarsplint(-0.00146484375D0*h + t) + &
        1.0033413486461455D11*GDUbarsplint(-0.0009765625D0*h + t) - &
        8.919073656458385D10*GDUbarsplint(-0.00048828125D0*h + t))/h**3


		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine GDUqDerivs12

	subroutine GDUqDerivs15(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: dgdt(3),t!,gs(22)
		! This is a 12 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 12] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		! gs=(/GDUbarsplint(t),&						! 1
		! 		GDUbarsplint(t-3D0*h),&				! 2
		! 		GDUbarsplint(t-2D0*h),&				! 3
		! 		GDUbarsplint(t-1.5D0*h),&			! 4
		! 		GDUbarsplint(t-1D0*h),&				! 5
		! 		GDUbarsplint(t-0.75D0*h),&		! 6
		! 		GDUbarsplint(t-0.5D0*h),&			! 7
		! 		GDUbarsplint(t-0.375D0*h),&		! 8
		! 		GDUbarsplint(t-0.25D0*h),&		! 9
		! 		GDUbarsplint(t-0.1875D0*h),&	! 10
		! 		GDUbarsplint(t-0.125D0*h),&		! 11
		! 		GDUbarsplint(t-0.09375D0*h),&	! 12
		! 		GDUbarsplint(t-0.0625D0*h),&	! 13
		! 		GDUbarsplint(t-0.046875D0*h),&! 14
		! 		GDUbarsplint(t-0.03125D0*h),&	! 15
		! 		GDUbarsplint(t-0.0234375D0*h),&! 16
		! 		GDUbarsplint(t-0.015625D0*h),&! 17
		! 		GDUbarsplint(t-0.01171875D0*h),&! 18
		! 		GDUbarsplint(t-0.0078125D0*h),&! 19
		! 		GDUbarsplint(t-0.005859375D0*h),&! 20
		! 		GDUbarsplint(t-0.00390625D0*h),&! 21
		! 		GDUbarsplint(t-0.001953125D0*h)/)! 22


		dgdt(1)=(32767.D0*GDUbarsplint(t) - 8.535808471816301D-32*GDUbarsplint(-h + t) + &
        5.593686007750658D-27*GDUbarsplint(-0.5D0*h + t) - &
        1.2218101890529505D-22*GDUbarsplint(-0.25D0*h + t) +&
        1.1436143369535618D-18*GDUbarsplint(-0.125D0*h + t) -&
        4.994087568520409D-15*GDUbarsplint(-0.0625D0*h + t) +&
        1.0547512944715103D-11*GDUbarsplint(-0.03125D0*h + t) -&
        1.095066232837977D-8*GDUbarsplint(-0.015625D0*h + t) +&
        5.628812888162451D-6*GDUbarsplint(-0.0078125D0*h + t) -&
        0.0014353252127053927D0*GDUbarsplint(-0.00390625D0*h + t) +&
        0.1812048926067521D0*GDUbarsplint(-0.001953125D0*h + t) -&
        11.24568545632207D0*GDUbarsplint(-0.0009765625D0*h + t) +&
        337.53537588491844D0*GDUbarsplint(-0.00048828125D0*h + t) -&
        4726.649229485899D0*GDUbarsplint(-0.000244140625D0*h + t) +&
        28363.35770085349D0*GDUbarsplint(-0.0001220703125D0*h + t) -&
        56730.17793698144D0*GDUbarsplint(-0.00006103515625D0*h + t))/h

		 dgdt(2)=(7.157951149999999D8*GDUbarsplint(t) + 8.535808471816301D-32*GDUbarsplint(-2.D0*h + t) -&
        1.1187542731670755D-26*GDUbarsplint(-h + t) +&
        4.887464503652113D-22*GDUbarsplint(-0.5D0*h + t) -&
        9.149892143779736D-18*GDUbarsplint(-0.25D0*h + t) +&
        7.992369892571779D-14*GDUbarsplint(-0.125D0*h + t) -&
        3.376802250330759D-10*GDUbarsplint(-0.0625D0*h + t) +&
        7.015174298447668D-7*GDUbarsplint(-0.03125D0*h + t) -&
        0.0007218897344628261D0*GDUbarsplint(-0.015625D0*h + t) +&
        0.3688842305519501D0*GDUbarsplint(-0.0078125D0*h + t) -&
        93.51179152356221D0*GDUbarsplint(-0.00390625D0*h + t) +&
        11701.13571730311D0*GDUbarsplint(-0.001953125D0*h + t) -&
        714303.6136268605D0*GDUbarsplint(-0.0009765625D0*h + t) +&
        2.074290014359887D7*GDUbarsplint(-0.00048828125D0*h + t) -&
        2.710733367733403D8*GDUbarsplint(-0.000244140625D0*h + t) +&
        1.3941724878902874D9*GDUbarsplint(-0.0001220703125D0*h + t) -&
        1.858934470639008D9*GDUbarsplint(-0.00006103515625D0*h + t))/h**2

		dgdt(3)=(1.3402854502595D13*GDUbarsplint(t) - 8.535808471816305D-32*GDUbarsplint(-3.D0*h + t) +&
        2.5607425415448905D-31*GDUbarsplint(-2.D0*h + t) +&
        2.2374744031002642D-26*GDUbarsplint(-1.5D0*h + t) -&
        6.712448816726206D-26*GDUbarsplint(-h + t) -&
        1.9548963024847215D-21*GDUbarsplint(-0.75D0*h + t) +&
        5.8647560316862555D-21*GDUbarsplint(-0.5D0*h + t) +&
        7.319131756502798D-17*GDUbarsplint(-0.375D0*h + t) -&
        2.1957981738399134D-16*GDUbarsplint(-0.25D0*h + t) -&
        1.2784864175412247D-12*GDUbarsplint(-0.1875D0*h + t) +&
        3.835678826576369D-12*GDUbarsplint(-0.125D0*h + t) +&
        1.0800653255388266D-8*GDUbarsplint(-0.09375D0*h + t) -&
        3.240579522541743D-8*GDUbarsplint(-0.0625D0*h + t) -&
        0.000044853912897043535D0*GDUbarsplint(-0.046875D0*h + t) +&
        0.00013459414065089678D0*GDUbarsplint(-0.03125D0*h + t) +&
        0.09222247035965361D0*GDUbarsplint(-0.0234375D0*h + t) -&
        0.27680197281765195D0*GDUbarsplint(-0.015625D0*h + t) -&
        94.06547313986064D0*GDUbarsplint(-0.01171875D0*h + t) +&
        282.4730868306609D0*GDUbarsplint(-0.0078125D0*h + t) +&
        47501.77536750442D0*GDUbarsplint(-0.005859375D0*h + t) -&
        142787.52252193284D0*GDUbarsplint(-0.00390625D0*h + t) -&
        1.179195587304837D7*GDUbarsplint(-0.0029296875D0*h + t) +&
        3.551837294524763D7*GDUbarsplint(-0.001953125D0*h + t) +&
        1.415725977215617D9*GDUbarsplint(-0.00146484375D0*h + t) -&
        4.2825537992659955D9*GDUbarsplint(-0.0009765625D0*h + t) -&
        7.930001507931851D10*GDUbarsplint(-0.000732421875D0*h + t) +&
        2.421472231696024D11*GDUbarsplint(-0.00048828125D0*h + t) +&
        1.9034327145299297D12*GDUbarsplint(-0.0003662109375D0*h + t) -&
        5.948198188827745D12*GDUbarsplint(-0.000244140625D0*h + t) -&
        1.5228391183474758D13*GDUbarsplint(-0.00018310546875D0*h + t) +&
        5.139547169401406D13*GDUbarsplint(-0.0001220703125D0*h + t) -&
        4.568517355042427D13*GDUbarsplint(-0.00006103515625D0*h + t))/h**3


		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine GDUqDerivs15

	subroutine fDUqDerivs12(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: dgdt(3),t!,gs(22)
		! This is a 12 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 12] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		! gs=(/fDUbarsplint(t),&						! 1
		! 		fDUbarsplint(t-3D0*h),&				! 2
		! 		fDUbarsplint(t-2D0*h),&				! 3
		! 		fDUbarsplint(t-1.5D0*h),&			! 4
		! 		fDUbarsplint(t-1D0*h),&				! 5
		! 		fDUbarsplint(t-0.75D0*h),&		! 6
		! 		fDUbarsplint(t-0.5D0*h),&			! 7
		! 		fDUbarsplint(t-0.375D0*h),&		! 8
		! 		fDUbarsplint(t-0.25D0*h),&		! 9
		! 		fDUbarsplint(t-0.1875D0*h),&	! 10
		! 		fDUbarsplint(t-0.125D0*h),&		! 11
		! 		fDUbarsplint(t-0.09375D0*h),&	! 12
		! 		fDUbarsplint(t-0.0625D0*h),&	! 13
		! 		fDUbarsplint(t-0.046875D0*h),&! 14
		! 		fDUbarsplint(t-0.03125D0*h),&	! 15
		! 		fDUbarsplint(t-0.0234375D0*h),&! 16
		! 		fDUbarsplint(t-0.015625D0*h),&! 17
		! 		fDUbarsplint(t-0.01171875D0*h),&! 18
		! 		fDUbarsplint(t-0.0078125D0*h),&! 19
		! 		fDUbarsplint(t-0.005859375D0*h),&! 20
		! 		fDUbarsplint(t-0.00390625D0*h),&! 21
		! 		fDUbarsplint(t-0.001953125D0*h)/)! 22


		dgdt(1)=(4095.D0*fDUbarsplint(t) + 4.690605678911093D-20*fDUbarsplint(-h + t) - &
         3.840667929892403D-16*fDUbarsplint(-0.5D0*h + t) + &
         1.0477342112746479D-12*fDUbarsplint(-0.25D0*h + t) - &
         1.2237535587687886D-9*fDUbarsplint(-0.125D0*h + t) + &
         6.657219359702211D-7*fDUbarsplint(-0.0625D0*h + t) - &
         0.00017454799663115986D0*fDUbarsplint(-0.03125D0*h + t) + &
         0.02234214356878847D0*fDUbarsplint(-0.015625D0*h + t) - &
         1.3961200894638213D0*fDUbarsplint(-0.0078125D0*h + t) + &
         42.047852106204495D0*fDUbarsplint(-0.00390625D0*h + t) - &
         589.8219254349781D0*fDUbarsplint(-0.001953125D0*h + t) + &
         3542.390918741451D0*fDUbarsplint(-0.0009765625D0*h + t) - &
         7088.242893583284D0*fDUbarsplint(-0.00048828125D0*h + t))/h

		 dgdt(2)=(1.1180714999999998D7*fDUbarsplint(t) - &
         4.690605678911093D-20*fDUbarsplint(-2.D0*h + t) + &
         7.68227398092059D-16*fDUbarsplint(-h + t) - &
         4.1924731122705474D-12*fDUbarsplint(-0.5D0*h + t) + &
         9.798410343840506D-9*fDUbarsplint(-0.25D0*h + t) - &
         0.000010671131032463835D0*fDUbarsplint(-0.125D0*h + t) + &
         0.005606838994148162D0*fDUbarsplint(-0.0625D0*h + t) - &
         1.4410682601868559D0*fDUbarsplint(-0.03125D0*h + t) + &
         181.563165828174D0*fDUbarsplint(-0.015625D0*h + t) - &
         11121.656882091089D0*fDUbarsplint(-0.0078125D0*h + t) + &
         323517.32610108546D0*fDUbarsplint(-0.00390625D0*h + t) - &
         4.231385952436665D6*fDUbarsplint(-0.001953125D0*h + t) + &
         2.1771538047641065D7*fDUbarsplint(-0.0009765625D0*h + t) - &
         2.903344289211713D7*fDUbarsplint(-0.00048828125D0*h + t))/h**2

		dgdt(3)=(2.6167664835D10*fDUbarsplint(t) + 4.690605678911093D-20*fDUbarsplint(-3.D0*h + t) - &
         1.4071817036733277D-19*fDUbarsplint(-2.D0*h + t) - &
         1.5362671719569611D-15*fDUbarsplint(-1.5D0*h + t) + &
        4.608942234041251D-15*fDUbarsplint(-h + t) + &
        1.676374738039436D-11*fDUbarsplint(-0.75D0*h + t) - &
        5.029585094269895D-11*fDUbarsplint(-0.5D0*h + t) - &
        7.832022776120246D-8*fDUbarsplint(-0.375D0*h + t) + &
        2.350109745257485D-7*fDUbarsplint(-0.25D0*h + t) + &
        0.00017042481560837657D0*fDUbarsplint(-0.1875D0*h + t) - &
        0.0005115094075084134D0*fDUbarsplint(-0.125D0*h + t) - &
        0.1787371485503077D0*fDUbarsplint(-0.09375D0*h + t) + &
        0.5367227200977482D0*fDUbarsplint(-0.0625D0*h + t) + &
        91.51342005775754D0*fDUbarsplint(-0.046875D0*h + t) - &
        275.07647161892356D0*fDUbarsplint(-0.03125D0*h + t) - &
        22874.03154577524D0*fDUbarsplint(-0.0234375D0*h + t) + &
        68896.634897499D0*fDUbarsplint(-0.015625D0*h + t) + &
        2.7556480356322164D6*fDUbarsplint(-0.01171875D0*h + t) - &
        8.335566201533977D6*fDUbarsplint(-0.0078125D0*h + t) - &
        1.5461827882122684D8*fDUbarsplint(-0.005859375D0*h + t) + &
        4.721217805705772D8*fDUbarsplint(-0.00390625D0*h + t) + &
        3.7144661000102353D9*fDUbarsplint(-0.0029296875D0*h + t) - &
        1.1607253136494389D10*fDUbarsplint(-0.001953125D0*h + t) - &
        2.9730245521527943D10*fDUbarsplint(-0.00146484375D0*h + t) + &
        1.0033413486461455D11*fDUbarsplint(-0.0009765625D0*h + t) - &
        8.919073656458385D10*fDUbarsplint(-0.00048828125D0*h + t))/h**3


		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine fDUqDerivs12

	subroutine fDUqDerivs15(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: dgdt(3),t!,gs(22)
		! This is a 12 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 12] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		! gs=(/fDUbarsplint(t),&						! 1
		! 		fDUbarsplint(t-3D0*h),&				! 2
		! 		fDUbarsplint(t-2D0*h),&				! 3
		! 		fDUbarsplint(t-1.5D0*h),&			! 4
		! 		fDUbarsplint(t-1D0*h),&				! 5
		! 		fDUbarsplint(t-0.75D0*h),&		! 6
		! 		fDUbarsplint(t-0.5D0*h),&			! 7
		! 		fDUbarsplint(t-0.375D0*h),&		! 8
		! 		fDUbarsplint(t-0.25D0*h),&		! 9
		! 		fDUbarsplint(t-0.1875D0*h),&	! 10
		! 		fDUbarsplint(t-0.125D0*h),&		! 11
		! 		fDUbarsplint(t-0.09375D0*h),&	! 12
		! 		fDUbarsplint(t-0.0625D0*h),&	! 13
		! 		fDUbarsplint(t-0.046875D0*h),&! 14
		! 		fDUbarsplint(t-0.03125D0*h),&	! 15
		! 		fDUbarsplint(t-0.0234375D0*h),&! 16
		! 		fDUbarsplint(t-0.015625D0*h),&! 17
		! 		fDUbarsplint(t-0.01171875D0*h),&! 18
		! 		fDUbarsplint(t-0.0078125D0*h),&! 19
		! 		fDUbarsplint(t-0.005859375D0*h),&! 20
		! 		fDUbarsplint(t-0.00390625D0*h),&! 21
		! 		fDUbarsplint(t-0.001953125D0*h)/)! 22


		dgdt(1)=(32767.D0*fDUbarsplint(t) - 8.535808471816301D-32*fDUbarsplint(-h + t) + &
        5.593686007750658D-27*fDUbarsplint(-0.5D0*h + t) - &
        1.2218101890529505D-22*fDUbarsplint(-0.25D0*h + t) +&
        1.1436143369535618D-18*fDUbarsplint(-0.125D0*h + t) -&
        4.994087568520409D-15*fDUbarsplint(-0.0625D0*h + t) +&
        1.0547512944715103D-11*fDUbarsplint(-0.03125D0*h + t) -&
        1.095066232837977D-8*fDUbarsplint(-0.015625D0*h + t) +&
        5.628812888162451D-6*fDUbarsplint(-0.0078125D0*h + t) -&
        0.0014353252127053927D0*fDUbarsplint(-0.00390625D0*h + t) +&
        0.1812048926067521D0*fDUbarsplint(-0.001953125D0*h + t) -&
        11.24568545632207D0*fDUbarsplint(-0.0009765625D0*h + t) +&
        337.53537588491844D0*fDUbarsplint(-0.00048828125D0*h + t) -&
        4726.649229485899D0*fDUbarsplint(-0.000244140625D0*h + t) +&
        28363.35770085349D0*fDUbarsplint(-0.0001220703125D0*h + t) -&
        56730.17793698144D0*fDUbarsplint(-0.00006103515625D0*h + t))/h

		 dgdt(2)=(7.157951149999999D8*fDUbarsplint(t) + 8.535808471816301D-32*fDUbarsplint(-2.D0*h + t) -&
        1.1187542731670755D-26*fDUbarsplint(-h + t) +&
        4.887464503652113D-22*fDUbarsplint(-0.5D0*h + t) -&
        9.149892143779736D-18*fDUbarsplint(-0.25D0*h + t) +&
        7.992369892571779D-14*fDUbarsplint(-0.125D0*h + t) -&
        3.376802250330759D-10*fDUbarsplint(-0.0625D0*h + t) +&
        7.015174298447668D-7*fDUbarsplint(-0.03125D0*h + t) -&
        0.0007218897344628261D0*fDUbarsplint(-0.015625D0*h + t) +&
        0.3688842305519501D0*fDUbarsplint(-0.0078125D0*h + t) -&
        93.51179152356221D0*fDUbarsplint(-0.00390625D0*h + t) +&
        11701.13571730311D0*fDUbarsplint(-0.001953125D0*h + t) -&
        714303.6136268605D0*fDUbarsplint(-0.0009765625D0*h + t) +&
        2.074290014359887D7*fDUbarsplint(-0.00048828125D0*h + t) -&
        2.710733367733403D8*fDUbarsplint(-0.000244140625D0*h + t) +&
        1.3941724878902874D9*fDUbarsplint(-0.0001220703125D0*h + t) -&
        1.858934470639008D9*fDUbarsplint(-0.00006103515625D0*h + t))/h**2

		dgdt(3)=(1.3402854502595D13*fDUbarsplint(t) - 8.535808471816305D-32*fDUbarsplint(-3.D0*h + t) +&
        2.5607425415448905D-31*fDUbarsplint(-2.D0*h + t) +&
        2.2374744031002642D-26*fDUbarsplint(-1.5D0*h + t) -&
        6.712448816726206D-26*fDUbarsplint(-h + t) -&
        1.9548963024847215D-21*fDUbarsplint(-0.75D0*h + t) +&
        5.8647560316862555D-21*fDUbarsplint(-0.5D0*h + t) +&
        7.319131756502798D-17*fDUbarsplint(-0.375D0*h + t) -&
        2.1957981738399134D-16*fDUbarsplint(-0.25D0*h + t) -&
        1.2784864175412247D-12*fDUbarsplint(-0.1875D0*h + t) +&
        3.835678826576369D-12*fDUbarsplint(-0.125D0*h + t) +&
        1.0800653255388266D-8*fDUbarsplint(-0.09375D0*h + t) -&
        3.240579522541743D-8*fDUbarsplint(-0.0625D0*h + t) -&
        0.000044853912897043535D0*fDUbarsplint(-0.046875D0*h + t) +&
        0.00013459414065089678D0*fDUbarsplint(-0.03125D0*h + t) +&
        0.09222247035965361D0*fDUbarsplint(-0.0234375D0*h + t) -&
        0.27680197281765195D0*fDUbarsplint(-0.015625D0*h + t) -&
        94.06547313986064D0*fDUbarsplint(-0.01171875D0*h + t) +&
        282.4730868306609D0*fDUbarsplint(-0.0078125D0*h + t) +&
        47501.77536750442D0*fDUbarsplint(-0.005859375D0*h + t) -&
        142787.52252193284D0*fDUbarsplint(-0.00390625D0*h + t) -&
        1.179195587304837D7*fDUbarsplint(-0.0029296875D0*h + t) +&
        3.551837294524763D7*fDUbarsplint(-0.001953125D0*h + t) +&
        1.415725977215617D9*fDUbarsplint(-0.00146484375D0*h + t) -&
        4.2825537992659955D9*fDUbarsplint(-0.0009765625D0*h + t) -&
        7.930001507931851D10*fDUbarsplint(-0.000732421875D0*h + t) +&
        2.421472231696024D11*fDUbarsplint(-0.00048828125D0*h + t) +&
        1.9034327145299297D12*fDUbarsplint(-0.0003662109375D0*h + t) -&
        5.948198188827745D12*fDUbarsplint(-0.000244140625D0*h + t) -&
        1.5228391183474758D13*fDUbarsplint(-0.00018310546875D0*h + t) +&
        5.139547169401406D13*fDUbarsplint(-0.0001220703125D0*h + t) -&
        4.568517355042427D13*fDUbarsplint(-0.00006103515625D0*h + t))/h**3


		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine fDUqDerivs15

	subroutine GDUqDerivs21(q,h,dgdq)
		implicit none
		real(8), intent(in) ::q,h
		real(8), intent(out)::dgdq(3)
		real(8) :: dgdt(3),t!,gs(22)
		! This is a 12 term Euler sum from Mathematica
		! ND[gs[q], {q, 1}, t, Scale -> -h, Terms -> 12] //FullSimplify // FortranForm



		t=1D0/(q+1D0)

		! gs=(/GDUbarsplint(t),&						! 1
		! 		GDUbarsplint(t-3D0*h),&				! 2
		! 		GDUbarsplint(t-2D0*h),&				! 3
		! 		GDUbarsplint(t-1.5D0*h),&			! 4
		! 		GDUbarsplint(t-1D0*h),&				! 5
		! 		GDUbarsplint(t-0.75D0*h),&		! 6
		! 		GDUbarsplint(t-0.5D0*h),&			! 7
		! 		GDUbarsplint(t-0.375D0*h),&		! 8
		! 		GDUbarsplint(t-0.25D0*h),&		! 9
		! 		GDUbarsplint(t-0.1875D0*h),&	! 10
		! 		GDUbarsplint(t-0.125D0*h),&		! 11
		! 		GDUbarsplint(t-0.09375D0*h),&	! 12
		! 		GDUbarsplint(t-0.0625D0*h),&	! 13
		! 		GDUbarsplint(t-0.046875D0*h),&! 14
		! 		GDUbarsplint(t-0.03125D0*h),&	! 15
		! 		GDUbarsplint(t-0.0234375D0*h),&! 16
		! 		GDUbarsplint(t-0.015625D0*h),&! 17
		! 		GDUbarsplint(t-0.01171875D0*h),&! 18
		! 		GDUbarsplint(t-0.0078125D0*h),&! 19
		! 		GDUbarsplint(t-0.005859375D0*h),&! 20
		! 		GDUbarsplint(t-0.00390625D0*h),&! 21
		! 		GDUbarsplint(t-0.001953125D0*h)/)! 22


		dgdt(1)=(2.0971509999999998D6*GDUbarsplint(t) - &
        2.1043656802511434D-63*GDUbarsplint(-h + t) + &
        8.826340972677372D-57*GDUbarsplint(-0.5D0*h + t) - &
        1.2340095545445604D-50*GDUbarsplint(-0.25D0*h + t) + &
        7.393987809302279D-45*GDUbarsplint(-0.125D0*h + t) - &
        2.067493069126526D-39*GDUbarsplint(-0.0625D0*h + t) + &
        2.7972781065333037D-34*GDUbarsplint(-0.03125D0*h + t) - &
        1.8622661428170516D-29*GDUbarsplint(-0.015625D0*h + t) + &
        6.149947710039031D-25*GDUbarsplint(-0.0078125D0*h + t) - &
        1.011435353207059D-20*GDUbarsplint(-0.00390625D0*h + t) + &
        8.29986621897475D-17*GDUbarsplint(-0.001953125D0*h + t) - &
        3.4012867991880955D-13*GDUbarsplint(-0.0009765625D0*h + t) + &
        6.962432416342135D-10*GDUbarsplint(-0.00048828125D0*h + t) - &
        7.117343562207278D-7*GDUbarsplint(-0.000244140625D0*h + t) + &
        0.00036302883671611566D0*GDUbarsplint(-0.0001220703125D0*h + t) - &
        0.092214952879951D0*GDUbarsplint(-0.00006103515625D0*h + t) + &
        11.619438660000904D0*GDUbarsplint(-0.000030517578125D0*h + t) - &
        720.4161895987302D0*GDUbarsplint(-0.0000152587890625D0*h + t) + &
        21612.650579400037D0*GDUbarsplint(-7.62939453125D-6*h + t) - &
        302578.26235607074D0*GDUbarsplint(-3.814697265625D-6*h + t) + &
        1.8154730368764382D6*GDUbarsplint(-1.9073486328125D-6*h + t) - &
        3.630949536496193D6*GDUbarsplint(-9.5367431640625D-7*h + t))/h

		 dgdt(2)=(2.9320289102510005D12*GDUbarsplint(t) + &
        2.104365680251144D-63*GDUbarsplint(-2.D0*h + t) - &
        1.7652686154086107D-56*GDUbarsplint(-h + t) + &
        4.936041748714631D-50*GDUbarsplint(-0.5D0*h + t) - &
        5.91520011951826D-44*GDUbarsplint(-0.25D0*h + t) + &
        3.3080007409829376D-38*GDUbarsplint(-0.125D0*h + t) - &
        8.951356100684785D-33*GDUbarsplint(-0.0625D0*h + t) + &
        1.1918682339827951D-27*GDUbarsplint(-0.03125D0*h + t) - &
        7.872171438916242D-23*GDUbarsplint(-0.015625D0*h + t) + &
        2.589431942871449D-18*GDUbarsplint(-0.0078125D0*h + t) - &
        4.250049359015915D-14*GDUbarsplint(-0.00390625D0*h + t) + &
        3.483767588669433D-10*GDUbarsplint(-0.001953125D0*h + t) - &
        1.4266027424033427D-6*GDUbarsplint(-0.0009765625D0*h + t) + &
        0.002918115735397835D0*GDUbarsplint(-0.00048828125D0*h + t) - &
        2.97976275822458D0*GDUbarsplint(-0.000244140625D0*h + t) + &
        1516.797652445874D0*GDUbarsplint(-0.0001220703125D0*h + t) - &
        383767.4655868797D0*GDUbarsplint(-0.00006103515625D0*h + t) + &
        4.79746869335642D7*GDUbarsplint(-0.000030517578125D0*h + t) - &
        2.927239727546207D9*GDUbarsplint(-0.0000152587890625D0*h + t) + &
        8.498470268055603D10*GDUbarsplint(-7.62939453125D-6*h + t) - &
        1.1104688795720137D12*GDUbarsplint(-3.814697265625D-6*h + t) + &
        5.710987996296781D12*GDUbarsplint(-1.9073486328125D-6*h + t) - &
        7.614653082362066D12*GDUbarsplint(-9.5367431640625D-7*h + t))/h**2

		dgdt(3)=(3.51366260581913D18*GDUbarsplint(t) -&
        2.1043656802511445D-63*GDUbarsplint(-3.D0*h + t) + &
        6.313097040753431D-63*GDUbarsplint(-2.D0*h + t) +&
        3.53053638907095D-56*GDUbarsplint(-1.5D0*h + t) -&
        1.0591609798522552D-55*GDUbarsplint(-h + t) -&
        1.9744152872712974D-49*GDUbarsplint(-0.75D0*h + t) +&
        5.923246920974807D-49*GDUbarsplint(-0.5D0*h + t) +&
        4.73215219795346D-43*GDUbarsplint(-0.375D0*h + t) -&
        1.4196462517106239D-42*GDUbarsplint(-0.25D0*h + t) -&
        5.292782256963908D-37*GDUbarsplint(-0.1875D0*h + t) +&
        1.587836096734832D-36*GDUbarsplint(-0.125D0*h + t) +&
        2.8644127810901034D-31*GDUbarsplint(-0.09375D0*h + t) -&
        8.59325422161708D-31*GDUbarsplint(-0.0625D0*h + t) -&
        7.627842120978644D-26*GDUbarsplint(-0.046875D0*h + t) +&
        2.288361229531937D-25*GDUbarsplint(-0.03125D0*h + t) +&
        1.007607432812795D-20*GDUbarsplint(-0.0234375D0*h + t) -&
        3.0228451819647485D-20*GDUbarsplint(-0.015625D0*h + t) -&
        6.628542730777783D-16*GDUbarsplint(-0.01171875D0*h + t) +&
        1.98859304745632D-15*GDUbarsplint(-0.0078125D0*h + t) +&
        2.1757601301069172D-11*GDUbarsplint(-0.005859375D0*h + t) -&
        6.527479246602676D-11*GDUbarsplint(-0.00390625D0*h + t) -&
        3.566507706745457D-7*GDUbarsplint(-0.0029296875D0*h + t) +&
        1.07001758482754D-6*GDUbarsplint(-0.001953125D0*h + t) +&
        0.0029202558133593483D0*GDUbarsplint(-0.00146484375D0*h + t) -&
        0.00876183739239007D0*GDUbarsplint(-0.0009765625D0*h + t) -&
        11.940921028936096D0*GDUbarsplint(-0.000732421875D0*h + t) +&
        35.83152385424837D0*GDUbarsplint(-0.00048828125D0*h + t) +&
        24362.452831260016D0*GDUbarsplint(-0.0003662109375D0*h + t) -&
        73123.18125686684D0*GDUbarsplint(-0.000244140625D0*h + t) -&
        2.4753762926348154D7*GDUbarsplint(-0.00018310546875D0*h + t) +&
        7.433437613753825D7*GDUbarsplint(-0.0001220703125D0*h + t) +&
        1.2476277260645483D10*GDUbarsplint(-0.000091552734375D0*h + t) -&
        3.7503093070715485D10*GDUbarsplint(-0.00006103515625D0*h + t) -&
        3.0941639738354814D12*GDUbarsplint(-0.0000457763671875D0*h + t) +&
        9.31992075328838D12*GDUbarsplint(-0.000030517578125D0*h + t) +&
        3.713025096735945D14*GDUbarsplint(-0.00002288818359375D0*h + t) -&
        1.1231900209422899D15*GDUbarsplint(-0.0000152587890625D0*h + t) -&
        2.07930198607973D16*GDUbarsplint(-0.000011444091796875D0*h + t) +&
        6.349296711141268D16*GDUbarsplint(-7.62939453125D-6*h + t) +&
        4.990334284898627D17*GDUbarsplint(-5.7220458984375D-6*h + t) -&
        1.55947934505198D18*GDUbarsplint(-3.814697265625D-6*h + t) -&
        3.992271235245443D18*GDUbarsplint(-2.86102294921875D-6*h + t) +&
        1.3473913991205917D19*GDUbarsplint(-1.9073486328125D-6*h + t) -&
        1.197681370573633D19*GDUbarsplint(-9.5367431640625D-7*h + t))/h**3


		dgdq(1)=-dgdt(1)*t**2
		dgdq(2)=2D0*dgdt(1)*t**3+dgdt(2)*t**4
		dgdq(3)=-(6D0*dgdt(1)*t**4+6D0*dgdt(2)*t**5+dgdt(3)*t**6)

	end subroutine GDUqDerivs21

		real (kind=8) function GDUscalebar(p)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble
			real (kind=8)::q,p
			integer (kind=4), parameter::limit=1000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-10
			integer (kind=4)::neval,ier,last,iord(limit)
			real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
								,elist(limit),temp1,temp2,temp3,d
			real(8), parameter::xa=0D0,xb=1D0,xc=100D0



					temp1=0D0
					temp2=0D0
					temp3=0D0
					GDUscalebar=0D0
					q=(1D0-p)/p
					if (p.eq.0D0) then
						q=qgrid(2)!/10D0
						q=(1D0-q)/q
					endif



					! dabs(tiny(0D0))
					d=qgrid(1)!/100D0
					! d=0D0
					call StupidIntegrateG8(anc,d,0.5D0,5000,temp1,ier)
					call StupidIntegrateG8(anc,0.5D0,1D0-d,5000,temp2,ier)

					! GDUbar=-q**2*(temp1+temp2)/Globn**2/pi**2
					! GDUbar=-16D0*q**2*(temp1+temp2)/Globn**2
					GDUscalebar=-4D0*q**2*(temp1+temp2)/Globn**2/pi**2

					if ( isnan(GDUscalebar) ) then
						write(*,*) "GDUbar NaN"
						write(*,*) "p",p,q
					end if

			contains
			real (kind=8) function anc(t)
				use Elliptic
				real (kind=8)::t

				anc=(1D0-t)/t
				anc=(SDUbarsplint(1D0/(q*anc+1D0)))*anc*Kell(2D0*dsqrt(t*(1D0-t)))/t

				if ( isnan(anc) ) then
					write(*,*) 'Found a NaN'
					write(*,*) p,t,anc,q
					call exit()
				end if
			end function anc
		end function GDUscalebar

		real (kind=8) function GDUforcebar(p)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble
			real (kind=8)::q,p
			integer (kind=4), parameter::limit=1000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-10
			integer (kind=4)::neval,ier,last
			real (kind=8)   ::temp1,temp2,temp3,d
			real(8), parameter::xa=0D0,xb=1D0,xc=100D0



					temp1=0D0
					temp2=0D0
					temp3=0D0
					GDUforcebar=0D0
					q=(1D0-p)/p
					if (p.eq.0D0) then
						q=qgrid(2)!/10D0
						q=(1D0-q)/q
					endif



					! dabs(tiny(0D0))
					d=qgrid(2)/100D0!/100D0
					! d=0D0
					call StupidIntegrateG8(anc,d,0.5D0,5000,temp1,ier)
					call StupidIntegrateG8(anc,0.5D0,1D0-d,5000,temp2,ier)

					! GDUbar=-q**2*(temp1+temp2)/Globn**2/pi**2
					! GDUbar=-16D0*q**2*(temp1+temp2)/Globn**2
					GDUforcebar=-2D0*q**2*(temp1+temp2)/Globn**2/pi**2

					if ( isnan(GDUforcebar) ) then
						write(*,*) "GDUbar NaN"
						write(*,*) "p",p,q
					end if

			contains
			real (kind=8) function anc(t)
				use Elliptic
				real (kind=8)::t

				anc=(1D0-t)/t
				anc=(SDUbarsplint(1D0/(q*anc+1D0)))*anc*&
				((-1D0+2D0*t)*Kell(2D0*dsqrt(t*(1D0-t)))+Eell(2D0*dsqrt(t*(1D0-t))))/t**3

				if ( isnan(anc) ) then
					write(*,*) 'Found a NaN'
					write(*,*) p,t,anc,q
					call exit()
				end if
			end function anc
		end function GDUforcebar

		real (kind=8) function GUDforcebar(p)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble
			real (kind=8)::q,p
			integer (kind=4), parameter::limit=1000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-10
			integer (kind=4)::neval,ier,last
			real (kind=8)   ::temp1,temp2,temp3,d
			real(8), parameter::xa=0D0,xb=1D0,xc=100D0



					temp1=0D0
					temp2=0D0
					temp3=0D0
					GUDforcebar=0D0
					q=(1D0-p)/p
					if (p.eq.0D0) then
						q=qgrid(2)!/10D0
						q=(1D0-q)/q
					endif



					! dabs(tiny(0D0))
					d=qgrid(2)/100D0!/100D0
					! d=0D0
					call StupidIntegrateG8(anc,d,0.5D0,5000,temp1,ier)
					call StupidIntegrateG8(anc,0.5D0,1D0-d,5000,temp2,ier)

					! GDUbar=-q**2*(temp1+temp2)/Globn**2/pi**2
					! GDUbar=-16D0*q**2*(temp1+temp2)/Globn**2
					GUDforcebar=-2D0*q**2*(temp1+temp2)/Globn**2/pi**2

					if ( isnan(GUDforcebar) ) then
						write(*,*) "GDUbar NaN"
						write(*,*) "p",p,q
					end if

			contains
			real (kind=8) function anc(t)
				use Elliptic
				real (kind=8)::t

				anc=(1D0-t)/t
				anc=(SUDbarsplint(1D0/(q*anc+1D0)))*anc*&
				((-1D0+2D0*t)*Kell(2D0*dsqrt(t*(1D0-t)))+Eell(2D0*dsqrt(t*(1D0-t))))/t**3

				if ( isnan(anc) ) then
					write(*,*) 'Found a NaN'
					write(*,*) p,t,anc,q
					call exit()
				end if
			end function anc
		end function GUDforcebar


		real (kind=8) function GDUForcebar2(p)
			use STLS
			use QuadPackDouble
			use AuxiliaryFunctions
			implicit none
			real (kind=8),parameter::mintwopisq=-1D0/pi**2/2D0
			integer (kind=4), parameter::limit=1000000
			integer (kind=4)::neval,ier,last,iord(limit)
			real (kind=8)   ::p,abserr,alist(limit),blist(limit),rlist(limit)&
							  ,elist(limit),temp1,temp2,dp,pp




			pp=p
			if ( p.eq.0D0 ) then
				pp=qgrid(2)
			elseif (pp.eq.1D0) then
				pp=1D0-qgrid(2)
			end if
			dp = qgrid(2)/100D0
			if ( (1D0-dp).le.0d0 ) then
				write(*,*) "dp",dp
				call exit()
			end if
			!call dqagie ( anc, 0d0, 1, 1D-6, 1D-6, limit , GpForce, abserr, &
	 		!			 neval, ier, alist, blist, rlist, elist, iord, last )
			! call StupidIntegrateNC(anc,dp,p-dp,10000,temp1,ier)
			! call StupidIntegrateNC(anc,p+dp,1D0-dp,10000,temp2,ier)
			! call StupidIntegrateNC(anc,dp,1D0-dp,30000,temp1,ier)
			! call GaussAdaptQuad2(anc,dp,1D0-dp,1D-8,temp1)
			! call StupidIntegrateG8(anc,dp,1D0-dp,10000,temp1,ier)
			! call StupidIntegrateG8(anc,dp,pp-dp,5000,temp1,ier)
			! call StupidIntegrateG8(anc,pp+dp,1D0-dp,5000,temp2,ier)
			call StupidIntegrateG8(anc,0D0,pp-dp,5000,temp1,ier)
			call StupidIntegrateG8(anc,pp+dp,1D0-dp,5000,temp2,ier)

			ier=0
			! temp2=0d0

			if (ier.gt.0) then
				write(*,*) "Integration of GpForcebar has failed."
				write(*,*) "dqagie threw an error, ier:",ier
				write(*,*) "See documentation for more information."
				call exit(-ier)
			endif
			if ( isnan(temp1+temp2) ) then
				write(*,*) 'Found a NaN'
				write(*,*) pp,temp1,temp2
				call exit()
			end if

			! GpForcebar=GpForcebar*mintwopisq/Globn/(p-1D0)!*2D0/(2D0*pi)**2
			GDUForcebar2=4D0*(temp1+temp2)*mintwopisq/Globn**2/(pp-1D0)!*2D0/(2D0*pi)**2

			contains
				real (kind=8) function anc(t)
					use Elliptic
					implicit none
					real (kind=8)::t,temp,temp2,temp3


					! call splint(qgrid,SDUbargrid,SDUbarspline,qsteps,t,temp3)
					if ( t.gt.1D0 ) then
						write(*,*) "t > 1",pp,t
					end if
					temp3=SDUbarsplint(t)
					temp2=pp+t-2D0*pp*t
					temp=2D0*dsqrt(pp*t*(pp-1D0)*(t-1D0))/(temp2)
					! write(*,*) "p,t",p,t,temp

					if ( temp.ge.1D0 ) then
						anc=temp2
					else
						anc=(temp2)*Eell(temp)+(t-pp)*Kell(temp)
					end if
					anc=anc*(temp3)*(t-1D0)/t**4



					if ( isnan(anc) ) then
						write(*,*) 'Found a NaN'
						write(*,*) pp,t,anc
						call exit()
					end if
				end function anc
		end function GDUForcebar2

		real (kind=8) function GDUbar0(p)
			use STLS
			use AuxiliaryFunctions
			use QuadPackDouble
			real (kind=8)::q,p
			integer (kind=4), parameter::limit=1000
			real (kind=8)   , parameter::minpi=-pi**(-1),tol=1D-10
			integer (kind=4)::neval,ier,last,iord(limit)
			real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
								,elist(limit),temp1,temp2,temp3
			real(8), parameter::xa=0D0,xb=1D0,xc=100D0



					temp1=0D0
					temp2=0D0
					temp3=0D0
					q=(1D0-p)/p


					! dabs(tiny(0D0))
					call StupidIntegrateNC(anc,qgrid(1)/100D0,p-qgrid(1)/100D0,5050,temp1,ier)
					call StupidIntegrateNC(anc,p+qgrid(1)/100D0,1D0-qgrid(1)/100D0,5050,temp2,ier)

					GDUbar0=-(temp1+temp2)/Globn**2/pi**2*(p-1D0)

			contains
			real (kind=8) function anc(t)
				use Elliptic
				real (kind=8)::t

				! ! The single K method
				temp1=p+t-2D0*p*t
				anc=(t-1D0)/(t**2*temp1)
				anc=anc*Kell(2D0*dsqrt(p*t*(p-1D0)*(t-1D0))/temp1)
				anc=anc*(SDUbarsplint(t))
				! anc=anc*(SDUbarsplint(t)-Globnu)



				if ( isnan(anc) ) then
					write(*,*) 'Found a NaN'
					write(*,*) p,t,anc
					call exit()
				end if
			end function anc
		end function GDUbar0


	real (kind=8) function PGGDU(q)
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=10000
		real (kind=8)   , parameter::minpi=-pi**(-1)
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp

		call dqagie ( anc, 0d0, 1, 1D-8, 1D-8, limit , temp, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		PGGDU=-2D0*temp*GlobkfU*GlobkfD/pi/Globn**2

		contains
			real (kind=8) function anc(r)
				implicit none
				real (kind=8)::r

				if (r.eq.0D0) then
					anc=0D0
				endif
				anc=bessel_jn(0,q*r)*bessel_jn(1,GlobkfU*r)*bessel_jn(1,GlobkfD*r)/r**2

			end function anc
	end function PGGDU

	real (kind=8) function GPGGDU(q)
		use QuadPackDouble
		implicit none
		real (kind=8)::q


		GPGGDU=-PGGDU(q)/Vc(q)
	end function GPGGDU

	real (kind=8) function PGGssp(s,sp,q)
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=10000
		real (kind=8)   , parameter::minpi=-pi**(-1)
		integer (kind=4)::neval,ier,last,iord(limit),s,sp
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,k1,k2

		k1=kfs(s)
		k2=kfs(sp)

		call dqagie ( anc, 0d0, 1, 1D-10, 1D-10, limit , temp, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )



		PGGssp=-2D0*temp*k1*k2/pi/(ns(s)+ns(sp))**2

		contains
			real (kind=8) function anc(r)
				implicit none
				real (kind=8)::r

				if (r.eq.0D0) then
					anc=k1*k2/4D0
					return
				endif
				anc=bessel_jn(0,q*r)*bessel_jn(1,k1*r)*bessel_jn(1,k2*r)/r**2
			end function anc
	end function PGGssp

	real (kind=8) function SDUInt(q)
		use STLS
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=100000,lenw=limit*5
		real (kind=8)   , parameter::minpi=-1D0/pi
		integer (kind=4)::neval,ier,last,iord(limit),iwork(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,work(lenw),epsabs,epsrel,ws(4)

		!write(*,*) lenw

		epsabs=1D-2
		epsrel=1D-2

		temp1=Ens(1)-Ens(-1)
		temp2=0D0
		ws(1)=-(q*GlobkfD+q**2/2D0+temp1)
		ws(2)=q*GlobkfD-q**2/2D0-temp1
		ws(3)=-q*GlobkfU+q**2/2D0-temp1
		ws(4)=q*GlobkfU+q**2/2D0-temp1
		temp1=0D0


		call StupidIntegrate(anc,max(0D0,min(ws(1),ws(3))),1.5D0*max(0D0,ws(1),ws(2),ws(3),ws(4)),1000,temp1,ier)



		SDUInt=minpi*temp1

		contains
			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				anc=dimag(Chi0ssp(-1,1,q,w))
			end function
	end function SDUInt

	real (kind=8) function SDDInt(q)
		use STLS
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=100000,lenw=limit*5
		real (kind=8)   , parameter::minpi=-1D0/pi
		integer (kind=4)::neval,ier,last,iord(limit),iwork(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,work(lenw),epsabs,epsrel,ws(4)

		!write(*,*) lenw

		epsabs=1D-2
		epsrel=1D-2

		temp1=0D0!Ens(1)-Ens(1)
		temp2=0D0
		ws(1)=-(q*GlobkfD+q**2/2D0+temp1)
		ws(2)=q*GlobkfD-q**2/2D0-temp1
		ws(3)=-q*GlobkfU+q**2/2D0-temp1
		ws(4)=q*GlobkfU+q**2/2D0-temp1
		temp1=0D0


		call StupidIntegrate(anc,max(0D0,min(ws(1),ws(3))),1.5D0*max(0D0,ws(1),ws(2),ws(3),ws(4)),1000,temp1,ier)


		SDDInt=minpi*temp1

		contains
			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				anc=dimag(Chi0ssp(-1,-1,q,w))
			end function
	end function SDDInt

	real (kind=8) function SUUInt(q)
		use STLS
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=100000,lenw=limit*5
		real (kind=8)   , parameter::minpi=-1D0/pi
		integer (kind=4)::neval,ier,last,iord(limit),iwork(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,work(lenw),epsabs,epsrel,ws(4)

		!write(*,*) lenw

		epsabs=1D-2
		epsrel=1D-2

		temp1=0D0!Ens(1)-Ens(1)
		temp2=0D0
		ws(1)=-(q*GlobkfD+q**2/2D0+temp1)
		ws(2)=q*GlobkfD-q**2/2D0-temp1
		ws(3)=-q*GlobkfU+q**2/2D0-temp1
		ws(4)=q*GlobkfU+q**2/2D0-temp1
		temp1=0D0


		call StupidIntegrate(anc,max(0D0,min(ws(1),ws(3))),1.5D0*max(0D0,ws(1),ws(2),ws(3),ws(4)),1000,temp1,ier)


		SUUInt=minpi*temp1

		contains
			real (kind=8) function anc(w)
				implicit none
				real (kind=8)::w

				anc=dimag(Chi0ssp(1,1,q,w))
			end function
	end function SUUInt

	real (kind=8) function S0ssp(s,sp,q)
		implicit none
		real    (kind=8)::q,temp1,temp2
		integer (kind=4)::s,sp


			temp1=(Ens(sp)-Ens(s))/q + 5D-1*q
			temp2=(Ens(sp)-Ens(s))/q - 5D-1*q
			temp1=temp1/kfs(s)
			temp2=temp2/kfs(sp)

			S0ssp=0D0

			S0ssp=(ns(sp)*anc(temp2)-ns(s)*anc(temp1))

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x
				real (kind=8), parameter::inpi=1D0/pi

				if (x.ge.1D0) then
					anc=0D0
				elseif (x.le.-1D0) then
					anc=1D0
				elseif (dabs(x).lt.1D0) then
					anc=5D-1-inpi*(dasin(x)+x*dsqrt(1D0-x**2))
				endif
			end function anc
	end function S0ssp


	subroutine S0check(s,sp,q,res)
		implicit none
		integer (kind=4), intent(in)::s,sp
		real    (kind=8), intent(in)::q
		real    (kind=8), intent(out)::res(2)
		real    (kind=8)::temp1,temp2

			temp1=(Ens(sp)-Ens(s))/q + 5D-1*q
			temp2=(Ens(sp)-Ens(s))/q - 5D-1*q
			temp1=temp1/kfs(s)
			temp2=temp2/kfs(sp)


			!res=(/-ns(s)*anc(temp1),ns(sp)*anc(temp2)/)

			res=(/-ns(s)*anc(temp1),ns(sp)*anc(temp2)/)

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x
				real (kind=8), parameter::inpi=1D0/pi

				if (x.ge.1D0) then
					anc=0D0
				elseif (x.le.-1D0) then
					anc=1D0
				elseif (dabs(x).lt.1D0) then
					anc=5D-1-inpi*(dasin(x)+x*dsqrt(1D0-x**2))
				endif
			end function anc
	end subroutine S0Check


	subroutine SDUIntCheck(q,res)
		use STLS
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		real (kind=8),intent(out)::res(2)
		integer (kind=4), parameter::limit=100000,lenw=limit*5
		real (kind=8)   , parameter::minpi=-1D0/pi
		integer (kind=4)::neval,ier,last,iord(limit),iwork(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2,work(lenw),epsabs,epsrel,ws(4)

		!write(*,*) lenw

		epsabs=1D-2
		epsrel=1D-2

		temp1=Ens(1)-Ens(-1)
		temp2=0D0
		ws(1)=-(q*GlobkfD+q**2/2D0+temp1)
		ws(2)=q*GlobkfD-q**2/2D0-temp1
		ws(3)=-q*GlobkfU+q**2/2D0-temp1
		ws(4)=q*GlobkfU+q**2/2D0-temp1
		temp1=0D0


		call StupidIntegrate(anc1,max(0D0,min(ws(1),ws(3))),1.5D0*max(0D0,ws(1),ws(2),ws(3),ws(4)),10000,res(1),ier)

		!call StupidIntegrate(anc2,max(0D0,min(ws(1),ws(3))),1.5D0*max(0D0,ws(1),ws(2),ws(3),ws(4)),10000,res(2),ier)

		call StupidIntegrate(anc2,max(0D0,min(ws(1),ws(3))),1.5D0*max(0D0,ws(1),ws(2),ws(3),ws(4)),10000,res(2),ier)


		res=res*minpi
		res(1)=res(1)!*ns(-1)
		res(2)=-res(2)!*ns(1)

		contains
			real (kind=8) function anc1(w)
				implicit none
				integer (kind=4)::s,sp
				real (kind=8)::w,v,qbs(2),temp4
				real (kind=8), parameter::nzero=1D0/2D0/pi



				v=(w+Ens(1)-Ens(-1))/q+q/2D0
				v=v/kfs(-1)
				temp4=0D0
				if ((1D0-v**2).gt.0D0) then
				    temp4=dsqrt(1D0-v**2)
				endif
				temp4=temp4/q*kfs(-1)

				anc1=-temp4*nzero

			end function anc1

			real (kind=8) function anc2(w)
				implicit none
				integer (kind=4)::s,sp
				real (kind=8)::w,v,qbs(2),temp3
				real (kind=8), parameter::nzero=1D0/2D0/pi

				v=(w+Ens(1)-Ens(-1))/q-q/2D0
				v=v/kfs(1)
				temp3=0D0
				if ((1D0-v**2).gt.0D0) then
				    temp3=dsqrt(1D0-v**2)
				endif
				temp3=temp3/q*kfs(1)

				anc2=temp3*nzero

			end function anc2

            real (kind=8) function wssp(s,sp,w)
                implicit none
                integer (kind=4)::s,sp
                real    (kind=8)::w
                real    (kind=8), parameter:: zero(2)=0D0

                wssp=w+En(real(sp, kind=8),zero)-En(real(s, kind=8),zero)
            end function wssp
	end subroutine SDUIntCheck



	real (kind=8) function ns(s)
		implicit none
		integer (kind=4)::s

		if (s.eq.1) then
			ns=Globnu
		elseif (s.eq.-1) then
			ns=Globnd
		else
			write(*,*) "ns passed an invalid spin index s:",s
			call exit(-s)
		endif
	end function ns

	real (kind=8) function Ens(s)
		implicit none
		integer (kind=4)::s

		if (s.eq.1) then
			Ens=GlobZeemHalf
		elseif (s.eq.-1) then
			Ens=-GlobZeemHalf
		else
			write(*,*) "Ens passed an invalid spin index s:",s
			call exit(-s)
		endif
	end function Ens

    real (kind=8) function kfs(s)
        implicit none
        integer (kind=4)::s

        if (s.eq.1) then
            kfs=GlobkfU
        elseif (s.eq.(-1)) then
            kfs=GlobkfD
        else
            write(*,*) "Wrong value passed to kfs, s=+-1", s
            call exit(-1)
        endif
    end function kfs

		real (kind=8) function kfs2(s)
        implicit none
        real (kind=8)::s

        if (s.eq.1) then
            kfs2=GlobkfU
        elseif (s.eq.(-1)) then
            kfs2=GlobkfD
        else
            write(*,*) "Wrong value passed to kfs2, s=+-1D0", s
            call exit(-1)
        endif
    end function kfs2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!! Testing !!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	real (kind=8) function PGGDDInt(q)
		use STLS
		use QuadPackDouble2
		implicit none
		integer (kind=4), parameter::limit=100000,steps=1000
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8)
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2

		th=0D0
		PGGDDInt=0D0
		do i=1,steps
			call StupidIntegrate(anc, 0D0, 100D0*Globkf0, 1000, temp1, ier)

			call dqagie ( anc, 100d0*Globkf0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
						  neval, ier, alist, blist, rlist, elist, iord, last )

			PGGDDInt=PGGDDInt+temp1+temp2
			th=th+dth
		enddo

		PGGDDInt=2D0*pi*(PGGDDInt*dth)/Globnd**2


		contains
			real (kind=8) function anc(qp)
				implicit none
				real (kind=8)::qp,qb

				qb=dsqrt(q**2+qp**2-2D0*qp*q*dcos(th))
				anc=S0ssp(-1,-1,qb)-Globnd
			end function anc
	end function PGGDDInt

	real (kind=8) function PGGDDInt2(q)
		use QuadPackDouble2
		use Elliptic
		use STLS
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8), singtol=1D-8
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,temp1,temp2,temp3

		th=0D0
		PGGDDInt2=0D0

		temp1=0D0
		temp2=0D0
		temp3=0D0

		call dqage ( anc, 0d0, 1D0, 1D-10, 1D-10, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 50D0, 1D-10, 1D-10, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 50D0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		PGGDDInt2=2D0*pi*(4D0*q*(temp1+temp2+temp3))/Globnd**2

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x

				anc=(S0ssp(-1,-1,q*x)-Globnd)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)
			end function anc
	end function PGGDDInt2

	real (kind=8) function PGGUUInt(q)
		use STLS
		use QuadPackDouble2
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8)
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2

		th=0D0
		PGGUUInt=0D0
		temp1=0D0
		temp2=0D0

		do i=1,steps
			call StupidIntegrate(anc, 0D0, 100D0*Globkf0, 1000, temp1, ier)

			call dqagie ( anc, 100d0*Globkf0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
						  neval, ier, alist, blist, rlist, elist, iord, last )

			PGGUUInt=PGGUUInt+temp1+temp2
			th=th+dth
		enddo

		PGGUUInt=2D0*pi*(PGGUUInt*dth)/Globnu**2

		contains
			real (kind=8) function anc(qp)
				implicit none
				real (kind=8)::qp,qb

				qb=dsqrt(q**2+qp**2-2D0*qp*q*dcos(th))
				anc=S0ssp(1,1,qb)-Globnu
			end function anc
	end function PGGUUInt


	real (kind=8) function PGGUUInt2(q)
		use QuadPackDouble2
		use Elliptic
		use STLS
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8), singtol=1D-8
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,temp1,temp2,temp3

		th=0D0
		PGGUUInt2=0D0

		temp1=0D0
		temp2=0D0
		temp3=0D0

		call dqage ( anc, 0d0, 1D0, 1D-10, 1D-10, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 50D0, 1D-10, 1D-10, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 50D0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		PGGUUInt2=2D0*pi*(4D0*q*(temp1+temp2+temp3))/Globnu**2

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x

				!anc=(S0ssp(1,1,q*x)-Globnu)*Kell((4D0*x)/(1D0+x)**2)*x/(1D0+x)
				anc=(S0ssp(1,1,q*x)-Globnu)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)
			end function anc
	end function PGGUUInt2


	real (kind=8) function PGGDUInt(q)
		use STLS
		use QuadPackDouble2
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8)
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2

		th=0D0
		PGGDUInt=0D0
		do i=1,steps
			call StupidIntegrate(anc, 0D0, 40D0*Globkf0, 1000, temp1, ier)

			call dqagie ( anc, 40d0*Globkf0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
						  neval, ier, alist, blist, rlist, elist, iord, last )

			PGGDUInt=PGGDUInt+temp1+temp2
			th=th+dth
		enddo

		PGGDUInt=8D0*pi*(PGGDUInt*dth)/Globn**2
		!PGGDUInt=8D0*pi*(PGGDUInt*dth-1D0)/Globn**2


		contains
			real (kind=8) function anc(qp)
				implicit none
				real (kind=8)::qp,qb

				qb=dsqrt(q**2+qp**2-2D0*qp*q*dcos(th))
				anc=S0ssp(-1,1,qb)-Globnu
			end function anc
	end function PGGDUInt

	real (kind=8) function PGGDUInt2(q)
		use QuadPackDouble2
		use Elliptic
		use STLS
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8), singtol=1D-8
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,temp1,temp2,temp3

		th=0D0
		PGGDUInt2=0D0

		temp1=0D0
		temp2=0D0
		temp3=0D0

		call dqage ( anc, 0d0, 1D0, 1D-10, 1D-10, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 10D0, 1D-10, 1D-10, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 10D0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		PGGDUInt2=8D0*pi*(4D0*q*(temp1+temp2+temp3))/Globn**2

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x

				!anc=(S0ssp(1,1,q*x)-Globnu)*Kell((4D0*x)/(1D0+x)**2)*x/(1D0+x)
				anc=(S0ssp(-1,1,q*x)-Globnu)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)
			end function anc
	end function PGGDUInt2

	real (kind=8) function PGGUDInt(q)
		use STLS
		use QuadPackDouble2
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8)
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp1,temp2

		th=0D0
		PGGUDInt=0D0
		do i=1,steps
			call StupidIntegrate(anc, 0D0, 100D0*Globkf0, 1000, temp1, ier)

			call dqagie ( anc, 100d0*Globkf0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
						  neval, ier, alist, blist, rlist, elist, iord, last )

			PGGUDInt=PGGUDInt+temp1+temp2
			th=th+dth
		enddo

		PGGUDInt=8D0*pi*(PGGUDInt*dth)/Globn**2
		!PGGDUInt=8D0*pi*(PGGDUInt*dth-1D0)/Globn**2


		contains
			real (kind=8) function anc(qp)
				implicit none
				real (kind=8)::qp,qb

				qb=dsqrt(q**2+qp**2-2D0*qp*q*dcos(th))
				anc=S0ssp(-1,1,qb)-Globnu
			end function anc
	end function PGGUDInt

	real (kind=8) function PGGUDInt2(q)
		use QuadPackDouble2
		use Elliptic
		use STLS
		implicit none
		integer (kind=4), parameter::limit=100000,steps=100
		real    (kind=8), parameter::dth=2D0*pi/real(steps,kind=8), singtol=1D-8
		real (kind=8)::q,th
		integer (kind=4)::neval,ier,last,iord(limit),i
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,temp1,temp2,temp3

		th=0D0
		PGGUDInt2=0D0

		temp1=0D0
		temp2=0D0
		temp3=0D0

		call dqage ( anc, 0d0, 1D0, 1D-10, 1D-10, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 50D0, 1D-10, 1D-10, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 50D0, 1, 1D-10, 1D-10, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		PGGUDInt2=8D0*pi*(4D0*q*(temp1+temp2+temp3))/Globn**2

		contains
			real (kind=8) function anc(x)
				implicit none
				real (kind=8)::x

				!anc=(S0ssp(1,1,q*x)-Globnu)*Kell((4D0*x)/(1D0+x)**2)*x/(1D0+x)
				anc=(S0ssp(1,-1,q*x)-Globnd)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)
			end function anc
	end function PGGUDInt2


	real (kind=8) function DMatssp(s,sp,q)
		use QuadPackDouble
		implicit none
		real (kind=8)::q
		integer (kind=4), parameter::limit=10000
		real (kind=8)   , parameter::minpi=-pi**(-1)
		integer (kind=4)::neval,ier,last,iord(limit),s,sp
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit),temp,k1,k2

		k1=kfs(s)
		k2=kfs(sp)

		call dqagie ( anc, 0D0, 1, 1D-10, 1D-10, limit , temp, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )



		DMatssp=temp*k1*k2/2D0/pi

		contains
			real (kind=8) function anc(r)
				implicit none
				real (kind=8)::r

				if (r.eq.0D0) then
					anc=k1*k2/4D0
					return
				endif
				anc=bessel_jn(0,q*r)*bessel_jn(1,k1*r)*bessel_jn(1,k2*r)/r
			end function anc
	end function DMatssp

	subroutine Chi0Import(filename,Chi0out,qsteps,wsteps)
		implicit none
		character(len=32), intent(in) :: filename
		real(8), allocatable, intent(out) ::Chi0out(:,:)
		integer(4) ::ios,u,NProc,IntSteps,i
		integer(4), intent(out) :: qsteps,wsteps
		character(len=64) :: empty,fmt2

		u=99
		open(unit=u, file=filename, iostat=ios, status="old", action="read")
		if ( ios /= 0 ) then
			write(*,*) filename, ios
			stop "Error opening file in Chi0Import"
		endif

		read(u,'(A)') empty !!! Throw out header lines
		read(u,'(A)') empty !!!
		read(u,'(A)') empty !!!

		read(u,'(4I10)') NProc, IntSteps, qsteps, wsteps !!! Read metadata

		!write(*,*) qsteps,wsteps
		allocate(Chi0out(qsteps,wsteps))

		write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'

		read(u,'(A)') empty !!! Throw out more comments
		read(u,'(A)') empty

		do i=1,qsteps
			read(u,'(A)') empty !!! Read q,w,dw line. (Useless for the log grid)
			read(u,fmt2) Chi0out(i,:) !!! Read Chi0 line
		enddo

		close(unit=u)

	end subroutine Chi0Import

	subroutine qwImport(filename,steps,qwout)
		implicit none
		character(len=64), intent(in) :: filename
		real(8), allocatable, intent(out) ::qwout(:)
		integer(4) ::i,u,ios
		integer(4), intent(in)::steps
		!real(8) ::
		!character(len=32) ::

		u=99
		open(unit=u, file=filename, iostat=ios, status="old", action="read")
		if ( ios /= 0 ) then
			write(*,*) filename,ios
			stop "Error opening file in qwgrid"
		endif

		allocate(qwout(steps))


		do i=1,steps
			read(u,*) qwout(i)
		enddo

		close(unit=u)

	end subroutine qwImport


	subroutine Chi0ImportAll(Cname,Chi0out,Qname,qout,Wname,wout,qsteps,wsteps)
		implicit none
		character(len=64), intent(in) :: Cname,Qname,Wname
		real(8),allocatable, intent(out)::Chi0out(:,:),qout(:),wout(:)
		integer(4),intent(out)::qsteps,wsteps


		call Chi0Import(Cname,Chi0out,qsteps,wsteps)
		call qwImport(Qname,qsteps,qout)
		call qwImport(Wname,wsteps,wout)
	end subroutine Chi0ImportAll

	subroutine ChiBuildGrid(ReChi0grid,ImChi0grid,qgrid,wgrid,qsteps,wsteps,Gfn,ReChigridOut,ImChigridOut)
		implicit none
		integer(4), intent(in)::qsteps,wsteps
		real(8), intent(in)::ReChi0grid(qsteps,wsteps),ImChi0grid(qsteps,wsteps),qgrid(qsteps),wgrid(wsteps)
		real(8), allocatable, intent(out)::ReChigridOut(:,:),ImChigridOut(:,:)
		real(8), external::Gfn
		complex(8),parameter::one=dcmplx(1D0,0D0)
		complex(8)::temp,temp2
		integer(4)::i,j
		real(8), parameter::a=-2d0*pi
		real(8)::temp3

		if (.not.allocated(ReChigridOut)) allocate(ReChigridOut(qsteps,wsteps))
		if (.not.allocated(ImChigridOut)) allocate(ImChigridOut(qsteps,wsteps))

		do i=1,qsteps
			do j=1,wsteps
				temp=dcmplx(ReChi0grid(i,j),ImChi0grid(i,j))
				temp2=dcmplx(a*Gfn(qgrid(i),wgrid(j))/qgrid(i),0D0)
				temp=temp/(one+temp2*temp)
				!temp3=a*Gfn(qgrid(i),wgrid(j))/qgrid(i)
				ReChigridOut(i,j)=real(temp,kind=8)!ReChi(ReChi0grid(i,j),ImChi0grid(i,j),temp3)!
				ImChigridOut(i,j)=dimag(temp)!ImChi(ReChi0grid(i,j),ImChi0grid(i,j),temp3)!
			enddo
		enddo
		contains
			real(8) function ReChi(R0,I0,fxc)
				implicit none
				real(8)::R0,I0,fxc

				ReChi=R0
				!if(dabs(fxc).lt.1D-5) return
				!ReChi=(R0+fxc*(R0**2+I0**2))/((1D0+fxc*R0)**2+(fxc*I0)**2)
			end function ReChi

			real(8) function ImChi(R0,I0,fxc)
				implicit none
				real(8)::R0,I0,fxc

				ImChi=I0
				!if(dabs(fxc).lt.1D-5) return
				!ImChi=I0/((1D0+fxc*R0)**2+(fxc*I0)**2)
			end function ImChi

	end subroutine ChiBuildGrid

	real(8) function ImChiInterp(q,w)
		use Interp
		implicit none
		real(8):: q,w,tempq,tempw!,res

		ImChiInterp=0D0
		tempq=q
		tempw=w
		if (q.lt.Modqgrid(1)) tempq=Modqgrid(1) !return
		if (w.lt.Modwgrid(1)) tempw=Modwgrid(1) !return
		if (q.gt.Modqgrid(Modqsteps)) tempq=Modqgrid(Modqsteps)
		if (w.gt.Modwgrid(Modwsteps)) tempw=Modwgrid(Modwsteps)

		!call BiLinterp(tempq,tempw,Modqsteps,Modqgrid,Modwgrid,ImChiGrid,ImChiInterp)
		call BiCubInterp(tempq,tempw,Modqsteps,Modqgrid,Modwgrid,ImChiGrid,ImChiInterp)
	end function ImChiInterp

	real(8) function SUD(q)
		use AuxiliaryFunctions
		!use QuadPackDouble
		implicit none
		real(8)::q,temp
		integer (kind=4), parameter::limit=1000
		real (kind=8)   , parameter::minpi=-1D0/pi, tol=1D-8
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
							,elist(limit)

		!call dqagie ( anc, 0D0, 1, tol, tol, limit , temp, abserr, &
		!			  neval, ier, alist, blist, rlist, elist, iord, last )

		call GaussAdaptQuad(anc,0D0,Modwgrid(Modwsteps),1D-8,temp)


		SUD=temp*minpi

		contains
			real(8) function anc(w)
				implicit none
				real(8)::w

				anc=ImChiInterp(q,w)
			end function anc
	end function SUD

	real(8) function fxcUD2(q)
		use QuadPackDouble
		implicit none
		real    (kind=8)::q,temp1,temp2,temp3,a,nsp
		integer (kind=4), parameter::limit=10000
		real (kind=8)   , parameter::minpi=1D0/2D0/pi, tol=1D-8
		integer (kind=4)::neval,ier,last,iord(limit)
		real (kind=8)   ::abserr,alist(limit),blist(limit),rlist(limit)&
						  ,elist(limit)

		nsp=GlobnD


		a=4D0/Globn**2

		fxcUD2=0D0

		!call StupidIntegrate(anc,1D-12,10D0*Globkf0,1000,temp,ier)
		!write(*,*)
		call dqage ( anc, 0d0, 1D0, tol, tol, 2, limit , temp1, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqage ( anc, 1d0, 20D0, tol, tol, 2, limit , temp3, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		call dqagie ( anc, 20D0, 1, tol, tol, limit , temp2, abserr, &
					  neval, ier, alist, blist, rlist, elist, iord, last )

		fxcUD2=a*(temp1+temp2+temp3)*q*8D0*pi

		contains
			real (kind=8) function anc(x)
				use AuxiliaryFunctions
				use Elliptic
				implicit none
				real (kind=8)::x,temp

				anc=0D0
				if (q*x.gt.Modqgrid(Modqsteps)) return
				call splint(Modqgrid,SUDgrid,SUDspline,Modqsteps,q*x,temp)
				anc=(temp-nsp)*Kell((2D0*dsqrt(x))/(1D0+x))*x/(1D0+x)

			end function anc

	end function fxcUD2

	real(8) function GUD(q)
		use AuxiliaryFunctions
		implicit none
		real(8)::q
		real(8), parameter::twopi=2D0*pi

		if (q.gt.Modqgrid(Modqsteps)) then
			q=Modqgrid(Modqsteps)
		endif
		call splint(Modqgrid,fxcgrid,fxcspline,Modqsteps,q,GUD)
		GUD=-GUD*q/twopi
	end function GUD

	subroutine Gupdate(Gfn,Gout,Gsplout)
		use AuxiliaryFunctions
		implicit none
		real(8),external::Gfn
		real(8), intent(out)::Gout(Modqsteps),Gsplout(Modqsteps)
		integer(4)::i

		do i=1,Modqsteps
			Gout(i)=Gfn(Modqgrid(i))
		enddo

		call spline(Modqgrid,Gout,Modqsteps,1D33,1D33,Gsplout)
	end subroutine Gupdate

	real(8) function GUDspl(q,w)
		use AuxiliaryFunctions
		implicit none
		real(8)::q,w

		call splint(Modqgrid,GSCold2,GSCspline2,Modqsteps,q,GUDspl)
		!GUDspl=0d0
	end function GUDspl

	subroutine SUpdate()
		use AuxiliaryFunctions
		implicit none
		integer(4)::i

		write(*,*) "SUpdate"
		do i=1,Modqsteps
			!write(*,*) i
			SUDgrid(i)=SUD(Modqgrid(i))
		enddo

		call spline(Modqgrid,SUDgrid,Modqsteps,1D33,1D33,SUDspline)
	end subroutine SUpdate

	subroutine fxcUpdate()
		use AuxiliaryFunctions
		implicit none
		integer(4)::i

		write(*,*) "fxcupdate"
		do i=1,Modqsteps
			fxcgrid(i)=fxcUD2(Modqgrid(i))
		enddo

		call spline(Modqgrid,fxcgrid,Modqsteps,1D33,1D33,fxcspline)

	end subroutine fxcUpdate

	subroutine fDUbarUpdate()
		use AuxiliaryFunctions
		use STLS
		implicit none
		real(8) :: temp(3),q
		real(8), parameter::mintwopi=-2D0*pi
		integer(4)::i

		write(*,*) "fxcupdate"

		call GDUqDerivs21(0D0,qgrid(3),temp)
		fxcgrid(qsteps) = mintwopi*temp(1)
		fxcgrid(1) = 0D0


		do i=2,qsteps-1
			q=(1D0-qgrid(i))/qgrid(i)
			fxcgrid(i)=mintwopi*GSCold(i)/q
		enddo

		call spline(qgrid,fxcgrid,qsteps,1D33,1D33,fxcspline)

	end subroutine fDUbarUpdate

	subroutine SCUpdate()
		implicit none
		integer(4)::i
		!real(8)::

		write(*,*) "ChiBuildGrid"
		call ChiBuildGrid(ReChi0grid,ImChi0grid,Modqgrid,Modwgrid,Modqsteps,&
												Modwsteps,GUDspl,ReChigrid,ImChigrid)

		call Supdate()
		do i=1,Modqsteps
			write(56,*) Modqgrid(i)/Globkf0, SUDgrid(i), S0ssp(1,-1,Modqgrid(i))
		enddo
		close(unit=56)

		call fxcUpdate()
	end subroutine SCUpdate


	real(8) function w1(q,k,pm,s,sp)
		implicit none
		real(8)::q,k,s,sp,pm

		w1=-q**2+pm*q*k+GlobZeemHalf*(sp-s)
	end function w1

	real(8) function w2(q,k,pm,s,sp)
		implicit none
		real(8)::q,k,s,sp,pm

		w2=q**2+pm*q*k+GlobZeemHalf*(sp-s)
	end function w2

end module PhysicsPol2DEG
