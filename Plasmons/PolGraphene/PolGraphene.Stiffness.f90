program PolGrapheneStiffness
use Constants
use Globals
use PhysicsPolGraphene
use AuxiliaryFunctions
use fxc2DEG
use fxcPGG
use Response
!use SomeModule !As many as you need
implicit none
real (kind=8)::n,z,Stiff,excdz,ztilde,fxUDUD,abserr
real (kind=8), allocatable::LindLine(:),EpsLine(:),qwgrid(:,:)&
						   ,Chigrid(:,:),EV(:,:),EVwline(:,:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,labelnum,temp,k,neval,ier
integer (kind=4), allocatable::labels(:),info(:,:)
character (len=32)::fmt1,fmt2,star,fmt3
character (len=32),allocatable::empty(:)




	!!!! Initialize some situational constants
	Globgs=2D0
	Globgv=2D0
	n=1D12/cmSq2a0sq
	write(*,*) "n",n
	z=-4D-2
	call ConstInit(n,z)
	call KernelConstr(Globrs,-z,GlobKernVec)


	ztilde=(dsqrt(1D0+z)+dsqrt(1D0-z))/dsqrt(2D0)
	write(*,*) "ztilde", ztilde
	excdz=(1D0-0.95D0)*GlobZeem/2D0!/n/z
	write(*,*) excdz
	call DPPGGud(0D0,GlobkfU,GlobkfD,fxUDUD,abserr,neval,ier)
	!excdz=-3D0*n*z/2D0
	write(*,*) "excdz", excdz*2D0/n/z, fxUDUD, fxUDUD*n*z/excdz/2D0/2D0/pi
	Stiff=excdz*dlog((1D0-z)/(1D0+z))/(4D0*z*GlobEf**2)*gam**2
	write(*,*) "Stiff1", Stiff
	Stiff=Stiff-gam**2*ztilde/z/GlobEf/2D0
	write(*,*) "Stiff2", Stiff
	Stiff=Stiff+gam**2/2D0/excdz
	write(*,*) "Stiff3", Stiff
	write(*,*) "Z", GlobZeem-2D0*excdz
	write(*,*) "S/Z*", Stiff/GlobZeem*Globkf0**2
	write(*,*) "Ef", GlobEf,GlobZeem, 2D0*9.5D-1*GlobZeem, gam

end program PolGrapheneStiffness
















