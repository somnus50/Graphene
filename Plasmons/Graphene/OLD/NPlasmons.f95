program Plasmons
	use constants	
	use GLindFunctions
	use NGLindFunctions
	use HelperFunctions
	implicit none
	real (kind=8)::dw,q
	real (kind=8)::array(5,3),LindScan(101,101),Lindy(100)
	integer (kind=4)::steps,i,j,k



	dw=3D-2
	steps=100
	open(unit=99,file='LindScan.dat')
	do i=1,101
		read(99,*) LindScan(i,:)
	enddo	
	close(unit=99)

	write(*,*) "q , w1 , w2"
!i=50	
	do i=2,101
		do j=2,101
			Lindy(j-1)=LindScan(i,j)
		enddo
		q=LindScan(i,1)
		call FindBracketList2(Lindy,q,dw,array)
		do k=1,5
			if (array(k,1).gt.0D0) then
				write(*,*) array(k,:), ((array(k,2)+array(k,3))/2D0)
			endif
		enddo
	enddo


end program Plasmons
