program TwoDEGSTLSGsc
use Constants
use Globals
use Physics2DEG
use STLS
use AuxiliaryFunctions
implicit none
real (kind=8)::rs,qs(2)
integer (kind=4)::i,j,k,SCsteps
!complex (kind=8)::


	rs=1D0
	SCsteps=1
	qsteps=100



	!!!! Initialize some situational constants
	call ConstInit(rs,0D0,GlobEf,Globeta,Globfx,Globfc) 
	qs=(/0D0,10D0*Globkf/)
	write(*,*) qs/Globkf
	call SCInit(SCsteps,qsteps,qs)
	write(*,*) qgrid(1)/Globkf,qgrid(qsteps)/Globkf
!	call exit()

	do i=1,qsteps

	write(1,*) qgrid(i)/Globkf
	enddo

	!!! Initialize some files for data storage
	open(unit=9, file='STLS0.dat')
	open(unit=19, file='plots.dat')
	

	write(9,*) "# STLS0"
	write(9,*) "# q/kf, Gp"
	write(19,*) "# STLS10"
	write(19,*) "# q/kf, Struct, SQ, Chi"

	SCCount=1

		!write(*,*) "SC step:",i
		call spline(qgrid,G1,qsteps,1D31,1D31,GSCspline)
		do j=1,qsteps
			write(*,*)"SC step:",i,j
			GSC(j)=Gp(qgrid(j))!FUNG(qgrid(j))
		enddo
		do j=1,qsteps
			G1(j)=0.5D0*GSC(j)+0.5D0*GSCold(j)
			GSCold(j)=G1(j)
		enddo
		if (i.eq.1) then
			do k=1,qsteps
				write(9,*) qgrid(k)/Globkf, G1(k)
			enddo
		endif

	do k=1,qsteps
		write(19,*) qgrid(k)/Globkf, Struct(qgrid(k)), SQ(qgrid(k)), Chi(qgrid(k),10D0)
	enddo


	do i=1,qsteps

	write(2,*) qgrid(i)/Globkf
	enddo	

	!!!!!!!!!!!!!!!!!!!
	!!!Do some Stuff!!!
	!!!!!!!!!!!!!!!!!!!





	!!! Close files
	close(unit=9)
	close(unit=19)

end program TwoDEGSTLSGsc
