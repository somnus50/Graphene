program TwoDEGSTLSGsc
use Constants
use Globals
use Physics2DEG
use STLS
use AuxiliaryFunctions
implicit none
real (kind=8)::rs,xs(2),vs(2),resa,resfn,rss(4),dx
real (kind=8), allocatable::Ggrid(:,:),plas(:,:,:),xgrid(:)
integer (kind=4)::i,j,k,xsteps
character (len=32)::tempchar


	rs=1D0
	rss=(/0.1D0,1D0,5D0,10D0/)
	xs=(/0D0,10D0/)
	xsteps=100
	allocate(Ggrid(4,xsteps),plas(4,xsteps,2),xgrid(xsteps))
	dx=(xs(2)-xs(1))/real(xsteps , kind=8)
	vs=(/0D0,10D0/)
	do i=1,xsteps
		xgrid(i)=xs(1)+dx*real(i-1, kind=8)
	enddo




	do i=1,4
		call ConstInit(rss(i),0D0,GlobEf,Globeta,Globfx,Globfc) 
		do j=1,xsteps
			write(*,*) i,j
			Ggrid(i,j)=GpA(xgrid(j)*Globkf)
			call RootFinder(eps2,vs(1)*GlobEf,vs(2)*GlobEf,xgrid(j)*Globkf/5D0,1D-6,resa,resfn)
			plas(i,j,:)=(/ resa/GlobEf,resfn /)
		enddo
	enddo

!	open(unit=29,file='Plas0.F.dat')
!	write(29,*) "#Plasmons STLS0"
!	write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


!	do i=1,qsteps
!		call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
!		write(29,*) qgrid(i)/Globkf,resa/GlobEf,LowQPlasmon(qgrid(i))/GlobEf,resfn
!	enddo

!	close(unit=29)
	open(unit=19, file='GpAnalytic.dat')
	write(19,*) "# q/kf, rs: 0.1, 1, 5, 10"

	do k=1,xsteps
		write(19,*) xgrid(k), Ggrid(:,k)
	enddo
	close(unit=19)



	open(unit=29, file='PlasmonsGpA.dat')
	write(29,*) "#Plasmons STLS10"

	do k=1,xsteps
		write(29,*) xgrid(k)/5D0, plas(:,k,1), plas(:,k,2)
	enddo

	close(unit=29)



end program TwoDEGSTLSGsc
