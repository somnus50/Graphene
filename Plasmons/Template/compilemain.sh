#!/bin/bash
set -e

usage() {
cat << EOF
Usage: compilemain.sh [OPTION] file.f90

   -c <ifort|mpif90|gfortran> Choose a compiler. Default is ifort.
   -l                         Links LAPACK during compilation.
   -h                         Display this help.
EOF
exit 1
}

bindir="bin"
mkdir -p $bindir
libdir=$(readlink -f $(git rev-parse --show-cdup))"/Plasmons/libs"
lpckdir="$HOME/bin/lib/lapack"
modopt="-I$libdir/ -L$libdir -lmodules"
comp="ifort"

while getopts :lc: opts
do	case "$opts" in
	l)  if [[ ! -d $lpckdir ]]; then
		   echo ""
		   echo "LAPACK include directory doesn't exist."
		   echo "If you compiled LAPACK yourself, please put the libraries in $lpckdir."
		   echo "Alternatively you can change the lpckdir variable in this script."
		   echo '   e.g. lpckdir="" if it is already in your path'
		   exit 1
		fi
		lpckopt=" -I$lpckdir -L$lpckdir -llapack -lblas"
		echo "Linking LAPACK from $lpckdir";;
	c)  comp=$OPTARG
		if [[ "$comp" =~ ^(ifort|gfortran|mpif90)$ ]]; then
			echo "Compiling with $comp."
		else
			echo "Invalid compiler."
			echo ""
			usage >&2
		fi;;
	*)	usage >&2;;
	esac
done
shift "$((OPTIND-1))"

for file in $@
do
if [[ ! -r $file ]]; then
	echo "File: $file is not accessible..."
	echo ""
	usage >&2
else
	prog=${file%.*}
	cmd="$comp -O3 $file -o $bindir/$prog.bin $modopt $lpckopt"
	#echo "Compiling..."
	echo $cmd
	$cmd
fi
done
