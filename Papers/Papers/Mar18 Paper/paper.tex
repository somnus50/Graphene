\documentclass[aps,prb,twocolumn,showpacs]{revtex4-1}

\usepackage{graphicx,bm,color,amsmath}

\usepackage{epsfig}
%\usepackage{cite}
\usepackage{url}
\usepackage{caption}
\usepackage{lipsum}
\usepackage{wrapfig}
\usepackage{bbold,color}


\newcommand{\bfB}{{\bf B}}
\newcommand{\bfq}{{\bf q}}
\newcommand{\bfr}{{\bf r}}
\newcommand{\bfk}{{\bf k}}
\newcommand{\ua}{\uparrow}
\newcommand{\da}{\downarrow}
\newcommand{\dy}{\displaystyle}
\newcommand{\hxc}{h^{\rm xc}}
\newcommand{\exc}{e_{\rm xc}}

\newcommand{\CU}{\bf \color{blue}}
\newcommand{\checkthis}{\bf \color{red}}

\newcommand{\alda}{^{\scriptscriptstyle \rm ALDA}}


\newcommand{\ud}{{\uparrow\downarrow}}
\newcommand{\du}{{\downarrow\uparrow}}
\newcommand{\uu}{{\uparrow\uparrow}}
\newcommand{\dd}{{\downarrow \downarrow}}
\newcommand{\si}{\sigma}
\newcommand{\sib}{{\bar{\sigma}}}
\newcommand{\sip}{{\sigma'}}



\newcommand{\bfqp}{{\bf q}_{||}}
\newcommand{\qp}{q_{||}}
\newcommand{\qz}{q_{z}}

\newcommand{\bfp}{{\bf p}}
\newcommand{\bfj}{{\bf j}}
\newcommand{\bfv}{{\bf v}}

\newcommand{\bfm}{{\bf m}}
\newcommand{\bfA}{{\bf A}}

\newcommand{\bsi}{{\bar{\sigma}}}
\newcommand{\RPA}{\rm RPA}
\newcommand{\tnsr}[1]{\underline{\underline{#1}}}

\begin{document}

\title{A very impressive title about STLS}

\author{Matthew J. Anderson}
\affiliation{Department of Physics and Astronomy, University of Missouri, Columbia, Missouri 65211, USA}

\author{Carsten A. Ullrich}
\affiliation{Department of Physics and Astronomy, University of Missouri, Columbia, Missouri 65211, USA}


\date{\today }

\begin{abstract}
The abstract should be about fully spin-dependent STLS, STLS-0, and spin-waves in 2DEG.
\end{abstract}

\pacs{
31.15.ee, %Time-dependent density functional theory
31.15.ej, %Spin-density functionals
71.45.Gm, %Exchange, correlation, dielectric and magnetic response functions, plasmons
73.21.Fg  %Electron states and collective excitations in multilayers, quantum wells, mesoscopic, and nanoscale systems: Quantum wells
}

\maketitle

\section{Introduction}

Say something interesting here.


\section{Background}

\subsection{Ground-state DFT for noncollinear spin-dependent systems}

\vspace{-2mm}

The vast majority of applications of DFT and TDDFT in electronic structure theory and in quantum chemistry
are for systems with a fixed spin quantization
axis: at each point in space, the Kohn-Sham single-particle wave functions are products of spatial orbitals
and up- or down-spinors.
This formalism is applicable if the system has a magnetization whose strength
may vary in space but whose direction is constant and coincides with the chosen spin quantization axis.
Examples are conventional ferromagnets and antiferromagnets, any finite system placed in a homogeneous
magnetic field, as well as open- or closed-shell molecules in the absence of external magnetic fields.


In general, magnetic fields are vector fields whose strength {\em and } direction may vary in space.
``Spin-up'' and ``spin-down'' then cease to be good quantum numbers, giving rise to
systems which have noncollinear spins. Noncollinear magnetism occurs frequently in nature, for instance
in magnetic metals such $\gamma$-Fe whose ground state features helical spin-density waves;
in exchange-frustrated solids such as spin glasses and Kagom\'e antiferromagnetic lattices; and
in molecular magnets and magnetic clusters \cite{Kubler1988,Sandratskii1998,Freeman2004,Peralta2007,Sharma2007,Zhou2017}.
In noncollinear spin systems, the single-particle wave
functions have a two-component form which is a mixture of up- and down-spinor wave functions, and
the Kohn--Sham equation couples the up- and down-components of $\Psi_j(\bfr)$ \cite{Barth1972,Gunnarsson1976}:
\begin{widetext}
\begin{equation} \label{2.2}
\Psi_j(\bfr) = \left( \! \begin{array}{c} \varphi_{j\ua}(\bfr)\\[2mm]
\varphi_{j\da}(\bfr)\end{array} \! \right) , \qquad
\sum_{\sigma' = \uparrow\downarrow}
\left[ \left( - \frac{\nabla^2}{2} + v_{\rm H}(\bfr)\right)
\delta_{\sigma \sigma'}
+ v_{\rm ext,\sigma\sigma'}(\bfr) + v_{\rm xc,\sigma \sigma'}(\bfr) \right]
\varphi_{j\sigma'}(\bfr) = \varepsilon_j \varphi_{j\sigma}(\bfr) ,
\end{equation}
\end{widetext}
where $v_{\rm H}(\bfr)$ is the Hartree potential and $v_{\rm ext,\sigma\sigma'}(\bfr)$ is a spin-dependent external potential
(here and in the following, Greek letters are used to denote spin indices).
From the self-consistent solution, the spin-density matrix follows as
\begin{equation}\label{2.3}
\begin{split}
\underline{n}({\bf r}) &= \sum_{j=1}^{\rm occ}
\Psi_j(\bfr) \Psi_j^\dagger(\bfr) = \sum_{j=1}^{\rm occ}\left( \begin{array}{cc}
|\varphi_{j\ua}(\bfr)|^2&
\varphi_{j\ua}(\bfr)\varphi^*_{j\da}(\bfr)\\[2mm]
\varphi_{j\da}(\bfr)\varphi^*_{j\ua}(\bfr)&
|\varphi_{j\da}(\bfr)|^2\end{array}\right) \\
&\equiv
 \left( \begin{array}{cc} n_\uu & n_\ud\\[2mm] n_\du & n_\dd \end{array} \right) \;.
 \end{split}
\end{equation}
The xc energy $E_{\rm xc}[\underline n]$ is defined as a functional of the spin-density matrix,
and the xc potential is given by the functional derivative
$v_{\rm xc,\sigma\sigma'}(\bfr) = \delta E_{\rm xc}[\underline n]/\delta
n_{\sigma \sigma'}(\bfr)$.

Instead of the spin-density matrix $\underline n$, we can use the total density $n(\bfr) =  n_\uu(\bfr)+n_\dd(\bfr)$ and the magnetization
$\bfm(\bfr)$ as basic variables, so that the xc energy functional becomes $E_{\rm xc}[n,\bfm]$.
The three components of the magnetization are
$m_i(\bfr) = \mbox{tr} \{ \hat\sigma_i \underline{n}(\bfr)\}$,
where  $\hat \sigma_{x,y,z}$ are the Pauli matrices. The Kohn-Sham equation (\ref{2.2}) can then be rewritten as
\begin{equation}\label{2.4}
\left[ \left( - \frac{\nabla^2}{2} + v_{\rm eff}(\bfr)  \right)\underline I + \mu_B \vec \sigma \cdot \bfB_{\rm eff}(\bfr)
\right] \Psi_j(\bfr) = \varepsilon_j \Psi_j(\bfr),
\end{equation}
where $\underline I$ is the $2\times 2$ unit matrix,
$v_{\rm eff} = v_{\rm H} +  \mbox{tr}\left\{\underline v_{\rm ext} + \underline v_{\rm xc} \right\}/2$,
and $B_{{\rm eff},i} = \mbox{tr}\left\{\hat \sigma_i (\underline v_{\rm ext} + \underline v_{\rm xc}) \right\}$.
The three components of the xc magnetic field are $\mu_B B_{{\rm xc},i}(\bfr) = \delta E_{\rm xc}[n,\bfm]
/\delta m_i(\bfr)$; $\mu_B$ is the Bohr magneton.

As always in DFT, the key to success is finding good approximations for the xc functional. The most widely used
method, which is practically synonymous with ``DFT for noncollinear spins'', is to assume a local
reference frame in which the local $z$ axis is the spin quantization axis \cite{Kubler1988,Sandratskii1998,Heinonen1999}.
One then defines the LSDA as
\begin{equation}
\mu_B \bfB_{\rm xc}^{\rm LSDA}(\bfr) = \frac{\delta E_{\rm xc}^{\rm LSDA}[n,\bfm]}{\delta \bfm(\bfr)}
= \frac{\partial e_{\rm xc}^{\rm unif}}{\partial m} \frac{n(\bfr) \bfm(\bfr)}{m(\bfr)} \:,
\end{equation}
where $e_{\rm xc}^{\rm unif}$ is the xc energy density of a uniform spin-polarized electron gas. By construction,
$\bfB_{\rm xc}^{\rm LSDA}(\bfr)$ is always parallel to $\bfm(\bfr)$; similarly for gradient approximations (GGAs)
in which a rotation to a local spin reference frame is used. However, the exact xc functional will depend on the
transverse gradients of $\bfm(\bfr)$, which causes local magnetic torques. This local xc
torque is important for the description of spin dynamics \cite{Capelle2001}, and it is missing in the LSDA.

There have been several proposals in the literature to develop xc functionals that include magnetic torque effects
\cite{Kleinman1999,Katsnelson2003,Sharma2007,Scalmani2012,Bulik2013,Eich2013a,Eich2013b}. Eich and Gross used the spin-spiral state
of the electron gas as reference system \cite{Eich2013a}.
Scalmani and Frisch \cite{Scalmani2012} proposed a modification of the GGA which leads to nonvanishing magnetic
torques; however, this approximation imposes the unphysical restriction of treating longitudinal and transverse gradients of $\bfm$
equally \cite{Eich2013b}. The optimized effective potential (OEP) approach by Sharma {\em et al.} is free from such restrictions,
but applies only to the exchange-only limit \cite{Sharma2007}. Furthermore, each of these methods is quite complicated and
difficult to implement, in particular if one wants to go beyond ground-state calculations and study response properties.

\vspace{-2mm}

\subsection{Noncollinear-spin TDDFT}

\vspace{-2mm}

The vast majority of applications of TDDFT can be broadly categorized as follows \cite{TDDFTbook}:

\vspace{-2mm}

\begin{itemize}\itemsep0mm

\item Linear (or higher-order) response TDDFT, i.e., calculating the frequency-dependent response to a periodic perturbation.
The main application of linear response TDDFT is to calculate excitations and associated
optical and magnetic spectral properties.

\item Real-time TDDFT, i.e., calculating the time evolution under the influence of time-dependent fields
of arbitrary strength. For weak fields this provides efficient ways  to obtain spectral properties, but strongly driven,
highly nonlinear, or ultrafast processes can also be treated.

\end{itemize}

\vspace{-2mm}

Noncollinear magnetic systems have been studied using TDDFT in both the linear-response regime
\cite{Shao2003,Wang2004,Wang2006,Vahtras2007,Capelle2010,Huix2010,Rinkevicius2010,Li2010,Li2011,Li2012,Bernard2012}
and in the real-time domain \cite{Stamenova2013,Stamenova2016,Simoni2017,Ding2014,Goings2016,Peralta2015,Krieger2015,Elliott2016a,Elliott2016b,Krieger2017}.

In this work, we will focus on the {\em linear-response} regime.
The frequency-dependent linear response equation for the spin-density matrix is given by \cite{TDDFTbook}
\begin{equation}\label{nsd}
n^{(1)}_{\sigma\sigma'}(\bfr,\omega) = \sum_{\tau,\tau'} \int d^3r'  \chi_{\sigma\sigma',\tau \tau'}(\bfr,\bfr',\omega)
v^{(1)}_{\tau\tau'}(\bfr',\omega)\:,
\end{equation}
where $ \chi_{\sigma\sigma',\tau \tau'}(\bfr,\bfr',\omega)$ is the noninteracting Kohn-Sham response function.
The effective perturbing potential $v^{(1)}_{\tau\tau'}$ is the sum of external, Hartree and xc contributions; the latter is given by
\begin{equation}\label{vsd}
v^{\rm (1)xc}_{\tau \tau'} ({\bf r}',\omega) =
\sum_{\lambda\lambda'} \int\!d^3 r'' \: f^{\rm xc}_{\tau \tau',\lambda \lambda'}
({\bf r}',{\bf r}'',\omega) n^{(1)}_{\lambda \lambda'}({\bf r}'',\omega) \;.
\end{equation}
The key quantity here is the xc kernel $f^{\rm xc}_{\tau \tau',\lambda \lambda'}({\bf r},{\bf r}',\omega)$, which needs to be approximated.
Instead of the spin-density-matrix response (\ref{nsd}) we can also work with the density-magnetization response equation, which
describes the response of a system to frequency-dependent magnetic fields coupling to the spins (we do not consider orbital currents).
The associated xc kernel, in LSDA, is given by



\begin{equation}
h^{\rm xc}_{ij}(\bfr,\bfr',\omega) = \left. \frac{\partial^2 [n e^{\rm unif}_{\rm xc}]}{\partial m_i \partial m_j}\right|_{\bfm(\bfr)}
\delta(\bfr - \bfr') \:.
\end{equation}
In LSDA, the xc kernel is frequency-independent and local.
Our group has extensively used this formalism in earlier work \cite{Ullrich2002,Ullrich2003,Karimi2016a}. The xc kernels
$f^{\rm xc}_{\tau\tau',\lambda\lambda'}$ and $h^{\rm xc}_{ij}$ are related through a simple transformation.

\hspace{1cm}
The linear response formalism is used to calculate excitation energies and related spectral properties \cite{TDDFTbook}.
In the noncollinear spin case, there have been two main areas of applications: to describe spin-flip excitations
and molecular multiplets \cite{Capelle2010,Shao2003,Wang2004,Wang2006,Vahtras2007,Huix2010,Rinkevicius2010,Li2010,Li2011,Li2012,Bernard2012},
and to calculate collective spin plasmon excitations and spin waves in the presence of SOC
\cite{Ullrich2002,Ullrich2003,Karimi2016a}.


\subsection{Calculating collective excitations}

To obtain the collective modes of a system, we need the response function.
To find the interacting response function we return to the definition of $\delta\tnsr{n}$:
\begin{equation}
\delta \tnsr{n} = \tnsr{\chi}\cdot v_{ext} \:.
\end{equation}
In TDDFT, we obtain $\delta\tnsr{n}$ as the response of a noninteracting system to an effective perturbation:
\begin{equation}
\delta \tnsr{n} = \tnsr{\chi}^{(0)} \cdot (f_{Hxc}+v_{ext})
\end{equation}
so that the interacting response function can be constructed as
\begin{equation}
\tnsr{\chi} = \left(\mathbb{1}-\tnsr{\chi}^{(0)}\cdot f_{Hxc}  \right)^{-1}\cdot\tnsr{\chi}^{(0)}\:. \label{FullResponse}
\end{equation}
Therefore, we only need to calculate the non-interacting response function and the Hartree-xc kernel. The non-interacting response function is defined in terms of the band structure of the system and the xc kernel is an approximation of many-body effects that we are left to choose.
The non-interacting linear response is given by
\begin{equation}
 \chi^{(0)}(\bfr,\bfr',\omega) = \sum_{j,k} (f_k-f_j) \frac{\varphi_j(\bfr)\varphi_k^*(\bfr)\varphi_j^*(\bfr')\varphi_k(\bfr')}{\omega-\omega_{jk}+i\eta} \:,
\end{equation}
where the $f_j$ are occupation factors. In the case of a homogeneous system, the matrix inverse in
Eq. (\ref{FullResponse}) becomes a simple algebraic relation, so that
\begin{equation}
\chi(\bfq,\omega) = \frac{\chi^{(0)}(\bfq,\omega)}{1-(v_\bfq+ f_{xc})\chi^{(0)}(\bfq,\omega)} \:.
\end{equation}
From this, we find the dynamic screening function
\begin{equation}
\epsilon(\bfq,\omega) = 1 - (v_\bfq+ f_{xc}) \chi^{(0)}(\bfq,\omega) \:.
\end{equation}
Finding the zeroes of this function gives us collective modes, such as plasmons, analogous to the way that plasmons are defined in classical electrodynamics.

To describe spin waves, we need to include the spin degrees of freedom.
The spin-resolved response function becomes a tensor whose components are defined as:
\begin{equation}
\chi_{\sigma\sigma'\tau\tau'}^{(0)}(\bfr,\bfr',\omega)= \sum_{j,k} (f_k-f_j) \frac{\varphi_{j\sigma}(\bfr)\varphi_{k\sigma'}^*(\bfr)\varphi_{j\tau}^*(\bfr')\varphi_{k\tau'}(\bfr')}{\omega-\omega_{jk}+i\eta}
\end{equation}
This tensor is diagonal when there is no transverse magnetization, i.e. $n_{\ua\da}=n_{\da\ua}=0$, which implies that the interactions separate cleanly into spin-conserving and spin-flipping channels:
\begin{equation}
\tnsr{\chi}^{(0)}=\left( \begin{array}{cccc}
\chi_{\ua\ua,\ua\ua}^{(0)} & 0 & 0 & 0\\
0 & \chi_{\ua\da,\ua\da}^{(0)} & 0 & 0 \\
0 & 0 & \chi_{\da\ua,\da\ua}^{(0)} & 0 \\
0 & 0 & 0 & \chi_{\da\da,\da\da}^{(0)}\\
  \end{array}\right)
\end{equation}
The interacting response function then becomes:
\begin{widetext}
\begin{equation}
\tnsr{\chi}=\left( \begin{array}{cccc}
1-(v_\bfq+f_{\ua\ua,\ua\ua}^{xc})\chi_{\ua\ua,\ua\ua}^{(0)} &  &  & -(v_\bfq+f_{\ua\ua,\da\da}^{xc})\chi_{\ua\ua,\ua\ua}^{(0)}\\
 & 1-f_{\ua\da,\ua\da}^{xc}\chi_{\ua\da,\ua\da}^{(0)} &  &  \\
 &  & 1-f_{\da\ua,\da\ua}^{xc}\chi_{\da\ua,\da\ua}^{(0)} &  \\
-(v_\bfq+f_{\da\da,\ua\ua}^{xc})\chi_{\da\da,\da\da}^{(0)} &  &  & 1-(v_\bfq+f_{\da\da,\da\da}^{xc})\chi_{\da\da,\da\da}^{(0)}\\
  \end{array}\right)^{-1}
\end{equation}
\end{widetext}

By finding poles in the interacting response function, we find collective excitations. When there is no external potential, the solutions that we find are eigenmodes of the system.
Since the response function decouples the spin-flip and spin-conserving channels, we can search for plasmons and magnons separately. To find spin-flip collective excitations that correspond to magnons, it is sufficient to solve:
\begin{equation} \label{17}
(1-f_{\ua\da,\ua\da}^{xc}\chi^{(0)}_{\ua\da,\ua\da}(\bfq,\omega))(1-f_{\da\ua,\da\ua}^{xc}\chi^{(0)}_{\da\ua,\da\ua}(\bfq,\omega))=0 \:.
\end{equation}

\subsection{Spin-Waves}




\subsection{fxc effects}

{\checkthis We know that LSDA becomes a poor choice when systems behave less like its reference system, the electron gas.  We believe that a good choice for our graphene system will be PGG+STLS \cite{Petersilka1996,Singwi1968}. Our group's efforts have previously shown that PGG makes a better approximation of $Z^*$ in a quasi-2DEG \cite{Karimi2017}. PGG is also an orbital based functional that does not rely on a reference system which is good for these Dirac Cone systems.}

%%% Carsten Stuff


%\textbf{PGG kernel for reference \cite{Petersilka1996}}
\begin{equation}\label{PGGkernelr}
f_{\rm x}^{\rm PGG}(\bfr,\bfr') = - 2\:
\frac{\left|\sum_{j=1}^{N_{\rm occ}} \varphi_{j}(\bfr) \varphi^*_{j}(\bfr')\right|^2}
{|\bfr - \bfr'| n(\bfr) n(\bfr')} =
2\:\frac{g_0(\bfr,\bfr')-1}{|\bfr - \bfr'|} \:,
\end{equation}

%\textbf{Slater for reference \cite{GiulianiVignale}}
\begin{equation} \label{slater}
v_{\rm x}^{\rm Slater}(\bfr) = -2\int  d^3 r'\:
\frac{|\sum_j^{\rm N_{occ}} \varphi_{j}(\bfr)\varphi_{j}^*(\bfr')|^2}
{|\bfr - \bfr'|n(\bfr)}
= 2\int d^3r' \:\frac{[g_0(\bfr,\bfr')-1]}{|\bfr - \bfr'|} \:n(\bfr') \:.
\end{equation}


%\textbf{Slaters for reference \cite{Ullrich2012}}
\begin{equation} \label{slaters}
v^{\rm x}_{\sigma\sigma'}(\bfr) = -\sum_{ij}^{\rm occ}\sum_{\alpha \tau}
\int d^3r' \: \frac{\varphi_{j\sigma}(\bfr)\varphi^*_{j\alpha}(\bfr') \varphi_{i\alpha}(\bfr')\varphi^*_{i\tau}(\bfr)}{|\bfr - \bfr'|}
 \:   n^{-1}_{\tau\sigma'}(\bfr) \:.
\end{equation}

The central object in linear-response TDDFT is the xc kernel $f_{\rm xc}$, which was defined
in Sec. 2.2. Having an approximation for $f_{\rm xc}$ allows one to calculate excitation energies and optical
spectra in atoms, molecules and solids, including collective excitations such as plasmons, spin waves, and excitons.
Our goal is to derive and test explicit approximations for the noncollinear xc kernel.

In the exchange-only limit, it is possible to linearize the OEP equation and obtain the exact exchange kernel \cite{TDDFTbook};
however, this is computationally very costly. The PGG kernel (\ref{PGGkernelr}) is an
approximation to the exact exchange kernel, and follows from the Slater exchange potential (\ref{slater})
using a trick: when calculating the frequency-independent exchange kernel via the functional derivative
$f_{\rm x}(\bfr,\bfr') = \delta v_{\rm x}^{\rm Slater}(\bfr)/\delta n(\bfr')$ (which could, with considerable effort, be done exactly), we
simply ignore the density dependence of the pair correlation function \cite{GiulianiVignale}.
Despite this crude approximation, the PGG kernel has been useful for atomic and molecular excitation
energies \cite{Petersilka1996,Petersilka2000}.


Using the same trick, the generalization of the PGG kernel for noncollinear systems can be derived from the
Slater exchange potential (\ref{slaters}), with the following result:
\begin{equation} \label{PGGspin}
f^{\rm x}_{\sigma\sigma',\alpha\alpha'}(\bfr,\bfr') =
-\sum_{ij}^{\rm occ} \sum_{\tau\tau'} \varphi_{j\sigma}(\bfr) \varphi^*_{i\tau}(\bfr)n^{-1}_{\tau\sigma'}(\bfr)
\frac{1}{|\bfr - \bfr'|} \varphi_{i\alpha}(\bfr')\varphi^*_{j\tau'}(\bfr')n^{-1}_{\tau'\alpha'}(\bfr') \:.
\end{equation}
The noncollinear PGG kernel was used in an earlier study on the spin gap \cite{Capelle2010}, and it was found
that it gives reasonably good approximations to the xc spin stiffness for the Li atom.

An obvious test for the noncollinear PGG kernel will be the spin waves in 2DEGs in the presence of SO coupling
which we studied in our previous work, using LSDA. However, PGG will be unlikely to produce the correct spin-wave stiffness:
correlation effects will be very important.
Another issue is that Slater combined with PGG will not satisfy Larmor's theorem for spin waves \cite{Karimi2016a}.
The reason is that the PGG kernel is not the full functional derivative of $v_{\rm x}^{\rm Slater}$. Likewise, it is unclear whether
the PGG kernel satisfies exact conditions such as the zero-force or zero-torque theorems. We will systematically investigate
these formal properties and derive ways in which our approximate functionals can be modified to enforce them \cite{Kurzweil2008}.


\subsubsection{PGG}
{\CU Use Sharzad's previous PGG results.}
{\checkthis
The next step in this project is to investigate xc effects for this system. Our next xc choice is the PGG exchange kernel because it is non-local.  As an auxiliary result we have derived the PGG kernel for graphene using the pseudospinor wavefunctions. The result collapses to the same result as the 2DEG\cite{Karimi2014} when near the Dirac cone.

\begin{equation}
f_{x;\ua\ua,\ua\ua}^{PGG}(q)=f_{x;\da\da,\da\da}^{PGG}(q)=\frac{-k_f^2}{2\pi n^2} \int_0^\infty \frac{J_0(q\rho)}{\rho^2}J_1^2(k_f\rho)d\rho
\label{GraphenePGG}
\end{equation}
\begin{equation}
f_{x;\da\ua,\da\ua}^{PGG}(q)=f_{x;\ua\da,\ua\da}^{PGG}(q)=\frac{-1}{4k_{f\ua}k_{f\da}} \int_0^\infty \frac{J_0(q\rho)}{\rho^2}J_1(k_{f\ua}\rho)J_1(k_{f\da}\rho)d\rho
\label{GraphenePGGSpin}
\end{equation}
Where $J_i(x)$ are Bessel functions of the first kind.

We wish to calculate this kernel using the full spin-resolved TBWFs in the future. We also plan to write modules to implement our developed kernels into existing TDDFT software suites.}


\vspace{-2mm}

\subsubsection{The STLS approach for noncollinear spin systems}



\vspace{-1mm}

In order to develop a correlation functional for noncollinear spin dynamics, we
propose to extend the so-called STLS formalism, which goes back to the work of Singwi, Tosi, Land and Sj\"olander \cite{Singwi1968}.
In this theory, the static local field factor $G(q)$ of the homogeneous electron gas
(which is related to the xc kernel by $f_{\rm xc}(q) =- v_q G(q)$, where $v_q$ is the Coulomb interaction) is defined via \cite{GiulianiVignale}
\begin{equation}\label{stls1}
\chi(q,\omega) = \frac{\chi_0(q,\omega)}{1 - v_q[1-G(q)]\chi_0(q,\omega)} \:,
\end{equation}
where $\chi(q,\omega)$ is the interacting density-density response function and $\chi_0(q,\omega)$ is the Lindhard function.
The local field factor can be expressed as
\begin{equation}\label{stls2}
G(q) = -\frac{1}{n} \int \frac{d^dq'}{(2\pi)^d} \:\frac{ \bfq \cdot \bfq'}{q^2} \frac{v_{q'}}{v_q}[S(\bfq - \bfq')n^{-1}-1],
\end{equation}
where $S(q)$ is the static structure factor. Via the fluctuation-dissipation theorem, $S(q)$ is given by
\begin{equation}\label{stls3}
S(q) = -\frac{1}{\pi} \int_0^\infty \Im \chi(q,\omega)d\omega  \:.
\end{equation}


\begin{figure}
    \includegraphics[width=0.50\textwidth]{G.pdf}
  \caption{Local field factor $G(q)$ of a 2DEG with $r_s=1$. Red:
fit to QMC data \cite{Davoudi2001}. Green: PGG. Blue: STLS, Eq. (\ref{stls2}).} \label{STLS}
\end{figure}


Equations (\ref{stls1})--(\ref{stls3}) are solved self-consistently
(convergence is reached quite rapidly). The left panel of
Fig. \ref{STLS} shows results for a 2DEG with $r_s=1$, comparing with $G(q)$ obtained from a fit to Quantum Monte Carlo (QMC) data
\cite{Davoudi2001}. The exchange-only PGG approach clearly underestimates the exact result; STLS gives a significant improvement.
The small-$q$ behavior in STLS does not respect the compressibility sum rule, which, however, can be corrected for
\cite{Vashishta1972}.
The STLS method has been very successfully used to calculate correlation energies, where it performs
much better than the RPA or other simpler methods. It also yields a good description of plasmon dispersions \cite{GiulianiVignale}.

A collinear spin-dependent STLS generalization can be found in the literature \cite{Lobo1969,Dobson2004,Tas2004,Kumar2009}.
We now propose to go further and include situations where the spin is noncollinear.
To do so, we introduce the spin-resolved static structure factor as
$S_{\sigma\sigma',\tau\tau'}(\bfr,\bfr') = \langle \hat n_{\sigma\sigma'}(\bfr')\hat n_{\tau'\tau}(\bfr)\rangle - n_{\sigma\sigma'}(\bfr)
n_{\tau'\tau}(\bfr')$, where $\hat n_{\sigma\sigma'}(\bfr) = \hat \psi^\dagger_{\sigma'}(\bfr)\hat \psi_\sigma(\bfr)$
is the spin-density operator in second quantization.
We then obtain the general noncollinear xc kernel for $d$-dimensional homogeneous systems as follows:
\begin{equation} \label{spinSTLS}
f^{\rm xc}_{\sigma\sigma',\tau\tau'}(q) = \int \frac{d^d q'}{(2\pi)^d} \:\frac{ \bfq \cdot \bfq'}{q^2} \frac{v_{q'}}{v_q}
\sum_\alpha \left[ \sum_\beta S_{\sigma\beta,\alpha \tau'}(|\bfq - \bfq|')n^{-1}_{\beta \sigma'} - \delta_{\sigma\alpha}\delta_{\sigma'\tau'}\right]
n^{-1}_{\alpha \tau} \:.
\end{equation}
This, together with straightforward spin-dependent generalizations of Eqs. (\ref{stls1}) and (\ref{stls3}), then defines the
noncollinear STLS formalism.


\subsubsection{STLS-0}
Instead of performing a full self-consistent STLS calculation, one can choose a better initial function and perform only one step. We call this \emph{STLS-0 [Initial Functional]}, e.g. \emph{STLS-0 LDA}. This provides a substantial speed boost when compared to the full STLS calculation.

\textbf{Figure comparing STLS-0}

\section{Conclusion}

\pagebreak

\bibliography{comp}

\end{document}
