program GrapheneTest
use Constants
use Globals
use PhysicsGraphene
use AuxiliaryFunctions
!use SomeModule !As many as you need
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,MPIa,MPIb,LindSum&
		,dq(2),Lind,MPILind,limits(4),k(2),dkx,dky,phia,phib
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,steps
!complex (kind=8)::
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	call ConstInit(1D12/cmSq2a0sq,0D0,GlobEf,Globeta,Globfx,Globfc)

	!write(*,*) GlobEf!,En(1D0,(/0D0,BZkmax/)),En(1D0,(/0D0,0D0/))


	!a=FermiVec((/dcos(2D0*pi/3D0),dsin(2D0*pi/3D0)/),1D-12)
	a=FermiVec((/0D0,1D0/),1D-12)

	write(*,*) "Ef      :", GlobEf!1D12/(10D0)**12
	write(*,*) "BZkmax  :", BZkmax
	write(*,*) "Dirac kf:", Globkf
	write(*,*) "Act   kf:", a


	phia=-BZkmax
	phib=-BZkmax
	dkx=2D0*BZkmax/100D0
	dky=dkx
	do i=1,100
		phib=-BZkmax
		do j=1,100
			call Cart2Pol((/phia,phib/),q)
			if (q(1).le.HexBound(BZkmax,q(2))) then
				write(*,*) phia, phib, En(1D0,(/phia,phib/)),GlobEf
				write(*,*) phia, phib, En(-1D0,(/phia,phib/)),GlobEf
			endif
			phib=phib+dky
		enddo
		phia=phia+dkx
	enddo

end program GrapheneTest

















