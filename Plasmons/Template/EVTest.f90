program MiscPlot
	use AuxiliaryFunctions
	implicit none
	real    (kind=8):: Mat(4,4),EV(2,4),k,a
!	real (kind=8), allocatable :: vect(:)
	integer (kind=4):: i,j

	!Mat=(/(/1D0,1D0,2D0,3D0/),(/4D0,5D0,6D0,7D0/),(/8D0,9D0,10D0,11D0/),(/12D0,13D0,14D0,1D0/)/)

	k=0D0
	do i=1,4
		do j=1,4
			Mat(i,j)=k
			k=k+1D0
		enddo
	enddo
	Mat(1,1)=1D0
	Mat(4,4)=1D0

	call EigVals(Mat,4,EV)

	a=1D0
	do i=1,4
		write(*,*) EV(:,i)
		a=a*EV(1,i)
	enddo
	write(*,*) a
end program MiscPlot
