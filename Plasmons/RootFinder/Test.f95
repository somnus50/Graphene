program Test
	implicit none
	real (kind=8) ::a,b,delta,func,RootBrentsMethod,ans
	!integer (kind=4) ::
	
	delta=0.00000000001
	a=0
	b=4.0/3.0
	ans=RootBrentsMethod(a,b,delta,func)

	write(*,*) ans,func(ans)




end program Test

real (kind=8) function func(x)
	implicit none
	real (kind=8) ::x

	func=exp(x)-3.0
	return
end function func

real (kind=8) function RootBrentsMethod(a,b,delta,f)
	implicit none
	real (kind=8) ::a,b,f,c,d,s,swap1,swap2,delta
	integer (kind=4) ::mflag
	
	if (f(a)*f(b).gt.(0.0)) then
		call EXIT(1)
	endif

	if (abs(f(a)).gt.abs(f(b))) then
		swap1=a
		swap2=b
		a=swap2
		b=swap1
	endif

	c=a
	mflag=1
	s=a
	
	do while ((abs(b-a).gt.delta).or.(abs(f(b)).gt.delta).or.(abs(f(s)).gt.delta))
		if ((f(a).ne.f(c)).and.(f(b).ne.f(c))) then
			s=(a*f(b)*f(c))/((f(a)-f(b))*(f(a)-f(c)))+(b*f(a)*f(c))/((f(b)-f(a))*(f(b)-f(c)))+(c*f(a)*f(b))/((f(c)-f(a))*(f(c)-f(b)))
			!Inverse Quadratic Interpolation
		else
			s=b-f(b)*(b-a)/(f(b)-f(a))
			!Secant Method
		endif
		
		if (((s.lt.((3.0*a+b)/4.0)).or.(s.gt.b)).or.((mflag.eq.(1)).and.((abs(s-b)).ge.(abs(b-c)/2.0))).or.&
		   ((mflag.eq.(0)).and.((abs(s-b)).ge.(abs(c-d)/2.0))).or.((mflag.eq.(1)).and.(abs(b-c).lt.delta))&
		   .or.((mflag.eq.(0)).and.(abs(c-d).lt.delta))) then
			s=(a+b)/2
			!Bisection Method
			mflag=1
		else
			mflag=0
		endif
		
		d=c
		c=b
		
		if ((f(a)*f(s)).lt.(0.0)) then
			b=s
		else
			a=s
		endif

		if (abs(f(a)).gt.abs(f(b))) then
			swap1=a
			swap2=b
			a=swap2
			b=swap1
		endif
		
	enddo

	if ((f(b)).lt.(f(s))) then
		RootBrentsMethod=b
	else
		RootBrentsMethod=s
	endif

	return
end function RootBrentsMethod
