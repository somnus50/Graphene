module Globals
implicit none
! This module should contain no functions, subroutines, calculations, or parameters.
! The variables kept here can serve two purposes:
!	1. Constants that must be calculated at runtime, i.e. situational.
!	2. Variables that must be shared across the scope of functions, i.e. Global Variables.





real (kind=8)   ::Globr,Globkf,GlobEf,Globfx,Globfc,Globeta,Globn,Globrs,Globz,Globw
real (kind=8)   ::GlobZeem,GlobkfU,GlobkfD,GlobkfMAX,GlobKernVec(4),Globnvar,Globzvar
real (kind=8)   ::GlobvfU,GlobvfD,Globkf0,HalfGlobZeem,GlobStiff,Globgs,Globgv, Globfxc
real (kind=8)   ::Globzt,Globnu,Globnd,GlobZeemHalf,GlobkInt,GlobphiInt,Globkc,Globg
real (kind=8)   ::Globn0,Globntot, GlobetaSQ, Globzp
integer (kind=4)::Globi,MPIIntPts,GLobIntPts,Globstat,  GlobJzsN
complex (kind=8)::Globc
real(8), allocatable::GlobJ0zs(:),GlobJ1zs(:),GlobPGGSpinZs(:)


end module Globals
