\documentclass[11pt]{article}
\usepackage{ amssymb }
\usepackage{ amsmath }
\usepackage{cite}
\usepackage{physics}
\usepackage{bm}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage[toc,page]{appendix}
%Gummi|065|=)
\title{\textbf{Exchange Field Notes}}
\author{Matt Anderson}
\date{}

\topmargin -1.5cm
\oddsidemargin -0.25cm
\textheight 23cm
\textwidth16.5cm
\parindent1.0cm

\newcommand{\gmb}{{g \mu_B B}}
\newcommand{\gsmb}{{g^* \mu_B B}}
\newcommand{\ua}{\uparrow}
\newcommand{\da}{\downarrow}
\newcommand{\ef}{\varepsilon_f}
\newcommand{\efone}{{\varepsilon_f^{(1)}}}
\newcommand{\ec}{\varepsilon_c}
\newcommand{\es}{\varepsilon_\sigma}
\newcommand{\eu}{\varepsilon_\ua}
\newcommand{\ed}{\varepsilon_\da}
\newcommand{\bfr}{{\bf r}}
\newcommand{\exc}{\varepsilon_{xc}}
\newcommand{\bfk}{{\bf k}}
\newcommand{\vxccell}{\left<\Delta v_{xc\sigma}(\bfr)\right>_{\text{unit cell}}}
\newcommand{\nbar}{\bar n}
\newcommand{\ebar}{\bar \varepsilon}
\newcommand{\ku}{k_\ua}
\newcommand{\kd}{k_\da}
\newcommand{\kz}{k_{Z^*}}
\newcommand{\kf}{k_f}
\newcommand{\kc}{k_c}
\newcommand{\dk}{\Delta k}
\newcommand{\kbar}{\bar k}
\makeatletter
\newcommand{\braphi}{\bra{\Phi_\alpha^{(0)}}}
\newcommand{\ketphi}{\ket{\Phi_\alpha^{(0)}}}
\newcommand{\vast}{\bBigg@{4}}
\newcommand{\Vast}{\bBigg@{5}}
\makeatother
\begin{document}
\maketitle

\section{Intro}
When applying a magnetic field to a system, the bands split into spin up and down.
Due to exchange interactions, the observed splitting, $Z^*$, is larger than what one would expect from the applied magnetic field, $Z$.
\begin{eqnarray}
Z^* &=& Z+Z_x\\
Z^* &=& \gmb_{ext}+\gmb_{x}
\end{eqnarray}
We can define an effective gyromagnetic ratio, $g^*$ that captures this effect.
\begin{equation}
\gsmb_{ext} = g \mu_B (B_{ext}+B_{x})
\end{equation}
$B_x$, however remains to be calculated. 

There also remains a clarification to be made regarding $g^*$.
Some people use it as an overall effective splitting, possibly including many-body effects ($Z^*=\gsmb_{ext}$). Others use it as a characteristic of the band structure with a single electron filling it, placing the many-body effects in $B_{xc}$, ($Z^*=g^*\mu_B(B_{ext}+B_{xc})$). Caveat Emptor.

\begin{figure}[h]
\begin{center}
    \includegraphics[width=0.7\textwidth]{splits.pdf}
  \caption{\small Diagram showing how the energy bands of graphene are split due to an external magnetic field.}
  \label{fig:splits}
  \end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{$\Delta V_{xc}$ Approximation}


We wish to get an idea of the order of magnitude of the exchange-correlation splitting, $Z_{xc}$. We expect $Z_{xc} \approx 0.1Z^*$ to be experimentally viable. To calculate this, we need to find the overall shift in the band structure due to the external magnetic field.

\begin{eqnarray}
\Delta\varepsilon_{n\bfk \sigma}&=&\Delta\varepsilon_{n\bfk \sigma}^{Zeeman}+\Delta\varepsilon_{n\bfk \sigma}^{xc}\\
\Delta\varepsilon_{n\bfk \sigma}^{xc}&=&\int_{\text{unit cell}}\left| \varphi_{n\bfk \sigma}^{(0)}(\bfr) \right|^2\Delta v_{xc}(\bfr)d^3\bfr
\end{eqnarray}
%
And here starts the approximations. 
%
\begin{equation}
\Delta v_{xc\sigma} \approx \vxccell
\end{equation}
%
Averaging over the cell and using the fact that the WFs are normalized to the cell, we get:
%
\begin{equation}
\Delta \varepsilon_{n\bfk\sigma}^{xc}=\vxccell
\end{equation}
%
\begin{equation}
\Delta v_{xc\sigma}(\bfr)=\sum_{\sigma'}\int f_{xc\sigma\sigma'}(\bfr,\bfr')\Delta n_{\sigma'}d^3\bfr'
\end{equation}
%
We'll use LSDA for our approximation because it kills the integral.
%
\begin{equation}
\Delta v_{xc\sigma}(\bfr)=\sum_{\sigma'} f^{LSDA}_{xc\sigma\sigma'}(\bfr)\Delta n_{\sigma'}
\end{equation}
%
Therefore,
%
\begin{equation}
\vxccell=\left< \sum_{\sigma'} f^{LSDA}_{xc\sigma\sigma'}(\bfr)\Delta n_{\sigma'}(\bfr) \right>_{\text{unit cell}}
\end{equation}
%
We just keep on approximating.
%
\begin{equation}
\vxccell= \sum_{\sigma'} \left<f^{LSDA}_{xc\sigma\sigma'}(\bfr) \right>_{\text{unit cell}} \left<\Delta n_{\sigma'}(\bfr)\right>_{\text{unit cell}}
\end{equation}
%
We then approximate the xc kernel from the homogeneous electron gas kernel evaluated at the average density, $N/V$. The change in density is calculated from a bare Zeeman split band without xc effects.
%
\begin{equation}
\vxccell= \sum_{\sigma'} f^{LSDA,HOM}_{xc\sigma\sigma'}[\bar n] \left<\Delta n_{\sigma'}(\bfr)\right>_{\text{unit cell}}
\end{equation}
\begin{equation}
\left<\Delta n_{\sigma'}(\bfr)\right>_{\text{unit cell}}=\sigma\frac{\nbar}{2}\zeta(n,\nbar,Z^*)
\end{equation}
Where $\zeta$ is the dimensionless spin polarization given by equation \ref{eq:zeta}.
%
See appendix \ref{apx:SepCut} for a detailed derivation.


\begin{figure}[h]
\includegraphics[width=0.49\textwidth]{ZxcStar.pdf}
\includegraphics[width=0.49\textwidth]{Zxclog.pdf}
\begin{center}
\includegraphics[width=0.49\textwidth]{ZxcBext.pdf}
\end{center}
\caption{$\Delta v_{xc}$ for various doping levels ($n_0$ is the density of $\pi\pi^*$ electrons)  }
\end{figure}
 
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagebreak
\section{Literature}
\subsection{MOS, Landau Levels, and GW}
Using the static GW approximation in an MOS Inversion layer\cite{Ando1974}:
\begin{equation}
\gsmb_{ext}=\gmb_{ext}+(\Sigma_{N\da}-\Sigma_{N\ua})
\end{equation}
Where $\Sigma_{N\sigma}$ is the static approximation (i.e. $\omega=0$) of the GW self energy of the N-th Landau level for spin $\sigma$. It is important to note that the other Landau levels do not contribute in this calculation.

This form has been further parameterized in terms of the exchange energy in graphene\cite{Kurganova2011}.
\begin{equation}
\gsmb=\gmb+E^0_{ex}(n_\da-n_\ua)
\end{equation}
Where it is assumed that the exchange energy has the form\cite{Nicholas1988}:
\begin{equation}
E_{ex}=E^0_{ex}(n_\da-n_\ua)
\end{equation}
Again, the $n_\sigma$ correspond to spin populations of a given Landau level. They experimentally found $g^*=2.7\pm0.2$ for graphene using tilted magnetic fields\cite{Kurganova2011}.

\subsection{GNR}
$g^*$ has been calculated for graphene nano ribbons using a Hubbard-type Hamiltonian in the mean-field approximation, $H=H^\ua+H^\da$, which this section is largely copied from \cite{Ihnatsenka2012}.

\begin{equation}
H^\sigma=-\sum_{\bfr,\Delta}t_{\bfr,\bfr+\Delta}a^+_{\bfr,\sigma}a_{\bfr+\Delta,\sigma}
+\sum_\bfr\left[ V^\sigma_Z + V_H(\bfr) + V_U^{\sigma'}(\bfr) \right]
a^+_{\bfr\sigma}a_{\bfr\sigma}
\label{hamiltonian}
\end{equation}
where $\sigma$ and $\sigma'$ correspond to two opposite spin states, $\ua$
and $\da$; the summation runs over all sites $\bfr = (x,y)$ of the
graphene lattice, and $\Delta$ includes the nearest neighbors only.
The magnetic field is included in a standard way via Pierel’s
substitution, $t_{\bfr,\bfr+\Delta}=t_0\exp(i2\pi\phi_{\bfr.\bfr+\Delta}/\phi_0)$, where $\phi_{\bfr.\bfr+\Delta}=\int_\bfr^{\bfr+\Delta}{\bf A}\cdot d{\bf l}$ with {\bf A } being the vector potential, $\phi_0=h/e$ being the magnetic flux quantum, and $t_0=2.7$ eV. [In this calculation, the Landau Guage is used, ${\bf A}=(-By,0)$]
The first two terms in Eq. (\ref{hamiltonian}) correspond to the noninteracting part of the Hamiltonian, with the first term describing the kinetic energy of the electrons on a graphene lattice. 
The second term describes the Zeeman energy triggering the spin splitting in the magnetic field, $V_Z^\sigma=\sigma\frac{1}{2}\gmb$, where $g=2$ is the bare g factor of pristine graphene, and the Bohr Magneton $\mu_B=e\hbar/2m_e$. 
The last two terms in Eq. (\ref{hamiltonian}) describe the electron interaction.
The long-range Coulomb interaction between induced charges
in the GNR is given by the standard Hartree term,
\begin{equation}
V_H(\bfr)=\frac{e^2}{4\pi\epsilon_0\epsilon_r}\sum_{\bfr'\ne\bfr}n_{\bfr'}\left( \frac{1}{|\bfr-\bfr'|} - \frac{1}{\sqrt{|\bfr-\bfr'|^2+4d^2}} \right)
\end{equation}
where $n_\bfr=n_{\bfr\ua}+n_{\bfr\da}$ is the total electron density and the
second term corresponds to a contribution from the mirror
charges. The last term in Hamiltonian (\ref{hamiltonian}) corresponds to the
Hubbard energy,
\begin{equation}
V_U^{\sigma'}=U(n_\bfr^{\sigma'}-1/2)
\label{hubbard}
\end{equation}
and describes repulsion between electrons of opposite spins on
the same site. The number of excess electrons at site $\bfr$ reads
\begin{equation}
n_\bfr^{\sigma}=\int_{-\infty}^\infty\rho^\sigma(\bfr,E)f_{FD}(E,E_F)dE-n_{ions}
\label{excessdensity}
\end{equation}
where $\rho^\sigma(\bfr,E)=-\frac{1}{\pi}\Im[G^\sigma(\bfr,\bfr,E)]$ is the energy-dependent
local density of states (LDOS) at zero temperature, $G^\sigma(\bfr,\bfr,E)$
is the Green’s function in the real-space representation of
an electron of spin $\sigma$ residing on site $\bfr$, $E_F=eV_g$ is the
Fermi energy, which value is adjusted by the gate voltage,
and $n_{ions} = 1/A_0 = 3.8 \times 10
m^{-2}$ is the positive-charge background of ions ($A_0 = \frac{3\sqrt{3}}{4}a_0^2$ is the area per one C atom
and $a_0 = 0.142$ nm is the C-C distance). Equations (\ref{hamiltonian})-(\ref{excessdensity}) are
solved self-consistently using the Green’s function technique
in order to calculate the band structure, the charge density, and
the potential distribution \cite{PhysRevB.77.245401,PhysRevB.82.121410,PhysRevB.84.075407}. 

This paper defines $g^*$ as:
\begin{equation}
g^*=\frac{\langle V_\ua-V_\da\rangle}{\mu_BB} =\frac{\langle Z^* \rangle}{\mu_BB}
\end{equation}
where the averaging is done over all carbon atoms (They do not clearly define $V^\sigma$. It could be just the energy difference of the bands, or it is the spin components of the total potential, i.e. Zeeman+Hubbard).
This corresponds to the overall effective $g$ rather than the one that is characteristic of the band structure. They see a difference in $g^*$ between armchair and zigzag GNRs. Zigzag GNRs have instrinsically spin-polarized edge states that have a zero energy mode. This leads to a massive enhancement of $g^* (\approx 30)$ at low $B (\lesssim50T)$ fields.

\subsection{Questions}
Can we use these results even though they've been derived with Landau levels? I think we could get away with it since they don't mix between the levels. Could we just use a DFT-calculated exchange energy and call it good? Maybe just reparameterize the spin-wave stiffness in terms of $g^*,\varepsilon_f,$ and $B_{ext}$.

\section{Extras}

The Nature paper\cite{Lundeberg2009} detected the Zeeman Splitting in graphene using spin-resolved conductance. They don't actually report the splitting as a function of the applied magnetic field. It *might* be able to be calculated from their data. However, their work seems promising because they applied fields up to 12 T, which is not completely infeasible. $Z\left(B=12T\right)=1.4$ meV. So, room temperature is a problem...

Someone has calculated $g^*$ in graphene using "pseudo-QED" which I don't understand at all\cite{Menezes2017}. Maybe we could just use it?... Or at least make comparisons.

Another person has calculated the exchange enrgy in graphene using pair distribution functions \cite{DHARMAWARDANA2006}\cite{Peres2005}. They actually explain how to make the appropriate cut-off of the bottom half of the Dirac Cone.

The screened-exchange approximation as alternative method for DFT calculations on graphene structures\cite{Gillen2010}.

There is still a paper behind a paywall I need to get\cite{Englert1982}.

\section{Density Functional Perturbation Theory}

Using the $(2n+1)$ theorem\cite{TulipDFPT}, the first order energy correction is:
\begin{equation}
E^{(1)}=\sum_{\alpha=1}^N\braphi(T+V_{ext})^{(1)}\ketphi+\frac{d}{d\lambda}E_{Hxc}[n^{(0)}]\bigg|_{\lambda=0}
\label{FirstOrderE}
\end{equation}
Where $\lambda$ is the perturbing parameter, and $\ketphi$ are the unperturbed Kohn-Sham wavefunctions. The first order correction to the kinetic and Hartree energies are non-zero only if they explicitly depend on $\lambda$. They don't in the case of an applied magnetic field.
N.B. eq. (\ref{FirstOrderE}) represents a generalised Hellmann-Feynman theorem.
\begin{equation}
E^{(1)}=\sum_{\alpha=1}^N\braphi V_{ext}^{(1)}\ketphi+\frac{d}{d\lambda}E_{xc}[n^{(0)}]\bigg|_{\lambda=0}
\end{equation}
We have to chose the form of the perturbing potential. 
\begin{equation}
V_{ext}^{(0)}=V_{crystal}\ ,\ \ V_{ext}^{(1)}=g\mu_B{\bf B}\cdot {\bm\sigma}
\end{equation}
\begin{equation}
E^{(1)} = \sum_{\alpha=1}^N\braphi g\mu_B{\bf B}\cdot {\bm\sigma} \ketphi+\frac{d}{d\lambda}E_{xc}[n^{(0)}]\bigg|_{\lambda=0} 
\label{ZeemanPert} 
\end{equation}
We assume that $\bf B$ and $\bm \sigma$ are parallel.
\begin{eqnarray}
E^{(1)} &=& g\mu_BB\sum_{\alpha=1}^N\sigma_\alpha\braphi\ketphi+\frac{d}{d\lambda}E_{xc}[n^{(0)}]\bigg|_{\lambda=0} \nonumber \\
E^{(1)} &=& g\mu_BB\frac{N_\ua-N_\da}{2}+\frac{d}{d\lambda}E_{xc}[n^{(0)}]\bigg|_{\lambda=0} \nonumber \\
E^{(1)} &=& \frac{Z\zeta N}{2}+\frac{d}{d\lambda}E_{xc}[n^{(0)}]\bigg|_{\lambda=0}
\end{eqnarray}
We still have to write $ V_{ext}^{(1)}$ in terms of $\lambda$ and figure out how to do the derivate of $E_{xc}$ with respect to it. Otherwise, it is easy to calculate the wavefunctions/density with a first principles calculation. However, care must be taken to preserve the normality of the wavefunctions in the calculation. Otherwise, we go back to eq. (\ref{ZeemanPert}) and use the whole WFs.


\section{Stiffness Parameterization}

\textbf{Need to redo this with results from \ref{apx:SepCut}}

It makes more sense to reparameterize the form of the stiffness into physically meaningful parameters. $\{e',\zeta,\varepsilon_f\} \rightarrow \{g^*,B_{ext},\varepsilon_f\}$. N.B. here, $g^*$ is the overall effective gyromagnetic ratio. $Z^*=\gsmb_{ext}$

\begin{equation}
S=\frac{\gamma^2}{2}\left( \frac{1}{e'} + \frac{e'}{2\zeta\varepsilon_f^2}\ln\left(\frac{1-\zeta}{1+\zeta}\right)
- \frac{\tilde\zeta}{\zeta\varepsilon_f} \right)
\end{equation}

\begin{eqnarray*}
S &=& \frac{\gamma^2}{2}\left( S_1 + S_{21}S_{22} + S_3 \right)\\
S_1 &=& \frac{1}{e'}\\
S_{21} &=& \frac{e'}{2\zeta\varepsilon_f^2}\\
S_{22} &=& \ln\left(\frac{1-\zeta}{1+\zeta}\right)\\
S_3 &=& - \frac{\tilde\zeta}{\zeta\varepsilon_f}
\end{eqnarray*}


$\{e',\zeta,\varepsilon_f\} \rightarrow \{g^*,B_{ext},\varepsilon_f\}$

\begin{equation}
\frac{\ec^2}{2}=\frac{4\pi\gamma^2}{g_s g_v}n^{(0)}
\end{equation}

$\ec$ is a reference energy that relates to the half-filled density, $n^{(0)}$ of natural graphene.

\begin{eqnarray*}
S &=& \frac{\gamma^2}{2}\left( S_1 + S_{21}S_{22} + S_3 \right)\\
S_1 &=& \frac{2}{\gsmb(1-\frac{g}{g^*})}\\
S_{21} &=&\\
S_{22} &=& \ln\left(\frac{}{}\right)\\
S_3 &=& - (S_{31}+S_{32})/\sqrt{2}\\
S_{31} &=& \frac{\sqrt{1+\zeta}}{\zeta}\\
S_{32} &=& \frac{\sqrt{1-\zeta}}{\zeta}\\
S_{31} &=& \\
S_{32} &=& \\
\end{eqnarray*}

\begin{equation}
\begin{split}
S &= 
\end{split}
\end{equation}

\pagebreak
\begin{appendices}
\section{Derivation of Band Splitting Parameters}

Using the Dirac cone as a model introduces some issues. The lower half of the cone extends down infinitely. One has to decide how to fill the band structure in a physically meaningful way. It makes the most sense to introduce an energy cut-off that defines the point below which the energy bands don't exist. This cutoff has been chosen to conserve the number of states in the undoped $\pi$-band (and by extension, the average density, $\bar n$).

\subsection{Separate Cut-offs \label{apx:SepCut}}
When working with the Dirac Cone, one must remember that there must be an energy cut-off for the lower band defined such that it still physically represents the system. In normal systems, there is some global minimum for every individual band within the Brillouin zone. This is not so for a Dirac cone. When we define the energy cut-off, we are saying that no bandstructure exists at a lower energy. The cone extends forever but, importantly, the band does not.

 So, when you split the bands with a vertical shift and start filling them, you MUST stop at each band's cut-off that was shifted. Figure \ref{fig:SplitsComparison2} illustrates this. This preserves our expectaions of the spin polarization.

\begin{figure}[h]
\includegraphics[width=\textwidth]{splitcomp2.pdf}
\caption{\small Illustrating the differences in filling between bare graphene (left) and Zeeman split (right) energy bands}
\label{fig:SplitsComparison2}
\end{figure}

Using this filling scheme does shift the Fermi energy to conserve particle number. The following is a derivation of all the parameters needed for future calculations.

We start by calculating the average density of both the split, $n_s$, and unsplit, $\nbar$, systems.
%
\begin{equation}
\nbar=n_0+n_1
\end{equation}
%
Where $n_0=\frac{2}{\sqrt{3}a^2},\ a=2.46{\text \AA}$ is the undoped density of itinerant electrons in graphene, and $n_1$ is the doped density. The density in a Dirac cone defines both a characteristic wavevector and energy given by:
\begin{equation}
n_i=\frac{g_sg_v}{4\pi}k_i^2=\frac{g_sg_v}{4\pi\gamma^2}\varepsilon_i^2
\end{equation}
Using this, we calculate the average density in the split system.
\begin{equation}
n_s=2\frac{\kc^2}{2\pi}+\frac{\eu^2}{2\pi\gamma^2}+\frac{\ed^2}{2\pi\gamma^2}
\end{equation}
From figure \ref{fig:SplitsComparison2} we can determine $\es$.
\begin{equation}
\es=\efone-\sigma \frac{Z^*}{2}
\end{equation}
%
\begin{eqnarray}
n_s &=& \frac{\kc^2}{\pi} +\frac{\efone^2 -\efone Z^*+Z^{*2}/4}{2\pi \gamma^2} +\frac{\efone^2 +\efone Z^*+Z^{*2}/4}{2\pi \gamma^2} \nonumber \\
n_s &=& n_0 +\frac{\efone^2 +Z^{*2}/4}{\pi \gamma^2}
\end{eqnarray}
We now assert that $n_s=\nbar$ to conserve particle number.
\begin{eqnarray}
\nbar &=& n_0 +\frac{\efone^2 +Z^{*2}/4}{\pi \gamma^2} \nonumber \\
\efone^2 &=& \pi\gamma^2(\nbar-n_0)-Z^{*2}/4 \nonumber \\
\efone^2 &=& \ef^2-Z^{*2}/4 \label{eq:efone2} \\
\efone &=& \ef \sqrt{1-\frac{1}{4}\left(\frac{Z^*}{\ef}\right)^2}
\end{eqnarray}
So, the Fermi energy must shift down. This also imposes an upper limit to $Z^*$ such that the new Fermi energy remains above the zero point of the upper cone. From equation \ref{eq:efone2}, we find:
\begin{equation}
Z^*\le \sqrt{2} \ef
\end{equation}
Now, let us calculate the spin resolved densities and the polarization.
\begin{eqnarray}
n_\sigma &=& \frac{n_0}{2}+\frac{\es^2}{2\pi\gamma^2} \\
n_\sigma &=& \frac{n_0}{2}+\frac{\efone^2 -\sigma\efone Z^*+Z^{*2}/4}{2\pi \gamma^2} \nonumber \\
n_\sigma &=& \frac{n_0}{2}+\frac{\ef^2 -\sigma\efone Z^*}{2\pi \gamma^2} \nonumber \\
n_\sigma &=& \frac{\nbar}{2}-\frac{\sigma\efone Z^*}{2\pi \gamma^2} = \frac{\nbar}{2}(1+\sigma\zeta) \\
\zeta &=& \frac{n_\ua-n_\da}{n_\ua+n_\da}=-\frac{\efone Z^*}{\pi \gamma^2 \nbar} = -\frac{\efone Z^*}{\ebar^2} =-\frac{Z^* }{\ebar^2}\sqrt{\ef^2-\frac{Z^{*2}}{4}} \\
\Delta n_\sigma &=& \sigma\zeta\frac{\nbar}{2}=-\sigma\frac{\nbar}{2}\frac{Z^* }{\ebar^2}\sqrt{\ef^2-\frac{Z^{*2}}{4}}
\end{eqnarray}

The previous results are only valid for $\efone>\frac{Z^*}{2}$ (Fermi energy is in upper part of the upper cone). We can extend these results by rederiving everything for when the Fermi energy falls below the center of upper cone.

\begin{eqnarray}
n_\sigma &=& \frac{n_0}{2}-\sigma\frac{(\frac{Z^*}{2}-\sigma \efone)^2}{2\pi\gamma^2} \\
&=& \frac{n_0}{2}+\frac{\efone Z^*}{2\pi\gamma^2} -\sigma\frac{\efone^2 +Z^{*2}/4}{2\pi \gamma^2} \\
\nbar &=& n_\ua+n_\da \nonumber\\
&=& n_0+\frac{\efone Z^*}{\pi\gamma^2} \nonumber \\
\efone &=& \frac{\ef^2}{Z^*}\\
n_\sigma &=& \frac{\nbar}{2} -\sigma\frac{\ef^4/Z^{*2} +Z^{*2}/4}{2\pi \gamma^2}
\end{eqnarray}
\begin{eqnarray}
\zeta &=& \frac{n_\ua-n_\da}{n_\ua+n_\da} \nonumber\\
&=&-\frac{\ef^4/Z^{*2} +Z^{*2}/4}{\pi \gamma^2\nbar} \nonumber \\
&=&-\frac{\ef^4/Z^{*2} +Z^{*2}/4}{\ebar^2} \nonumber \\
&=& -\frac{\ef^2}{\ebar^2}\left( \frac{\ef^2}{Z^{*2}}+\frac{Z^{*2}}{4\ef^2} \right) \nonumber\\
&=&-\left(\frac{\ef}{\ebar}\right)^2 \left(\frac{\ef}{Z^*}\right)^2 \left( 1+\frac{1}{4}\left(\frac{Z^*}{\ef}\right)^4 \right)\\
&=&\frac{-1}{y^2x^2}\left( 1+\frac{x^4}{4}\right),\ \ x=\frac{Z^*}{\ef},\ \ y=\frac{\ebar}{\ef}=\frac{\nbar}{n_1} \\
%
%
%
n_\sigma &=& \frac{\nbar}{2}(1+\sigma\zeta) \\
\Delta n_\sigma &=& \sigma\zeta\frac{\nbar}{2}
\end{eqnarray}
With these results, we can construct piecewise functions for $\zeta$ and $\efone$ which determine all the rest of the values.

\begin{equation}
\zeta = \left\{
     \begin{array}{llr}
        & \frac{-x}{y^2}\sqrt{1-\frac{x^2}{4}} & : x \le \sqrt{2}\\ 
        & \frac{-1}{y^2x^2}\left( 1+\frac{x^4}{4}\right) & : x > \sqrt{2}
     \end{array}
   \right.
   \label{eq:zeta}
\end{equation}

\begin{equation}
\efone = \left\{
     \begin{array}{llr}
        & \ef\sqrt{1-\frac{x^2}{4}} & : x \le \sqrt{2}\\ 
        & \ef/x & : x > \sqrt{2}
     \end{array}
   \right.
   \label{eq:efone}
\end{equation}
There is also a maximum $Z^*$ beyond which the upper cone is completely empty. We then have a single completely spin polarized dirac cone. We find this maximum by setting equation \ref{eq:zeta} equal to $-1$ and solving for $x_{\text{max}}$.
\begin{equation}
x_{\text{max}}=\sqrt{2}\sqrt{y^2+\sqrt{y^4-1}}\approx 2 y,\ \ y\gg1
\end{equation}

\begin{figure}[h]
\includegraphics[width=0.49\textwidth]{Zeta.pdf}
\includegraphics[width=0.49\textwidth]{Ef1.pdf}
\end{figure}

\subsection{$\bm\ef$ Downward (WRONG)}
 To conserve the average density in a split system, one must start at the Fermi Energy and then fill downward, as illustrated in Figure \ref{fig:SplitsComparison}. This can lead to unexpected results, such as a different energy cut-off or a polarization with an unexpected sign.

\begin{figure}[h]
\includegraphics[width=\textwidth]{splitcomp.pdf}
\caption{\small Illustrating the differences in filling between bare graphene (left) and Zeeman split (right) energy bands}
\label{fig:SplitsComparison}
\end{figure}

The following is a derivation of parameters for the split bands while still conserving the average density.
We start with the average density, $\nbar$ which is comprised of an intrinsic density and a doping density.
\begin{equation}
\nbar=n^{(0)}+n^{(1)}
\end{equation}
\begin{equation}
n^{(0)}=\frac{2}{\sqrt{3}a^2},\ \ \ a=2.46{\text \AA}
\end{equation}
\begin{equation}
n^{(1)}=\frac{g_sg_v\kf^2}{4\pi}=\frac{g_sg_v\ef^2}{4\pi\gamma^2}
\end{equation}
For bare graphene $g_s=2$, but for the split bands $g_s=1$ and $g_v=2$ for both.
Now, let us calculate the average density of a split system with an arbitrary cut-off, $(\ec,\kc)$.
\begin{equation}
\nbar_{\text split}=\frac{\ku^2}{2\pi}+\frac{\kd^2}{2\pi}
+\frac{(\kc+\dk)^2}{2\pi}+\frac{(\kc-\dk)^2}{2\pi}
\end{equation}
Where the wavevectors are shown in Figure \ref{fig:SplitsComparison} (right). We can calculate $\dk$ easily using the slope of the bands and the value of the splitting.
\begin{equation}
\dk=\frac{Z^*}{2\gamma}
\end{equation}
\begin{equation}
k_\sigma=\frac{\ef-\sigma Z^*/2}{\gamma}
\end{equation}

\begin{eqnarray}
2\pi\nbar_{\text split}&=&2\frac{\ef^2}{\gamma^2}+\frac{Z^{*2}}{2\gamma^2}
+2\kc^2+\frac{Z^{*2}}{2\gamma^2} \nonumber \\
&=&2\frac{\ef^2}{\gamma^2}+\frac{Z^{*2}}{\gamma^2}+2\kc^2 \nonumber \\
\kc^2&=& \pi\nbar_{\text split}-\frac{\ef^2}{\gamma^2}-\frac{Z^{*2}}{2\gamma^2}
\label{eq:kc1}
\end{eqnarray}
We now require that the average density is conserved, $\nbar_{\text split}=\nbar$. This defines an appropriate cut-off energy and wavevector. For simplicity, we will introduce some more characteristic wavevectors given by the form:
\begin{equation}
k_i=\frac{\varepsilon_i}{\gamma}=\sqrt{\pi n_i}
\end{equation}
Equation \ref{eq:kc1} then becomes:
\begin{eqnarray}
\kc^2&=&\kbar^2-\kf^2-\frac{\kz^2}{2}
\label{eq:kc2}\\
x(\ef,Z^*)&=&\sqrt{\left(\frac{\bar \varepsilon}{\ef}\right)^2-\frac{1}{2}\left(\frac{Z^*}{\ef}\right)^2-1}\\
\kc&=&\kf x(\ef,Z^*)
\label{eq:kc}
\end{eqnarray}
We now have an appropriate cut-off. Let's calculate some densities.
\begin{equation}
n_\sigma=\frac{(\kc+\sigma\dk)^2}{2\pi}+\frac{(\ef-\sigma Z^*/2)^2}{2\pi\gamma^2}
\end{equation}
\begin{eqnarray}
2\pi n_\sigma &=& \kc^2+2\sigma\dk\kc+\dk^2+\kf^2-\sigma\kf\kz+\frac{\kz^2}{4} \nonumber\\
&=& \kbar^2-\kf^2-\frac{\kz^2}{2}+2\sigma\frac{\kz}{2}\kc+\frac{\kz^2}{4}+\kf^2-\sigma\kf\kz+\frac{\kz^2}{4} \nonumber \\
n_\sigma&=&\frac{\kbar^2+\sigma(\kc\kz-\kf\kz)}{2\pi} \nonumber\\
n_\sigma&=&\frac{\kbar^2+\sigma\kf\kz(x-1)}{2\pi}\\
\Delta n_\sigma &=& n_\sigma -\nbar/2=n_\sigma-\frac{\kbar^2}{2\pi} \nonumber\\
\Delta n_\sigma &=& \frac{\sigma\kf\kz(x-1)}{2\pi} 
= \sigma\frac{\nbar}{2}\frac{\ef Z^*}{\ebar^2}(x-1)\\
n_\ua-n_\da &=& \frac{\kf\kz(x-1)}{\pi} \nonumber\\
n_\ua+n_\da &=& \frac{\kbar^2}{\pi}=\nbar \nonumber\\
\zeta &=&\frac{\kf\kz(x-1)}{\pi\nbar}=\frac{\ef Z^*}{\ebar^2}(x-1)
\end{eqnarray}
We see that the polarization, $\zeta$ is only negative for $x<1$ which is unexpected...


\subsection{$\bm\ec$ Upward (WRONG)}
We try this again but argue that we should fill upward

\begin{figure}[h]
\includegraphics[width=\textwidth]{splitcomp1.pdf}
\caption{\small Illustrating the differences in filling between bare graphene (left) and Zeeman split (right) energy bands}
\label{fig:SplitsComparison}
\end{figure}


\end{appendices}
\pagebreak

\begin{equation}
\end{equation}

\begin{eqnarray}
\end{eqnarray}


\pagebreak
\bibliographystyle{unsrt}
\bibliography{comp}
\end{document}































