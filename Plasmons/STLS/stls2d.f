      PROGRAM STLS
      IMPLICIT NONE

      INTEGER I,GDIM,CNT
      PARAMETER (GDIM=100)
      DOUBLE PRECISION RS,PI,KF,N,Q,DQ,FUNG,GOLD(GDIM),
     &                 G(GDIM),G1(GDIM),Q1(GDIM),Y2(GDIM)

      COMMON /PARAMS/ PI,KF
      COMMON /GFAC/ G1,Q1,Y2
      EXTERNAL FUNG

      PI = 3.141592653589793D0
      RS = 1.D0
      KF = DSQRT(2.D0)/RS
      N = KF**2/(2.D0*PI)

      CNT=0
      DQ = 5.D0*KF/GDIM
      
      DO 2 I=1,GDIM
         Q1(I) = I*DQ
         G1(I) = 1.D0
2     CONTINUE

1     CONTINUE
      CNT = CNT+1
      
      CALL spline(Q1,G1,GDIM,1.D30,1.D30,y2) 
      
      DO 10 I=1,GDIM
         WRITE(*,*)CNT,I
         Q = I*DQ

         G(I) = FUNG(Q)/(PI**2*N)
         
         WRITE(1,*)real(Q/KF),real(G(I))
         If (CNT.EQ.1) WRITE(10,*)real(Q/KF),real(G(I))
         If (CNT.EQ.10) WRITE(11,*)real(Q/KF),real(G(I))

10    CONTINUE

      IF (CNT.EQ.1) THEN
         DO 21 I=1,GDIM
            G1(I) = G(I)
21       CONTINUE
      ELSE
         DO 22 I=1,GDIM
            G1(I) = 0.5D0*G(I) + 0.5D0*GOLD(I)
22       CONTINUE
      ENDIF

      DO 20 I=1,GDIM
         GOLD(I) = G1(I)
20    CONTINUE

      IF (CNT.GE.10) STOP
      
      GOTO 1

      END
C*********************************************************************
C*********************************************************************
C*********************************************************************      
      DOUBLE PRECISION FUNCTION FUNG(Q)
      IMPLICIT NONE
      
      INTEGER I,NINT
      DOUBLE PRECISION Q,QP,DQ,PI,KF,SQ
      REAL ELLF,ELLE,RPI2,RK
      COMMON /PARAMS/ PI,KF
      EXTERNAL SQ,ELLF,ELLE
      
      NINT=1000
      DQ = 5.D0*KF/NINT
!C      DQ = 0.01D0
      RPI2 = REAL(PI/2.D0)
      
      FUNG=0.D0
      DO 10 I=1,NINT
          QP = I*DQ - DQ/2.D0

          RK = REAL(2.D0*DSQRT(Q*QP)/(Q+QP))

!C         This is the "simple" STLS
          FUNG = FUNG - 2.*ELLF(RPI2,RK)*(SQ(QP)-1.D0)*Q**2*QP/(Q+QP)

!C         This is the "force" STLS
!          FUNG = FUNG - (SQ(QP)-1.D0)*QP
!     &         *(ELLE(RPI2,RK)*(Q+QP) + ELLF(RPI2,RK)*(Q-QP))

10    CONTINUE   

      FUNG = 0.5D0*FUNG*DQ/Q
      
      RETURN
      END
C*********************************************************************
C*********************************************************************
C********************************************************************* 
      DOUBLE PRECISION FUNCTION SQ(Q)
      IMPLICIT NONE
      
      INTEGER I,GDIM
      PARAMETER (GDIM=100)
      DOUBLE PRECISION Q,PI,KF,N,RECHI,IMCHI,OMEGA,DOMEGA,VQ,
     &                 G1(GDIM),Q1(GDIM),Y2(GDIM),G,QKF
      DOUBLE COMPLEX CHI0,CHI,ONE,IONE
      COMMON /PARAMS/ PI,KF
      COMMON /GFAC/ G1,Q1,Y2
      EXTERNAL RECHI,IMCHI
      
      ONE = (1.D0,0.D0)
      IONE = (0.D0,1.D0)
      
      N = KF**2/(2.D0*PI)
      VQ = 2.D0*PI/Q
      
      DOMEGA = 0.1D0
      
      SQ = 0.D0
      DO 10 I=1,1000
          OMEGA = I*DOMEGA - DOMEGA/2.D0
          
          CALL splint(Q1,G1,y2,GDIM,Q,G)
          
          CHI0 = ONE*RECHI(Q,OMEGA) + IONE*IMCHI(Q,OMEGA)
          
          CHI = CHI0/(ONE - VQ*(1.D0 - G)*CHI0)
          
          SQ = SQ + DIMAG(CHI)
10    CONTINUE

      SQ = -SQ*DOMEGA/(PI*N)    

C      IF (Q.GT.2.D0*KF) THEN
C          SQ = 1.D0
C      ELSE
C          QKF  = Q/(2.D0*KF)
C          SQ = (2.D0/PI)*(ASIN(QKF) + QKF*DSQRT(1.D0-QKF**2))
C      ENDIF
      
      RETURN
      END
C*********************************************************************
C*********************************************************************
C*********************************************************************
      DOUBLE PRECISION FUNCTION RECHI(Q,OMEGA)
      IMPLICIT NONE

      DOUBLE PRECISION PI,KF,NUP,NUM,OMEGA,Q,SIP,SIM
      COMMON /PARAMS/ PI,KF

      NUP = (OMEGA/Q + Q/2.D0)/KF
      NUM = (OMEGA/Q - Q/2.D0)/KF

      SIM = 1.D0
      IF (NUM.LT.0.D0) SIM = -1.D0
      SIP = 1.D0
      IF (NUP.LT.0.D0) SIP = -1.D0

      RECHI = 0.D0

      IF (NUM**2.GT.1.D0) RECHI = SIM*DSQRT(NUM**2-1.D0)
      IF (NUP**2.GT.1.D0) RECHI = RECHI - SIP*DSQRT(NUP**2-1.D0)
   
      RECHI = -1.D0/PI -RECHI*KF/(PI*Q)

      RETURN
      END
C*********************************************************************
C*********************************************************************
C*********************************************************************
      DOUBLE PRECISION FUNCTION IMCHI(Q,OMEGA)
      IMPLICIT NONE

      DOUBLE PRECISION PI,KF,NUP,NUM,OMEGA,Q
      COMMON /PARAMS/ PI,KF

      NUP = (OMEGA/Q + Q/2.D0)/KF
      NUM = (OMEGA/Q - Q/2.D0)/KF

      IMCHI = 0.D0

      IF (NUM**2.LT.1.D0) IMCHI = DSQRT(1.D0-NUM**2)
      IF (NUP**2.LT.1.D0) IMCHI = IMCHI - DSQRT(1.D0-NUP**2)

      IMCHI = -KF*IMCHI/(PI*Q)

      RETURN
      END
C**********************************************************************
C**********************************************************************
C**********************************************************************
      SUBROUTINE spline(x,y,n,yp1,ypn,y2)
      IMPLICIT NONE
      INTEGER n,NMAX
      DOUBLE PRECISION yp1,ypn,x(n),y(n),y2(n)
      PARAMETER(NMAX=500)
      INTEGER i,k
      DOUBLE PRECISION p,qn,sig,un,u(NMAX)
      if (yp1.gt..99d30) then
         y2(1)=0.d0
         u(1)=0.d0
      else
         y2(1)=-0.5d0
         u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
         sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
         p=sig*y2(i-1)+2.
         y2(i)=(sig-1.)/p
         u(i)=(6.d0*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     &        /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99d30) then
         qn=0.d0
         un=0.d0
      else
         qn=0.5d0
         un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
         y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      END
C****************************************************************
      SUBROUTINE splint(xa,ya,y2a,n,x,y)
      IMPLICIT NONE
      INTEGER n
      DOUBLE PRECISION x,y,xa(n),ya(n),y2a(n)
      INTEGER k,khi,klo
      DOUBLE PRECISION a,b,h
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
         k=(khi+klo)/2
         if (xa(k).gt.x) then
            khi=k
         else
            klo=k
         endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.d0) then
          WRITE(*,*)'bad xa input in splint'
          STOP
      endif
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     &      ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      return
      END
C****************************************************************
C****************************************************************
      FUNCTION ellf(phi,ak)
      REAL ellf,ak,phi
CU    USES rf
      REAL s,rf
      s=sin(phi)
      ellf=s*rf(cos(phi)**2,(1.-s*ak)*(1.+s*ak),1.)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
C****************************************************************
      FUNCTION rf(x,y,z)
      REAL rf,x,y,z,ERRTOL,TINY,BIG,THIRD,C1,C2,C3,C4
      PARAMETER (ERRTOL=.08,TINY=1.5e-38,BIG=3.E37,THIRD=1./3.,
     *C1=1./24.,C2=.1,C3=3./44.,C4=1./14.)
      REAL alamb,ave,delx,dely,delz,e2,e3,sqrtx,sqrty,sqrtz,xt,yt,zt
      if(min(x,y,z).lt.0..or.min(x+y,x+z,y+z).lt.TINY.or.max(x,y,
     *z).gt.BIG)pause 'invalid arguments in rf'
      xt=x
      yt=y
      zt=z
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        zt=.25*(zt+alamb)
        ave=THIRD*(xt+yt+zt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
      if(max(abs(delx),abs(dely),abs(delz)).gt.ERRTOL)goto 1
      e2=delx*dely-delz**2
      e3=delx*dely*delz
      rf=(1.+(C1*e2-C2-C3*e3)*e2+C4*e3)/sqrt(ave)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY
C****************************************************************
      FUNCTION ellpi(phi,en,ak)
      REAL ellpi,ak,en,phi
CU    USES rf,rj
      REAL cc,enss,q,s,rf,rj
      s=sin(phi)
      enss=en*s*s
      cc=cos(phi)**2
      q=(1.-s*ak)*(1.+s*ak)
      ellpi=s*(rf(cc,q,1.)-enss*rj(cc,q,1.,1.+enss)/3.)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
C****************************************************************
      FUNCTION rj(x,y,z,p)
      REAL rj,p,x,y,z,ERRTOL,TINY,BIG,C1,C2,C3,C4,C5,C6,C7,C8
      PARAMETER (ERRTOL=.05,TINY=2.5e-13,BIG=9.E11,C1=3./14.,C2=1./3.,
     *C3=3./22.,C4=3./26.,C5=.75*C3,C6=1.5*C4,C7=.5*C2,C8=C3+C3)
CU    USES rc,rf
      REAL a,alamb,alpha,ave,b,beta,delp,delx,dely,delz,ea,eb,ec,ed,ee,
     *fac,pt,rcx,rho,sqrtx,sqrty,sqrtz,sum,tau,xt,yt,zt,rc,rf
      if(min(x,y,z).lt.0..or.min(x+y,x+z,y+z,abs(p)).lt.TINY.or.max(x,y,
     *z,abs(p)).gt.BIG)pause 'invalid arguments in rj'
      sum=0.
      fac=1.
      if(p.gt.0.)then
        xt=x
        yt=y
        zt=z
        pt=p
      else
        xt=min(x,y,z)
        zt=max(x,y,z)
        yt=x+y+z-xt-zt
        a=1./(yt-p)
        b=a*(zt-yt)*(yt-xt)
        pt=yt+b
        rho=xt*zt/yt
        tau=p*pt/yt
        rcx=rc(rho,tau)
      endif
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        alpha=(pt*(sqrtx+sqrty+sqrtz)+sqrtx*sqrty*sqrtz)**2
        beta=pt*(pt+alamb)**2
        sum=sum+fac*rc(alpha,beta)
        fac=.25*fac
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        zt=.25*(zt+alamb)
        pt=.25*(pt+alamb)
        ave=.2*(xt+yt+zt+pt+pt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
        delp=(ave-pt)/ave
      if(max(abs(delx),abs(dely),abs(delz),abs(delp)).gt.ERRTOL)goto 1
      ea=delx*(dely+delz)+dely*delz
      eb=delx*dely*delz
      ec=delp**2
      ed=ea-3.*ec
      ee=eb+2.*delp*(ea-ec)
      rj=3.*sum+fac*(1.+ed*(-C1+C5*ed-C6*ee)+eb*(C7+delp*(-C8+delp*C4))+
     *delp*ea*(C2-delp*C3)-C2*delp*ec)/(ave*sqrt(ave))
      if (p.le.0.) rj=a*(b*rj+3.*(rcx-rf(xt,yt,zt)))
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
C****************************************************************
      FUNCTION rc(x,y)
      REAL rc,x,y,ERRTOL,TINY,SQRTNY,BIG,TNBG,COMP1,COMP2,THIRD,C1,C2,
     *C3,C4
      PARAMETER (ERRTOL=.04,TINY=1.69e-38,SQRTNY=1.3e-19,BIG=3.E37,
     *TNBG=TINY*BIG,COMP1=2.236/SQRTNY,COMP2=TNBG*TNBG/25.,THIRD=1./3.,
     *C1=.3,C2=1./7.,C3=.375,C4=9./22.)
      REAL alamb,ave,s,w,xt,yt
      if(x.lt.0..or.y.eq.0..or.(x+abs(y)).lt.TINY.or.(x+
     *abs(y)).gt.BIG.or.(y.lt.-COMP1.and.x.gt.0..and.x.lt.COMP2))pause 
     *'invalid arguments in rc'
      if(y.gt.0.)then
        xt=x
        yt=y
        w=1.
      else
        xt=x-y
        yt=-y
        w=sqrt(x)/sqrt(xt)
      endif
1     continue
        alamb=2.*sqrt(xt)*sqrt(yt)+yt
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        ave=THIRD*(xt+yt+yt)
        s=(yt-ave)/ave
      if(abs(s).gt.ERRTOL)goto 1
      rc=w*(1.+s*s*(C1+s*(C2+s*(C3+s*C4))))/sqrt(ave)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
C*******************************************************************
      FUNCTION elle(phi,ak)
      REAL elle,ak,phi
CU    USES rd,rf
      REAL cc,q,s,rd,rf
      s=sin(phi)
      cc=cos(phi)**2
      q=(1.-s*ak)*(1.+s*ak)
      elle=s*(rf(cc,q,1.)-((s*ak)**2)*rd(cc,q,1.)/3.)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
C*********************************************************************
      FUNCTION rd(x,y,z)
      REAL rd,x,y,z,ERRTOL,TINY,BIG,C1,C2,C3,C4,C5,C6
      PARAMETER (ERRTOL=.05,TINY=1.e-25,BIG=4.5E21,C1=3./14.,C2=1./6.,
     *C3=9./22.,C4=3./26.,C5=.25*C3,C6=1.5*C4)
      REAL alamb,ave,delx,dely,delz,ea,eb,ec,ed,ee,fac,sqrtx,sqrty,
     *sqrtz,sum,xt,yt,zt
      if(min(x,y).lt.0..or.min(x+y,z).lt.TINY.or.max(x,y,
     *z).gt.BIG)pause 'invalid arguments in rd'
      xt=x
      yt=y
      zt=z
      sum=0.
      fac=1.
1     continue
        sqrtx=sqrt(xt)
        sqrty=sqrt(yt)
        sqrtz=sqrt(zt)
        alamb=sqrtx*(sqrty+sqrtz)+sqrty*sqrtz
        sum=sum+fac/(sqrtz*(zt+alamb))
        fac=.25*fac
        xt=.25*(xt+alamb)
        yt=.25*(yt+alamb)
        zt=.25*(zt+alamb)
        ave=.2*(xt+yt+3.*zt)
        delx=(ave-xt)/ave
        dely=(ave-yt)/ave
        delz=(ave-zt)/ave
      if(max(abs(delx),abs(dely),abs(delz)).gt.ERRTOL)goto 1
      ea=delx*dely
      eb=delz*delz
      ec=ea-eb
      ed=ea-6.*eb
      ee=ed+ec+ec
      rd=3.*sum+fac*(1.+ed*(-C1+C5*ed-C6*delz*ee)+delz*(C2*ee+delz*(-C3*
     *ec+delz*C4*ea)))/(ave*sqrt(ave))
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 6?6>)AY.
