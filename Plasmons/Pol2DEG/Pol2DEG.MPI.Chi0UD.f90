program Pol2DEGMPIChi0UD
use mpi
use Constants
use Globals
use PhysicsPol2DEG
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),MPIa,MPIb
real (kind=8), allocatable::wline(:),MPIas(:),MPIbs(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	call ConstInit(1.25D0,-4D-2,GlobEf,Globeta)
	MPImaster=0
	IntSteps=2000   !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=50
	wsteps=300
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps))
	w=0D0
	dw=3D0*GlobEf/real(wsteps, kind=8)
	dq=3D0*GlobkfMAX/real(qsteps, kind=8)
	q=(/dq,0D0/)
	!!!!
	!!!! Initialize MPI
	call MPI_Init(MPIerr)
	call MPI_Comm_size(MPI_COMM_WORLD,NProc,MPIerr)
	call MPI_Comm_rank(MPI_COMM_WORLD,MPIid,MPIerr)


	allocate(MPIas(NProc))
	allocate(MPIbs(NProc))
	!!!!
	!!!! Initialize Data files
	if (MPIid.eq.MPImaster) then
		open(unit=9, file='MPI.Chi0UD.z4.dat')
		MPISteps=IntSteps/NProc
		wtime=MPI_Wtime()
		do i=1,NProc
			MPIas(i)=GlobkfMAX*real(i-1, kind=8)/real(NProc, kind=8)
			MPIbs(i)=GlobkfMAX*real(i, kind=8)/real(NProc, kind=8)
		enddo
		write(9,*) "### Chi0UD(q,w)"
		write(9,*) "###",Globrs,Globz
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# Chi0UD(q,wstart)   Chi0UD(q,wstart+dw)..."
	endif

	call MPI_Bcast(MPISteps,1,MPI_INTEGER,MPIMaster,MPI_COMM_WORLD,MPIerr)
	call MPI_Bcast(MPIas,NProc,MPI_INTEGER,MPIMaster,MPI_COMM_WORLD,MPIerr)
	call MPI_Bcast(MPIbs,NProc,MPI_INTEGER,MPIMaster,MPI_COMM_WORLD,MPIerr)
	MPIa=MPIas(MPIid+1)
	MPIb=MPIbs(MPIid+1)
	!!!!
	!!!! Begin integration loop

	do i=1,qsteps
		MPILind=0D0
		Lind=0D0
		w=0D0
		wline=0D0
		if (MPIid.eq.MPImaster) then
			write(9,fmt1) q(1)/GlobkfMAX, w, dw
			write(*,*) "q",i,"of",qsteps
		endif
		call MPI_Bcast(q,2,MPI_DOUBLE_PRECISION,MPIMaster,MPI_COMM_WORLD,MPIerr)	
		do j=1,wsteps
			!!! Broadcast w. Mostly used for synchronization of w loop
			call MPI_Bcast(w,1,MPI_DOUBLE_PRECISION,MPIMaster,MPI_COMM_WORLD,MPIerr)		

				MPILind=PolIntegratek(ReChi0UDInt,MPIa,MPIb,MPISteps,q,w)
			call MPI_Reduce(MPILind,Lind,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPImaster,MPI_COMM_WORLD,MPIerr)
			if (MPIid.eq.MPImaster) then
				wline(j)=Lind
				Lind=0D0
				w=w+dw
			endif
		enddo
		if (MPIid.eq.MPImaster) then
			!!! Write data
			write(9,fmt2) wline
			q(1)=q(1)+dq
		endif
	enddo
	!!!!
	if (MPIid.eq.MPImaster) then
		wtime=MPI_Wtime()-wtime
		write(*,*) "Time:",wtime
	endif
	call MPI_Finalize(MPIerr)
	close(unit=9)
end program Pol2DEGMPIChi0UD
