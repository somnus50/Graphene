program MiscPlot
	use Constants	
	use PhysicsPolGraphene
	use Globals
	use AuxiliaryFunctions
	use fxc2DEG
	!use HelperFunctions
	implicit none
	real    (kind=8):: dphi,dk,k(2),q(2),w,temp1,temp2,k2(2),n,rs,z,drs,dw,temp3
!	real (kind=8), allocatable :: vect(:)
	integer (kind=4):: i,j,wsteps

	z=-4D-2
	n=10D12/cmSq2a0sq
	Globgs=2D0
	Globgv=2D0
	call ConstInit(n,z)
!	GlobEta=1D-5*GlobEf
	wsteps=10
	w=1.5D-2
	dw=1.001D-2/real(wsteps, kind=8)
	do i=1,wsteps
		temp1=-Globn*z/(w-GlobZeem)
		!temp2=PolIntegratek(ReChi0UDIntraU,0D0,2D0*Globkf0,3000,(/0D0,0D0/),w)
		temp3=PolIntegratek(ReChi0UDIntraU,0D0,1.1D0*Globkf0,3000,(/0D0,0D0/),w)*Globgv/2D0/4D0/pi**2!/4D0/pi**2
		write(*,*) w,temp3,temp1, temp1/temp3
		w=w+dw
	enddo


end program MiscPlot
