# Graphene Nanoribbons Program

I've included a shell script that is the preferable way to compile and run the program.

It checks to see if the program is already compiled. If not, it will try to compile Graphene.f95.

Then it will run the program, list the files available to plot and give an example command to plot them.
