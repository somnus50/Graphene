module fxcPGG
implicit none

contains


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine DPPGGuu(q,kf,PGGresult,abserr,neval,ier)
use Constants
use QuadPackDouble
implicit none
real (kind=8), intent(in)::q,kf
real (kind=8), intent(out)::PGGresult,abserr
real (kind=8)::tempresult
real (kind=8), allocatable::work(:)
integer (kind=4),intent(out)::neval,ier
integer (kind=4)::limit,lenw,last
integer (kind=4), allocatable::iwork(:)


	limit=500
	lenw=5*limit
	allocate(iwork(limit),work(lenw))
	call dqagi ( PGGInt, 0._8, 1, 10D-8, 10D-8, tempresult, abserr, neval, ier, limit,lenw,last,iwork,work )

	PGGresult=-tempresult*pi/2D0/kf**2




contains
	real (kind=8) function PGGInt(r)
	implicit none
	real (kind=8)::a,b,r

		a=BESSEL_JN(0,q*r)
		b=BESSEL_JN(1,kf*r)
		PGGInt=a*b**2/r**2

	end function PGGInt

end subroutine DPPGGuu


subroutine DPPGGud(q,kfu,kfd,PGGresult,abserr,neval,ier)
use Constants
use QuadPackDouble
implicit none
real (kind=8), intent(in)::q,kfu,kfd
real (kind=8), intent(out)::PGGresult,abserr
real (kind=8)::tempresult
real (kind=8), allocatable::work(:)
integer (kind=4),intent(out)::neval,ier
integer (kind=4)::limit,lenw,last
integer (kind=4), allocatable::iwork(:)


	limit=500
	lenw=5*limit
	allocate(iwork(limit),work(lenw))
	call dqagi ( PGGInt, 0._8, 1, 10D-8, 10D-8, tempresult, abserr, neval, ier, limit,lenw,last,iwork,work )

	PGGresult=-tempresult*pi/2D0/kfu/kfd




contains
	real (kind=8) function PGGInt(r)
	implicit none
	real (kind=8)::a,b,c,r

		a=BESSEL_JN(0,q*r)
		b=BESSEL_JN(1,kfu*r)
		c=BESSEL_JN(1,kfd*r)
		PGGInt=a*b*c/r**2

	end function PGGInt

end subroutine DPPGGud



subroutine DPPGGdu(q,kfu,kfd,PGGresult,abserr,neval,ier)
use Constants
use QuadPackDouble
implicit none
real (kind=8), intent(in)::q,kfu,kfd
real (kind=8), intent(out)::PGGresult,abserr
real (kind=8)::tempresult
integer (kind=4),intent(out)::neval,ier


	call DPPGGud(q,kfu,kfd,PGGresult,abserr,neval,ier)

end subroutine DPPGGdu


subroutine DPPGGdd(q,kf,PGGresult,abserr,neval,ier)
use Constants
use QuadPackDouble
implicit none
real (kind=8), intent(in)::q,kf
real (kind=8), intent(out)::PGGresult,abserr
real (kind=8)::tempresult
integer (kind=4),intent(out)::neval,ier

	call DPPGGuu(q,kf,PGGresult,abserr,neval,ier)

end subroutine DPPGGdd
























end module fxcPGG
