program NSqHPlasmons
	use constants	
	use NSquareHLindFunctions
	use HelperFunctions
	implicit none
	real (kind=8)::q,dw,phi,dphi,Gx,Gy,DATalat,DATt,DATFillingFrac,DATetafrac,bp
	real (kind=8), allocatable:: LindLine(:), EpsLine(:,:),PlasmonArray(:,:),blist(:)
	integer (kind=4):: steps,gstep,qstep,phistep,i,gsteps,qsteps,phisteps,wsteps,bstep
	integer (kind=4):: Gindex,wstep,j
	character (len=20):: trash

	
	!open(unit=20,file='SqHPlasmonG00.dat')
	!open(unit=21,file='SqHPlasmonG10.dat')
	!open(unit=22,file='SqHPlasmonG01.dat')
	!open(unit=23,file='SqHPlasmonGm0.dat')
	!open(unit=24,file='SqHPlasmonG0m.dat')

	open(unit=20,file='SqHPlasmonb1.dat')
	open(unit=21,file='SqHPlasmonb2.dat')
	open(unit=22,file='SqHPlasmonb3.dat')
	open(unit=23,file='SqHPlasmonb4.dat')
	open(unit=24,file='SqHPlasmonb5.dat')

	open(unit=10,file='SqHLindMatrix.Ef.2.5.dat',status='old')
	read(10,*) !trash
	read(10,*) DATalat, DATt, DATFillingFrac, DATetafrac
	read(10,*) !trash
	read(10,*) qsteps, phisteps, wsteps
	read(10,*) !trash

	allocate(LindLine(wsteps))
	allocate(EpsLine(5,wsteps))
	allocate(PlasmonArray(10,3))

	allocate(blist(5))
	blist=(1D0/alat)*(/75D0,80D0,85D0,120D0,120D0/)*1D-2



	i=0
	Gindex=19
	do qstep=1,qsteps
		read(10,*) !trash
		read(10,*) q, dw, dphi
		read(10,*) !trash
		do phistep=1,phisteps
			phi=dphi*real(phistep)
			read(10,*) LindLine(:)
			do bstep=1,size(blist)
			bp=blist(bstep)
			call ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
			Gstep=1
			!do Gstep=1,5
				write(*,*) i, qstep, phistep, Gstep, wsteps
				call FindBracketList2(EpsLine(Gstep,:),wsteps,q,dw,PlasmonArray)
				i=i+1
				do j=1,10
					if ((PlasmonArray(j,2)).gt.(0D0)) then
						!write(Gindex+Gstep,*) phi, PlasmonArray(j,1)/kf,&
						write(Gindex+bstep,*) phi, PlasmonArray(j,1)/FermiVec(PlasmonArray(j,1)*(/dcos(phi),dsin(phi)/)),&
						(PlasmonArray(j,2)+PlasmonArray(j,3))/(2D0*Ef),&
						FermiVec(PlasmonArray(j,1)*(/dcos(phi),dsin(phi)/))*dsqrt(q)/Ef
					endif
				enddo
			!enddo
			enddo
		enddo
	enddo


	close(unit=10,status='keep')
	close(unit=20,status='keep')
	close(unit=21,status='keep')
	close(unit=22,status='keep')
	close(unit=23,status='keep')
	close(unit=24,status='keep')





end program NSqHPlasmons
