#!/usr/bin/env bash



input="rszgen.dat"

template="SCTest.Pol.Parallel.f90"
ptemp="bin/sample.pbs"

delimiter=' '

#rm -rf "bin/tmp/fxc.rs.z.txt"

while IFS= read -r line
do

echo "'"$line"'"

IFS=' ' read -ra array <<< "$line"

tmpfile="tmp"${array[2]}
echo "$tmpfile"

sed -e "s/myrs/${array[0]}/;s/myz/${array[1]}/;s/myfile/fSTLS-par-${array[2]}.dat/" "$template" > "$tmpfile.f90"

./compilemain.sh -lc gfortran "$tmpfile.f90"


sed -e "s/myname/$tmpfile.bin/g;" "$ptemp" > "bin/$tmpfile.pbs"


echo "(cd bin && qsub $tmpfile.pbs)"
(cd ./bin && qsub "$tmpfile.pbs")

#(cd ./bin && echo " $(pwd) $tmpfile.pbs")

echo "rm -f $tmpfile.f90" "$tmpfile.pbs"

# rm -f "$tmpfile.f90" "$tmpfile.pbs"


done < "$input"


