program Pol2DEGChi0UDLogrsz
use Constants
use Globals
use PhysicsPol2DEG
!use fxc2DEG
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z,wa,wb&
		,rss(5),zs(5),dz
real (kind=8), allocatable::wline(:),qgrid(:),wgrid(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex,rsi,zi
character (len=32)::fmt1,fmt2,name1

rsi=4
rss=(/2D0,5D0,10D0,20D0,30D0/)
dz=(1D0-2D0*0.01D0)/4D0
zs(1)=0.01D0
do i=2,5
	zs(i)=zs(i-1)+dz
enddo

MPImaster=0
IntSteps=1  !Multiplied by 10 because of Newton-Cotes 10pt integration
qsteps=500
wsteps=500
NProc=1
write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
fmt1='(3ES24.15)'
allocate(wline(wsteps),qgrid(qsteps),wgrid(wsteps))

!do rsi=1,5
	do zi=1,5
	!!!! Initialize some situational constants
  call ConstInit(rss(rsi),zs(zi),GlobEf,Globeta)
	!write(*,*) Globkf0,GLobkfU,GlobkfD
	write(name1, '(a, i0, a, i0, a )') 'Pol2DEG.ReChi0UD.rs',rsi,'.z',zi,'.log.dat'

	!wa=0.9D0*GlobZeem
	!wb=1.01D0*GlobZeem
	!dw=(wb-wa)/real(wsteps, kind=8)
	!dq=2D0*(GlobZeem-wa)/gam/real(qsteps, kind=8)
	call LogGrid(LGdrSuggest(7D0/50D0,qsteps),qsteps,qgrid)
	qgrid=qgrid*Globkf0*50D0
	call LogGrid(LGdrSuggest(7D0/50D0,wsteps),wsteps,wgrid)
	wgrid=wgrid*GlobEf*50D0
	q=0D0!(/dq,0D0/)
	open(unit=9, file=name1)
	open(unit=10, file='Pol2DEG.ReChi0UD.rs.z.log.qgrid.dat')
	open(unit=11, file='Pol2DEG.ReChi0UD.rs.z.log.wgrid.dat')

	do i=1,qsteps
		write(10,*) qgrid(i)/Globkf0
	enddo

	do i=1,wsteps
		write(11,*) wgrid(i)/GlobEf
	enddo

	close(unit=10)
	close(unit=11)

		write(9,*) "### ReChi0UD(q,w) LogGrid"
		write(9,*) "###", Globn, Globz, Globrs
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	!write(*,*) "Ef   kf0"
	!write(*,*) GlobEf,Globkf0
	q=0D0
	q(1)=qgrid(1)
	do i=1,qsteps
		w=wgrid(1)
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "rs",rsi,", zi",zi,", q",i,qsteps
		do j=1,wsteps
			!wline(j)=PolIntegratek(ReChi0DUIntraU,max(q(1)-GlobkfMAX,0D0),q(1)+Globkf0,IntSteps,q,w)*temp
			wline(j)=ReChi0sspNum((/qgrid(i),0D0/),w,1D0,-1D0,IntSteps)
			w=wgrid(j)
		enddo
		write(9,fmt2) wline
		q(1)=qgrid(i)
	enddo

	close(unit=9)

enddo
!enddo



end program Pol2DEGChi0UDLogrsz
