program Lindhard
	use constants	
	use LindFns
	implicit none
	real    (kind=8):: q,w
	integer (kind=4):: i
!	complex (kind=8):: Chi,ChiCalc


	q = 1D0*kf

	open(unit=8, file='ChiReal.dat')
	open(unit=9, file='ChiImag.dat')

	do i=1,100
		write(*,*) i
		w = 0.01D0*i*kf**2
		write(8,*) w/kf**2,dreal(Chi(q,w)),dreal(ChiCalc(q,w))
		write(9,*) w/kf**2,dimag(Chi(q,w)),dimag(ChiCalc(q,w))
	enddo

	close(unit=8)
	close(unit=9)



end program Lindhard

