#! /usr/bin/gnunplot


##########################################
### Graphene LDA/PGG Spin Wave ###########
##########################################
reset
set termoption enhanced
set key opaque
set xlabel "q/k_f"
set ylabel "{/Symbol w}/Z^*"
set title "Graphene Spin Wave"
set size square


p 'wmagnon.LDA-PGG.z20.dat' u 1:2 w l title 'LDA', '' u 1:3 w l title 'PGG', '' u 1:4:(1.0) w filledcurves lc 'gray' title 'Spin-flip Ctm'


### You'll probably need to adjust these coordinates
set label "n=2x10^{13} cm^{-2}" at 0.02,0.8
set label "{/Symbol z}=-20%"    at 0.02,0.775
rep

##########################################
