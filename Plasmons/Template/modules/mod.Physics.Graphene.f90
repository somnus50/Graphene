!##############################################################################
!                                    References
!##############################################################################
!
! [1] E. Hwang and S. Das Sarma, Physical Review B 75, (2007).
!     DOI: 10.1103/PhysRevB.75.205418
!
! [2]
!
! [3]
!
module PhysicsGraphene
use Constants
use Globals
implicit none

contains

	real (kind=8) function En(b,q)
		implicit none
		real (kind=8) ::q(2),b,temp,temp2
		! Nearest Neighbor Tight Binding energy band of Graphene
		! b :: real, band index, (+/-)1 => (upper/lower) band
		! q :: real, wavevector

			!temp=anc(q)
			!temp2=-b*temp
			!En=tTBEh*temp2!+e2p !e2p usually zero. Trying to be efficient.
			!En=En/(1D0+sTB*temp2)
			En=b*gam*NormC(q-(/0D0,BZkmax/))
		contains
			real (kind=8) function anc(q)
				implicit none
				real (kind=8) :: q(2)
				real (kind=8),parameter::temp1=dsqrt(3D0)/2D0
				! Ancillary function

				anc=dcos(q(2)*aGraphene/2D0)
				anc=dcos(q(1)*aGraphene*temp1)*anc+anc**2
				anc=1D0+4D0*anc
				anc=dsqrt(anc)
			end function anc
	end function En

	real (kind=8) function f(b,q)
		implicit none
		real (kind=8) ::q(2),b
		! Fermi-Dirac Distribution for electrons
		! b :: real, band index
		! q :: real, wavevector

			f=0D0
			!!!Zero Temperature
			if ((En(b,q)).le.(GlobEf)) then
				f=1D0
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(q)-GlobEf)/kT))
	end function f

	real (kind=8) function FermiVec(q,delta)
		use AuxiliaryFunctions
		implicit none
		real (kind=8)::q(2),delta,phi
		! Returns Fermi wavevector magnitude in direction of q.
		! Used to deal with anisotropic energy bands.
		! q     :: real, wavevector
		! delta :: real, tolerance for rootfinder

		phi=datan2(q(2),q(1))
		FermiVec=RootBrentsMethod2(BZkmax,0D0,delta,anc,phi)

		contains
			real (kind=8) function anc(phi,q2)
				implicit none
				real (kind=8)::phi,q2
				! Ancillary function to find when En(b,q)=Ef

					anc=GlobEf-En(1D0,(/0D0,BZkmax/)-q2*(/dcos(phi),dsin(phi)/))
			end function anc
	end function FermiVec

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of 2D screened Coulomb Potential
		! [1].(2)
		! q   :: real, wavevector

		NVc=2D0*pi*e**2/NormC(q)/kappa
	end function NVc

	real (kind=8) function NormC(v)
		implicit none
		real (kind=8) ::v(2)
		! Norm of 2D Cartesian vector
		! v   :: real wavevector

		NormC=(v(1))**2+(v(2))**2
		NormC=dsqrt(NormC)
	end function NormC

	real (kind=8) function ReNLindIntk(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),kp(2),w,temp,s1,s2,cth
		real (kind=8), parameter::sv(2)=(/1D0,-1D0/)
		integer (kind=4) ::i,j
		! Real part of Lindhard Function
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ReNlindIntk=0D0
			kp=k+q
			cth=costh(k,kp)
			do i=1,2
				s1=sv(i)
				do j=1,2
					s2=sv(j)
					if (f(s1,k).eq.f(s2,kp)) cycle
					temp=w+En(s1,k)-En(s2,kp)
					temp=temp/(temp**2+Globeta**2)
					ReNlindIntk=ReNLindIntk+temp*(f(s1,k)-f(s2,kp))*(1D0+s1*s2*cth)/2D0
				enddo
			enddo

		contains
			real (kind=8) function costh(k,kp)
				real (kind=8)::k(2),kp(2)

				costh=datan2(kp(2),kp(1))-datan2(k(2),k(1))
				costh=dcos(costh)

			end function costh
	end function ReNLindIntk

	real (kind=8) function ReNLindIntInter(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),kp(2),w,temp,s1,s2,cth,temp2
		real (kind=8), parameter::sv(2)=(/1D0,-1D0/)
		integer (kind=4) ::i,j
		! Real part of Lindhard Function for only interband transitions
		!     interband => s1=-s2
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ReNlindIntInter=0D0
			kp=k+q
			cth=costh(k,kp)
			do i=1,2
				s1=sv(i)
				s2=-s1
				temp2=f(s1,k)-f(s2,kp)
					if (abs(temp2).le.(5D-1)) cycle
					temp=w+En(s1,k)-En(s2,kp)
					temp=temp/(temp**2+Globeta**2)
					ReNlindIntInter=ReNLindIntInter+temp*temp2*(1D0-cth)/2D0
			enddo

		contains
			real (kind=8) function costh(k,kp)
				real (kind=8)::k(2),kp(2)

				costh=datan2(kp(2),kp(1))-datan2(k(2),k(1))
				costh=dcos(costh)

			end function costh
	end function ReNLindIntInter

	real (kind=8) function ReNLindIntIntra(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),kp(2),w,temp,s1,s2,cth,temp2
		real (kind=8), parameter::sv(2)=(/1D0,-1D0/)
		integer (kind=4) ::i,j
		! Real part of Lindhard Function for only intraband transitions
		!     intraband => s1=s2 (=1 for n-doping)
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ReNlindIntIntra=0D0
			kp=k+q
			cth=costh(k,kp)
			s1=1D0
			temp2=f(s1,k)-f(s1,kp)
			if (abs(temp2).ge.(5D-1)) then
				temp=w+En(s1,k)-En(s1,kp)
				temp=temp/(temp**2+Globeta**2)
				ReNlindIntIntra=temp*temp2*(1D0+cth)/2D0
			endif

		contains
			real (kind=8) function costh(k,kp)
				real (kind=8)::k(2),kp(2)

				costh=datan2(kp(2),kp(1))-datan2(k(2),k(1))
				costh=dcos(costh)

			end function costh
	end function ReNLindIntIntra

	real (kind=8) function ReNLindInt(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),w
		! Real part of Lindhard Function
		!
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ReNLindInt=ReNLindIntIntra(q,w,k)+ReNLindIntInter(q,w,k)
	end function ReNLindInt

	real (kind=8) function ImNLindIntk(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),kp(2),w,temp,s1,s2,cth
		real (kind=8), parameter::sv(2)=(/1D0,-1D0/)
		integer (kind=4) ::i,j
		! Imaginary part of Lindhard Function
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ImNlindIntk=0D0
			kp=k+q
			cth=costh(k,kp)
			do i=1,2
				s1=sv(i)
				do j=1,2
					s2=sv(j)
					if (f(s1,k).eq.f(s2,kp)) cycle
					temp=w+En(s1,k)-En(s2,kp)
					temp=-Globeta/(temp**2+Globeta**2)
					ImNlindIntk=ImNLindIntk+temp*(f(s1,k)-f(s2,kp))*(1D0+s1*s2*cth)/2D0
				enddo
			enddo

		contains
			real (kind=8) function costh(k,kp)
				real (kind=8)::k(2),kp(2)

				costh=datan2(kp(2),kp(1))-datan2(k(2),k(1))
				costh=dcos(costh)

			end function costh
	end function ImNLindIntk

	real (kind=8) function ImNLindIntInter(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),kp(2),w,temp,s1,s2,cth
		real (kind=8), parameter::sv(2)=(/1D0,-1D0/)
		integer (kind=4) ::i,j
		! Imaginary part of Lindhard Function for only interband transitions
		!     interband => s1=-s2
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ImNlindIntInter=0D0
			kp=k+q
			cth=costh(k,kp)
			do i=1,2
				s1=sv(i)
				s2=-s1
					if (abs(f(s1,k)-f(s2,kp)).le.(5D-1)) cycle
					temp=w+En(s1,k)-En(s2,kp)
					temp=-Globeta/(temp**2+Globeta**2)
					ImNlindIntInter=ImNLindIntInter+temp*(f(s1,k)-f(s2,kp))*(1D0+s1*s2*cth)/2D0
			enddo

		contains
			real (kind=8) function costh(k,kp)
				real (kind=8)::k(2),kp(2)

				costh=datan2(kp(2),kp(1))-datan2(k(2),k(1))
				costh=dcos(costh)

			end function costh
	end function ImNLindIntInter

	real (kind=8) function ImNLindIntIntra(q,w,k)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::q(2),k(2),kp(2),w,temp,s1,s2,cth
		real (kind=8), parameter::sv(2)=(/1D0,-1D0/)
		integer (kind=4) ::i,j
		! Imaginary part of Lindhard Function for only intraband transitions
		!     intraband => s1=s2 (=1 for n-doping)
		! [1].(3)
		! q   :: real, wavevector
		! w   :: real, frequency
		! q and k must be Cartesian!

		ImNlindIntIntra=0D0
			kp=k+q
			cth=costh(k,kp)
			s1=1D0
			if (abs(f(s1,k)-f(s1,kp)).ge.(5D-1)) then
				temp=w+En(s1,k)-En(s1,kp)
				temp=-Globeta/(temp**2+Globeta**2)
				ImNlindIntIntra=temp*(f(s1,k)-f(s1,kp))*(1D0+cth)/2D0
			endif

		contains
			real (kind=8) function costh(k,kp)
				real (kind=8)::k(2),kp(2)

				costh=datan2(kp(2),kp(1))-datan2(k(2),k(1))
				costh=dcos(costh)

			end function costh
	end function ImNLindIntIntra

	real (kind=8) function RadFnPolInt2(radfn,phia,phib,steps,intfn,q,w)
		implicit none
		integer (kind=4):: rstep,phistep,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,phia,phib,khat(2),kp(2),kshift(2),temp
		real (kind=8), external:: intfn,radfn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*k*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of steps in both k and phi
		! q,w   :: real, arguments of fn

			!Pts=0 !!! Thinking about counting the points since some get thrown out.
			kshift=(/0D0,BZkmax/)
			dphi=(phib-phia)/real(steps)
			RadFnPolInt2=0D0
			phi=phia
			do phistep=1,steps!+1
				dk=radfn(phi)/real(steps,kind=8)
				k=dk
				khat=(/dcos(phi),dsin(phi)/)
				do rstep=1,steps!rstepfn(steps,phi,(phib+phia)/2D0)

					!!! Debug Block
					!write(*,*) kshift+k*khat, interiorkp(kshift+kp,phia,phib)*k*intfn(q,w,kshift+k*khat)*dk
					!if ((steps-rstepfn(steps,phi,(phib+phia)/2D0).gt.0).and.(rstep.eq.rstepfn(steps,phi,(phib+phia)/2D0))) then
					!	write(*,*) kshift+(k+dk)*khat, 0D0
					!endif

					!if (interiorkp(kshift+kp,phia,phib).lt.1D0) cycle
					RadFnPolInt2=RadFnPolInt2+k*intfn(q,w,kshift+k*khat)*dk
					k=k+dk
				enddo
				phi=phi+dphi
			enddo
			RadFnPolInt2=RadFnPolInt2*dphi
			!MPIIntPts=MPIIntPts+Pts

			contains
				integer (kind=4) function rstepfn(steps,phi,phi2)
					real (kind=8)::phi,phi2
					integer (kind=4)::steps
					! Determines number of steps to take in radial direction.
					! Used to keep one edge of the integration diamond open
					! to avoid double counting the edges.
					!!!!! NO LONGER NEEDED WITH TRIANGLE DOMAIN

					rstepfn=steps
					if (phi.ge.phi2) then
						rstepfn=rstepfn-1
					endif

				end function rstepfn

				real (kind=8) function interiorkp(v,phi1,phi2)
					use AuxiliaryFunctions
					implicit none
					real (kind=8)::v(2),p(2),phi1,phi2
					!real (kind=8), external::fn

					interiorkp=0D0
					call Cart2Pol(v,p)
					if (p(1).le.HexBound(BZkmax,p(2))) then !Check for kp in hexagon
						interiorkp=1D0
					endif

				end function interiorkp

	end function RadFnPolInt2

	real (kind=8) function NewtPolInt(radfn,phia,phib,steps,intfn,q,w)
		implicit none
		integer (kind=4):: rstep,phistep,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,phia,phib
		real (kind=8)::k2,khat(2),kp(2),kshift(2),temp
		real (kind=8), external:: intfn,radfn

		! 2D polar integrator over k and phi.
		! S fn(q,w)*k*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of steps in both k and phi
		! q,w   :: real, arguments of fn

			!Pts=0 !!! Thinking about counting the points since some get thrown out.
			kshift=(/0D0,BZkmax/)
			dphi=(phib-phia)/real(steps)
			NewtPolInt=0D0
			phi=phia
			do phistep=1,steps!+1
				dk=radfn(phi)/real(steps,kind=8)
				k=0D0
				k2=dk
				khat=(/dcos(phi),dsin(phi)/)
				do rstep=1,steps!rstepfn(steps,phi,(phib+phia)/2D0)

					!!! Debug Block
					!write(*,*) kshift+k*khat, interiorkp(kshift+kp,phia,phib)*k*intfn(q,w,kshift+k*khat)*dk
					!if ((steps-rstepfn(steps,phi,(phib+phia)/2D0).gt.0).and.(rstep.eq.rstepfn(steps,phi,(phib+phia)/2D0))) then
					!	write(*,*) kshift+(k+dk)*khat, 0D0
					!endif

					!NewtPolInt=NewtPolInt+k*intfn(q,w,kshift+k*khat)*dk
					k=k2
					k2=k2+dk
				enddo
				phi=phi+dphi
			enddo
			NewtPolInt=NewtPolInt*dphi
			!MPIIntPts=MPIIntPts+Pts

			contains

				real (kind=8) function NC10(a,b,fn,khat)
					integer (kind=4)::i
					real (kind=8) ::a,b,x,dx,khat(2)
					real (kind=8), external::fn
					real (kind=8), parameter:: Newt10(10)=(/2857D0,15741D0,&
															1080D0,19344D0,&
															5778D0,	5778D0,&
															19344D0,1080D0,&
															15741D0,2857D0&
															/)*9D0/89600D0

					NC10=0D0
					x=a
					dx=(b-a)/9D0
					do i=1,10
						NC10=NC10+Newt10(i)*x*fn(x)
						x=x+dx
					enddo
				end function NC10

	end function NewtPolInt

	subroutine qrots(q,qarray)
		use AuxiliaryFunctions
		implicit none
		real (kind=8),intent(in) ::q(2)
		real (kind=8),intent(out)::qarray(6)
		real (kind=8)::qp(2),temp(2)
		integer(kind=8)::i
		! Rotates vector 3 times
		! Returns flat array of rotations
		! (x1,y1,x2,y2,x3,y3)

		call Cart2Pol(q,qp)
		qarray(1)=q(1)
		qarray(2)=q(2)
		do i=2,3
			qp(2)=qp(2)+2D0*pi/3D0
			call Pol2Cart(qp,temp)
			qarray(2*i-1)=temp(1)
			qarray(2*i)=temp(2)
		enddo
	end subroutine qrots

	real (kind=8) function DBwrapper(phi)
		use AuxiliaryFunctions
		implicit none
		real (kind=8) ::phi
		! Wrapper to reduce number of arguments of DiamondBound
		! from AuxiliaryFunctions module

		DBwrapper=DiamondBound(BZkmax,phi)
	end function DBwrapper

	real (kind=8) function fxc(q)
		implicit none
		integer (kind=4)::flag
		real (kind=8) ::q(2)
		! Exchange Correlation
		! q :: real, wavevector
		! flag :: int, turns exchange on or off

		flag=1
		fxc=0D0
		if (flag.eq.1) then
			fxc=0D0!Globfx
			!fxc=Globfc
			!fxc=Globfx+Globfc
		endif
	end function fxc

	subroutine ReNEpsLine(qv,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,wstep
		real (kind=8), intent(in)  ::qv(2),LindLine(wsteps)
		real (kind=8), intent(out) ::EpsLine(wsteps)
		! Takes line of Lindhard Function for fixed q and varying w
		! Returns dielectric function at same points

		do wstep=1,wsteps
			EpsLine(wstep)=1D0+NVc(qv)*LindLine(wstep)
		enddo
	end subroutine ReNEpsLine

	subroutine ConstInit(n,z,Ef,eta,fx,fc)
		use Globals
		implicit none
		!integer (kind=4) ::
		real    (kind=8), intent(in)::n,z
		real    (kind=8), intent(out)::Ef,eta,fx,fc
		real    (kind=8)::frac
		! Initializes a bunch of constants


			! Easy Stuff
			!Ef=8D0*t*frac !Specific to how I calculated the Lindhard Matrix
			!n=Ef/pi
			Globn=n
			Ef=gam*dsqrt(n*pi)
			eta=1D-3*Ef
			Globkf=dsqrt(n*pi)
			Globrs=n2rs(n)

			! Hard Stuff
			fx=2D0*dnexht(n,z)+n*d2nexht(n,z)
			!fc=2D0*dnecht(Globrs,z)+n*d2necht(Globrs,z)
			fc=Numfc(n,z)
			!Debug

			!fx=n*echt(Globrs,z)
			!fc=Numdnech(n,z)
			!Globw=fx+fc

		contains
		!!!! Thinking about moving this to another module.
			real (kind=8) function rs2n(rs)
				implicit none
				real (kind=8)::rs

					rs2n=pi*rs**2
					rs2n=1D0/rs2n
			end function rs2n

			real (kind=8) function n2rs(n)
				implicit none
				real (kind=8)::n

					n2rs=1D0/dsqrt(pi*n)
			end function n2rs

			real (kind=8) function dnz(n,z)
				implicit none
				real (kind=8)::n,z

					dnz=0D0
					!dnz=-z/n
			end function dnz

			real (kind=8) function dnrs(n,rs)
				implicit none
				real (kind=8)::n,rs

					dnrs=-1D0/(2D0*dsqrt(pi))*n**(-1.5D0)
			end function dnrs

			real (kind=8) function d2nrs(n,rs)
				implicit none
				real (kind=8)::n,rs

					d2nrs=1.5D0/(2D0*dsqrt(pi))*n**(-2.5D0)
			end function d2nrs

			real (kind=8) function fz(z)
				implicit none
				real (kind=8)::z

					fz=(1D0+z)**(4D0/3D0)+(1D0-z)**(4D0/3D0)-2D0
					fz=fz/(2D0*(2D0**(1D0/3D0)-1D0))
			end function fz

			real (kind=8) function dzfz(z)
				implicit none
				real (kind=8)::z

					dzfz=(1D0+z)**(1D0/3D0)-(1D0-z)**(1D0/3D0)
					dzfz=dzfz*2D0/3D0/(2D0**(1D0/3D0)-1D0)
			end function dzfz

			real (kind=8) function exh(n,z)
				implicit none
				real (kind=8)::n,z

					exh=1D0+(dsqrt(2D0)-1D0)*fz(z)
					exh=-exh*4D0*dsqrt(2D0/pi)/3D0*n**(1.5D0)
			end function exh

			real (kind=8) function dnexh(n,z)
				implicit none
				real (kind=8)::n,z

					dnexh=-2D0*dsqrt(2D0*n/pi)
					dnexh=dnexh*(1D0+(dsqrt(2D0)-1D0)*fz(z))
			end function dnexh

			real (kind=8) function d2nexh(n,z)
				implicit none
				real (kind=8)::n,z

					d2nexh=-dsqrt(2D0/(pi*n))
					d2nexh=d2nexh*(1D0+(dsqrt(2D0)-1D0)*fz(z))
			end function d2nexh

			real (kind=8) function alphay(indy,indalpha,rs)
				implicit none
				integer (kind=4)::indy,indalpha,ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
										  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
										  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
										  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
										  fi(3)=(/-2.069D-2,0D0,0D0/),&
										  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
										  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
										  di(3)=-ai(:)*hi(:)

					alphay=0D0
					ind=indalpha+1
					if (indy.eq.1) then
						alphay=bi(ind)*rs+ci(ind)*rs**2&
							  +di(ind)*rs**3
					elseif (indy.eq.2) then
						alphay=ei(ind)*rs+fi(ind)*rs**(1.5D0)&
							  +gi(ind)*rs**2+hi(ind)*rs**3
					endif
			end function alphay

			real (kind=8) function drsalphay(indy,indalpha,rs)
				implicit none
				integer (kind=4)::indy,indalpha,ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
										  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
										  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
										  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
										  fi(3)=(/-2.069D-2,0D0,0D0/),&
										  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
										  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
										  di(3)=-ai(:)*hi(:)

					drsalphay=0D0
					ind=indalpha+1
					if (indy.eq.1) then
						drsalphay=bi(ind)+2D0*ci(ind)*rs&
							  +3D0*di(ind)*rs**2
					elseif (indy.eq.2) then
						drsalphay=ei(ind)+1.5D0*fi(ind)*dsqrt(rs)&
							  +2D0*gi(ind)*rs+3D0*hi(ind)*rs**2
					endif
			end function drsalphay

			real (kind=8) function d2rsalphay(indy,indalpha,rs)
				implicit none
				integer (kind=4)::indy,indalpha,ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/),&
										  bi(3)=(/8.63136D-2,-3.394D-2,-3.7093D-2/),&
										  ci(3)=(/5.72384D-2,-7.66765D-3,1.63618D-2/),&
										  ei(3)=(/1.0022D0,4.133D-1,1.424301D0/),&
										  fi(3)=(/-2.069D-2,0D0,0D0/),&
										  gi(3)=(/3.3997D-1,6.68467D-2,0D0/),&
										  hi(3)=(/1.747D-2,7.799D-4,1.163099D0/),&
										  di(3)=-ai(:)*hi(:)

					d2rsalphay=0D0
					ind=indalpha+1
					if (indy.eq.1) then
						d2rsalphay=2D0*ci(ind)+6D0*di(ind)*rs
					elseif (indy.eq.2) then
						d2rsalphay=0.75D0*fi(ind)/dsqrt(rs)&
								  +2D0*gi(ind)+6D0*hi(ind)*rs
					endif
			end function d2rsalphay

			real (kind=8) function alpha(ind,rs)
				implicit none
				integer (kind=4)::ind
				real (kind=8)::rs
				real (kind=8), parameter::ai(3)=(/-1.925D-1,1.17331D-1,2.34188D-2/)

					alpha=alphay(2,ind,rs)
					alpha=1D0+1D0/alpha
					alpha=dlog(alpha)
					alpha=alpha*alphay(1,ind,rs)+ai(ind+1)
			end function alpha

			real (kind=8) function dnalpha(ind,rs)
				implicit none
				integer (kind=4)::ind
				real (kind=8)::rs,temp


					dnalpha=dlog(1D0+1D0/alphay(2,ind,rs))*drsalphay(1,ind,rs)
					dnalpha=dnalpha-alphay(1,ind,rs)*drsalphay(2,ind,rs)&
							/alphay(2,ind,rs)/(alphay(2,ind,rs)+1D0)
					dnalpha=dnalpha*dnrs(n,rs)


					!Debug
					!dnalpha=1D0+1D0/alphay(1,ind,rs)
			end function dnalpha

			real (kind=8) function d2nalpha(ind,rs)
				implicit none
				integer (kind=4)::ind
				real (kind=8)::rs,temp


					!dnf1aj
					d2nalpha=-1D0/(1D0+alphay(2,ind,rs))/(alphay(2,ind,rs)**2)&
							*drsalphay(1,ind,rs)*drsalphay(2,ind,rs)
					d2nalpha=d2nalpha+dlog(1D0/(1D0+alphay(2,ind,rs)))*d2rsalphay(1,ind,rs)
					!dnf2aj
					temp=-alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0)*(drsalphay(2,ind,rs)&
						 *drsalphay(1,ind,rs)+alphay(1,ind,rs)*d2rsalphay(2,ind,rs))&
						 +(2D0*alphay(2,ind,rs)+1D0)*alphay(1,ind,rs)*drsalphay(2,ind,rs)**2
					temp=temp/((alphay(2,ind,rs)*(alphay(2,ind,rs)+1D0))**2)


					d2nalpha=d2nalpha+temp
					d2nalpha=d2nalpha*dnrs(n,rs)
					d2nalpha=d2nalpha+dnalpha(ind,rs)*d2nrs(n,rs)/dnrs(n,rs)
			end function d2nalpha

			real (kind=8) function ex6(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)
					ex6=exh(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*exh(nrs,0D0)
					ex6=ex6/nrs
			end function ex6

			real (kind=8) function dnex6(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)

					dnex6=dnexht(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*dnexht(nrs,0D0)
			end function dnex6

			real (kind=8) function d2nex6(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)

					d2nex6=d2nexht(nrs,z)-(1D0+3D0*z**2/8D0+3D0*z**4/128D0)*d2nexht(nrs,0D0)
			end function d2nex6

			real (kind=8) function dnexht(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)

					dnexht=dnexh(nrs,z)/n-exh(nrs,z)/n**2
			end function dnexht

			real (kind=8) function d2nexht(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs

					nrs=rs2n(rs)

					d2nexht=d2nexh(nrs,z)/n-2D0*dnexh(nrs,z)/n**2+2D0*exh(nrs,z)/n**3
			end function d2nexht

			real (kind=8) function echt(rs,z)
				implicit none
				real (kind=8)::rs,z
				real (kind=8), parameter::beta=1.3386D0

					echt=(dexp(-beta*rs)-1D0)*ex6(rs,z)
					echt=echt+alpha(0,rs)+alpha(1,rs)*z**2+alpha(2,rs)*z**4
			end function echt

			real (kind=8) function dnecht(rs,z)
				implicit none
				real (kind=8)::rs,z,nrs
				real (kind=8), parameter::beta=1.3386D0

					nrs=rs2n(rs)

					dnecht=(dexp(-beta*rs)-1D0)*dnex6(rs,z)+dnalpha(0,rs)&
					+dnalpha(1,rs)*z**2+dnalpha(2,rs)*z**4&
					-beta*dexp(-beta*rs)*dnrs(nrs,rs)*ex6(rs,z)

					!Debug
					!dnecht=dnalpha(0,rs)
			end function dnecht

			real (kind=8) function d2necht(rs,z)
				implicit none
				real (kind=8)::nrs,z,rs
				real (kind=8), parameter::beta=1.3386D0

					nrs=rs2n(rs)

					d2necht=(dexp(-beta*rs)-1D0)*d2nex6(rs,z)&
					-2D0*beta*dexp(-beta*rs)*dnrs(nrs,rs)*dnex6(rs,z)&
					-beta*dexp(-beta*rs)*d2nrs(nrs,rs)*ex6(rs,z)&
					+ex6(rs,z)*dexp(-beta*rs)*beta*(beta*(dnrs(nrs,rs)**2)-d2nrs(nrs,rs))&
					+d2nalpha(0,rs)+d2nalpha(1,rs)*z**2+d2nalpha(2,rs)*z**4


					!Debug
			end function d2necht

			real (kind=8) function NumDeriv1(f,x,h)
				real (kind=8)::x,h
				real (kind=8), external::f

					NumDeriv1=-f(x+2D0*h)+8D0*f(x+h)-8D0*f(x-h)+f(x-2D0*h)
					NumDeriv1=NumDeriv1/12D0/h

			end function NumDeriv1

			real (kind=8) function NumDeriv2(f,x,h)
				real (kind=8)::x,h
				real (kind=8), external::f

				NumDeriv2=f(x-h)-2D0*f(x)+f(x+h)
				NumDeriv2=NumDeriv2/h**2

			end function NumDeriv2

			real (kind=8) function Numfc(n,z)
				implicit none
				real (kind=8)::n,z

					Globz=z
					Numfc=NumDeriv2(help,n,1D-4)
			end function Numfc

			real (kind=8) function Numdnech(n,z)
				implicit none
				real (kind=8)::n,z

					Globz=z
					Numdnech=NumDeriv1(help,n,1D-4)
			end function Numdnech

			real (kind=8) function help(n)
				real (kind=8)::n,rsn
					rsn=n2rs(n)
					help=n*echt(rsn,Globz)
			end function help

	end subroutine ConstInit


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!Deprecated!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Need to remove these

	real (kind=8) function CartIntegrate(limits,steps,fn,q,w)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),k(2),limits(4),w,dkx,dky
		real (kind=8), external:: fn
		! 2D Cartesian integrator over k
		!
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dkx=(limits(2)-limits(1))/real(steps, kind=8)
			dky=(limits(4)-limits(3))/real(steps, kind=8)
			CartIntegrate=0D0
			k=(/limits(1),limits(3)/)
			do i=0,steps
				do j=0,steps
					CartIntegrate=CartIntegrate+fn(q,w,k)
					k(2)=k(2)+dky
				enddo
				k=(/k(1)+dkx,limits(3)/)
			enddo
			CartIntegrate=CartIntegrate*dkx*dky/(4D0*pi**2)
	end function CartIntegrate

	real (kind=8) function PolIntegrate(fn,a,b,steps,q,w,G)
		implicit none
		integer (kind=4):: i,j,steps
		real (kind=8):: q(2),w,k,dk,phi,dphi,a,b,G
		real (kind=8), external:: fn
		! 2D polar integrator over k and phi.
		! S fn(q,w)*k*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn

			dk=(b-a)/real(steps)
			dphi=2D0*pi/real(steps)
			PolIntegrate=0D0
			k=a
			phi=0D0
			do i=0,steps
				do j=0,steps
					PolIntegrate=PolIntegrate+fn(k,phi,q,w)
					phi=phi+dphi
				enddo
				k=k+dk
				phi=0D0
			enddo
			PolIntegrate=PolIntegrate*dk*dphi/(4D0*pi**2)
	end function PolIntegrate

end module PhysicsGraphene
