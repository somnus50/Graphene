program TwoDEGSTLSGsc
use Constants
use Globals
use Physics2DEG
use STLS
use AuxiliaryFunctions
implicit none
real (kind=8)::rs,qs(2),mix,wp,wa,wb,w,dw,q,qp,ws(2),resa,resfn
real (kind=8), allocatable::wline(:)
integer (kind=4)::i,j,k,SCsteps,qstps,MPImaster,wsteps,Nproc,IntSteps,params(4)
character (len=32)::tempchar


	rs=2D0
	mix=0.5D0
	SCsteps=10
	qstps=100


	Nproc=1
	MPImaster=0
	IntSteps=1000
	!qsteps=200
	wsteps=200


	!!!! Initialize some situational constants
	call ConstInit(rs,0D0,GlobEf,Globeta,Globfx,Globfc) 
	qs=(/0D0,5D0*Globkf/)
	ws=(/0D0,5D0*GlobEf/)
	call STLSSCInit(SCsteps,qstps,qs)

	params=(/Nproc,IntSteps,qsteps,wsteps/)

	!!! Initialize some files for data storage
	open(unit=9, file='STLS0.dat')
	open(unit=19, file='STLS10.dat')




!	call exit(-1)

	SCCount=1

	do i=1,SCSteps
		call spline(qgrid,G1,qsteps,1D31,1D31,GSCspline)
		do j=1,qsteps
			write(*,*)"SC step:",i,j
			GSC(j)=GpForce(qgrid(j))!GpForce(qgrid(j))
		enddo

		if (i.eq.1) then
			do k=1,qsteps
				write(9,*) qgrid(k)/Globkf, GSC(k)
			enddo
			close(unit=9)

			tempchar='Eps0.dat'

			call FnToFile(29,tempchar,params,eps,qs,ws)

			open(unit=29,file='Plas0.F.dat')
			write(29,*) "#Plasmons STLS0"
			write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


			do k=1,qsteps
				call RootFinder(eps,ws(1),ws(2),qgrid(k),1D-15,resa,resfn)
				write(29,*) qgrid(k)/Globkf,resa/GlobEf,AnalyticPlasmon(qgrid(k))/GlobEf,PH(1D0,qgrid(k))/GlobEf,PH(-1D0,qgrid(k))/GlobEf,resfn
			enddo

			close(unit=29)

		endif
		GSC=(1D0-mix)*GSC+ mix*GSCold
		GSCold=GSC
	enddo

	do k=1,qsteps
		write(19,*) qgrid(k)/Globkf, GSC(k)
	enddo
	close(unit=19)


	tempchar='Eps10.dat'

	call FnToFile(29,tempchar,params,eps,qs,ws)

	open(unit=29,file='Plas10.F.dat')
	write(29,*) "#Plasmons STLS10"
	write(29,*) "#q/kf,wp/Ef,Lowq wp, Eps(q,w)"


	do i=1,qsteps
		call RootFinder(eps,ws(1),ws(2),qgrid(i),1D-6,resa,resfn)
		write(29,*) qgrid(i)/Globkf,resa/GlobEf,LowQPlasmon(qgrid(i))/GlobEf,resfn
	enddo

	close(unit=29)



end program TwoDEGSTLSGsc
