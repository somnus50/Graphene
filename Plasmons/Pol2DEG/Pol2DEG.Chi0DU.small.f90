program Pol2DEGChi0DD
use Constants
use Globals
use PhysicsPol2DEG
use fxc2DEG
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),wa&
		,GraphEf,TwoDEGn,rs,Graphkf0
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	GraphEf=gam*dsqrt(pi*1D12/cmSq2a0sq)!4.2339311421431972D-003
	!GraphEf=gam**2/2D0
	Graphkf0=gam!GraphEf/gam
	TwoDEGn=GraphEf/pi
	TwoDEGn=Graphkf0**2/2D0/pi
	rs=n2rs(TwoDEGn)
	write(*,*) rs
	call ConstInit(rs,-4D-2,GlobEf,Globeta)
	!GraphZeem=8D-2*GraphEf
	write(*,*) -4D-2*GraphEf/GLobEf, GlobEf, GraphEf,-4D-2*GraphEf/GLobEf*2D0*GlobEf, -4D-2*GraphEf*2D0!, 4.2339311421431972D-003
	MPImaster=0
	IntSteps=3000  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=25
	wsteps=100
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps))
	wa=1.5D-2*GlobEf
	dw=(GlobZeem-wa)/real(wsteps, kind=8)
	dq=3D-2*Globkf0/real(qsteps, kind=8)
	q=(/dq,0D0/)
	open(unit=9, file='Pol2DEG.Chi0DU.z4.small.dat')

		write(9,*) "### Chi0DU(q,w)"
		write(9,*) "###", Globrs, Globz
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	do i=1,qsteps
		w=wa
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			wline(j)=PolIntegratek(ReChi0DUInt2,0D0,GlobkfMAX*1.1D0,IntSteps,q,w)/4D0/pi**2
			w=w+dw
		enddo
		write(9,fmt2) wline
		q(1)=q(1)+dq
	enddo


	close(unit=9)
end program Pol2DEGChi0DD
