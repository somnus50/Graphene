program PolGrapheneChi0UDSmall
use Constants
use Globals
use PhysicsPolGraphene
use fxc2DEG
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
	n=1D12/cmSq2a0sq
	z=-4D-2
	call ConstInit(n,z)
	MPImaster=0
	IntSteps=3000  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=50
	wsteps=300
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps))
	w=0D0
	dw=(GlobZeem)/real(wsteps, kind=8)
	dq=2D0*Globz*Globkf0/real(qsteps, kind=8)
	q=(/dq,pi/2D0/)
	open(unit=9, file='PolGraphene.Chi0UD.z4.small.q2.dat')

		write(9,*) "### Chi0UDIntra(q,w)"
		write(9,*) "###", n, z, Globrs
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	write(*,*) "Ef   kf0"
	write(*,*) GlobEf,Globkf0
	do i=1,qsteps
		w=0D0
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			wline(j)=PolIntegratek(ReChi0UDIntraU,0D0,1.1D0*GlobkfMAX,IntSteps,q,w)
			w=w+dw
		enddo
		write(9,fmt2) wline
		q(1)=q(1)+dq
	enddo


	close(unit=9)
end program PolGrapheneChi0UDSmall
