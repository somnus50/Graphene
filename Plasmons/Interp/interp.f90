program main
use Interp
use SelfConsistent
implicit none
integer (kind=4)::i,j,nd,ni
real (kind=8)::dx,r0,grid(4),pt(2)
real (kind=8), allocatable::xd(:,:),fd(:),w(:),xi(:,:),fi(:)




  grid=(/0D0,1D0,0D0,1D0/)
  pt=(/7D-1,7D-1/)

  call BiLinInterp(grid,fn2,pt,dx)

  write(*,*) dx, fn2(pt(1),pt(2))



  return
end
