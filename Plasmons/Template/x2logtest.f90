program x2logtest
use AuxiliaryFunctions
implicit none
real(8)::x,dx,a,b
integer(4)::i,steps,ios


steps=100
dx=10D0/real(steps, kind=8)
x=dx


open(unit=9, file="x2logx.dat", iostat=ios, status="new", action="write")
!if ( ios /= 0 ) stop "Error opening file "

write(9,*) "#x,intrinsic,mine,diff"

do i=1,steps
  a=x**2*dlog(x)
  b=x2logx(x)
  write(*,*) i
  write(9,*) x,a,b,a-b
  x=x+dx
enddo





end program x2logtest
