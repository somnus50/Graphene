!##############################################################################
!                                    References
!##############################################################################
!
! [1] 
!
! [2] 
!
! [3] 
!
!###############################################################################
!                                    Constants
!###############################################################################

module constants
	implicit none
	real (kind=8), parameter :: pi=dacos(-1.0D0), alat=1D0, & 
				    kappa=1D0, hbar=1D0, e=1D0, m=1D0, &
				    eV2Eh=1D0/(27.211D0), Ang2a0=1D0/(0.529D0),&
				    cm2Ang=1D8, FillingFrac=2.5D-2, &
				    bgauss=1D-2

	real (kind=8), parameter :: t=0.5D0/(alat**2), & 
				    Glat=2D0*pi/alat, b=8D-1/alat!,n=1D0/alat**2

	real (kind=8), parameter :: &
	Gs(5,2)=transpose(reshape((/0D0,0D0,Glat,0D0,0D0,Glat,-Glat,0D0,0D0,-Glat/), (/2,5/)))


	!!!Global variables to help pass parameters in the 1D integrators
	real (kind=8)::Globkphi,Globq(2),Globqphi,Globw,Globkhat(2),&
				    Ef,eta,Globfx,Globfc


end module constants

!###############################################################################
!                          Numerical Lindhard Function
!###############################################################################

module NSquareHLindFunctions
use constants
use HelperFunctions
use dgauss
implicit none

contains

	real (kind=8) function ReNLind3(q,w)
		implicit none
		real (kind=8):: q(2),w
		!Calculates the Lindhard function using 
		!Gaussian Adaptive Quadrature.
		
		!Set some global variables for integrating
		Globq=q
		Globw=w

!		call GaussAdaptQuad(Lindphi,0D0,2D0*pi,1D-8,ReNLind3)

		contains

		!Helper functions to allow 2D integration

			real (kind=8) function Lindphi(phi)
				implicit none
				real (kind=8)::phi,rb
	
				Globkhat=(/dcos(phi),dsin(phi)/)
				rb=FermiVec(Globkhat,1D-8)
!				call GaussAdaptQuad(Lindr,0D0,rb,1D-8,Lindphi)		
	
			end function Lindphi
	
			real (kind=8) function Lindr(r)
				implicit none
				real (kind=8)::r,k(2)
	
					k=r*Globkhat
!					Lindr=ReNLindIntPol(Globq,Globw,k)			
	
			end function Lindr

	end function ReNLind3


	real (kind=8) function ReNLind2(q,w)
		implicit none
		real (kind=8):: q(2),w
		! Calculates Lindhard function using a prescribed method.
		! Uncomment the line that corresponds to the method you 
		! want to use. (Right now, NC1 is the best)
		! NCi: Newton-Cotes formula of order i
		!   http://mathworld.wolfram.com/Newton-CotesFormulas.html
		! AQ: Rectangular Adaptive Quadrature (very slow/doesn't work)
		

			ReNLind2=PolIntegrate(200,ReNLindInt,q,w)/4D0/pi**2  	!NC1
			!ReNLind2=PolIntegrate(50,ReNLindInt,q,w)/4D0/pi**2  	!NC4
			!ReNLind2=PolIntegrate(40,ReNLindInt,q,w)/4D0/pi**2  	!NC5
			!ReNLind2=PolIntegrate(20,ReNLindInt,q,w)/4D0/pi**2  	!NC10
			!ReNLind2=PolIntegrate(18,ReNLindInt,q,w)/4D0/pi**2  	!NC11
			!ReNLind2=PolIntegrate(15,ReNLindIntPol,q,w)/4D0/pi**2	!AQ

	end function ReNLind2

	real (kind=8) function ImNLind2(q,w)
		implicit none
		real (kind=8):: q(2),w
		! Calculates Lindhard function using a prescribed method.
		! Uncomment the line that corresponds to the method you 
		! want to use. (Right now, NC1 is the best)
		! NCi: Newton-Cotes formula of order i
		!   http://mathworld.wolfram.com/Newton-CotesFormulas.html
		! AQ: Rectangular Adaptive Quadrature (very slow/doesn't work)
		

			ImNLind2=PolIntegrate(200,ImNLindInt,q,w)/4D0/pi**2  	!NC1
			!ImNLind2=PolIntegrate(50,ImNLindInt,q,w)/4D0/pi**2  	!NC4
			!ImNLind2=PolIntegrate(40,ImNLindInt,q,w)/4D0/pi**2  	!NC5
			!ImNLind2=PolIntegrate(20,ImNLindInt,q,w)/4D0/pi**2  	!NC10
			!ImNLind2=PolIntegrate(18,ImNLindInt,q,w)/4D0/pi**2  	!NC11
			!ImNLind2=PolIntegrate(15,ImNLindIntPol,q,w)/4D0/pi**2	!AQ

	end function ImNLind2


	real (kind=8) function ReNLindIntPol(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! ReNLindInt*|k|
		! q   :: real, wavevector 
		! w   :: real, frequency
		! This is a workaround for integrators that can't 
		! internally multiply |k| for polar coordinates

			ReNLindIntPol=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=temp/(temp**2+eta**2)
				ReNLindIntPol=temp
				temp=w-En(k-q)+En(k)
				temp=temp/(temp**2+eta**2)
				ReNLindIntPol=ReNlindIntPol-temp
				ReNLindIntPol=ReNLindIntPol*f(k)*Norm(k)
			endif
	end function ReNLindIntPol

	real (kind=8) function PolIntegrate(steps,fn,q,w)
		implicit none
		integer (kind=4):: steps,phistep,kstep,NCorder,IERR
		real (kind=8):: q(2),w,k,dk,bk,phi,dphi,philine
		real (kind=8), external:: fn
		
		! NCorder must match what was chosen in ReNLind2
		NCorder=1

		bk=0D0
		PolIntegrate=0D0
		dphi=2D0*pi/real(NCorder*steps, kind=8)
		phi=0D0
		k=0D0
		do phistep=1,NCorder*steps+1
			philine=0D0
			bk=FermiVec((/dcos(phi),dsin(phi)/),1D-6)
			dk=bk/real(NCorder*steps, kind=8)
			k=dk


	!!!AdaptQuad (this one sucks)
	!		dk=(bk-1D-4)/5D0
	!		k=dk
	!		do kstep=1,5
	!			philine=philine+AdaptQuad(fn,k-dk,k,phi,q,w,5D-6,1)
	!			k=k+dk
	!		enddo
	!		PolIntegrate=PolIntegrate+philine
	!!!AdaptQuad

	!!!NewtonCotes
			do kstep=1,NCorder*steps+1
				philine=philine+dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  									!NC1
				!philine=philine+IntCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC4
				!philine=philine+BooleCoeff(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC5
				!philine=philine+IntCoeff10(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC10
				!philine=philine+IntCoeff11(kstep,NCorder*steps+1)*dk*k*fn(q,w,k*(/dcos(phi),dsin(phi)/))  	!NC11
				k=k+dk
			enddo
  
			PolIntegrate=PolIntegrate+philine  									!1
			!PolIntegrate=PolIntegrate+IntCoeff(phistep,NCorder*steps+1)*philine  	!4
			!PolIntegrate=PolIntegrate+BooleCoeff(phistep,NCorder*steps+1)*philine  !5
			!PolIntegrate=PolIntegrate+IntCoeff10(phistep,NCorder*steps+1)*philine  !10
			!PolIntegrate=PolIntegrate+IntCoeff11(phistep,NCorder*steps+1)*philine  	!11
	!!!NewtonCotes


			phi=phi+dphi
		enddo



		PolIntegrate=PolIntegrate*dphi

	end function PolIntegrate

	real (kind=8) function Norm(v)
		implicit none
		real (kind=8) ::v(2)
		! Magnitude of a vector

		Norm=(v(1))**2+(v(2))**2
		Norm=dsqrt(Norm)
	end function 

	real (kind=8) function En(q)
		implicit none
		real (kind=8) ::q(2)
		! Energy of a square hydrogen lattice


			En=2D0*t*(2D0-(dcos(q(1)*alat)+dcos(q(2)*alat)))
	end function En

	real (kind=8) function f(q)
		implicit none
		real (kind=8) ::q(2)
		! Fermi-Dirac Distribution for electrons
		! q :: real, wavevector 
	
			f=0D0
			!!!Zero Temperature
			if ((En(q)).lt.(Ef)) then
				f=1D0
			endif
			!!!Non-Zero Temperature
			!f=1D0/(1D0+dexp((En(q)-Ef)/kT))
	end function f

	real (kind=8) function fxc(q)
		implicit none
		integer (kind=4)::flag
		real (kind=8) ::q(2)
		! Exchange
		! q :: real, wavevector 
		! flag :: int, turns exchange on or off

		flag=1
		fxc=0D0
		if (flag.eq.1) then	
			fxc=Globfx+Globfc
		endif

	end function fxc

	real (kind=8) function FermiTest(phi,q)
		implicit none
		real (kind=8) ::q,phi
		
			FermiTest=-Ef+En(q*(/dcos(phi),dsin(phi)/))
	end function FermiTest

	real (kind=8) function FermiVec(q,tol)
		implicit none
		real (kind=8) ::q(2),phi,maxq,tol
		

			!Calculate the zone boundary (square)
			phi=datan(q(2)/q(1))
			maxq=pi/alat/dcos(modulo(phi+pi/4D0,pi/2D0)+7D0*pi/4D0)
			FermiVec=maxq
			if ((En()*En()).le.(0D0)) then
				FermiVec=RootBrentsMethod(0D0,maxq,tol,FermiTest,phi)
			endif
	end function FermiVec

	real (kind=8) function ReNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of 
		! q   :: real, wavevector 
		! w   :: real, frequency

			ReNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=temp/(temp**2+eta**2)
				ReNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=temp/(temp**2+eta**2)
				ReNLindInt=ReNlindInt-temp
				ReNLindInt=ReNLindInt*f(k)
			endif
	end function ReNLindInt


	real (kind=8) function ImNLindInt(q,w,k)
		implicit none
		real (kind=8) ::q(2),k(2),w,temp
		! Real part of 
		! q   :: real, wavevector 
		! w   :: real, frequency

			ImNLindInt=0D0
			if ((f(k)).gt.(0D0)) then
				temp=w-En(k)+En(k+q)
				temp=1D0/(temp**2+eta**2)
				ImNLindInt=temp
				temp=w-En(k-q)+En(k)
				temp=1D0/(temp**2+eta**2)
				ImNLindInt=ImNlindInt-temp
				ImNLindInt=-ImNLindInt*f(k)*eta
			endif
	end function ImNLindInt

	real (kind=8) function NVc(q)
		implicit none
		real (kind=8) ::q(2)
		! Fourier Transform of screened Coulomb Potential
		! q   :: real wavevector 
	
		NVc=2D0*pi*e**2/(kappa*Norm(q))
	end function NVc

	real (kind=8) function KsiG(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		! 
		! 
	
		KsiG=(1D0+(1D0/(b**2))*(Norm(q+G))**2)**3
		KsiG=1D0/KsiG
	end function KsiG

	real (kind=8) function KsiGb(bparam,q,G)
		implicit none
		real (kind=8) ::q(2),G(2),bparam
		! 
		! 
	
		KsiGb=(1D0+(1D0/(bparam**2))*(Norm(q+G))**2)**3
		KsiGb=1D0/KsiGb
	end function KsiGb

	real (kind=8) function KsiGauss(q,G)
		implicit none
		real (kind=8) ::q(2),G(2)
		! 
		! 
	
		KsiGauss=dexp(-bgauss**2*(Norm(q+G))**2)
	end function KsiGauss

	real (kind=8) function ReNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function


		ReNEps=1D0+(2D0*(NVc(q+G)+fxc(q))*ReNLind2(q,w)*KsiG(q,G))

	end function ReNEps

	real (kind=8) function ImNEps(q,w,G)
		implicit none
		real (kind=8) ::q(2),G(2),w
		! Real part of Epsilon, dielectric function
		! q   :: real, wavevector magnitude
		! w   :: real, frequency

		ImNEps=2D0*NVc(q+G)*ImNLind2(q,w)*KsiG(q,G)

	end function ImNEps

	subroutine ReNEpsLine(q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2)
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)
		! Calculates ReNEps from a tabulated Lindhard function

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*KsiG(qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLine

	subroutine ReNEpsLineb(bp,q,phi,dw,wsteps,LindLine,EpsLine)
		implicit none
		integer (kind=4):: wsteps,Gstep,wstep
		real (kind=8) ::q,phi,dw,w,G(2),qv(2),bp
		real (kind=8) ::LindLine(wsteps),EpsLine(5,wsteps)
		! Calculates ReNEps from a tabulated Lindhard function
		! and also allows for the b parameter of Ksi to be changed

		qv=q*(/dcos(phi),dsin(phi)/)
		do wstep=1,wsteps
			w=dw*real(wsteps)
			do Gstep=1,5
				G=Gs(Gstep,:)
				EpsLine(Gstep,wstep)=1D0+(2D0*(NVc(qv+G)+fxc(qv))*KsiGb(bp,qv,G)*LindLine(wstep))
			enddo
		enddo

	end subroutine ReNEpsLineb

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!Newton-Cotes Coefficients
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	real (kind=8) function BooleCoeff(x,n)
		implicit none
		integer (kind=4):: x,n,i
		real    (kind=8), parameter:: temp(4)=&
			(/32D0,12D0,32D0,14D0/)


			if (((x).eq.(1)).or.((x).eq.(n))) then
				BooleCoeff=7D0
			else
				i=modulo((x-2),4)+1
				BooleCoeff=temp(i)
			endif
			BooleCoeff=BooleCoeff*(2D0/45D0)
	end function BooleCoeff

	real (kind=8) function IntCoeff(x,n)
		implicit none
		integer (kind=4):: x,n,i
		real    (kind=8), parameter:: temp(3)=&
			(/3D0,3D0,2D0/)


			if (((x).eq.(1)).or.((x).eq.(n))) then
				IntCoeff=1D0
			else
				i=modulo((x-2),3)+1
				IntCoeff=temp(i)
			endif
			IntCoeff=IntCoeff*(3D0/8D0)
	end function IntCoeff


	real (kind=8) function IntCoeff11(x,n)
		implicit none
		integer (kind=4):: x,n,i
		real    (kind=8), parameter:: temp(10)=&
			(/106300D0,-48525D0,272400D0,-260550D0,427368D0,-260550D0,272400D0,-48525D0,106300D0,32134D0/)


			if (((x).eq.(1)).or.((x).eq.(n))) then
				IntCoeff11=16067D0
			else
				i=modulo((x-2),10)+1
				IntCoeff11=temp(i)
			endif
			IntCoeff11=IntCoeff11*(5D0/299376D0)
	end function IntCoeff11


	real (kind=8) function IntCoeff10(x,n)
		implicit none
		integer (kind=4):: x,n,i
		real    (kind=8), parameter:: temp(9)=&
			(/15741D0,1080D0,19344D0,5778D0,5778D0,19344D0,1080D0,15741D0,5714D0/)


			if (((x).eq.(1)).or.((x).eq.(n))) then
				IntCoeff10=2857D0
			else
				i=modulo((x-2),9)+1
				IntCoeff10=temp(i)
			endif
			IntCoeff10=IntCoeff10*(9D0/89600D0)
	end function IntCoeff10
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end module NSquareHLindFunctions





























