program Plasmons
	use constants	
	use GLindFunctions
	use NGLindFunctions
	use HelperFunctions
	implicit none
	real (kind=8)::a,b,delta,x,w0,ao,bo,v,c,d
	real (kind=8)::array(4,3),subplots(5)
	real (kind=8), allocatable::Plots(:,:)
	integer (kind=4)::steps,i,stat,j
	!complex (kind=8)::

	ao=0.5D0
	bo=2D0
	c=0D0
	d=3D0
!	x=1.2D0
	delta=10**(-10)
	steps=100
	allocate(Plots(steps,5))
!	call FindBracket(ReEps,x,ao,bo,a,b,steps,stat)
	do i=1,steps
		x=c+i*(d-c)/steps
		x=x*kf
!		if (x.gt.(1D0)) then
!			ao=x
!			bo=x+1D0
!		endif
		call FindBracketList(ReEps,x,c,d,array,100)
		!w0=RootBrentsMethod(a,b,delta,ReEps,x)
		subplots=(/ x/kf,0D0,0D0,0D0,0D0/)
		do j=1,4
			if ((array(j,3)).gt.(0D0)) then
				subplots(j+1)=(array(j,1)+array(j,2))/(2D0*Ef)
			endif
		enddo
		Plots(i,:)=subplots!,ReEps(x,(a+b)/2)/)
		write(*,*) Plots(i,:)
	enddo

	!w0=RootBrentsMethod(a,b,delta,ReEps,x)
!	write(*,*) w0
!	write(*,*) "   a   b   Stat"
!	write(*,*) a,b,stat
!	write(*,*) "   ReEps(a,v)   ReEps(b,v)"
!	write(*,*) ReEps(x,a),ReEps(x,b)

end program Plasmons


