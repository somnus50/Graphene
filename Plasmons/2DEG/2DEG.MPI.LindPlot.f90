program TWODEGMPILindPlot
use mpi
use Constants
use Globals
use Physics2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,MPIa,MPIb,LindSum
real (kind=8), allocatable::MPILind(:),Lind(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps,i,j
!complex (kind=8)::





	!!!! Initialize some situational constants
	call ConstInit(5D-1,0D0,GlobEf,Globeta,Globfx,Globfc) 
	MPImaster=0
	IntSteps=1000
	q=(/0.5D0,0D0/)*Globkf
	a=0D0
	b2=FermiVec(q,1D-6)+1D-1
	w=0D0
	dw=(3D0*GlobEf)/100D0
	!!! Initialize some files for data storage
	open(unit=9, file='LindPlot.dat')

	call MPI_Init(MPIerr)
	call MPI_Comm_size(MPI_COMM_WORLD,NProc,MPIerr)
	call MPI_Comm_rank(MPI_COMM_WORLD,MPIid,MPIerr)



	if (MPIid.eq.MPImaster) then
		MPISteps=IntSteps/NProc
		wtime=MPI_Wtime()
		write(9,*) "# w/Ef, ReNLind   ##q=kf*(1,0)"

	endif

	call MPI_Bcast(MPISteps,1,MPI_INTEGER,MPIMaster,MPI_COMM_WORLD,MPIerr)



	temp=real(MPIid, kind=8)/real(NProc, kind=8)
	MPIa=a*(1D0-temp)+temp*b2
	!MPIa=a !DEBUG
	temp=real((MPIid+1), kind=8)/real(NProc, kind=8)
	MPIb=a*(1D0-temp)+temp*b2
	!MPIb=b !DEBUG
	write(*,*) "Process ",MPIid,"claims",MPIa," to ",MPIb, MPISteps,"steps"

	!!!!!!!!!!!!!!!!!!!
	!!!Do some Stuff!!!
	!!!!!!!!!!!!!!!!!!!
	allocate(MPILind(NProc))
	allocate(Lind(NProc))
	MPILind=0D0
	Lind=0D0
	w=0D0
	do i=1,100
		call MPI_Bcast(w,1,MPI_DOUBLE_PRECISION,MPIMaster,MPI_COMM_WORLD,MPIerr)		
		Lind=0D0
		MPILind(MPIid+1)=PolIntegrate3(MPIa,MPIb,MPIsteps,MPISteps*NProc,ReNLindInt,q,w)
		!MPILind=PolIntegrate2(MPIa,MPIb,MPISteps,MPITest2,q,w)
		!MPILind=ReNLindInt(q,w,MPIa*(/1D0,0D0/))
		!MPILind=0D0
		!MPILind=MPITest(MPIid,i)
		call MPI_Reduce(MPILind,Lind,NProc,MPI_DOUBLE_PRECISION,MPI_SUM,MPImaster,MPI_COMM_WORLD,MPIerr)
		if (MPIid.eq.MPImaster) then
			LindSum=0D0
			do j=1,NProc
				LindSum=LindSum+Lind(j)
			enddo
			write(9,*) w/GlobEf,LindSum,Lind
			w=w+dw
		endif
	enddo

	if (MPIid.eq.MPImaster) then
		wtime=MPI_Wtime()-wtime
		write(*,*) "Time:",wtime
	endif

	call MPI_Finalize(MPIerr)

	!!! Close files
	close(unit=9)

end program TWODEGMPILindPlot
