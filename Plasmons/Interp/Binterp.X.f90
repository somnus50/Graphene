program TwoDEGSTLSGsc
use Constants
use Globals
use PhysicsPol2DEG
use STLS
use AuxiliaryFunctions
use SelfConsistent
use Interp
implicit none
real (kind=8)::rs,qs(2),mix,wp,wa,wb,w,dw,q,qp,ws(2),resa,resfn,resa2,resfn2,temp1,temp2,&
				res1(2),res2(2),z,dq,r,r2,r3,p1,p2
real (kind=8), allocatable::wline(:),Ggrid(:,:),res(:),plas(:,:),wgrid(:),fng(:,:)
integer (kind=4)::i,j,k,SCsteps,qstps,MPImaster,wsteps,Nproc,IntSteps,params(4),stat,mixsteps
character (len=32)::tempchar


	rs=2D0
	mix=0.95D0
	SCsteps=500
	qstps=5000
	z=-1D-1!5D-1

	Nproc=1
	MPImaster=0
	IntSteps=10
	!qsteps=200
	wsteps=200
	qsteps=qstps


	!!!! Initialize some situational constants
	call ConstInit(rs,z,GlobEf,Globeta)
	qs=(/0D0,100D0*Globkf0/)
	ws=(/0D0,10D0*GlobEf/)
	call STLSSCInit(SCsteps,qstps,qs)

	params=(/Nproc,IntSteps,qsteps,wsteps/)

	!!! Initialize some files for data storage




	call SCInit(qstps,qs,fn0) !GPGGDU

	allocate(res(SCsteps),plas(xsteps,3),wline(qsteps),SDUspline(qsteps),SDUgrid(qsteps),wgrid(qsteps),fng(qsteps,qsteps))

	call LogGrid(0.01D0,qstps,qgrid)
    qgrid=qgrid*qs(2)

	call LogGrid(0.01D0,qstps,wgrid)
    wgrid=wgrid*ws(2)

    do i=1,qstps
        do j=1,qstps
            fng(i,j)=dimag(Chi0ssp(-1,1,qgrid(i),wgrid(j)))
        enddo
    enddo




		q=qgrid(1)
		dq=2D0*Globkf0/real(IntSteps-1, kind=8)
		w=wgrid(1)
		dw=2D0*GlobEf/real(IntSteps-1, kind=8)
		p1=0D0
		p2=0D0
		do i=1,IntSteps
			w=wgrid(1)
      do j=1,IntSteps
				call BiLinterp(q,w,qsteps,qgrid,wgrid,fng,r)
				call BiCubInterp(q,w,qsteps,qgrid,wgrid,fng,r2)
				r3=dimag(Chi0ssp(1,1,q,w))
        write(*,*) q,w,r,r2,r3,ImChi0DU(q,w)
				p1=p1+(r3-r)**2
				p2=p2+(r3-r2)**2
				w=w+dw
			enddo
			q=q+dq
    enddo

write(*,*) p1,p2


end program TwoDEGSTLSGsc
