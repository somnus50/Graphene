program PolGraphenePlasmons
use readlib
use Constants
use Globals
use PhysicsPolGraphene
use AuxiliaryFunctions
use Response
use GV
use fxc2DEG
use fxcPGG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q,dw,PlasmonArray(4,20,3),wstart,qhat(2),w0,Chi0(4),Chi(4,4),n,z,w,wa&
				,fxUDUD,abserr,fx0,stiff
real (kind=8), allocatable::LindLine(:),EpsLine(:),qwgrid(:,:)&
						   ,Chigrid(:,:),EV(:,:),EVwline(:,:)
integer (kind=4)::NProc,i,j,wsteps,qsteps,IntSteps,labelnum,temp,k,neval,ier
integer (kind=4), allocatable::labels(:),info(:,:)
character (len=32)::fmt1,fmt2,star,fmt3
character (len=32),allocatable::empty(:)


	stiff=5155.95D0 !From the analytic expansion


	!!!! Initialize some situational constants
	Globgs=2D0
	Globgv=2D0
	n=1D12/cmSq2a0sq
	z=-9D-2
	call ConstInit(n,z)
	call KernelConstr(Globrs,z,GlobKernVec)
	GlobKernVec(4)=(1D0-0.95D0)*GlobZeem/n/z
	!write(*,*) GlobKernVec
	fmt1='(3ES24.15)'
	!!! Initialize some files for data storage
	labelnum=4
	star='(A)'
	fmt3='(4I10)'
	allocate(labels(labelnum), empty(labelnum), qwgrid(labelnum,3), info(labelnum,4),EV(2,labelnum)) 

	!!! Create labels for files
	temp=19
	do i=1,labelnum
		labels(i)=temp
		temp=temp+10
	enddo

	!!! Open files
	open(unit=labels(1), file='PolGraphene.Chi0UU.z4.xsmall.q1.FAKE.dat', status='old', action='read')
	open(unit=labels(2), file='PolGraphene.Chi0DD.z4.xsmall.q1.FAKE.dat', status='old', action='read')
	open(unit=labels(3), file='PolGraphene.Chi0UD.z9.xsmall.q1.dat', status='old', action='read')
	open(unit=labels(4), file='PolGraphene.Chi0DU.z9.xsmall.q1.dat', status='old', action='read')
	open(unit=9,       file='PolGraphene.Plasmons.z9.small.q1.dat')
	open(unit=22,      file='PolGraphene.Plasmons.z9.small.q1.UNSCALED.dat')
	!open(unit=22, file='someplots.dat')
	write(9,*) "# q/kf,wp/ef,low q limit"
	write(22,*) "# q/kf,wp/ef,low q limit"

	!!! Throw out comment lines
	call multiread(labels,star,empty)
	call multiread(labels,star,empty)
	call multiread(labels,star,empty)

	!!! Read the metadata at the top of the file
	call multiread(labels,fmt3,info)
	!!! Sanity check to make sure all the data files are compatible
	do i=2,labelnum
		do j=1,4
			if ((info(1,j)).ne.(info(i,j))) then
				write(*,*) "Data file parameters don't match. Please investigate."
				!call exit(-1)
			endif
		enddo
	enddo
	NProc=info(3,1)
	IntSteps=info(3,2)
	qsteps=info(3,3)
	wsteps=info(3,4)

	!!! Throw out comment lines
	call multiread(labels,star,empty) 
	call multiread(labels,star,empty) 

	allocate(Chigrid(labelnum,wsteps),EVwline(labelnum,wsteps))
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	!write(*,*) fmt2
	allocate(LindLine(wsteps),EpsLine(wsteps))
	w0=4D0*GlobEf/2D0!/kappa*e**2
	w0=dsqrt(w0)
	qhat=(/0D0,-1D0/)

	write(*,*) "kf",Globkf0
	write(*,*) "Ef",GlobEf
	write(*,*) GlobKernVec(4), GlobKernVec(4)*n*z/2D0
	!write(22,*) "q,w,fxUD,ChiUD,ChiDU,fx*Chi"


	call DPPGGud(0D0,GlobkfU,GlobkfD,fx0,abserr,neval,ier)

	do i=1,qsteps
		!write(*,*) "qstep:",i
		q=0D0
		call multiread(labels,fmt1,qwgrid)
		!Do something to readvals (q, wstart, dw)
		q=qwgrid(3,1)*Globkf0
		!write(*,*) q
		wa=qwgrid(3,2)
		w=wa
		dw=qwgrid(3,3)
		!write(*,*) "q",q
		call multiread(labels,fmt2,Chigrid) !read(19,fmt2) LindLine
		do j=1,wsteps
			Chi0=Chigrid(:,j)/2D0
			call RespCnstr(q,w,Chi0,Chi)
			!call RespCnstrPGG(q,w,Chi0,Chi)
			!call EigVals(Chi,labelnum,EV) !LAPACK isn't working...
			EV(1,:)=(/Chi(2,2),Chi(3,3),0D0,0D0/)
			!if (j.le.3) then
			!	write(*,*) "q: ",q," Chi: ",EV(1,:)
			!endif
			EVwline(:,j)=EV(1,:)!*10D0
			!call DPPGGud(q,GlobkfU,GlobkfD,fxUDUD,abserr,neval,ier)
			!write(22,*) q,w/GlobZeem,fxUDUD,Chi0(3),Chi0(4),EV(1,:),1D0-Chi0(3)*GlobKernVec(4)
			w=w+dw
		enddo
		do j=1,labelnum
			call FindBracketList2(EVwline(j,:),wsteps,q,wa,dw,PlasmonArray(j,:,:))
		enddo
		do k=1,labelnum
			do j=1,20
				if ((PlasmonArray(k,j,2)).gt.(0D0)) then
					write(*,*) PlasmonArray(k,j,1)/Globkf0,&
					(PlasmonArray(k,j,2)+PlasmonArray(k,j,3))/(2D0*GlobZeem)!,&       
					!w0*dsqrt(q)/GlobEf
		
					write(9,'(5ES24.15)')  PlasmonArray(k,j,1)/Globkf0,&
						((PlasmonArray(k,j,2)+PlasmonArray(k,j,3))/(2D0)-GlobZeem)/GlobEf,&!  
							((GlobZeem-gam*PlasmonArray(k,j,1))-GlobZeem)/GlobEf,&
							((0.95D0*GlobZeem-stiff/2D0*PlasmonArray(k,j,1)**2)-GlobZeem)/GlobEf,&
							((stiff/2D0*PlasmonArray(k,j,1)**2))/GlobEf!,&
							!(0.95D0*GlobZeem-10832.230155318133D0/2D0*PlasmonArray(k,j,1)**2)/GlobZeem


					write(22,'(5ES24.15)') PlasmonArray(k,j,1),&
						(PlasmonArray(k,j,2)+PlasmonArray(k,j,3))/(2D0)-GlobZeem,&!  
							(GlobZeem-gam*PlasmonArray(k,j,1))-GlobZeem,&
							(0.95D0*GlobZeem-stiff/2D0*PlasmonArray(k,j,1)**2)-GlobZeem,&
							(stiff/2D0*PlasmonArray(k,j,1)**2)!,&
							!(0.95D0*GlobZeem-10832.230155318133D0/2D0*PlasmonArray(k,j,1)**2)/GlobZeem

				endif
			enddo
		enddo
	enddo

	!!! Close files
	do i=1,labelnum
		close(unit=labels(i))
	enddo
	close(unit=9)
	close(unit=22)

end program PolGraphenePlasmons

