program GVPlasmons
use constants
use PhysicsPol2DEG
use GV
implicit none
real (kind=8)::q,w,dq,dw,temp1,temp2,rs,z
integer (kind=4)::i,j,qsteps,wsteps


rs=2D0
z=0D0
call ConstInit(rs,z,GlobEf,Globeta)

qsteps=100
wsteps=300
dq=1.5D0*Globkf0/real(qsteps, kind=8)
dw=5D0*GlobEf/real(wsteps, kind=8)
q=dq
w=0D0
temp1=0D0
temp2=0D0
write(*,*) "q/kf,  wp/Ef"
do i=1,qsteps
	w=0D0
	temp1=0D0
	temp2=0D0
	do j=1,wsteps
		temp1=temp2
		temp2=1D0-2D0*pi/q*(ReChi0s(q,w,1D0)+ReChi0s(q,w,-1D0))
		if ((temp1*temp2).lt.0D0) then
			write(*,*) q/Globkf0,(w-dw/2D0)/GlobEf
		endif
		w=w+dw
	enddo
	q=q+dq
enddo


























end program
