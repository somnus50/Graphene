module Constants
	implicit none
	real (kind=8), parameter :: pi=dacos(-1.0D0), alat=1D0, & 
				    kappa=2.5D0, hbar=1D0, e=1D0, m=1D0, &
				    eV2Eh=1D0/(27.211D0), Ang2a0=1D0/(0.529D0),&
				    cm2Ang=1D8, cmSq2a0sq=cm2Ang**2*Ang2a0**2 , &
				    bgauss=1D-2, aGraphene=2.46D0*Ang2a0, &
					BZkmax=4D0*pi/3D0/aGraphene,e2p=0D0, &
					tTB=-3.033D0,sTB=1.29D-1, &
					gam=6.5D0*eV2Eh*Ang2a0, &
					tTBEh=tTB*eV2Eh,HalfaGraphene=aGraphene/2D0, &
					bmHapT=5.7883818012D-5*eV2Eh

	real (kind=8), parameter :: t=0.5D0/(alat**2), & 
				    Glat=2D0*pi/alat, b=1D0/alat,Gs=0D0

	complex (kind=8), parameter :: one=(1D0,0D0), ione=(0D0,1D0)


! These should be physical constants

end module Constants
