#!/bin/bash

usage() {
cat << EOF
Usage: MPIrun.sh [OPTION] MPIexecutable

   -n                         Specify number of processes. Default is 2.
   -h                         Display this help.
EOF
exit 1
}

cwd=$(pwd)
nproc="2"

while getopts :n: opts
do	case "$opts" in
	n)	nproc=$OPTARG;;
	*)	usage >&2;;
	esac
done
shift "$((OPTIND-1))" 



if [[ ! -r $1 ]]
then 
	echo "File: $1 is not accessible..."
	echo ""
	usage >&2
else
    cmd="mpiexec -n $nproc $cwd/$1 2>&1 | tee -a $1.log"
    echo $cmd | tee $1.log
	$cmd
fi
