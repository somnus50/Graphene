program qScan
	use constants	
	use GLindFunctions
	use NGLindFunctions
	use HelperFunctions
	implicit none
	real (kind=8)::x,v,q,w,lindy,d
	real (kind=8)::array(101,101)
	real (kind=8), allocatable::Plots(:,:)
	integer (kind=4)::steps,i,stat,j
	!complex (kind=8)::

	x=0D0
	v=0D0
	d=3D-2

	array(1,1)=0D0
	do i=2,101
		x=d*real(i)
		array(i,1)=x
		array(1,i)=x
	enddo

	do i=1,100
		x=d*real(i)
		do j=1,100
			v=d*real(j)
			q=x*kf
			w=v*Ef
			lindy=ReNEps(q,w)
			array(i+1,j+1)=lindy
			write(*,*) i,j
		enddo
	enddo

	open(unit=10,file='LindScan.dat')
	do i=1,101
		write(10,*) array(i,:)
	enddo	
	close(unit=10)

end program qScan
