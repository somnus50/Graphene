program Pol2DEGChi0DULog
use Constants
use Globals
use PhysicsPol2DEG
!use fxc2DEG
use AuxiliaryFunctions
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,phia,phib&
		,dq,Lind,MPILind,qarray(6),dphi,qtemp(2),n,rs,z,wa,wb
real (kind=8), allocatable::wline(:),qgrid(:),wgrid(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps,qindex
character (len=32)::fmt1,fmt2

	!!!! Initialize some situational constants
  call ConstInit(2D0,-1D-1,GlobEf,Globeta)
	write(*,*) Globkf0,GLobkfU,GlobkfD

	MPImaster=0
	IntSteps=300  !Multiplied by 10 because of Newton-Cotes 10pt integration
	qsteps=1000
	wsteps=1000
	NProc=1
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
	fmt1='(3ES24.15)'
	allocate(wline(wsteps),qgrid(qsteps),wgrid(wsteps))
	!wa=0.9D0*GlobZeem
	!wb=1.01D0*GlobZeem
	!dw=(wb-wa)/real(wsteps, kind=8)
	!dq=2D0*(GlobZeem-wa)/gam/real(qsteps, kind=8)
	call LogGrid(LGdrSuggest(7D0/50D0,qsteps),qsteps,qgrid)
	qgrid=qgrid*Globkf0*50D0
	call LogGrid(LGdrSuggest(7D0/50D0,wsteps),wsteps,wgrid)
	wgrid=wgrid*GlobEf*50D0
	q=0D0!(/dq,0D0/)
	open(unit=9, file='Pol2DEG.ReChi0DU.z1.log.dat')
	open(unit=10, file='Pol2DEG.ReChi0DU.z1.log.qgrid.dat')
	open(unit=11, file='Pol2DEG.ReChi0DU.z1.log.wgrid.dat')

	do i=1,qsteps
		write(10,*) qgrid(i)/Globkf0
	enddo

	do i=1,wsteps
		write(11,*) wgrid(i)/GlobEf
	enddo

	close(unit=10)
	close(unit=11)

		write(9,*) "### ReChi0DU(q,w) LogGrid"
		write(9,*) "###", n, z, Globrs
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	write(*,*) "Ef   kf0"
	write(*,*) GlobEf,Globkf0
	q=0D0
	q(1)=qgrid(1)
	do i=1,qsteps
		w=wgrid(1)
		wline=0D0
		write(9,fmt1) q(1)/Globkf0, w, dw
		write(*,*) "q",i,"of",qsteps
		do j=1,wsteps
			!wline(j)=PolIntegratek(ReChi0DUIntraU,max(q(1)-GlobkfMAX,0D0),q(1)+Globkf0,IntSteps,q,w)*temp
			wline(j)=ReChi0sspNum((/qgrid(i),0D0/),w,-1D0,1D0,50)
			w=wgrid(j)
		enddo
		write(9,fmt2) wline
		q(1)=qgrid(i)
	enddo


	close(unit=9)
end program Pol2DEGChi0DULog
