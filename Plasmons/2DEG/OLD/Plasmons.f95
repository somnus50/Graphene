program Plasmons
	use funcs
	use constants
	implicit none
	real (kind=8)::a,b,delta,x,w0,ao,bo,v,c,d
	real (kind=8), allocatable::Plots(:,:)
	integer (kind=4)::steps,i,stat
	!complex (kind=8)::

	ao=0.5D0
	bo=2D0
	c=0D0
	d=5D0
!	x=1.2D0
	delta=10**(-10)
	steps=1000
	allocate(Plots(steps,2))
!	call FindBracket(ReEps,x,ao,bo,a,b,steps,stat)
	do i=1,steps
		x=c+i*(d-c)/steps
		ao=x-0.5D0
		bo=x+0.5D0
		call FindBracket(ReEpsLind,x,ao,bo,a,b,10000,stat)
		!w0=RootBrentsMethod(a,b,delta,ReEps,x)
		Plots(i,:)=(/x,(a+b)/2/)!,ReEps(x,(a+b)/2)/)
		write(*,*) Plots(i,:)
	enddo

	!w0=RootBrentsMethod(a,b,delta,ReEps,x)
!	write(*,*) w0
!	write(*,*) "   a   b   Stat"
!	write(*,*) a,b,stat
!	write(*,*) "   ReEps(a,v)   ReEps(b,v)"
!	write(*,*) ReEps(x,a),ReEps(x,b)

end program Plasmons


