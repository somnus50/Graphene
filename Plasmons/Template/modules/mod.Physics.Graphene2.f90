!##############################################################################
!                                    References
!##############################################################################
!
! [1] E. Hwang and S. Das Sarma, Physical Review B 75, (2007).
!     DOI: 10.1103/PhysRevB.75.205418
!
! [2]
!
! [3]
!
module PhysicsGraphene2
use Constants
use Globals
implicit none

contains

	subroutine ConstInit(n,z)
		use Globals
		use fxc2DEG
		implicit none
		!integer (kind=4) ::
		real    (kind=8), intent(in) ::n,z
		!real    (kind=8), intent(out)::Ef,eta
		real    (kind=8)             ::rs
		!real    (kind=8)::


			! Easy Stuff
			!Ef=8D0*t*frac !Specific to how I calculated the Lindhard Matrix
			!n=Ef/pi
			rs=1D0/kappa/gam
			Globkf0=dsqrt(4D0*pi*n/Globgs/Globgv) !dsqrt(4D0*pi*n/(gs*gv))
			Globkc=dsqrt(4D0*pi/Globgs/Globgv*2D0/dsqrt(3D0)/aGraphene**2)
			GlobEf=gam*Globkf0
			Globn=n
			Globeta=1D-10*GlobEf
			GlobkfU=Globkf0*dsqrt((1D0+z))
			GlobkfD=Globkf0*dsqrt((1D0-z))
			GlobvfU=hbar*GlobkfU/m
			GlobvfD=hbar*GlobkfD/m
			GlobkfMAX=max(GlobkfU,GlobkfD)
			Globrs=rs!n2rs(n)!rs
			GLobZeem=-2D0*GlobEf*z
			HalfGlobZeem=GlobZeem/2D0
			Globz=z
			Globzt=dsqrt(1D0-z)+dsqrt(1D0+z)
			Globzt=Globzt/dsqrt(2D0)
			Globkf=Globkf0


	end subroutine ConstInit

	real(8) function En(b,k)
		implicit none
		real (8) ::k(2),b,temp,temp2
		! Dirac energy band of Graphene
		! b :: real, band index, (+/-)1 => (upper/lower) band
		! k :: real, polar wavevector

			En=b*gam*k(1)
	end function En

	real(8) function f(b,k)
		implicit none
		real (kind=8) ::k(2),b
		! Fermi-Dirac Distribution for electrons
		! b :: real, band index
		! q :: real, polar wavevector

			f=0D0
			!!!Zero Temperature
			if ((En(b,k)).le.(GlobEf)) then
				f=1D0
			endif
	end function f

	real(8) function ImLindInt(q,w,k,a,b)
		real(8)::q,w,k(2),a,b,kp

		ImLindInt=0D0
		kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*dcos(k(2)))
		if ( (f(1D0,k).gt.0D0).and.(f(1D0,(/kp,0D0/)).lt.1D0) ) then ! k occupied, kp unoccupied
			ImLindInt=-a*Globeta*0.5D0*(1D0+b*(k(1)+a*q*dcos(k(2)))/(kp))
			ImLindInt=ImLindInt/((w+a*gam*(b*k(1)-kp))**2+Globeta**2)
		endif
	end function ImLindInt

	real(8) function ImLindInt2(q,w,k,a,b)
		real(8)::q,w,k(2),a,b,kp
		logical:: kocc,kpunocc

		ImLindInt2=0D0
		kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*dcos(k(2)))

		if (a.eq.1D0) then
			kocc=(f(1D0,k).gt.0D0)
		else
			kocc=.true.
		endif

		if (b.eq.1D0) then
			kpunocc=(f(1D0,(/kp,0D0/)).lt.1D0)
		else
			kpunocc=.false.
		endif

		if ( kocc.and.kpunocc ) then ! k occupied, kp unoccupied
			ImLindInt2=-a*Globeta*0.5D0*(1D0+b*(k(1)+a*q*dcos(k(2)))/(kp))
			ImLindInt2=ImLindInt2/((w+a*gam*(b*k(1)-kp))**2+Globeta**2)
		endif
	end function ImLindInt2

	real(8) function ReLindInt(q,w,k,a,b)
		real(8)::q,w,k(2),a,b,kp

		ReLindInt=0D0
		kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*dcos(k(2)))
		if ( (f(1D0,k).gt.0D0).and.(f(1D0,(/kp,0D0/)).lt.1D0) ) then ! k occupied, kp unoccupied
			ReLindInt=(w+a*gam*(b*k(1)-kp))
			ReLindInt=a*ReLindInt*0.5D0*(1D0+b*(k(1)+a*q*dcos(k(2)))/(kp))&
								/(ReLindInt**2+GlobEta**2)
		endif
	end function ReLindInt

	real(8) function ImChiIntk(q,w,k,a,b)
		real(8)::q,w,k,a,b,t0,ta,wt

		wt=w/gam
		ImChiIntk=0D0
		t0=dacos(((wt+b*k)**2-k**2-q**2)/(2D0*k*q*a))
		ta=dacos((Globkf**2-k**2-q**2)/(2D0*k*q*a))
		if (((a*wt+b*k).ge.0D0).and.(0D0.le.t0).and.(t0.le.ta)) then
			ImChiIntk=-a*b/8D0/pi/gam*dsqrt(((a*wt+2D0*b*k)**2-q**2)/(dabs(q**2-wt**2)))
		end if
	end function ImChiIntk

	real(8) function ImNLind(q,w,a,b)
		real(8)::q,w,a,b,k1,k2
		integer(4)::steps
		real(8), parameter::infourpiSQ=1D0/(4D0*pi**2)

		steps=1000


		k1=max(0D0,Globkf-q)
		!k1=0D0
		!if (a.eq.1D0) then
		!	if (b.eq.1D0) then
		!		k2=Globkf
		!	else
		!		k2=Globkf+q
		!	end if
		!else
		!	k1=Globkf
		!	k2=Globkf+q
		!end if
		k2=Globkf+q
		ImNLind=PolIntegratephi11(anc,k1,k2,0D0,0D0,phirange0,steps,q,w)*infourpiSQ

	contains
		real(8) function anc(q,w,k)
			real(8)::k(2),q,w

			anc=ImLindInt2(q,w,k,a,b)
		end function anc
	end function ImNLind

	real(8) function ImNChiDelta(q,w,a,b)
		real(8)::q,w,a,b
		integer(4), parameter::steps=1000

		ImNChiDelta=PolIntegratephi11(anc,0D0,Globkf+q,0D0,0D0,phirange0,steps,q,w)
	contains
		real(8) function anc(q,w,k)
			use AuxiliaryFunctions
			real(8)::q,w,k(2),th,kp
			real(8), parameter::temp=-1D0/8D0/pi,sigma=1D-6
			logical::kocc,kpunocc

			th=dcos(k(2))
			kp=dsqrt(k(1)**2+q**2+2D0*a*k(1)*q*th)

			anc=0D0
			if (a.eq.1D0) then
				kocc=(f(1D0,k).gt.0D0)
			else
				kocc=.true.
			endif

			if (b.eq.1D0) then
				kpunocc=(f(1D0,(/kp,0D0/)).lt.1D0)
			else
				kpunocc=.false.
			endif

			if ( kocc.and.kpunocc ) then ! k occupied, kp unoccupied
				anc=temp*a*(1D0+b*(k(1)+a*q*th)/kp)*NumDelta(w+a*gam*(b*k(1)-kp),sigma)
			endif
		end function anc
	end function ImNChiDelta

	real(8) function ImChiHybrid(q,w,a,b)
		use AuxiliaryFunctions
		real(8)::q,w,a,b,k1,k2
		integer(4)::ier

		k1=max(0D0,Globkf-q)
		if (a.eq.1D0) then
			if (b.eq.1D0) then
				k2=Globkf
			else
				k2=Globkf+q
			end if
		else
			k1=Globkf
			k2=Globkf+q
		end if

		call StupidIntegrateNC2(anc,k1,k2,1000,ImChiHybrid,ier)
	contains
		real(8) function anc(k)
			real(8)::k

			anc=ImChiIntk(q,w,k,a,b)
		end function anc
	end function ImChiHybrid





	real(8) function PolIntegratephi11(fn,a,b,A1,B1,phisub,steps,q,w)
		use AuxiliaryFunctions
		implicit none
		integer(4):: i,j,steps,ier,ksteps,t1,t2,phisteps
		real(8):: q,w,k,dk,phi,dphi,a,b,kv(2),B1,A1
		real(8):: res,temp,phis(2),kf1,kf2,eps
		real(8),allocatable::temp1(:)
		real(8), external:: fn
		external :: phisub
		real(8), parameter:: twopi=2D0*pi,fourpiSQ=twopi**2
		! 2D polar integrator over k and phi.
		! S fn(q,w)*dk*dphi
		! fn    :: real, external function to be integrated
		! steps :: integer, number of
		! q,w   :: real, arguments of fn


			phisteps=steps
			eps=0D-2
			ksteps=(steps/9)*9 +1
			dk=(b-a)/real(ksteps, kind=8)
			PolIntegratephi11=0D0
			k=a
			allocate(temp1(ksteps))
			do i=1,ksteps
					res=0D0
					call phisub(k,q,A1,B1,phis)
					!call GaussAdaptQuad(anc,phis(1),phis(2),1D-8,res)
					call StupidIntegrateNC2(anc,phis(1),phis(2),phisteps,res,ier)
				temp1(i)=res!temp
				k=k+dk
			enddo


			t1=1
			t2=10
			do i=1,(ksteps/9)
				PolIntegratephi11=PolIntegratephi11+NC10PolList(temp1(t1:t2))!*dphi!/fourpisq     !NC10
				t1=t2
				t2=t2+9
			enddo

			PolIntegratephi11=PolIntegratephi11*dk    !/fourpisq     !NC10


		contains

			real(8) function anc(phi)
				implicit none
				real(8)::phi

				anc=k*fn(q,w,(/k,phi/))
			end function anc

			real(8) function NC10PolList(flist)
				integer(4)::i
				real(8)   ::flist(10)
				real(8), external::fn
				real(8), parameter:: Newt10(10)=(/2857D0,15741D0,&
														1080D0,19344D0,&
														5778D0,	5778D0,&
														19344D0,1080D0,&
														15741D0,2857D0&
														/)*9D0/89600D0

				NC10PolList=0D0
				do i=1,10
					NC10PolList=NC10PolList+Newt10(i)*flist(i)
				enddo
			end function NC10PolList
	end function PolIntegratephi11

	real(8) function test(q,w,k)
		real(8)::q,w,k(2)

		test=k(1)**2*dcos(k(2))**2
	end function test

	subroutine phirange0(k,q,A,B,phis)
		implicit none
		real(8), intent(in)::k,q(2),A,B
		real(8), intent(out)::phis(2)
		real(8), parameter::twopi=2D0*pi


		phis(2)=twopi
		phis(1)=0D0
	end subroutine phirange0

	real(8) function ImChiTest(q,w,a,b)
		real(8)::q,w,a,b,k1,k2

		ImChiTest=0D0

		if ( w.lt.0D0 ) then
			return
		end if

		if ( b.eq.-1D0 ) then
			return
		end if

		if ( a.eq.1D0 ) then
			if ( q.lt.w/gam ) then
				return
			end if
			k1=max(0D0,Globkf-w/gam)
			k2=Globkf
		else
			if ( q.lt.Globkf/2D0 ) then
				return
			end if
			if (.not.( 2D0*Globkf-q.le.w/gam).and.(w/gam.le.Globkf+q )) then
				return
			end if
			k1=max(Globkf,w/gam,0D0)
			k2=min(Globkf+q,(q+w/gam)/2D0,Globkf+w/gam)
			if ( k1.ge.k2 ) then
				return
			end if
		end if

		ImChiTest=ImChiAn(q,w,a,b,k1,k2)

	end function ImChiTest

	real(8) function ImChiAn(q,w,a,b,k1,k2)
		real(8)::q,w,a,b,k1,k2,wt
		real(8),parameter::temp=1D0/16D0/pi/gam

		wt=w/gam
		ImChiAn=anc((a*wt+2D0*b*k2)/q)-anc((a*wt+2D0*b*k1)/q)
		ImChiAn=-ImChiAn*a*q*temp/dsqrt(dabs(1D0-(wt/q)**2))
	contains
		real(8) function anc(x)
			real(8)::x

			anc=dsqrt(dabs(x**2-1D0))
			if ( x**2.lt.1D0 ) then
				anc=dabs(x)*anc+dasin(anc)
			else
				anc=dabs(x)*anc-dasinh(anc)
			end if
		end function anc
	end function ImChiAn

end module PhysicsGraphene2
