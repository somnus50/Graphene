program TWODEGMPILind
use mpi
use Constants
use Globals
use Physics2DEG
!use SomeModule !As many as you need
implicit none
real (kind=8)::q(2),a,b2,w,dw,wtime,temp,MPIa,MPIb,LindSum&
		,dq(2),Lind,MPILind,MPILimits(4,4)
real (kind=8), allocatable::wline(:)
integer (kind=4)::MPImaster,IntSteps,NProc,MPIerr,MPIid,MPISteps&
		 ,i,j,wsteps,qsteps
!complex (kind=8)::
character (len=32)::fmt1,fmt2




	!!!! Initialize some situational constants
	call ConstInit(1D0,0D0,GlobEf,Globeta,Globfx,Globfc) 
	MPImaster=0
	IntSteps=2000
	qsteps=200
	wsteps=200
	write(fmt2, '(a, i0, a )') '(',wsteps,'ES24.15)'
!	write(*,*) fmt2
	fmt1='(3ES24.15)'
	allocate(wline(wsteps))
	a=0D0
	b2=FermiVec(q,1D-6)+1D-1
	w=0D0
	dw=(3D0*GlobEf)/real(wsteps, kind=8)
	!!! Initialize some files for data storage
	open(unit=9, file='Lind.dat')
	!Conversion to get the same~ish amount of points between cartesian integration and polar
	IntSteps=IntSteps*4D0/pi
	!
	call MPI_Init(MPIerr)
	call MPI_Comm_size(MPI_COMM_WORLD,NProc,MPIerr)
	call MPI_Comm_rank(MPI_COMM_WORLD,MPIid,MPIerr)



	if (MPIid.eq.MPImaster) then
		MPISteps=IntSteps/NProc
		wtime=MPI_Wtime()
		write(9,*) "### ReNLind(q,w)"
		write(9,*) "###"
		write(9,*) "# NProc, IntSteps, qsteps, wsteps"
		write(9,'(4I10)') NProc,IntSteps,qsteps,wsteps 
		write(9,*) "# q/kf,  wstart, dw"
		write(9,*) "# ReNLind(q,wstart)   ReNLind(q,wstart+dw)..."

	endif

	call MPI_Bcast(MPISteps,1,MPI_INTEGER,MPIMaster,MPI_COMM_WORLD,MPIerr)



	temp=real(MPIid, kind=8)/real(NProc, kind=8)
	MPIa=a*(1D0-temp)+temp*b2
	temp=real((MPIid+1), kind=8)/real(NProc, kind=8)
	MPIb=a*(1D0-temp)+temp*b2

	MPILimits(1,:)=(/-b2,0D0,-b2,0D0/)
	MPILimits(2,:)=(/-b2,0D0,0D0,b2/)
	MPILimits(3,:)=(/0D0,b2,-b2,0D0/)
	MPILimits(4,:)=(/0D0,b2,0D0,b2/)
	!write(*,*) "Process ",MPIid,"claims",MPIa," to ",MPIb

	!!!!!!!!!!!!!!!!!!!
	!!!Do some Stuff!!!
	!!!!!!!!!!!!!!!!!!!

	q=(/0D0,0D0/)
	dq=(/3D0*Globkf,0D0/)/real(qsteps, kind=8)
	do i=1,qsteps
		write(*,*) "qstep", i
		MPILind=0D0
		Lind=0D0
		w=0D0
		wline=0D0
		if (MPIid.eq.MPImaster) then
			write(9,fmt1) Norm(q)/Globkf, w, dw
		endif
		do j=1,wsteps
			call MPI_Bcast(w,1,MPI_DOUBLE_PRECISION,MPIMaster,MPI_COMM_WORLD,MPIerr)		
			Lind=0D0
			!MPILind=PolIntegrate3(MPIa,MPIb,MPIsteps,MPISteps*NProc,ReNLindInt,q,w)
			MPILind=CartIntegrate(MPILimits(MPIid+1,:),MPISteps,ReNLindInt,q,w)
			call MPI_Reduce(MPILind,Lind,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPImaster,MPI_COMM_WORLD,MPIerr)
			if (MPIid.eq.MPImaster) then
				wline(j)=Lind
				w=w+dw
			endif
		enddo
		if (MPIid.eq.MPImaster) then
			write(9,fmt2) wline
		endif
		q=q+dq
	enddo

	if (MPIid.eq.MPImaster) then
		wtime=MPI_Wtime()-wtime
		write(*,*) "Time:",wtime
	endif

	call MPI_Finalize(MPIerr)

	!!! Close files
	close(unit=9)

end program TWODEGMPILind
